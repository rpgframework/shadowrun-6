/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.CustomDataHandler;
import de.rpgframework.core.CustomDataHandler.CustomDataPackage;
import de.rpgframework.core.CustomDataHandlerLoader;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun6DataPlugin implements RulePlugin<ShadowrunCharacter> {

	private final static Logger logger = LogManager.getLogger("shadowrun6.data");

	//-------------------------------------------------------------------
	public Shadowrun6DataPlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 6 Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return Arrays.asList(new RulePluginFeatures[]{RulePluginFeatures.DATA});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		//		ShadowrunCore.attachConfigurationTree(addBelow);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		return new CommandResult(type, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		Class<Shadowrun6DataPlugin> clazz = Shadowrun6DataPlugin.class;

		double totalPlugins = 20.0;
		double count = 0;

		logger.info("START -------------------------------Core-----------------------------------------------");
		PluginSkeleton CORE = new PluginSkeleton("CORE", "Shadowrun 6 Core Rules");
		ShadowrunCore.loadSkills        (CORE, clazz.getResourceAsStream("core/data/skills.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpellFeatures (CORE, clazz.getResourceAsStream("core/data/spellfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpells        (CORE, clazz.getResourceAsStream("core/data/spells.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRitualFeatures(CORE, clazz.getResourceAsStream("core/data/ritualfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRituals       (CORE, clazz.getResourceAsStream("core/data/rituals.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_auto.xml"), CORE.getResources(), CORE.getHelpResources());// Unarmed weapons
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_sensors_and_co.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadItemEnhancement(CORE, clazz.getResourceAsStream("core/data/weapon_modifications.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadQualities     (CORE, clazz.getResourceAsStream("core/data/qualities.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMagicOrResonanceTypes(CORE, clazz.getResourceAsStream("core/data/magicOrResonance.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadAmmoTypes     (CORE, clazz.getResourceAsStream("core/data/ammunition_types.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_melee.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_firearms_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_firearms.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_ammunition.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_armor.xml"), CORE.getResources(), CORE.getHelpResources());// Dermal Deposits need it
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_armor_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_electronics.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_security_survival.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_cyberware.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_cyberware_pass2.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_bioware.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_magical.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_vehicles.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_drones.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_drones_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_software.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetaTypes     (CORE, clazz.getResourceAsStream("core/data/metatypes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadPriorityTableEntries(CORE, clazz.getResourceAsStream("core/data/priorities.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadAdeptPowers(  CORE, clazz.getResourceAsStream("core/data/adeptpowers.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadComplexForms  (CORE, clazz.getResourceAsStream("core/data/complexforms.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLifestyles    (CORE, clazz.getResourceAsStream("core/data/lifestyles.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpirits       (CORE, clazz.getResourceAsStream("core/data/spirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSprites       (CORE, clazz.getResourceAsStream("core/data/sprites.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadTraditions    (CORE, clazz.getResourceAsStream("core/data/traditions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMentorSpirits (CORE, clazz.getResourceAsStream("core/data/mentorspirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSensorFunctions(CORE, clazz.getResourceAsStream("core/data/sensorfunctions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(CORE, clazz.getResourceAsStream("core/data/metamagics.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(CORE, clazz.getResourceAsStream("core/data/echoes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLicenseTypes  (CORE, clazz.getResourceAsStream("core/data/licensetypes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadFoci          (CORE, clazz.getResourceAsStream("core/data/foci.xml"), CORE.getResources(), CORE.getHelpResources());

		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_major.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_minor.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_edge.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_matrix.xml"), CORE.getResources(), CORE.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Double Clutch-----------------------------------------");
		PluginSkeleton DCLUTCH = new PluginSkeleton("DOUBLE_CLUTCH", "Double Clutch");
		DCLUTCH.setLanguages(Locale.ENGLISH, Locale.GERMAN);
		ShadowrunCore.loadEquipment   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/gear_preload.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadEquipment   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/gear_electronics.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadEquipment   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/gear_vehicle_accessories.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadEquipment   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/gear_vehicles.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadEquipment   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/gear_drones.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadChassisTypes(DCLUTCH, clazz.getResourceAsStream("double_clutch/data/chassisTypes.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadPowertrains (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/powertrains.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadConsoleTypes(DCLUTCH, clazz.getResourceAsStream("double_clutch/data/consoleTypes.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadDesignOption(DCLUTCH, clazz.getResourceAsStream("double_clutch/data/designOptions.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadDesignMods  (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/designMods.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadQualityFactors(DCLUTCH, clazz.getResourceAsStream("double_clutch/data/qualityFactors.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadEquipment   (CORE, clazz.getResourceAsStream("double_clutch/data/gear_vehicles_core.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions     (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/actions_edge.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		ShadowrunCore.loadQualities   (DCLUTCH, clazz.getResourceAsStream("double_clutch/data/qualities.xml"), DCLUTCH.getResources(), DCLUTCH.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(DCLUTCH);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Berlin 2080-----------------------------------------");
		PluginSkeleton BERLIN = new PluginSkeleton("BERLIN2080", "Berlin");
		BERLIN.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (BERLIN, clazz.getResourceAsStream("berlin2080/data/gear_firearms.xml"), BERLIN.getResources(), BERLIN.getHelpResources());
		ShadowrunCore.loadEquipment     (BERLIN, clazz.getResourceAsStream("berlin2080/data/gear_vehicles.xml"), BERLIN.getResources(), BERLIN.getHelpResources());
		ShadowrunCore.registerPlugin(BERLIN);
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------Firing Squad-----------------------------------------");
		PluginSkeleton FSQUAD = new PluginSkeleton("FIRING_SQUAD", "Firing Squad");
		ShadowrunCore.loadActions       (FSQUAD, clazz.getResourceAsStream("firing_squad/data/actions_edge.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_auto.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_underbarrel_weapons.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadItemEnhancement(FSQUAD, clazz.getResourceAsStream("firing_squad/data/weapon_modifications.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_melee.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_firearms_accessories.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadTechniques    (FSQUAD, clazz.getResourceAsStream("firing_squad/data/techniques.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadMartialArts   (FSQUAD, clazz.getResourceAsStream("firing_squad/data/martialarts.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_firearms.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadAmmoTypes     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/ammunition_types.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_revolution_arms.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_armor_accessories.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_armor.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_mems_accessories.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_electronics.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadEquipment     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/gear_ammunition.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.loadQualities     (FSQUAD, clazz.getResourceAsStream("firing_squad/data/qualities.xml"), FSQUAD.getResources(), FSQUAD.getHelpResources());
		ShadowrunCore.registerPlugin(FSQUAD);
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------SOTA 2081-----------------------------------------");
		PluginSkeleton SOTA2081 = new PluginSkeleton("SOTA2081", "Datapuls SOTA 2081");
		SOTA2081.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (SOTA2081, clazz.getResourceAsStream("sota2081/data/gear_firearms_accessories.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2081, clazz.getResourceAsStream("sota2081/data/gear_firearms_pre.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2081, clazz.getResourceAsStream("sota2081/data/gear_firearms.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2081, clazz.getResourceAsStream("sota2081/data/gear_vehicles.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2081, clazz.getResourceAsStream("sota2081/data/gear_drones.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		ShadowrunCore.loadMentorSpirits (SOTA2081, clazz.getResourceAsStream("sota2081/data/mentorspirits.xml"), SOTA2081.getResources(), SOTA2081.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(SOTA2081);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Schattenloads-----------------------------------------");
		PluginSkeleton SLOAD = new PluginSkeleton("SCHATTENLOAD", "Schattenload");
		SLOAD.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_preload.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_firearms.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_vehicles.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_drones.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_armor.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_software.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_electronics.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_ammunition.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		ShadowrunCore.loadQualities     (SLOAD, clazz.getResourceAsStream("schattenload/data/qualities.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(SLOAD);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Westphalen-----------------------------------------");
		PluginSkeleton WPHAL = new PluginSkeleton("WESTPHALEN", "Datapuls Westphalen");
		WPHAL.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (WPHAL, clazz.getResourceAsStream("westphalen/data/gear_preload.xml"), WPHAL.getResources(), WPHAL.getHelpResources());
		ShadowrunCore.loadEquipment     (WPHAL, clazz.getResourceAsStream("westphalen/data/gear_drones.xml"), WPHAL.getResources(), WPHAL.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(WPHAL);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Piraten-----------------------------------------");
		PluginSkeleton PIRAT = new PluginSkeleton("PIRATEN", "Datapuls Piraten");
		PIRAT.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (PIRAT, clazz.getResourceAsStream("piraten/data/gear_preload.xml"), PIRAT.getResources(), PIRAT.getHelpResources());
		ShadowrunCore.loadEquipment     (PIRAT, clazz.getResourceAsStream("piraten/data/gear_vehicles.xml"), PIRAT.getResources(), PIRAT.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(PIRAT);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Krime Katalog--------------------------------------");
		PluginSkeleton KRIME = new PluginSkeleton("KRIME", "Krime Katalog");
		ShadowrunCore.loadItemEnhancement(KRIME, clazz.getResourceAsStream("krime/data/weapon_modifications.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadEquipment     (KRIME, clazz.getResourceAsStream("krime/data/gear_firearms_accessories.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadEquipment     (KRIME, clazz.getResourceAsStream("krime/data/gear_firearms.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadEquipment     (KRIME, clazz.getResourceAsStream("krime/data/gear_ammunition.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadEquipment     (KRIME, clazz.getResourceAsStream("krime/data/gear_vehicles.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadEquipment     (KRIME, clazz.getResourceAsStream("krime/data/gear_drones.xml"), KRIME.getResources(), KRIME.getHelpResources());
		ShadowrunCore.loadAmmoTypes     (KRIME, clazz.getResourceAsStream("krime/data/ammunition_types.xml"), KRIME.getResources(), KRIME.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(KRIME);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Ingentis Athletes----------------------------------");
		PluginSkeleton INGENT = new PluginSkeleton("INGENTIS", "Ingentis Athletes");
		ShadowrunCore.loadEquipment (INGENT, clazz.getResourceAsStream("ingentis/data/gear_auto.xml"), INGENT.getResources(), INGENT.getHelpResources());
		ShadowrunCore.loadQualities (INGENT, clazz.getResourceAsStream("ingentis/data/qualities.xml"), INGENT.getResources(), INGENT.getHelpResources());
		ShadowrunCore.loadMetaTypes (INGENT, clazz.getResourceAsStream("ingentis/data/metatypes.xml"), INGENT.getResources(), INGENT.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(INGENT);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Enhanced Fiction-----------------------------------------");
		PluginSkeleton FICTION = new PluginSkeleton("FICTION", "Enhanced Fiction");
		ShadowrunCore.loadEquipment     (FICTION, clazz.getResourceAsStream("fiction/data/gear_vehicles.xml"), FICTION.getResources(), FICTION.getHelpResources());
		ShadowrunCore.loadEquipment     (FICTION, clazz.getResourceAsStream("fiction/data/gear_ammunition.xml"), FICTION.getResources(), FICTION.getHelpResources());
		ShadowrunCore.loadMentorSpirits (FICTION, clazz.getResourceAsStream("fiction/data/mentorspirits.xml"), FICTION.getResources(), FICTION.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(FICTION);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------No Future--------------------------------------");
		PluginSkeleton NO_FUTURE = new PluginSkeleton("NO_FUTURE", "No Future");
		NO_FUTURE.setLanguages(Locale.ENGLISH);
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_ammunition.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_bioware.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_clothing.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_cyberware.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_instruments.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_melee.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_tools.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadEquipment     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/gear_vehicles.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadQualities     (NO_FUTURE, clazz.getResourceAsStream("no_future/data/qualities.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		ShadowrunCore.loadLifestyles    (NO_FUTURE, clazz.getResourceAsStream("no_future/data/lifestyles.xml"), NO_FUTURE.getResources(), NO_FUTURE.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(NO_FUTURE);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Slip Streams----------------------------------");
		PluginSkeleton SLIP = new PluginSkeleton("SLIP_STREAMS", "Slip Streams");
		ShadowrunCore.loadEquipment (SLIP, clazz.getResourceAsStream("slip_streams/data/gear_auto.xml"), SLIP.getResources(), SLIP.getHelpResources());
		ShadowrunCore.loadQualities (SLIP, clazz.getResourceAsStream("slip_streams/data/qualities.xml"), SLIP.getResources(), SLIP.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(SLIP);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Collapsing Now--------------------------------");
		PluginSkeleton COLL = new PluginSkeleton("COLLAPSING_NOW", "Collapsing Now");
		ShadowrunCore.loadEquipment (COLL, clazz.getResourceAsStream("collapsing_now/data/gear_firearms.xml"), COLL.getResources(), COLL.getHelpResources());
		ShadowrunCore.loadAdeptPowers(COLL, clazz.getResourceAsStream("collapsing_now/data/adeptpowers.xml"), COLL.getResources(), COLL.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(COLL);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Street Wyrd-----------------------------------");
		PluginSkeleton WYRD = new PluginSkeleton("STREET_WYRD", "Street Wyrd");
//		WYRD.setLanguages(Locale.ENGLISH);
		ShadowrunCore.loadEquipment    (WYRD, clazz.getResourceAsStream("street_wyrd/data/gear_magical.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadAdeptPowers  (WYRD, clazz.getResourceAsStream("street_wyrd/data/adeptpowers.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadQualities    (WYRD, clazz.getResourceAsStream("street_wyrd/data/qualities1.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadQualities    (WYRD, clazz.getResourceAsStream("street_wyrd/data/qualities2.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(WYRD, clazz.getResourceAsStream("street_wyrd/data/metamagics.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(WYRD, clazz.getResourceAsStream("street_wyrd/data/metamagics2.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadTraditions   (WYRD, clazz.getResourceAsStream("street_wyrd/data/traditions.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadSpellFeatures(WYRD, clazz.getResourceAsStream("street_wyrd/data/spellfeatures.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadSpells       (WYRD, clazz.getResourceAsStream("street_wyrd/data/spells.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadSpirits      (WYRD, clazz.getResourceAsStream("street_wyrd/data/spirits.xml"), WYRD.getResources(), WYRD.getHelpResources());
		ShadowrunCore.loadFoci         (WYRD, clazz.getResourceAsStream("street_wyrd/data/foci.xml"), WYRD.getResources(), WYRD.getHelpResources());
		if ("de".equals(Locale.getDefault().getLanguage())) {
			// Mentor spirts have been only present in the german version
			ShadowrunCore.loadMentorSpirits(WYRD, clazz.getResourceAsStream("street_wyrd/data/mentorspirits.xml"), WYRD.getResources(), WYRD.getHelpResources());
		}
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(WYRD);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Power Plays--------------------------------");
		PluginSkeleton PP = new PluginSkeleton("POWER_PLAYS", "Power Plays");
		ShadowrunCore.loadQualities(PP, clazz.getResourceAsStream("power_plays/data/qualities.xml"), PP.getResources(), PP.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(PP);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Feuerlaeufer-----------------------------------------");
		PluginSkeleton FEUERL = new PluginSkeleton("FEUERLAEUFER", "Datapuls Feuerläufer");
		FEUERL.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_vehicle_accessories.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_ammunition.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_armor.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_drones.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_melee.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_survival.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		ShadowrunCore.loadEquipment     (FEUERL, clazz.getResourceAsStream("feuerlaeufer/data/gear_vehicles.xml"), FEUERL.getResources(), FEUERL.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(FEUERL);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Kechibi Code-----------------------------------------");
		PluginSkeleton KECHIBI = new PluginSkeleton("KECHIBI", "Kechibi Code");
		ShadowrunCore.loadEquipment     (KECHIBI, clazz.getResourceAsStream("kechibi/data/gear_firearms.xml"), KECHIBI.getResources(), KECHIBI.getHelpResources());
		ShadowrunCore.loadEquipment     (KECHIBI, clazz.getResourceAsStream("kechibi/data/gear_vision.xml"), KECHIBI.getResources(), KECHIBI.getHelpResources());
		ShadowrunCore.loadQualities     (KECHIBI, clazz.getResourceAsStream("kechibi/data/qualities.xml"), KECHIBI.getResources(), KECHIBI.getHelpResources());
		ShadowrunCore.registerPlugin(KECHIBI);
		count++; callback.progressChanged( (count/totalPlugins) );
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Alpen-----------------------------------------");
		PluginSkeleton ALPEN = new PluginSkeleton("ALPEN", "Datapuls ALpen");
		ALPEN.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (ALPEN, clazz.getResourceAsStream("alpen/data/gear_armor.xml"), ALPEN.getResources(), ALPEN.getHelpResources());
		ShadowrunCore.loadEquipment     (ALPEN, clazz.getResourceAsStream("alpen/data/gear_survival.xml"), ALPEN.getResources(), ALPEN.getHelpResources());
		ShadowrunCore.loadEquipment     (ALPEN, clazz.getResourceAsStream("alpen/data/gear_vehicles.xml"), ALPEN.getResources(), ALPEN.getHelpResources());
		ShadowrunCore.loadMentorSpirits (ALPEN, clazz.getResourceAsStream("alpen/data/mentorspirits.xml"), ALPEN.getResources(), ALPEN.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(ALPEN);
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------SOTA 2082-----------------------------------------");
		PluginSkeleton SOTA2082 = new PluginSkeleton("SOTA2082", "Datapuls SOTA 2082");
		SOTA2081.setLanguages(Locale.GERMAN);
		ShadowrunCore.loadEquipment     (SOTA2082, clazz.getResourceAsStream("sota2082/data/gear_misc.xml"), SOTA2082.getResources(), SOTA2082.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2082, clazz.getResourceAsStream("sota2082/data/gear_firearms.xml"), SOTA2082.getResources(), SOTA2082.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2082, clazz.getResourceAsStream("sota2082/data/gear_vehicles.xml"), SOTA2082.getResources(), SOTA2082.getHelpResources());
		ShadowrunCore.loadEquipment     (SOTA2082, clazz.getResourceAsStream("sota2082/data/gear_drones.xml"), SOTA2082.getResources(), SOTA2082.getHelpResources());
		ShadowrunCore.loadMentorSpirits (SOTA2082, clazz.getResourceAsStream("sota2082/data/mentorspirits.xml"), SOTA2082.getResources(), SOTA2082.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.registerPlugin(SOTA2082);
		BasePluginData.flushMissingKeys();

//		logger.info("START -------------------------------COMPANION-----------------------------------------");
//		PluginSkeleton COMPANION = new PluginSkeleton("COMPANION", "Sixth World Companion");
//		ShadowrunCore.loadMetaTypes     (COMPANION, clazz.getResourceAsStream("companion/data/metatypes.xml"), COMPANION.getResources(), COMPANION.getHelpResources());
//		ShadowrunCore.registerPlugin(COMPANION);
//		BasePluginData.flushMissingKeys();

//		System.exit(1);

		/*
		 * Load custom data
		 */
		logger.info("START -------------------------------Custom------------------------------------------");
		if (CustomDataHandlerLoader.getInstance()!=null) {
			CustomDataHandler custom = CustomDataHandlerLoader.getInstance();
			List<String> customIDs = custom.getAvailableCustomIDs(RoleplayingSystem.SHADOWRUN6);
			// Put modifications first
			Collections.sort(customIDs, new Comparator<String>() {
				public int compare(String o1, String o2) {
					if (o1.startsWith("modification") && !o2.startsWith("modification"))
						return -1;
					if (o1.startsWith("technique") && !o2.startsWith("technique"))
						return -1;
					return o1.compareTo(o2);
				}
			});

			PluginSkeleton CUSTOM = new PluginSkeleton("Custom", "Custom Data");
			ShadowrunCore.registerPlugin(CUSTOM);
			List<String> loadLast = new ArrayList<String>();
			for (String id : customIDs) {
				logger.info("  try loading custom data "+id);
				CustomDataPackage  bundle = custom.getCustomData(RoleplayingSystem.SHADOWRUN6, id);
				if (bundle!=null) {
					if (bundle.properties==null) {
						logger.fatal("\n\nNo properties file for custom data:  '"+id+".properties'\n\n");
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "No properties file for custom data:  '"+id+".properties'");
						continue;
					}

					try (InputStream datastream = new FileInputStream(bundle.datafile.toFile())) {
						if (id.startsWith("item") || id.startsWith("equipment") || id.startsWith("gear")) {
							ShadowrunCore.loadEquipment(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("modification")) {
							ShadowrunCore.loadItemEnhancement(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("skills")) {
							ShadowrunCore.loadSkills(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("qualities")) {
							ShadowrunCore.loadQualities(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("spell")) {
							ShadowrunCore.loadSpells(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("complex")) {
							ShadowrunCore.loadComplexForms(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("power")) {
							ShadowrunCore.loadAdeptPowers(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("tradition")) {
							ShadowrunCore.loadTraditions(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("mentor")) {
							ShadowrunCore.loadMentorSpirits(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("metatype")) {
							loadLast.add(id);
							logger.info("  load during 2. pass: "+id);
						} else if (id.startsWith("echoes") || id.startsWith("metamagic")) {
							ShadowrunCore.loadMetamagicOrEchos(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("prior")) {
							loadLast.add(id);
							logger.info("  load during 2. pass: "+id);
						} else if (id.startsWith("technique")) {
							ShadowrunCore.loadTechniques    (CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("martial")) {
							ShadowrunCore.loadMartialArts   (CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("ammo")) {
							ShadowrunCore.loadAmmoTypes (CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("license")) {
							ShadowrunCore.loadLicenseTypes(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else  {
							logger.warn("Don't know how to deal with custom data "+bundle.datafile);
							continue;
						}
						logger.info("Loaded custom data  "+bundle.datafile);
					} catch (IOException e) {
						logger.error("Failed for custom data "+bundle.datafile+" and its properties",e);
						StringWriter out = new StringWriter();
						e.printStackTrace(new PrintWriter(out));
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed for custom data "+bundle.datafile+"\n"+out.toString());
					}
				}
			}
			// Load last
			for (String id : loadLast) {
				logger.info("  2. pass try loading custom data "+id);
				CustomDataPackage  bundle = custom.getCustomData(RoleplayingSystem.SHADOWRUN6, id);
				if (bundle!=null) {
					if (bundle.properties==null) {
						logger.fatal("\n\nNo properties file for custom data:  '"+id+".properties'\n\n");
						continue;
					}

					try (InputStream datastream = new FileInputStream(bundle.datafile.toFile())) {
						if (id.startsWith("metatype")) {
							ShadowrunCore.loadMetaTypes(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("prior")) {
							ShadowrunCore.loadPriorityTableEntries(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else  {
							logger.warn("2. Pass: Don't know how to deal with custom data "+bundle.datafile);
							continue;
						}
						logger.info("Loaded custom data  "+bundle.datafile+" in second pass");
					} catch (IOException e) {
						logger.error("Failed for custom data "+bundle.datafile+" and its properties",e);
					}
				}
			}
		}

		BasePluginData.flushMissingKeys();
		callback.progressChanged( 1.0f );

		//		logger.fatal("Stop here");
		//		System.exit(0);
		logger.debug("STOP : -----Init "+getID()+"---------------------------");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return Shadowrun6DataPlugin.class.getResourceAsStream("data-plugin.html");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getLanguages()
	 */
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage(), Locale.FRENCH.getLanguage());
	}

	//-------------------------------------------------------------------
	public static byte[] getPlaceholderGraphic(CarriedItem item) {
		List<String> filenames = new ArrayList<>();
		// Build possible filenames
		if (item.getItem()!=null) {
			filenames.add(item.getItem().getId()+".png");
			filenames.add(item.getItem().getId()+".jpg");
		}
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".png");
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".jpg");
		filenames.add(item.getUsedAsType()+".png");
		filenames.add(item.getUsedAsType()+".jpg");
		// See which image is available
		Class<Shadowrun6DataPlugin> clazz = Shadowrun6DataPlugin.class;
		for (String file : filenames) {
			InputStream ins = clazz.getResourceAsStream("placeholder/"+file);
			if (ins!=null) {
				try {
					return ins.readAllBytes();
				} catch (IOException e) {
					logger.error("Failed accessing resource "+file+": "+e);
				}
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static URL getPlaceholderGraphicURL(CarriedItem item) {
		List<String> filenames = new ArrayList<>();
		// Build possible filenames
		if (item.getItem()!=null) {
			filenames.add(item.getItem().getId()+".png");
			filenames.add(item.getItem().getId()+".jpg");
		}
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".png");
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".jpg");
		filenames.add(item.getUsedAsType()+".png");
		filenames.add(item.getUsedAsType()+".jpg");
		// See which image is available
		Class<Shadowrun6DataPlugin> clazz = Shadowrun6DataPlugin.class;
		for (String file : filenames) {
			logger.log(Level.ERROR, "Check "+file);
			URL ins = clazz.getResource("placeholder/"+file);
			if (ins!=null) {
				return ins;
			}
		}
		return null;
	}

}
