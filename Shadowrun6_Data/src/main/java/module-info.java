module shadowrun6.data {
	exports org.prelle.rpgframework.shadowrun6.data;

	provides de.rpgframework.character.RulePlugin with org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun6.core;
}