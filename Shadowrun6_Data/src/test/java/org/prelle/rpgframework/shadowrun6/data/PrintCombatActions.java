/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Type;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class PrintCombatActions {

	private final static String NORMAL  = "%26s   %26s";
	
	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		System.setProperty("logdir", System.getProperty("user.home"));
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		
		System.out.println(String.format(NORMAL, "MINOR ACTIONS", "MAJOR ACTIONS"));
		
		List<ShadowrunAction> all = ShadowrunCore.getActions(ShadowrunAction.Category.COMBAT, ShadowrunAction.Category.POSITION);

		/* On initiative */
		System.out.println(String.format(NORMAL, "---In Initiative phase--", "---In Initiative phase--"));
		Map<Type,List<ShadowrunAction>> list = ShadowrunTools.sortToColumns(all, 
				ShadowrunAction.Time.INITIATIVE,
				(t) -> t.getType(),
				(t) -> t.getTime()
				);
		int longest = Math.max(list.get(Type.MINOR).size(), list.get(Type.MAJOR).size());
		for (int i=0; i<longest; i++) {
			ShadowrunAction leftA = (i<list.get(Type.MINOR).size())?list.get(Type.MINOR).get(i):null;
			String left = (leftA==null)?"":(leftA.getName()+" ("+leftA.getProductNameShort()+" "+leftA.getPage()+")");
			ShadowrunAction rightA = (i<list.get(Type.MAJOR).size())?list.get(Type.MAJOR).get(i):null;
			String right = (rightA==null)?"":(rightA.getName()+" ("+rightA.getProductNameShort()+" "+rightA.getPage()+")");
			System.out.println(String.format(NORMAL, left, right));
		}

		/* Anytime */
		System.out.println(String.format(NORMAL, "-----------Anytime--", "-------Anyxtime--"));
		 list = ShadowrunTools.sortToColumns(all, 
				ShadowrunAction.Time.ANYTIME,
				(t) -> t.getType(),
				(t) -> t.getTime()
				);
		longest = Math.max(list.get(Type.MINOR).size(), list.get(Type.MAJOR).size());
		for (int i=0; i<longest; i++) {
			ShadowrunAction leftA = (i<list.get(Type.MINOR).size())?list.get(Type.MINOR).get(i):null;
			String left = (leftA==null)?"":(leftA.getName()+" ("+leftA.getProductNameShort()+" "+leftA.getPage()+")");
			ShadowrunAction rightA = (i<list.get(Type.MAJOR).size())?list.get(Type.MAJOR).get(i):null;
			String right = (rightA==null)?"":(rightA.getName()+" ("+rightA.getProductNameShort()+" "+rightA.getPage()+")");
			System.out.println(String.format(NORMAL, left, right));
		}
	}

}
