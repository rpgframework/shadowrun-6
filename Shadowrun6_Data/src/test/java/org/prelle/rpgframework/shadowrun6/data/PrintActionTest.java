/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import org.junit.Test;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Category;
import org.prelle.shadowrun6.actions.ShadowrunAction.Time;
import org.prelle.shadowrun6.actions.ShadowrunAction.Type;

import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class PrintActionTest {

	private final static String NORMAL  = "%23s  %6s  %10s %4s %3s";
	private final static String MATRIX  = "%23s  %6s  %10s  %20s  %15s %4s %3s";
	
	//--------------------------------------------------------------------
	private static void printAction(ShadowrunAction act) {
		if (act.getCategory()!=Category.MATRIX) {
			System.out.println(String.format(NORMAL, 
					act.getName(), 
					act.getType(),					
					act.getApplyString(),
					act.getProductNameShort(), act.getPage()));	
		} else {
			System.out.println(String.format(MATRIX, act.getName(), act.getType(),
					act.isIllegal()?"Illeg":"legal",
					act.getAccess(),
					act.getApplyString(),
					act.getProductNameShort(), act.getPage()));	
		}
	}
	
	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		System.setProperty("logdir", System.getProperty("user.home"));
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		
		System.out.println("Combat Actions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.COMBAT)) {
			if (act.getType()!=Type.BOOST && act.getType()!=Type.EDGE && act.getTime()==Time.INITIATIVE)
				printAction(act);
		}		
		
		System.out.println("\nCombat Reactions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.COMBAT)) {
			if (act.getType()!=Type.BOOST && act.getType()!=Type.EDGE && act.getTime()==Time.ANYTIME)
				printAction(act);
		}		
		
		System.out.println("\nPosition Actions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.POSITION)) {
			printAction(act);
		}
		
		System.out.println("\nCombat Edge Actions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.COMBAT)) {
			if (act.getType()==Type.EDGE || act.getType()==Type.BOOST)
				printAction(act);
		}		
		
		System.out.println("\nMatrix Actions");
		System.out.println("--------------");
		System.out.println(String.format(MATRIX, "Name","Type","Illeg","Access","Skill","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.MATRIX)) {
			if (act.getType()!=Type.BOOST && act.getType()!=Type.EDGE)
				printAction(act);
		}
		
		System.out.println("\nMatrix Edge Actions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.MATRIX, ShadowrunAction.Category.OTHER)) {
			if (act.getType()==Type.EDGE || act.getType()==Type.BOOST)
					printAction(act);		
		}
		
		System.out.println("\nAstral Actions");
		System.out.println("--------------");
		System.out.println(String.format(NORMAL, "Name","Type","Combine with","Prod","Pg."));
		for (ShadowrunAction act : ShadowrunCore.getActions(ShadowrunAction.Category.MAGIC)) {
			if (act.getType()!=Type.BOOST && act.getType()!=Type.EDGE)
				printAction(act);
		}
	}

}
