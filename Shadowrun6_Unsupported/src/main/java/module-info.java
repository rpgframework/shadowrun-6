module shadowrun6.unsupported {
	exports org.prelle.rpgframework.shadowrun6.unsupported;

	provides de.rpgframework.character.RulePlugin with org.prelle.rpgframework.shadowrun6.unsupported.Shadowrun6UnsupportedPlugin;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun6.core;
}