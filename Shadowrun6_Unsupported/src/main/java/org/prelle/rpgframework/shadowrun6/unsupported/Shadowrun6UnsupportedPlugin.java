package org.prelle.rpgframework.shadowrun6.unsupported;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun6UnsupportedPlugin implements RulePlugin<ShadowrunCharacter> {

	private final static Logger logger = LogManager.getLogger("shadowrun6.unsupported");

	//-------------------------------------------------------------------
	public Shadowrun6UnsupportedPlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 6 Unsupported";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return Arrays.asList(new RulePluginFeatures[]{RulePluginFeatures.DATA});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		//		ShadowrunCore.attachConfigurationTree(addBelow);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		return new CommandResult(type, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		Class<Shadowrun6UnsupportedPlugin> clazz = Shadowrun6UnsupportedPlugin.class;

		double totalPlugins = 1.0;
		double count = 0;


		logger.info("START -------------------------------COMPANION-----------------------------------------");
		PluginSkeleton COMPANION = new PluginSkeleton("COMPANION", "Sixth World Companion");
//		ShadowrunCore.loadMetaTypes     (COMPANION, clazz.getResourceAsStream("companion/data/metatypes.xml"), COMPANION.getResources(), COMPANION.getHelpResources());
//		ShadowrunCore.registerPlugin(COMPANION);
		count++; callback.progressChanged( (count/totalPlugins) );
		BasePluginData.flushMissingKeys();

//		System.exit(1);

		BasePluginData.flushMissingKeys();
		callback.progressChanged( 1.0f );

		//		logger.fatal("Stop here");
		//		System.exit(0);
		logger.debug("STOP : -----Init "+getID()+"---------------------------");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return Shadowrun6UnsupportedPlugin.class.getResourceAsStream("data-plugin.html");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getLanguages()
	 */
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage(), Locale.FRENCH.getLanguage());
	}

	//-------------------------------------------------------------------
	public static byte[] getPlaceholderGraphic(CarriedItem item) {
		List<String> filenames = new ArrayList<>();
		// Build possible filenames
		if (item.getItem()!=null) {
			filenames.add(item.getItem().getId()+".png");
			filenames.add(item.getItem().getId()+".jpg");
		}
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".png");
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".jpg");
		filenames.add(item.getUsedAsType()+".png");
		filenames.add(item.getUsedAsType()+".jpg");
		// See which image is available
		Class<Shadowrun6UnsupportedPlugin> clazz = Shadowrun6UnsupportedPlugin.class;
		for (String file : filenames) {
			InputStream ins = clazz.getResourceAsStream("placeholder/"+file);
			if (ins!=null) {
				try {
					return ins.readAllBytes();
				} catch (IOException e) {
					logger.error("Failed accessing resource "+file+": "+e);
				}
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static URL getPlaceholderGraphicURL(CarriedItem item) {
		List<String> filenames = new ArrayList<>();
		// Build possible filenames
		if (item.getItem()!=null) {
			filenames.add(item.getItem().getId()+".png");
			filenames.add(item.getItem().getId()+".jpg");
		}
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".png");
		filenames.add(item.getUsedAsType()+"_"+item.getUsedAsSubType()+".jpg");
		filenames.add(item.getUsedAsType()+".png");
		filenames.add(item.getUsedAsType()+".jpg");
		// See which image is available
		Class<Shadowrun6UnsupportedPlugin> clazz = Shadowrun6UnsupportedPlugin.class;
		for (String file : filenames) {
			logger.log(Level.ERROR, "Check "+file);
			URL ins = clazz.getResource("placeholder/"+file);
			if (ins!=null) {
				return ins;
			}
		}
		return null;
	}

}
