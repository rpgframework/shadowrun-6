/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class MartialArtsConverter implements StringValueConverter<MartialArts> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(MartialArts value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public MartialArts read(String idref) throws Exception {
		MartialArts skill = ShadowrunCore.getMartialArts(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.MARTIAL_ART, idref);

		return skill;
	}

}
