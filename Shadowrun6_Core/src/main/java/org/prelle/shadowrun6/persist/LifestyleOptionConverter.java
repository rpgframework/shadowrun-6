package org.prelle.shadowrun6.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.LifestyleOption;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class LifestyleOptionConverter implements StringValueConverter<LifestyleOption> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public LifestyleOption read(String v) throws Exception {
		LifestyleOption item = ShadowrunCore.getLifestyleOption(v);
		if (item==null) {
			logger.error("Unknown lifestyle option reference: '"+v+"'");
			throw new ReferenceException(ReferenceType.LIFESTYLE_OPTION, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(LifestyleOption v) throws Exception {
		return v.getId();
	}
	
}