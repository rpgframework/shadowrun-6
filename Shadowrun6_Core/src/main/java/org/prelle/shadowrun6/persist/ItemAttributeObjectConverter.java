/**
 * 
 */
package org.prelle.shadowrun6.persist;

import java.util.StringTokenizer;

import org.prelle.shadowrun6.items.OnRoadOffRoadValue;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class ItemAttributeObjectConverter implements StringValueConverter<Object> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Object value) throws Exception {
		throw new IllegalArgumentException("Don't know how to convert '"+value+"'");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Object read(String v) throws Exception {
		long count = v.chars().filter(ch -> ch == ',').count();
		if (count==4) {
			return (new AttackRatingConverter()).read(v);
		}
		if (v.contains("/")) {
			OnRoadOffRoadValue handling = (new OnRoadOffRoadConverter()).read(v);
			return handling;
		}
		if (v.contains(".")) {
			try {
				return Float.parseFloat(v);
			} catch (Throwable e) {}
		}
		System.err.println("ItemAttributeObjectConverter.read: Cannot parse: "+v);
		throw new IllegalArgumentException("Don't know how to convert '"+v+"': "+v.trim().split(",").length+" / "+count);
	}

}
