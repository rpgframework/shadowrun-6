package org.prelle.shadowrun6.persist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.shadowrun6.items.AmmunitionSlot;
import org.prelle.shadowrun6.items.AmmunitionSlot.AmmoSlotType;
import org.prelle.simplepersist.StringValueConverter;

public class AmmunitionConverter implements StringValueConverter<List<AmmunitionSlot>> {

	//-------------------------------------------------------------------
	private static AmmunitionSlot.AmmoSlotType getType(String value) {
		if ("k".equalsIgnoreCase(value))
			return AmmoSlotType.BREAK_ACTION;
		if ("gurt".equalsIgnoreCase(value))
			return AmmoSlotType.BELT;
		if ("s".equalsIgnoreCase(value))
			return AmmoSlotType.CLIP;
		if ("t".equalsIgnoreCase(value))
			return AmmoSlotType.DRUM;
		if ("tr".equalsIgnoreCase(value))
			return AmmoSlotType.CYLINDER;
		if ("v".equalsIgnoreCase(value))
			return AmmoSlotType.MUZZLE_LOADER;
		throw new NoSuchElementException("Cannot convert '"+value+"' to AmmoSlotType");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public List<AmmunitionSlot> read(String v) throws Exception {
		v = v.trim();
		List<AmmunitionSlot> ret = new ArrayList<AmmunitionSlot>();
		
		StringTokenizer tok = new StringTokenizer(v, " /");
		while (tok.hasMoreTokens()) {
			String tmp = tok.nextToken();
			StringTokenizer tok2 = new StringTokenizer(tmp, " ()");
			int amount = Integer.valueOf(tok2.nextToken());
			AmmoSlotType type = null;
			if (tok2.hasMoreTokens()) {
				String val = tok2.nextToken();
				try {
					type = AmmoSlotType.getByValue(val);
				} catch (NoSuchElementException e) {
					type = getType(val);
				}
			}
			ret.add(new AmmunitionSlot(amount, type));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(List<AmmunitionSlot> v) throws Exception {
		if (v.isEmpty())
			return null;
		
		StringBuffer buf = new StringBuffer();
		for (Iterator<AmmunitionSlot> it = v.iterator(); it.hasNext(); ) {
			buf.append(it.next().toString());
			if (it.hasNext())
				buf.append("/");
		}
		
		return buf.toString();
	}
	
}