package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.ComplexForm;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.simplepersist.StringValueConverter;

public class FadingOrDrainConverter implements StringValueConverter<Integer> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Integer read(String v) throws Exception {
		if ("HITS".equals(v)) return ComplexForm.HITS;
		return Integer.parseInt(v);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Integer v) throws Exception {
		if (v==ComplexForm.HITS) return "HITS";
		return String.valueOf(v);
	}
	
}