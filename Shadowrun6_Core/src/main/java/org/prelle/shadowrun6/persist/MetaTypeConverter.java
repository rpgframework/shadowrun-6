package org.prelle.shadowrun6.persist;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

public class MetaTypeConverter implements StringValueConverter<MetaType> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public MetaType read(String v) throws Exception {
		MetaType data = ShadowrunCore.getMetaType(v);
		if (data==null) {
			List<String> valid = ShadowrunCore.getMetaTypes().stream().map(m -> m.getId()).collect(Collectors.toList());
			logger.error("No such MetaType: "+v+"\nValid are "+valid);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "No such Metatype: "+v+"\nValid are "+valid);
			throw new ReferenceException(ReferenceType.METATYPE, v);
		}
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(MetaType v) throws Exception {
		return v.getKey();
	}
	
}