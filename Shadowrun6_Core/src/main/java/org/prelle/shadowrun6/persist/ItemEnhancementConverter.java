package org.prelle.shadowrun6.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class ItemEnhancementConverter implements StringValueConverter<ItemEnhancement> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun6.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ItemEnhancement read(String v) throws Exception {
		ItemEnhancement item = ShadowrunCore.getItemEnhancement(v);
		if (item==null) {
			logger.error("Unknown item enhancement reference: '"+v+"'");
			throw new ReferenceException(ReferenceType.ITEM_ENHANCEMENT, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ItemEnhancement v) throws Exception {
		return v.getId();
	}
	
}