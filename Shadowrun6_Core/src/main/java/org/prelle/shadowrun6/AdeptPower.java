/**
 *
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.SelectableItem;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class AdeptPower extends BasePluginData implements Comparable<AdeptPower>, SelectableItem {

	private static Logger logger = LogManager.getLogger("shadowrun");
	
	public enum Activation {
		PASSIVE,
		MINOR_ACTION,
		MAJOR_ACTION
	}

	@Attribute
	private String id;
	@Attribute(name="base")
	private double basecost;
	@Attribute(required=true)
	private double cost;
	@Attribute(name="levels")
	private boolean levels;
	@Attribute(name="max")
	private int maxLevel;
	@Attribute(name="act")
	private Activation activation;
	@Attribute(name="select")
	private ChoiceType selectFrom;
	@Element
	private ModificationList modifications;
	@Element(name="requires")
	private RequirementList requirements;

	//--------------------------------------------------------------------
	public AdeptPower() {
		modifications = new ModificationList();
		requirements  = new RequirementList();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	@Override
	public String getTypeId() {
		return "power";
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("adeptpower."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "adeptpower."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "adeptpower."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AdeptPower o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the levels
	 */
	public boolean hasLevels() {
		return levels;
	}

	//--------------------------------------------------------------------
	/**
	 * @param level A value of 1 or higher
	 */
	public float getCostForLevel(int level) {
		return (float) ( basecost + level*cost );
	}
	//--------------------------------------------------------------------
	/**
	 * @return the selectFrom
	 */
	public ChoiceType getSelectFrom() {
		return selectFrom;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public boolean needsChoice() {
		return selectFrom!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(RequirementList requirements) {
		this.requirements = requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the maxLevel
	 */
	public int getMaxLevel() {
		return maxLevel;
	}

	//-------------------------------------------------------------------
	/**
	 * @param maxLevel the maxLevel to set
	 */
	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the activation
	 */
	public Activation getActivation() {
		return activation;
	}

}
