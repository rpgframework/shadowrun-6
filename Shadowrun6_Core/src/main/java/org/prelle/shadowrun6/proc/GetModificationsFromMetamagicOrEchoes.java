/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.WeaponData;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.QualityModification;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMetamagicOrEchoes implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public GetModificationsFromMetamagicOrEchoes() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Calculate effective modifications from available powers
			for (MetamagicOrEchoValue ref :model.getMetamagicOrEchoes()) {
				logger.debug("  add from metamagic/echo "+ref.getModifyable().getId());
				// Calculate modifications
				ref.clearModifications();
				int multiplier = (ref.getModifyable().hasLevels())?ref.getLevel():1;
				logger.warn("Multiplier for "+ref.getModifyable().getId()+" is "+multiplier+"    ");
				for (Modification mod : ref.getModifyable().getModifications()) {
					Modification realMod = ShadowrunTools.instantiateModification(mod, ref.getChoice(), multiplier);
					logger.info(mod+" -> "+realMod);
					ref.addModification(realMod);
				}
				
				if (ref.getChoice()!=null) {
					if (ref.getModifyable().getSelectFrom()==ChoiceType.ADEPT_WAY) {
						logger.info("Add adeot way "+ref.getChoice());
						QualityModification qualMod = new QualityModification((Quality) ref.getChoice());
						qualMod.setSource(ref);
						unprocessed.add(qualMod);
					}
				}
				
				
				if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
					logger.info(" - "+ref.getModifyable().getId()+" has modifications: "+ref.getModifications());
					for (Modification realMod : ref.getModifications()) {
//						Modification realMod = ShadowrunTools.instantiateModification(mod, ref.getChoice(), model.getInitiateSubmersionLevel());
						if (realMod instanceof SkillModification && ((SkillModification)realMod).getSkill()==null) {
							logger.info("Determine skill from choice "+ref.getChoice());
							SkillModification realMod2 = (SkillModification)realMod;
							if (ref.getChoice() instanceof CarriedItem) {
								if (ref.getModifyable().getId().equals("item_attunement")) {
									((CarriedItem)ref.getChoice()).setItemAttunement(ref);
								}
								List<WeaponData> weapons = ((CarriedItem)ref.getChoice()).getWeaponDataRecursive();
								if (!weapons.isEmpty()) {
									realMod2.setSkill(weapons.get(0).getSkill());
								}
							}
						}
						realMod.setSource(ref.getModifyable());
						unprocessed.add(realMod);
					}
//					logger.debug(" - add modifications: "+ref.getModifications());
				}
			}
			
			// Raise resonance or magic
			if (model.getInitiateSubmersionLevel()>0) {
				if (model.getMagicOrResonanceType().usesMagic()) {
					unprocessed.add(new AttributeModification(ModificationValueType.MAX, Attribute.MAGIC, model.getInitiateSubmersionLevel()));
				} else if (model.getMagicOrResonanceType().usesResonance()) {
					unprocessed.add(new AttributeModification(ModificationValueType.MAX, Attribute.RESONANCE, model.getInitiateSubmersionLevel()));
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
