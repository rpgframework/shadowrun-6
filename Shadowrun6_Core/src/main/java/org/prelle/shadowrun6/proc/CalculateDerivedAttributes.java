/**
 *
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.Damage;
import org.prelle.shadowrun6.items.Damage.Type;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.items.ItemAttributeObjectValue;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.DamageTypeModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ModificationBase.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.SpecialRuleModification;
import org.prelle.shadowrun6.modifications.SpecialRuleModification.Rule;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateDerivedAttributes implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.proc.deri");

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		List<Rule> rules = new ArrayList<Rule>();
		logger.trace("START: process");
		try {
			Damage.Type unarmedDamageType = Damage.Type.STUN;
			// Apply attribute modifications
			for (Modification _mod : previous) {
				if (_mod instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)_mod;
					if (mod.getType()==ModificationValueType.MAX) {
						unprocessed.add(mod);
					} else {
						AttributeValue val = model.getAttribute(mod.getAttribute());
						logger.info("  Add "+mod+" from "+mod.getSource());
						val.addModification(mod);
					}
				} else if (_mod instanceof ItemAttributeModification) {
					ItemAttributeModification mod = (ItemAttributeModification)_mod;
					for (CarriedItem item : model.getItems(true)) {
						logger.trace("check "+mod+" on "+item);
						logger.trace("item = "+item.getUsedAsType()+"   "+item.getItem().getNonAccessoryType()+"/"+item.getItem().getSubtype(null)+"   slot="+item.getSlot());
						if (mod.getType()!=null && mod.getType()!=item.getUsedAsType())
							continue;
						if (mod.getSubtype()!=null && mod.getSubtype()!=item.getUsedAsSubType())
							continue;
						logger.info("Apply "+mod+" to "+item);
						if (item.getModifications().size()>30) {
							logger.fatal("STOP HERE - too many modifications");
							System.exit(1);
						}
						item.addAutoModification(mod);
					}
				} else if (_mod instanceof DamageTypeModification) {
					DamageTypeModification mod = (DamageTypeModification)_mod;
					logger.info("  Set unarmed damage to "+mod.getType()+" from "+mod.getSource());
					unarmedDamageType = mod.getType();
				} else if (_mod instanceof SpecialRuleModification) {
					SpecialRuleModification mod = (SpecialRuleModification)_mod;
					switch (mod.getRule()) {
					case CHARISMATIC_DEFENSE:
						rules.add(mod.getRule());
						break;
					default:
						unprocessed.add(_mod);
					}
				} else
					unprocessed.add(_mod);
			}

			// Set character back to zero
			//			logger.debug("1. Calculate derived attributes");
			AttributeValue val = null;

			/*
			 * Physical condition monitor
			 */
			int phy = Math.round(model.getAttribute(Attribute.BODY).getModifiedValue()/2.0f) + 8;
			val = model.getAttribute(Attribute.PHYSICAL_MONITOR);
			val.setPoints(phy);
//			val.clearModifications();
			logger.debug(" Monitor Physical = "+val.getModifiedValue()+"    modifier="+val.getModifier());

			/*
			 * Stun condition monitor
			 */
			int stun = Math.round(model.getAttribute(Attribute.WILLPOWER).getModifiedValue()/2.0f) + 8;
			val = model.getAttribute(Attribute.STUN_MONITOR);
			val.setPoints(stun);
//			val.clearModifications();
			logger.debug(" Monitor Stun     = "+val.getModifiedValue());

			/*
			 * Physical initiative
			 */
			val = model.getAttribute(Attribute.INITIATIVE_PHYSICAL);
			val.setPoints(0);
//			val.clearModifications();
			val.addNaturalModifier(model.getAttribute(Attribute.REACTION).getAugmentedValue(), Attribute.REACTION);
			val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			logger.debug(" INI Physical = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getAugmentedValue()+" d6");
			// Minor actions (Physical)
			val = model.getAttribute(Attribute.MINOR_ACTION);
			val.setPoints(1);
			val.addNaturalModifier(model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getAugmentedValue(), Attribute.INITIATIVE_DICE_PHYSICAL);
			logger.debug("              = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * astral initiative
			 */
			val = model.getAttribute(Attribute.INITIATIVE_ASTRAL);
			val.setPoints(0);
//			val.clearModifications();
			val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
			val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			logger.debug(" Base INI Astral = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getAugmentedValue()+" d6");
			// Minor actions (Astral)
			val = model.getAttribute(Attribute.MINOR_ACTION_ASTRAL);
			val.setPoints(1);
			val.addNaturalModifier(model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getAugmentedValue(), Attribute.INITIATIVE_DICE_ASTRAL);
			logger.debug("                 = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * matrix initiative (AR)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX);
			val.setPoints(0);
//			val.clearModifications();
			CarriedItem bestDF = ShadowrunTools.getBestMatrixDF(model);
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				// With commlink
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX, model.getAttribute(Attribute.REACTION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			}
			logger.debug(" Base INI Matrix = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getAugmentedValue()+" d6");
			// Minor actions (Matrix)
			val = model.getAttribute(Attribute.MINOR_ACTION_MATRIX);
			val.setPoints(1 + model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getAugmentedValue());
			logger.debug("                 = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * matrix initiative (VR, cold sim)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX_VR_COLD);
			val.setPoints(0);
			val.clearModifications();
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_COLD, bestDF.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue(), ModificationType.RELATIVE, ItemAttribute.DATA_PROCESSING));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_COLD, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			}
			logger.debug(" Base INI Matrix VR = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_COLD).getAugmentedValue()+" d6");

			/*
			 * matrix initiative (VR, cold sim)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX_VR_HOT);
			val.setPoints(0);
			val.clearModifications();
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_HOT, bestDF.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue(), ModificationType.RELATIVE, ItemAttribute.DATA_PROCESSING));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_HOT, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			}
			logger.debug(" Base INI Matrix VR Hot = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_HOT).getAugmentedValue()+" d6");

			/*
			 * Defensive
			 */
			val = model.getAttribute(Attribute.DEFENSIVE_POOL);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL, model.getAttribute(Attribute.REACTION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			logger.debug(" Defensive pool = "+val.getAugmentedValue());

			/*
			 * Defensive Combat Direct
			 */
			val = model.getAttribute(Attribute.DEFENSIVE_POOL_COMBAT_DIRECT);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_COMBAT_DIRECT, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_COMBAT_DIRECT, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			logger.debug(" Defensive pool (Direct) = "+val.getAugmentedValue());

			/*
			 * Defensive Combat Indirect
			 */
			val = model.getAttribute(Attribute.DEFENSIVE_POOL_COMBAT_INDIRECT);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_COMBAT_INDIRECT, model.getAttribute(Attribute.REACTION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_COMBAT_INDIRECT, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.WILLPOWER));
			logger.debug(" Defensive pool (Indirect) = "+val.getAugmentedValue());

			/*
			 * Defensive Pool against toxin damage
			 */
			val = model.getAttribute(Attribute.DEFENSIVE_POOL_RESIST_TOXIN_DAMAGE);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_RESIST_TOXIN_DAMAGE, model.getAttribute(Attribute.BODY).getAugmentedValue(), ModificationType.RELATIVE, Attribute.BODY));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_RESIST_TOXIN_DAMAGE, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.WILLPOWER));
			logger.debug(" Defensive pool (Toxins) = "+val.getAugmentedValue());

			/*
			 *
			 */

			/*
			 * Drain
			 */
			val = model.getAttribute(Attribute.DEFENSIVE_POOL_DRAIN);
			val.setPoints(0);
			if (model.getTradition()!=null) {
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_DRAIN, model.getAttribute(model.getTradition().getDrainAttribute1()).getAugmentedValue(), ModificationType.RELATIVE, model.getTradition().getDrainAttribute1()));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DEFENSIVE_POOL_DRAIN, model.getAttribute(model.getTraditionAttribute()).getAugmentedValue(), ModificationType.RELATIVE, model.getTraditionAttribute()));
			}
			logger.debug(" Defensive pool (Drain) = "+val.getAugmentedValue());

//			/*
//			 * Dodge
//			 */
//			val = model.getAttribute(Attribute.DODGE);
//			val.setPoints(0);
//			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DODGE, model.getAttribute(Attribute.REACTION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
//			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DODGE, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
//			if (model.getSkillValue(ShadowrunCore.getSkill("athletics"))!=null)
//				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DODGE, model.getSkillValue(ShadowrunCore.getSkill("athletics")).getModifiedValue(), ModificationType.RELATIVE, ShadowrunCore.getSkill("athletics")));
//			logger.debug(" Dodge = "+val.getAugmentedValue());

			/*
			 * Composure
			 */
			val = model.getAttribute(Attribute.COMPOSURE);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.COMPOSURE, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.WILLPOWER));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.COMPOSURE, model.getAttribute(Attribute.CHARISMA).getAugmentedValue(), ModificationType.RELATIVE, Attribute.CHARISMA));
			logger.debug(" Composure = "+val.getAugmentedValue());

			/*
			 * Judge intentions
			 */
			val = model.getAttribute(Attribute.JUDGE_INTENTIONS);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.JUDGE_INTENTIONS, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.WILLPOWER));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.JUDGE_INTENTIONS, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			logger.debug(" Judge Intentions = "+val.getAugmentedValue());

			/*
			 * lifting/carrying
			 */
			val = model.getAttribute(Attribute.LIFT_CARRY);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.LIFT_CARRY, model.getAttribute(Attribute.BODY).getAugmentedValue(), ModificationType.RELATIVE, Attribute.BODY));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.LIFT_CARRY, model.getAttribute(Attribute.WILLPOWER).getAugmentedValue(), ModificationType.RELATIVE, Attribute.WILLPOWER));
			logger.debug(" Lift/Carry = "+val.getAugmentedValue());

			/*
			 * memory
			 */
			val = model.getAttribute(Attribute.MEMORY);
			val.setPoints(0);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.MEMORY, model.getAttribute(Attribute.LOGIC).getAugmentedValue(), ModificationType.RELATIVE, Attribute.LOGIC));
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.MEMORY, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			logger.debug(" Memory = "+val.getAugmentedValue());

			/*
			 * Damage Resistance
			 */
			val = model.getAttribute(Attribute.DAMAGE_RESISTANCE);
			val.setPoints(0);
//			val.clearModifications();
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DAMAGE_RESISTANCE, model.getAttribute(Attribute.BODY).getAugmentedValue(), ModificationType.RELATIVE, Attribute.BODY));
			logger.debug("Damage Resistance = "+val);

			/*
			 * Damage overflow
			 */
			val = model.getAttribute(Attribute.DAMAGE_OVERFLOW);
			val.setPoints(0);
			val.clearModifications();
			val.addNaturalModifier(model.getAttribute(Attribute.BODY).getAugmentedValue(), Attribute.BODY);
			val.addNaturalModifier(model.getAttribute(Attribute.BODY).getAugmentedValue(), Attribute.BODY);
			logger.debug(" Damage overflow = "+val.getAugmentedValue());

			/*
			 * Alternate cyberlimb attributes
			 */
			val = model.getAttribute(Attribute.AGILITY);
			logger.debug(" Agility alternate = "+val.getAlternateValue());
			val = model.getAttribute(Attribute.STRENGTH);
			logger.debug(" Strength alternate = "+val.getAlternateValue());

			/*
			 * Defense rating
			 * First of all find the highest normal armor
			 */
			CarriedItem bestArmor = null;
			for (CarriedItem item : model.getItems(false)) {
				if (!item.hasAttribute(ItemAttribute.ARMOR) || item.isType(ItemType.VEHICLES) || item.isType(Arrays.asList(ItemType.droneTypes())))
					continue;
				item.setIgnoredForCalculations(true);
				item.setPrimary(false);
				// If no previous selection or armor is better, use it
				if (bestArmor==null || item.getAsValue(ItemAttribute.ARMOR).getModifiedValue()> bestArmor.getAsValue(ItemAttribute.ARMOR).getModifiedValue() )
					bestArmor = item;
				// Gear pieces that add armor are also allowed
				if (item.getItem().getArmorData()!=null && item.getItem().getArmorData().addsToMain())
					item.setIgnoredForCalculations(false);
				logger.debug("*  "+item.getNameWithRating()+" \t"+item.getAsValue(ItemAttribute.ARMOR).getModifiedValue()+": ignored="+item.isIgnoredForCalculations());
			}
			if (bestArmor!=null) {
				bestArmor.setIgnoredForCalculations(false);
				bestArmor.setPrimary(true);
			}
			for (CarriedItem item : model.getItems(false)) {
				if (!item.hasAttribute(ItemAttribute.ARMOR) || item.isType(ItemType.VEHICLES) || item.isType(Arrays.asList(ItemType.droneTypes())))
					continue;
				item.setPrimary(bestArmor==item);
			}


			int defRating = model.getAttribute(Attribute.BODY).getAugmentedValue();
			// Power Plays "Charismatic Defense"
			if (rules.contains(Rule.CHARISMATIC_DEFENSE)) {
				defRating = model.getAttribute(Attribute.CHARISMA).getAugmentedValue();
			}
			logger.debug("  Base Defensive rating = "+defRating);

			for (CarriedItem item : model.getItems(true)) {
				if (!item.hasAttribute(ItemAttribute.ARMOR) || item.isIgnoredForCalculations() || item.isType(ItemType.VEHICLES) || item.isType(Arrays.asList(ItemType.droneTypes())))
					continue;
				ItemAttributeNumericalValue armorAtt = item.getAsValue(ItemAttribute.ARMOR);
				defRating += armorAtt.getModifiedValue();
				logger.debug("  Add Defensive rating = "+armorAtt.getModifiedValue()+" from "+item.getNameWithRating());
			}
			val = model.getAttribute(Attribute.DEFENSE_RATING);
			logger.info("Defensive rating = "+defRating+" = "+val);
			if (logger.isTraceEnabled()) {
				for (Modification mod : val.getModifications()) {
					logger.trace("#### "+mod+"  from "+mod.getSource());
				}
			}
			val.setPoints(defRating);

			/*
			 * Modify all weapons with attack rating
			 */
			// Store modifications for attack rating
			List<Object> sourcesUsed = new ArrayList<>();
			for (Modification mod : model.getAttribute(Attribute.ATTACK_RATING).getModifications()) {
				if (sourcesUsed.contains(mod.getSource())) {
					logger.info("Ignoring the AR modification from the same source appearing a second time");
					continue;
				}
				AttributeModification amod = (AttributeModification)mod;
				if (amod.getAttackRating()!=null) {
					logger.info("Apply global attack rating modification "+amod);
					sourcesUsed.add(mod.getSource());
					for (CarriedItem item : model.getItemsRecursive(true, ItemType.weaponTypes())) {
						ItemAttributeModification iMod = new ItemAttributeModification(ItemAttribute.ATTACK_RATING, amod.getAttackRating());
						iMod.setSource(amod.getSource());
						item.getAttribute(ItemAttribute.ATTACK_RATING).addModification(iMod);
						logger.debug("Added "+iMod+" to "+item);
					}
				}
			}

			/*
			 * Add strength to attack rating of all melee weapons (Errata 09.2021)
			 */
			int[] strengthAR = new int[] {model.getAttribute(Attribute.STRENGTH).getAugmentedValue(),0,0,0,0};
			ItemAttributeModification iMod = new ItemAttributeModification(ItemAttribute.ATTACK_RATING, strengthAR);
			iMod.setSource(Attribute.STRENGTH);
			int[] reactionAR = new int[] {model.getAttribute(Attribute.REACTION).getAugmentedValue(),0,0,0,0};
			ItemAttributeModification iModRea = new ItemAttributeModification(ItemAttribute.ATTACK_RATING, reactionAR);
			iModRea.setSource(Attribute.REACTION);
			Skill melee = ShadowrunCore.getSkill("close_combat");
			SkillSpecialization unarmed = melee.getSpecialization("unarmed");
			for (CarriedItem item : model.getItemsRecursive(true, ItemType.weaponTypes())) {
				if (item.getUsedAsSubType()==ItemSubType.WHIPS) {
					item.getAttribute(ItemAttribute.ATTACK_RATING).addModification(iModRea);
					logger.debug("Added "+iModRea+" to "+item);
				} else
				if (item.getItem().getWeaponData()!=null && item.getItem().getWeaponData().getSkill()==melee && item.getItem().getWeaponData().getSpecialization()!=unarmed) {
					item.getAttribute(ItemAttribute.ATTACK_RATING).addModification(iMod);
					logger.debug("Added "+iMod+" to "+item);
				}
			}


			/*
			 * Unarmed attacks
			 */
			int attRat = model.getAttribute(Attribute.REACTION).getAugmentedValue()+ model.getAttribute(Attribute.STRENGTH).getAugmentedValue();
//			logger.info("AR = "+model.getAttribute(Attribute.ATTACK_RATING));
//			logger.info("AR = "+model.getAttribute(Attribute.ATTACK_RATING).getModifications());
//			logger.info("AR = "+model.getAttribute(Attribute.ATTACK_RATING).getModifiedValue());
//			if (model.getAttribute(Attribute.ATTACK_RATING).getModifiedValue()>0) {
//				logger.info("Add "+model.getAttribute(Attribute.ATTACK_RATING).getModifiedValue()+" to unarmed attack rating");
//				attRat += model.getAttribute(Attribute.ATTACK_RATING).getModifiedValue();
//			}

			for (CarriedItem item : model.getItems(true)) {
				if (item.getItem().getId().startsWith("unarmed")) {
					item.setAttributeOverride(ItemAttribute.ATTACK_RATING, new int[] {attRat,0,0,0,0});
					// Apply eventually unarmed AR modifiers
					ItemAttributeObjectValue oVal = item.getAsObject(ItemAttribute.ATTACK_RATING);
					for (Modification mod : model.getAttribute(Attribute.ATTACK_RATING).getModifications()) {
						AttributeModification amod = (AttributeModification)mod;
						if (amod.getValue()!=0) {
							logger.info("Apply unarmed attack rating modification "+amod);
							ItemAttributeModification nMod = new ItemAttributeModification(ItemAttribute.ATTACK_RATING, new int[] {amod.getValue(),0,0,0,0});
							nMod.setSource(amod.getSource());
							item.addAutoModification(nMod);
							oVal.addModification(nMod);
						}
					}


					// Damage is calculated later on the fly by method ShadowrunTools.getWeaponDamage
					Damage dmg = (Damage) item.getAsValue(ItemAttribute.DAMAGE);
					dmg.setType(unarmedDamageType);
					if (unarmedDamageType!=Type.STUN)
						logger.info("  Set damage type to "+unarmedDamageType);
					// Apply MELEE_DAMAGE
					for (Modification mod : model.getAttribute(Attribute.MELEE_DAMAGE).getModifications()) {
						ItemAttributeModification itemMod = new ItemAttributeModification(ItemAttribute.DAMAGE, ((AttributeModification)mod).getValue());
						itemMod.setSource(mod.getSource());
						logger.debug("  add to unarmed damage: "+itemMod);
						dmg.addModification(itemMod);
					}

					logger.debug("Set Unarmed attack with attack rating "+Arrays.toString((int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue())+" and current damage "+dmg);
				}
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
