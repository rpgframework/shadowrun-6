/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Persona;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculatePersona implements CharacterProcessor {
	
	private PropertyResourceBundle RES = ShadowrunCore.getI18nResources();

	private final static Logger logger = LogManager.getLogger("shadowrun6.proc");

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Ensure a persona is present
			Persona persona = model.getPersona();
			if (persona==null) {
				persona = new Persona();
				model.setPersona(persona);
			} else
				persona.clear();

			/*
			 * Normally a persona is build from the used commlink or cyber jack
			 * plus the cyberdeck. Technomancers use their living persona.
			 */
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancer
				calculateTechnomancer(model, persona);				

			} else {
				// Non-Technomancer
				calculateNonTechnomancer(model, persona);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	private void calculateTechnomancer(ShadowrunCharacter model, Persona persona) {
		persona.setName(Resource.get(RES, "label.living_persona"));
		// Data Processing = LOGIC
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.DATA_PROCESSING, 
						model.getAttribute(Attribute.LOGIC).getAugmentedValue()));
		// Firewall = WILLOWER
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.FIREWALL, 
						model.getAttribute(Attribute.WILLPOWER).getAugmentedValue()));
		// Attack = CHARISMA
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.ATTACK, 
						model.getAttribute(Attribute.CHARISMA).getAugmentedValue()));
		// Sleaze = INTUITION
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.SLEAZE, 
						model.getAttribute(Attribute.INTUITION).getAugmentedValue()));

		// Device Rating = RESONANCE
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.DEVICE_RATING, 
						model.getAttribute(Attribute.RESONANCE).getAugmentedValue()));
		// Attack rating
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.ATTACK_RATING,
				persona.getAttack().getModifiedValue() + 
				persona.getSleaze().getModifiedValue()));
		// Defense rating
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.DEFENSE_RATING,
				persona.getDataProcessing().getModifiedValue() + 
				persona.getFirewall().getModifiedValue()));

		// Initiative
		persona.setAttribute(new AttributeValue(Attribute.INITIATIVE_MATRIX, 
				model.getAttribute(Attribute.LOGIC).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue()));
		persona.setAttribute(new AttributeValue(Attribute.INITIATIVE_MATRIX_VR_COLD, 
				model.getAttribute(Attribute.LOGIC).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue()));
		persona.setAttribute(new AttributeValue(Attribute.INITIATIVE_MATRIX_VR_HOT, 
				model.getAttribute(Attribute.LOGIC).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue()));
		persona.setAttribute(model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX));
		persona.setAttribute(model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_COLD));
		persona.setAttribute(model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_HOT));
		
		// Matrix condition monitor
		persona.setMonitor(getTechnomancerMonitorArray(model));
	}

	//--------------------------------------------------------------------
	private void calculateNonTechnomancer(ShadowrunCharacter model, Persona persona) {
		CarriedItem bestAccessDevice = null;

		/*
		 * Find the best cyberdeck
		 */
		CarriedItem bestAS = null;
		int bestSum = 0;
		for (CarriedItem item : model.getItems(true)) {
			if (!item.hasAttribute(ItemAttribute.ATTACK))
				continue;
			if (logger.isTraceEnabled())
				logger.trace("  consider for AS: "+item);
			int a = item.getAsValue(ItemAttribute.ATTACK).getModifiedValue();
			int s = item.getAsValue(ItemAttribute.SLEAZE).getModifiedValue();
			int sum = a+s;
			if (sum>bestSum) {
				bestAS = item;
				bestSum= sum;
				persona.setAttribute(item.getAsValue(ItemAttribute.ATTACK));
				persona.setAttribute(item.getAsValue(ItemAttribute.SLEAZE));
			}
			// Device rating
			if (item.hasAttribute(ItemAttribute.DEVICE_RATING)) {
				int dr = item.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue();
				if (bestAccessDevice==null || dr>bestAccessDevice.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue()) {
					bestAccessDevice = item;
				}
			}
		}
		if (bestAS!=null)
			bestAccessDevice = bestAS;
		for (CarriedItem item : model.getItems(true)) {
			if (!item.hasAttribute(ItemAttribute.ATTACK))
				continue;
			item.setPrimary(bestAS==item);
		}
		logger.info("best device for AS: "+bestAS);

		/*
		 * Find the best commlink or cyber jack
		 */
		CarriedItem bestDF = null;
		bestSum = 0;
		for (CarriedItem item : model.getItems(true)) {
			if (!item.hasAttribute(ItemAttribute.DATA_PROCESSING))
				continue;
			logger.info("  consider for DF: "+item);
			int d = item.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue();
			int f = item.getAsValue(ItemAttribute.FIREWALL).getModifiedValue();
			int sum = d+f;
			if (sum>bestSum) {
				bestDF = item;
				bestSum= sum;
				persona.setAttribute(item.getAsValue(ItemAttribute.DATA_PROCESSING));
				persona.setAttribute(item.getAsValue(ItemAttribute.FIREWALL));
			}
			// Device rating
			if (item.hasAttribute(ItemAttribute.DEVICE_RATING) && bestAS==null) {
				int dr = item.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue();
				if (bestAccessDevice==null || dr>bestAccessDevice.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue()) {
					bestAccessDevice = item;
				}
			}
		}
		if (bestAS!=null)
			bestAccessDevice = bestAS;
		for (CarriedItem item : model.getItems(true)) {
			if (!item.hasAttribute(ItemAttribute.DATA_PROCESSING))
				continue;
			item.setPrimary(bestAS==item);
		}
		logger.info("best device for DF: "+bestDF);
		logger.info("best access device: "+bestAccessDevice);

		// Device rating
		if (bestAccessDevice!=null)
			persona.setAttribute(bestAccessDevice.getAsValue(ItemAttribute.DEVICE_RATING));
		else
			persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.DEVICE_RATING,0));
		// Attack rating
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.ATTACK_RATING,
				persona.getAttack().getModifiedValue() + 
				persona.getSleaze().getModifiedValue()));
		// Defense rating
		persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.DEFENSE_RATING,
				persona.getDataProcessing().getModifiedValue() + 
				persona.getFirewall().getModifiedValue()));
		// Active program slots
		if (bestAccessDevice!=null)
			persona.setAttribute(bestAccessDevice.getAsValue(ItemAttribute.CONCURRENT_PROGRAMS));
		else
			persona.setAttribute(new ItemAttributeNumericalValue(ItemAttribute.CONCURRENT_PROGRAMS,0));
		
		/*
		 * Initiative (CRB 179)
		 */
		persona.setAttribute(model.getAttribute(Attribute.INITIATIVE_MATRIX));
		persona.setAttribute(model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX));
		persona.setAttribute(new AttributeValue(Attribute.INITIATIVE_MATRIX_VR_COLD, 
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				persona.getDataProcessing().getModifiedValue()));
		persona.setAttribute(new AttributeValue(Attribute.INITIATIVE_MATRIX_VR_HOT, 
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				persona.getDataProcessing().getModifiedValue()));
		
		// Matrix condition monitor
		persona.setMonitor(getNormalMonitorArray(persona.getDeviceRating()));

		if (bestAS==null && bestDF!=null)
			persona.setName(bestDF.getName());
		else if (bestAS!=null && bestDF!=null)
			persona.setName(bestDF.getName()+" + "+bestAS.getName());
		else {
			persona.setName("-None-");
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * CRB 189: Their Matrix Condition Monitor is (Logic/2, rounded up) + 8.
	 */
	public static int[] getTechnomancerMonitorArray(ShadowrunCharacter model) {
		return ShadowrunTools.getMonitorArray(model, Attribute.WILLPOWER);
	}
	
	//-------------------------------------------------------------------
	/**
	 * CRB 179: Devices have a Matrix Condition Monitor equal to
	 * [(Device Rating / 2) + 8].
	 */
	public static int[] getNormalMonitorArray(int devRating) {
		int stun = Math.round(devRating/2.0f) + 8;
		int[] ret = new int[stun];

		int start = 0;
		int every = 3;

		for (int i=start; i<ret.length; i++) {
			ret[i] = - ((i+1-start)/every);
		}

		return ret;
	}

}
