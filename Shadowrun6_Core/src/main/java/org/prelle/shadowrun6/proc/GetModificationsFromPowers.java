/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPower.Activation;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromPowers implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.power");
	
	//-------------------------------------------------------------------
	public GetModificationsFromPowers() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Calculate effective modifications from available powers
			for (AdeptPowerValue ref :model.getAdeptPowers()) {
				AdeptPower power = ref.getModifyable();
				logger.debug("add from power "+power.getId()+" / "+ref+" / choice="+ref.getChoiceReference());
				// Calculate modifications
				ref.clearModifications();
				if (power.needsChoice() && ref.getChoice()==null) {
					ref.setChoice(ShadowrunTools.resolveChoiceType(power.getSelectFrom(), ref.getChoiceReference(), model));
					logger.debug("resolve "+power.getSelectFrom()+" '"+ref.getChoiceReference()+"' to "+ref.getChoice());
				}
				int multiplier = (ref.getModifyable().hasLevels())?ref.getModifiedLevel():1;
				if (multiplier==0) {
					logger.warn("Strange! Found AdeptPower with value 0 that should have at least 1 - ignore it: "+ref);
//					continue;
				}
				for (Modification mod : ref.getModifyable().getModifications()) {
					try {
						Modification realMod = ShadowrunTools.instantiateModification(mod, ref.getChoice(), multiplier);
						logger.debug("  instantiated mod "+realMod+" with multiplier "+multiplier);
						if (ref.getModifyable().getActivation()!=Activation.PASSIVE) {
							if (realMod instanceof AttributeModification)
								((AttributeModification)realMod).setConditional(true);
							else if (realMod instanceof SkillModification)
								((SkillModification)realMod).setConditional(true);
						}
						ref.addModification(realMod);
					} catch (Exception e) {
						logger.error("Problem calculating modifications for adept power: "+ref,e);
					}
				}
				
				
				
				if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
					logger.debug(" - "+ref.getModifyable().getId()+" has modifications: "+ref.getModifications());
					for (Modification mod : ref.getModifications()) {
						mod.setSource(ref.getModifyable());
					}
//					logger.debug(" - add modifications: "+ref.getModifications());
					unprocessed.addAll(ref.getModifications());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
