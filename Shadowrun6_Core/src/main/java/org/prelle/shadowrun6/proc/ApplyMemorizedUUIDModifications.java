/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.LifestyleCostModification;
import org.prelle.shadowrun6.modifications.SINModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyMemorizedUUIDModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public ApplyMemorizedUUIDModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			/*
			 * Detect an existing real SIN
			 */
			SIN existingRealSIN = null;
			for (SIN sin : model.getSINs()) {
				logger.debug("Found existing SIN "+sin);
				if (sin.getQuality()==Quality.REAL_SIN) {
					existingRealSIN = sin;
					break;
				}
			}
			
			
			boolean needsRealSIN = false;
			List<UUID> memorizedSINs = new ArrayList<>();
			for (Modification tmp : previous) {
				if (tmp instanceof MemorizeUUIDModification) {
					memorizedSINs.add( ((MemorizeUUIDModification)tmp).getUUID() );
					logger.info("Added memorized SIN: "+memorizedSINs);
					logger.info("MemorizeUUIDModification: src="+tmp.getSource());
				} else if (tmp instanceof SINModification) {
					SINModification mod = (SINModification)tmp;
					logger.info("Needs real SIN (from "+mod.getSource()+")");
					needsRealSIN = true;
					if (existingRealSIN!=null) {
						logger.debug("use existing real SIN "+existingRealSIN.getUniqueId());
					unprocessed.add(new LifestyleCostModification(10, 0, existingRealSIN.getUniqueId(), mod.getSource())); 
					} else {
						// Create real sin
						SIN sin = new SIN(Quality.REAL_SIN);
						sin.setName(model.getRealName());
						model.addSIN(sin);
						if (memorizedSINs.isEmpty())
							logger.debug("created new SIN "+sin.getUniqueId());
						else {
							sin.setUniqueId(memorizedSINs.remove(0));
							logger.debug("created new SIN "+sin.getUniqueId()+" with reapplied UUID");
						}
						unprocessed.add(new LifestyleCostModification(10, 0, sin.getUniqueId(), mod.getSource())); 
					}					
				} else
					unprocessed.add(tmp);
			}
			
			// If char does not need a real SIN and there is - delete it
			if (existingRealSIN!=null && !needsRealSIN)
				model.removeSIN(existingRealSIN);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
