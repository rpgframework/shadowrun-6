/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.proc.MemorizeUUIDModification.Type;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ResetModifications implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	private UUID realSINUUID;

	//-------------------------------------------------------------------
	private void clearCharacter(ShadowrunCharacter model) {
		logger.debug("Reset modifications");
		// Reset qualitiesand powers
		model.setTraditionAttribute(null);
		if (model.getTradition()!=null)
			model.setTraditionAttribute(model.getTradition().getTraditionAttribute());
		model.clearRacialQualities();
		model.clearAutoAdeptPower();
		model.clearRelevanceModifications();
		model.clearEdgeModifications();
		// Reset attributes
		for (Attribute key : Attribute.values()) {
			model.ensureAttribute(key);
			AttributeValue val = model.getAttribute(key);
			val.clearModifications();
		}
		// Reset all global AR modifications
		for (CarriedItem item : model.getItemsRecursive(true, ItemType.weaponTypes())) {
			if (item.isCreatedByModification()) {
				item.getAttribute(ItemAttribute.ATTACK_RATING).clearModifications();
			}
		}

		
		// Ensure all attributes exist
		for (Attribute key : Attribute.secondaryValues())
			model.getAttribute(key).setPoints(0);
		// Set some to minimum 1
		for (Attribute key : new Attribute[] {
				Attribute.INITIATIVE_DICE_PHYSICAL, 
				Attribute.INITIATIVE_DICE_MATRIX, 
				Attribute.MINOR_ACTION, 
				Attribute.MINOR_ACTION_ASTRAL, 
				Attribute.MINOR_ACTION_MATRIX}) {
			model.getAttribute(key).setPoints(1);
		}
		model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).setPoints(2);
		model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_COLD).setPoints(2);
		model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_HOT).setPoints(3);
		// Remove modifications
		for (Attribute key : Attribute.values()) {
			AttributeValue val = model.getAttribute(key);
			val.clearModifications();
		}

		// Clear auto gear
		model.clearAutoGear();

		// Clear auto modifications
		for (CarriedItem item : model.getItems(true)) {
			item.clearAutoModifications();
		}

		// Add an "unarmed" item
		CarriedItem unarmedRef = new CarriedItem(ShadowrunCore.getItem("unarmed"));
		unarmedRef.setUsedAsType(ItemType.WEAPON_CLOSE_COMBAT);
		unarmedRef.setUsedAsSubType(ItemSubType.UNARMED);
		model.addAutoItem(unarmedRef);
		
		// Reset skills
		model.clearAutoSkills();
		for (SkillValue val : model.getSkillValues()) {
			if (val!=null) {
				val.clearModifications();
			}
		}
		
		// Reset spells
		for (SpellValue val : model.getSpells()) {
			if (val!=null) {
				val.clearModifications();
			}
		}
		
		// Reset rituals
		for (RitualValue val : model.getRituals()) {
			if (val!=null) {
				val.clearModifications();
			}
		}
		
		// Reset Adept powers
		for (AdeptPowerValue val : model.getAdeptPowers()) {
			if (val!=null) {
				val.clearModifications();
				val.clearValueModifications();
			}
		}
		
		// Remove SINs given by modifications
		for (SIN ref : new ArrayList<SIN>(model.getSINs())) {
			if (ref.getQuality()==SIN.Quality.REAL_SIN) {
				if (ref.getUniqueId()!=null)
					realSINUUID = ref.getUniqueId();
				model.removeSIN(ref);
			}
		}
	}

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Set character back to zero
			clearCharacter(model);
			if (realSINUUID!=null)
				unprocessed.add(new MemorizeUUIDModification(Type.SIN, realSINUUID));
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}
	
}
