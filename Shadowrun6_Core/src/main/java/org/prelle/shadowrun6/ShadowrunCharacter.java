/**
 *
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.CarriedItemList;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.EdgeModification.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationChoice;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.modifications.RelevanceModification;
import org.prelle.shadowrun6.modifications.RelevanceModification.RelevanceType;
import org.prelle.shadowrun6.persist.CharacterConceptConverter;
import org.prelle.shadowrun6.persist.ItemConverter;
import org.prelle.shadowrun6.persist.MagicOrResonanceTypeConverter;
import org.prelle.shadowrun6.persist.MappedModificationConverter;
import org.prelle.shadowrun6.persist.MetaTypeConverter;
import org.prelle.shadowrun6.persist.ReferenceException;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.shadowrun6.persist.SpellConverter;
import org.prelle.shadowrun6.persist.TraditionConverter;
import org.prelle.shadowrun6.persist.UUIDConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.Root;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="sr6char")
public class ShadowrunCharacter implements RuleSpecificCharacterObject {

	public enum Gender {
		MALE,
		FEMALE,
		DIVERSE
		;
		public String toString() {
			return ShadowrunCore.getI18nResources().getString("gender."+name().toLowerCase());
		}
	}

	public enum PluginMode {
		ALL,
		LANGUAGE,
		SELECTED
	}
	
	@Attribute(name="gen")
	private boolean generationMode;
	@Element(name="chargen")
	private String chargenUsed;
	@Element(name="chargenSettings")
	private String chargenSettings;
	private transient Object chargenSettingsObject;
	@Attribute(name="meta",required=true)
	@AttribConvert(MetaTypeConverter.class)
	private MetaType metatype;
	@Attribute(name="magic")
	@AttribConvert(MagicOrResonanceTypeConverter.class)
	private MagicOrResonanceType magicOrResonanceType;
	@Attribute(name="concept")
	@AttribConvert(CharacterConceptConverter.class)
	private CharacterConcept concept;
	@Attribute(name="karmaI")
	private int karmaInvested;
	@Attribute(name="karmaF")
	private int karmaFree;
	@Attribute(name="nuyen")
	private int nuyen;
	@Attribute(name="debt")
	private int debt;
	@Attribute(name="aspskill")
	@AttribConvert(SkillConverter.class)
	private Skill aspectSkill;
	@Element
	private String name;
	@Element(name="realname")
	private String realname;
	@ElementList(entry="attribute",type=AttributeValue.class)
	protected Attributes attributes;
    @ElementList(entry="skillval", type=SkillValue.class)
	protected List<SkillValue> skills;
	protected transient List<SkillValue> autoSkills;
	@Attribute(name="trad")
	@AttribConvert(TraditionConverter.class)
	protected Tradition tradition;
	@ElementList(entry="spellval", type=SpellValue.class)
	protected List<SpellValue> spells;
	@ElementList(entry="spelldef", type=Spell.class)
	protected List<Spell> customSpells;
	@ElementList(entry="ritualval", type=RitualValue.class)
	protected List<RitualValue> rituals;
	@ElementList(entry="qualityval", type=QualityValue.class)
	protected List<QualityValue> qualities;
	protected transient List<QualityValue> racialQualities;
	@ElementList(entry="powerval", type=AdeptPowerValue.class)
	protected List<AdeptPowerValue> powers;
	protected transient List<AdeptPowerValue> autoPowers;
	@ElementList(entry="complexval", type=ComplexFormValue.class)
	protected List<ComplexFormValue> complexforms;
	@ElementList(entry="summonables", type=SummonableValue.class)
	protected List<SummonableValue> summonables;
	@ElementList(entry="metaecho", type=MetamagicOrEchoValue.class)
	protected List<MetamagicOrEchoValue> metaEchos;
	@Element
	private byte[] image;
	@Element
	private String hairColor, eyeColor, ethnicity, age;
	@Element
	private int size;
	@Element
	private int weight;
	/**
	 * Essence hole (multiplied with 1000)
	 */
	@Attribute(name="ess")
	private int essenceHole;
	@Element
	private Gender gender;
	@ElementList(entry="connection", type=Connection.class)
	protected List<Connection> connections;
	@Element
	protected CarriedItemList gear;
	protected transient CarriedItemList autoGear;
	@Element(name="autoaccess")
	@Deprecated
	protected transient CarriedItemList autoGearAccessories;
	@ElementList(entry="sin", type=SIN.class)
	protected List<SIN> sins;
	@ElementList(entry="lifestyle", type=LifestyleValue.class)
	protected List<LifestyleValue> lifestyles;
	@ElementList(entry="licenses", type=LicenseValue.class)
	protected List<LicenseValue> licenses;
	@ElementList(entry="focus", type=FocusValue.class)
	protected List<FocusValue> foci;
	@ElementList(entry="martialartref", type=MartialArtsValue.class)
	protected List<MartialArtsValue> martialArts;
	@ElementList(entry="techniqueref", type=TechniqueValue.class)
	protected List<TechniqueValue> techniques;
	@ElementList(entry="geardef", type=ItemTemplate.class)
	protected List<ItemTemplate> customGear;
	@ElementList(entry="maneuvers", type=SignatureManeuver.class)
	protected List<SignatureManeuver> maneuvers;

	@Element
	protected ModificationList history;
	@Element
	protected RewardList rewards;
	protected transient List<Sense> specialSenses;
	@Element
	protected String notes;
	@Element
	@MapConvert(keyConvert=UUIDConverter.class,valConvert=MappedModificationConverter.class)
	protected Map<UUID,Integer> decisions;
	/** source type | source id | memory (usually UUID) */
	@ElementList(entry="memorized", type=String.class)
	protected List<String> memories;
	@Element
	protected PluginMode pluginMode = PluginMode.LANGUAGE;
	@ElementList(entry="plugins", type=String.class)
	protected List<String> permittedPlugins;
	
	protected transient float unusedEssence;
	protected transient Persona persona;
	protected transient List<RelevanceModification> relevanceMods;
	protected transient List<EdgeModification> edgeMods;
	protected transient org.prelle.shadowrun6.Attribute traditionAttribute;

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunCharacter() {
		name = "Unnamed chummer";
		generationMode = false;
		gender    = Gender.MALE;
		attributes = new Attributes();
		skills  = new ArrayList<>();
		autoSkills  = new ArrayList<>();
		spells  = new ArrayList<>();
		rituals = new ArrayList<>();
		qualities = new ArrayList<>();
		racialQualities = new ArrayList<>();
		powers    = new ArrayList<>();
		autoPowers   = new ArrayList<>();
		complexforms = new ArrayList<>();
		connections  = new ArrayList<>();
		decisions  = new HashMap<>();
		memories   = new ArrayList<>();

		gear       = new CarriedItemList();
		autoGear   = new CarriedItemList();
		sins       = new ArrayList<>();
		lifestyles = new ArrayList<>();
		licenses   = new ArrayList<>();
		summonables = new ArrayList<>();
		metaEchos  = new ArrayList<>();
		foci       = new ArrayList<>();
		techniques = new ArrayList<TechniqueValue>();
		martialArts= new ArrayList<MartialArtsValue>();

		history = new ModificationList();
		rewards = new RewardList();
		specialSenses = new ArrayList<>();
		magicOrResonanceType = ShadowrunCore.getMagicOrResonanceType("mundane");
		relevanceMods = new ArrayList<>();
		edgeMods  = new ArrayList<>();
		autoGearAccessories = new CarriedItemList();
		permittedPlugins = new ArrayList<>();
		
		customSpells  = new ArrayList<>();
		customGear    = new ArrayList<>();
		maneuvers     = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void setName(String newName) {
		this.name = newName.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getImage()
	 */
	@Override
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the metatype
	 */
	public MetaType getMetatype() {
		return metatype;
	}

	//--------------------------------------------------------------------
	/**
	 * @param metatype the metatype to set
	 */
	public void setMetatype(MetaType metatype) {
		this.metatype = metatype;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaInvested
	 */
	public int getKarmaInvested() {
		return karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaInvested the karmaInvested to set
	 */
	public void setKarmaInvested(int karmaInvested) {
		this.karmaInvested = karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaFree
	 */
	public int getKarmaFree() {
		return karmaFree;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaFree the karmaFree to set
	 */
	public void setKarmaFree(int karmaFree) {
		this.karmaFree = karmaFree;
	}

	//-------------------------------------------------------------------
	public AttributeValue getAttribute(org.prelle.shadowrun6.Attribute key) {
		return attributes.get(key);
	}

	//-------------------------------------------------------------------
	public void ensureAttribute(org.prelle.shadowrun6.Attribute key) {
		if (!attributes.containsAttribute(key))
			attributes.add(new AttributeValue(key,0));
	}

	//-------------------------------------------------------------------
	public String dumpAttributes() {
		StringBuffer buf = new StringBuffer();
		for (org.prelle.shadowrun6.Attribute key : org.prelle.shadowrun6.Attribute.values()) {
			if (key==org.prelle.shadowrun6.Attribute.ATTACK)
				break;
		  buf.append(String.valueOf(attributes.get(key)+"\n"));
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public Collection<AttributeValue> getAttributes(org.prelle.shadowrun6.Attribute[] select) {
		List<AttributeValue> ret = new ArrayList<>();
		for (org.prelle.shadowrun6.Attribute attr : select)
			ret.add(attributes.get(attr));
		return ret;
	}

	//-------------------------------------------------------------------
	public void clearAutoSkills() {
		autoSkills.clear();
	}

	//-------------------------------------------------------------------
	public void addAutoSkill(SkillValue ref) {
		if (!autoSkills.contains(ref))
			autoSkills.add(ref);
	}

	//-------------------------------------------------------------------
	public boolean isAutoSkill(SkillValue check) {
		return autoSkills.contains(check);
	}

	//-------------------------------------------------------------------
	public void addSkill(SkillValue sVal) {
		if (sVal==null)
			throw new NullPointerException();
//		if (sVal.getModifyable().getId().equals("knowledge")) {
//			try {
//				throw new RuntimeException("Trace");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		if (!skills.contains(sVal))
			skills.add(sVal);
	}

	//-------------------------------------------------------------------
	public void removeSkill(SkillValue sVal) {
		skills.remove(sVal);
	}

	//-------------------------------------------------------------------
	/**
	 * Return a list of all skills with a value of at least 0.
	 * @param withGroups Unfold all selected skillgroups into their corresponding skills
	 * @return
	 */
	public List<SkillValue> getSkillValues() {
		ArrayList<SkillValue> ret = new ArrayList<>(skills);
		// Add auto skills
		ret.addAll(autoSkills);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Return a list of all skills with a value of at least 0.
	 * @param withGroups Unfold all selected skillgroups into their corresponding skills
	 * @return
	 */
	public List<SkillValue> getSkillValues(SkillType...skillTypes) {
		ArrayList<SkillValue> ret = new ArrayList<>(skills.stream().filter(skl -> Arrays.asList(skillTypes).contains(skl.getModifyable().getType())).collect(Collectors.toList()));
		ret.addAll(autoSkills.stream().filter(skl -> Arrays.asList(skillTypes).contains(skl.getModifyable().getType())).collect(Collectors.toList()));
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Return a list of all skills with a value of at least 0.
	 * @param withGroups Unfold all selected skillgroups into their corresponding skills
	 * @return
	 */
	public List<Skill> getSkills() {
		ArrayList<Skill> ret = new ArrayList<>();
		skills.forEach(sVal -> ret.add(sVal.getModifyable()));
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public SkillValue getSkillValue(Skill skill) {
		for (SkillValue tmp : getSkillValues()) {
			if (tmp.getModifyable()==skill)
				return tmp;
		}
		return null;
	}

//	//-------------------------------------------------------------------
//	public SkillValue getSkillValueExisting(Skill skill) {
//		for (SkillValue tmp : getSkillValues()) {
//			if (tmp.getModifyable()==skill)
//				return tmp;
//		}
//		return null;
//	}

//	//--------------------------------------------------------------------
//	/**
//	 * @param skill
//	 * @param useAttrib  Attribute to use 
//	 * @param special IDs of specializations to use (only use highest)
//	 * @return
//	 */
//	public int getSkillPool(Skill skill, org.prelle.shadowrun6.Attribute useAttrib, String...special) {
//		AttributeValue aVal = getAttribute(useAttrib);
//		SkillValue     sVal = getSkillValue(skill);
//		int skillValue;
//		if (sVal==null) {
//			/*
//			 *  Skill is untrained.
//			 *  If it cannot be used untrained, return 0 
//			 */
//			if (!skill.isUseUntrained())
//				return 0;
//			else
//				skillValue = -1;
//		} else {
//			skillValue = sVal.getModifiedValue();
//			SkillSpecializationValue bestSpec = null;
//			for (SkillSpecializationValue spec : sVal.getSkillSpecializations()) {
//				// Test if specializ. matches requested specs
//				if (!Arrays.asList(special).contains(spec.getSpecial().getId()))
//					continue;
//				if (bestSpec==null || spec.isExpertise())
//					bestSpec = spec;
//			}
//			if (bestSpec!=null) {
//				skillValue += (bestSpec.isExpertise())?3:2;
//			}
//		}
//		// Add attribute
//		return aVal.getModifiedValue() + skillValue;
//	}
//
//	//-------------------------------------------------------------------
//	public int getSkillPool(Skill skill, String...special) {
//		return getSkillPool(skill, skill.getAttribute1(), special);
//	}
//
//	//-------------------------------------------------------------------
//	private String getSkillPoolExplanationString(Skill skill) {
//		// Attribute
//		List<String> data = new ArrayList<>();
//		AttributeValue aVal = getAttribute(skill.getAttribute1());
//		data.add(ShadowrunTools.getModificationSourceString(aVal.getAttribute(), aVal.getPoints()));
//		data.addAll(aVal.getModifierExplanation());
//		// Skill rating
//		SkillValue     sVal = getSkillValue(skill);
//		data.add(ShadowrunTools.getModificationSourceString(skill, sVal.getPoints()));
//		data.addAll(sVal.getModifierExplanation());
//		
//		return String.join("\n", data);
//	}
//
//	//-------------------------------------------------------------------
//	public List<String> getSkillPoolExplanation(Skill skill) {
//		// Attribute
//		List<String> data = new ArrayList<>();
//		AttributeValue aVal = getAttribute(skill.getAttribute1());
//		data.add(ShadowrunTools.getModificationSourceString(aVal.getAttribute(), aVal.getPoints()));
//		data.addAll(aVal.getModifierExplanation());
//		// Skill rating
//		SkillValue     sVal = getSkillValue(skill);
//		if (sVal!=null) {
//			data.add(ShadowrunTools.getModificationSourceString(skill, sVal.getPoints()));
//			data.addAll(sVal.getModifierExplanation());
//		} 
//		
//		return data;
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the magicOrResonanceType
	 */
	public MagicOrResonanceType getMagicOrResonanceType() {
		return magicOrResonanceType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param magicOrResonanceType the magicOrResonanceType to set
	 */
	public void setMagicOrResonanceType(MagicOrResonanceType magicOrResonanceType) {
		this.magicOrResonanceType = magicOrResonanceType;
	}

	//-------------------------------------------------------------------
	/**
	 * When using the SpellConverter on XML deserialization, the custom spells
	 * are not known at that point and thus replaced with a custom spell item.
	 * Replace those dummys with the real definitions
	 */
	public void replaceCustomSpellDummys() {
		for (SpellValue val : spells) {
			Spell spell = val.getModifyable();
			if (spell.getId().startsWith(SpellConverter.DUMMY_PREFIX)) {
				String key = spell.getId().substring(SpellConverter.DUMMY_PREFIX.length());
				Spell replacement = getSpellDefinition(key);
				if (replacement==null)
					throw new ReferenceException(ReferenceType.SPELL, key);
				val.setSpell(replacement);
			}
		}
		
		// Custom gear
		for (CarriedItem val : gear) {
			ItemTemplate spell = val.getItem();
			if (spell.getId().startsWith(ItemConverter.DUMMY_PREFIX)) {
				String key = spell.getId().substring(ItemConverter.DUMMY_PREFIX.length());
				ItemTemplate replacement = getGearDefinition(key);
				if (replacement==null)
					throw new ReferenceException(ReferenceType.ITEM, key);
				val.setItem(replacement);
			}
		}
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells() {
		ArrayList<SpellValue> ret = new ArrayList<>(spells);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells(boolean alchemistic) {
		ArrayList<SpellValue> ret = new ArrayList<>();
		for (SpellValue tmp : spells) {
			if (alchemistic == tmp.isAlchemistic())
				ret.add(tmp);
		}
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue ref) {
		if (!spells.contains(ref))
			spells.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSpell(SpellValue ref) {
		spells.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasSpell(String id) {
		for (SpellValue ref : spells) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public SpellValue getSpell(String id) {
		for (SpellValue ref : spells) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Spell "+id);
	}

	//-------------------------------------------------------------------
	public void addSpellDefinition(Spell ref) {
		if (!customSpells.contains(ref))
			customSpells.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSpellDefinition(Spell ref) {
		customSpells.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasSpellDefinition(String id) {
		for (Spell ref : customSpells) {
			if (ref.getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public Spell getSpellDefinition(String id) {
		for (Spell ref : customSpells) {
			System.err.println("Spell "+ref+" has category "+ref.getCategory());
			if (ref.getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Custom Spell "+id);
	}

	//-------------------------------------------------------------------
//	public List<Spell> getSpellDefinitions() {
//		return new ArrayList<Spell>(customSpells);
//	}

	//-------------------------------------------------------------------
	public List<RitualValue> getRituals() {
		ArrayList<RitualValue> ret = new ArrayList<>(rituals);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public RitualValue getRitual(String id) {
		for (RitualValue ref : rituals) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Ritual "+id);
	}

	//-------------------------------------------------------------------
	public void addRitual(RitualValue ref) {
		if (!rituals.contains(ref))
			rituals.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeRitual(RitualValue ref) {
		rituals.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasRitual(String id) {
		for (RitualValue ref : rituals) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getRacialQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(racialQualities);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(racialQualities);
		ret.addAll(qualities);
		// Sort them
//		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getUserSelectedQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(qualities);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void clearRacialQualities() {
		racialQualities.clear();
		for (QualityValue val : qualities) {
			val.clearValueModifications();
		}
	}

	//-------------------------------------------------------------------
	public boolean hasQuality(String id) {
		for (QualityValue ref : qualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public boolean hasRacialQuality(String id) {
		for (QualityValue ref : racialQualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addQuality(QualityValue ref) {
		if (!qualities.contains(ref))
			qualities.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeQuality(QualityValue ref) {
		qualities.remove(ref);
	}

	//-------------------------------------------------------------------
	public void addRacialQuality(QualityValue ref) {
		if (!racialQualities.contains(ref))
			racialQualities.add(ref);
	}

	//-------------------------------------------------------------------
	public QualityValue getQuality(String id) {
		for (QualityValue ref : getQualities()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
//		throw new NoSuchElementException("Quality "+id);
	}

	//-------------------------------------------------------------------
	public void addAutoAdeptPower(AdeptPowerValue ref) {
		if (!autoPowers.contains(ref))
			autoPowers.add(ref);
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getAutoAdeptPowers() {
		return new ArrayList<>(autoPowers);
	}

	//-------------------------------------------------------------------
	public void clearAutoAdeptPower() {
		autoPowers.clear();
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getAdeptPowers() {
		ArrayList<AdeptPowerValue> ret = new ArrayList<>(powers);
		ret.addAll(autoPowers);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getUserSelectedAdeptPowers() {
		ArrayList<AdeptPowerValue> ret = new ArrayList<>(powers);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasAdeptPower(String id) {
		for (AdeptPowerValue ref : getAdeptPowers()) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addAdeptPower(AdeptPowerValue ref) {
		if (!powers.contains(ref))
			powers.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeAdeptPower(AdeptPowerValue ref) {
		powers.remove(ref);
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue getAdeptPower(String id) {
		for (AdeptPowerValue ref : getAdeptPowers()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue getAdeptPower(String id, String choice) {
		for (AdeptPowerValue ref : getAdeptPowers()) {
			if (ref.getModifyable().getId().equals(id) && choice.equals(ref.getChoiceReference()))
				return ref;
		}
		throw new NoSuchElementException("AdeptPower "+id+" with choice "+choice);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the hairColor
	 */
	public String getHairColor() {
		return hairColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param hairColor the hairColor to set
	 */
	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the eyeColor
	 */
	public String getEyeColor() {
		return eyeColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param eyeColor the eyeColor to set
	 */
	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	//-------------------------------------------------------------------
	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	//-------------------------------------------------------------------
	public int getWeight() {
		return weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	//-------------------------------------------------------------------
	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realname;
	}

	//-------------------------------------------------------------------
	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realname = realName;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the concept
	 */
	public CharacterConcept getConcept() {
		return concept;
	}

	//--------------------------------------------------------------------
	/**
	 * @param concept the concept to set
	 */
	public void setConcept(CharacterConcept concept) {
		this.concept = concept;
	}

	//--------------------------------------------------------------------
	public int getPowerPoints() {
		if (magicOrResonanceType==null || !magicOrResonanceType.usesPowers()) 
			return 0;
		
		AttributeValue val = getAttribute(org.prelle.shadowrun6.Attribute.POWER_POINTS);
		if (val==null) {
			val = new AttributeValue(org.prelle.shadowrun6.Attribute.POWER_POINTS, 0);
			attributes.add(val);
		}
			
		return val.getModifiedValue();
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItemsAddedByUser() {
		ArrayList<CarriedItem> ret = new ArrayList<>(gear);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get a list of all items that are not embedded
	 * @param includeAutoGear Include gear given automatically - e.h. natural armor from metatype
	 */
	public List<CarriedItem> getItems(boolean includeAutoGear) {
		ArrayList<CarriedItem> ret = new ArrayList<>();
		for (CarriedItem item : gear) {
			// Exclude embedded items
			if (item.getEmbeddedIn()==null)
				ret.add(item);
		}
		
		if (includeAutoGear)
			ret.addAll(autoGear);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItemsRecursive(boolean includeAutoGear) {
		ArrayList<CarriedItem> ret = new ArrayList<>(gear);
		if (includeAutoGear)
			ret.addAll(autoGear);
		for (CarriedItem item : gear) {
			ret.addAll(item.getEffectiveAccessories());
		}
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItems(boolean includeAutoGear, ItemType... types) {
		final List<CarriedItem> ret = new ArrayList<>();
		final List<ItemType> valid = Arrays.asList(types);
		getItems(includeAutoGear).forEach(new Consumer<CarriedItem>() {
			public void accept(CarriedItem item) {
				if (valid.contains(item.getUsedAsType()))
					ret.add(item);
			}
		});
//		// Special for WEAPON - add cyber weapons
//		if (valid.contains(ItemType.WEAPON)) {
//			gear.forEach(new Consumer<CarriedItem>() {
//				public void accept(CarriedItem item) {
//					if (item.getWeaponData()!=null && !item.getWeaponData().isEmpty() && !ret.contains(item)) {
//						System.err.println("Also add "+item);
//						ret.add(item);
//					}
//				}
//			});
//		}
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItemsRecursive(boolean includeAutoGear, ItemType... types) {
		final List<CarriedItem> ret = new ArrayList<>();
		final List<ItemType> valid = Arrays.asList(types);
		getItems(includeAutoGear).forEach(new Consumer<CarriedItem>() {
			public void accept(CarriedItem item) {
				if (valid.contains(item.getUsedAsType())) {
					ret.add(item);
				} else if (valid.contains(ItemType.ARMOR) && item.getItem().getArmorData()!=null) {
					ret.add(item);
//				} else if (valid.contains(ItemType.WEAPON_SPECIAL) && item.getItem().getWeaponData()!=null) {
//					ret.add(item);
				}
				for (CarriedItem item2 : item.getEffectiveAccessories()) {
					if (valid.contains(item2.getUsedAsType()))
						ret.add(item2);
				}
			}
		});
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasItem(String id) {
		for (CarriedItem ref : getItems(true)) {
			if (ref.getItem().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addItem(CarriedItem ref) {
		if (getItem(ref.getUniqueId())!=null)
			return;
//		if (ref.getItem().getId().equals("hk_227")) {
//			try {
//				throw new RuntimeException("Trace - add to "+gear);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				System.exit(1);
//			}
//		}
//		if (!gear.contains(ref))
			gear.add(ref);
	}

	//-------------------------------------------------------------------
	public boolean removeItem(CarriedItem ref) {
		return gear.remove(ref);
	}

	//-------------------------------------------------------------------
	public CarriedItem getItem(String id) {
		for (CarriedItem ref : getItems(true)) {
			if (ref.getItem().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public CarriedItem getItem(UUID uuid) {
		for (CarriedItem ref : getItemsRecursive(true)) {
			if (ref.getUniqueId()==null) {
				LogManager.getLogger("shadowrun6").warn("Add missing UUID to item "+this);
				ref.setUniqueId(UUID.randomUUID());
			}
			if (ref.getUniqueId()!=null & ref.getUniqueId().equals(uuid))
				return ref;
		}
		return null;
//		throw new NoSuchElementException("Item with UUID "+uuid);
	}

	//-------------------------------------------------------------------
	public void clearAutoGear() {
		autoGear.clear();
	}

	//-------------------------------------------------------------------
	public void addAutoItem(CarriedItem ref) {
		if (!autoGear.contains(ref))
			autoGear.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeAutoItem(CarriedItem toRemove) {
		autoGear.remove(toRemove);
	}

	//-------------------------------------------------------------------
	public boolean isAutoItem(CarriedItem check) {
		return autoGear.contains(check);
	}

	//-------------------------------------------------------------------
	@Deprecated
	public void addAutoItemAccessory(CarriedItem ref) {
		if (!autoGearAccessories.contains(ref))
			autoGearAccessories.add(ref);
	}

	//-------------------------------------------------------------------
	@Deprecated
	public void removeAutoItemAccessories(CarriedItem toRemove) {
		autoGearAccessories.remove(toRemove);
	}

	//-------------------------------------------------------------------
	@Deprecated
	public List<CarriedItem> getAutoItemAccessoriesFor(UUID containerUUID) {
		return autoGearAccessories.stream().filter(item -> containerUUID.equals(item.getEmbeddedIn())).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	@Deprecated
	public List<CarriedItem> getAutoItemAccessories() {
		return new ArrayList<>(autoGearAccessories);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the nuyen
	 */
	public int getNuyen() {
		return nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @param nuyen the nuyen to set
	 */
	public void setNuyen(int nuyen) {
		this.nuyen = nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<ComplexFormValue> getComplexForms() {
		return complexforms;
	}

	//--------------------------------------------------------------------
	public void addComplexForm(ComplexFormValue val) {
		if (val.getModifyable().needsChoice() && val.getChoiceReference()==null)
			throw new IllegalArgumentException("Missing choice for complex form "+val.getModifyable().getId());
		complexforms.add(val);
	}

	//--------------------------------------------------------------------
	public void removeComplexForm(ComplexFormValue val) {
		complexforms.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<Connection> getConnections() {
		return connections;
	}

	//--------------------------------------------------------------------
	public void addConnection(Connection val) {
		connections.add(val);
	}

	//--------------------------------------------------------------------
	public void removeConnection(Connection val) {
		connections.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<SIN> getSINs() {
		return sins;
	}

	//--------------------------------------------------------------------
	public void addSIN(SIN val) {
		if (!sins.contains(val))
			sins.add(val);
	}

	//--------------------------------------------------------------------
	public void removeSIN(SIN val) {
		sins.remove(val);
	}

	//--------------------------------------------------------------------
	public SIN getSIN(UUID uuid) {
		for (SIN sin : sins) {
			if (sin.getUniqueId().equals(uuid))
				return sin;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LifestyleValue> getLifestyle() {
		return new ArrayList<>(lifestyles);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LifestyleValue> getLifestyles(SIN sin) {
		List<LifestyleValue> ret = new ArrayList<>();
		for (LifestyleValue tmp : lifestyles) {
			if (tmp.getSIN()!=null && tmp.getSIN().equals(sin.getUniqueId()))
				ret.add(tmp);
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public void addLifestyle(LifestyleValue val) {
		if (!lifestyles.contains(val))
			lifestyles.add(val);
	}

	//--------------------------------------------------------------------
	public void removeLifestyle(LifestyleValue val) {
		lifestyles.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses() {
		return new ArrayList<>(licenses);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses(SIN sin) {
		List<LicenseValue> ret = new ArrayList<>();
		for (LicenseValue tmp : licenses) {
			if (tmp.getSIN().equals(sin.getUniqueId()))
				ret.add(tmp);
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public void addLicense(LicenseValue val) {
		if (!licenses.contains(val))
			licenses.add(val);
	}

	//--------------------------------------------------------------------
	public void removeLicense(LicenseValue val) {
		licenses.remove(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the ethnicity
	 */
	public String getEthnicity() {
		return ethnicity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ethnicity the ethnicity to set
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	//--------------------------------------------------------------------
	public void addToHistory(Modification mod) {
		history.add(mod);
	}

	//--------------------------------------------------------------------
	public boolean removeFromHistory(Modification mod) {
		return history.remove(mod);
	}

	//--------------------------------------------------------------------
	public List<Modification> getHistory() {
		return new ArrayList<Modification>(history);
	}

	//--------------------------------------------------------------------
	public void addReward(Reward rew) {
		if (!rewards.contains(rew))
			rewards.add(rew);
	}

	//--------------------------------------------------------------------
	public boolean removeReward(Reward rew) {
		return rewards.remove(rew);
	}

	//--------------------------------------------------------------------
	public List<Reward> getRewards() {
		return new ArrayList<Reward>(rewards);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tradition
	 */
	public Tradition getTradition() {
		return tradition;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tradition the tradition to set
	 */
	public void setTradition(Tradition tradition) {
		this.tradition = tradition;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	//-------------------------------------------------------------------
	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the summonables
	 */
	public List<SummonableValue> getSummonables() {
		return summonables;
	}

	//--------------------------------------------------------------------
	public void addSummonable(SummonableValue toAdd) {
		summonables.add(toAdd);
	}

	//--------------------------------------------------------------------
	public void removeSummonable(SummonableValue toRem) {
		summonables.remove(toRem);
	}

	//--------------------------------------------------------------------
	public List<Sense> getSpecialSenses() {
		return specialSenses;
	}

	//--------------------------------------------------------------------
	public void setSpecialSenses(List<Sense> specialSenses) {
		this.specialSenses = specialSenses;
	}

	//-------------------------------------------------------------------
	public String getNotes() {
		return notes;
	}

	//-------------------------------------------------------------------
	public void setNotes(String txt) {
		this.notes = txt;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the aspectSkill
	 */
	public Skill getAspectSkill() {
		return aspectSkill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param aspectSkill the aspectSkill to set
	 */
	public void setAspectSkill(Skill aspectSkill) {
		this.aspectSkill = aspectSkill;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the initiateSubmersionLevel
	 */
	public int getInitiateSubmersionLevel() {
		int count = 0;
		for (MetamagicOrEchoValue ref : metaEchos) {
			if (ref.getModifyable().hasLevels())
				count+=ref.getLevel();
			else
				count++;
		}
		return count;
	}

	//-------------------------------------------------------------------
	public List<MetamagicOrEchoValue> getMetamagicOrEchoes() {
		ArrayList<MetamagicOrEchoValue> ret = new ArrayList<>(metaEchos);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasMetamagicOrEcho(String id) {
		for (MetamagicOrEchoValue ref : getMetamagicOrEchoes()) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addMetamagicOrEcho(MetamagicOrEchoValue ref) {
		if (!metaEchos.contains(ref))
			metaEchos.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeMetamagicOrEcho(MetamagicOrEchoValue ref) {
		metaEchos.remove(ref);
	}

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue getMetamagicOrEcho(String id) {
		for (MetamagicOrEchoValue ref : getMetamagicOrEchoes()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public List<FocusValue> getFoci() {
		ArrayList<FocusValue> ret = new ArrayList<>(foci);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addFocus(FocusValue ref) {
		if (!foci.contains(ref))
			foci.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeFocus(FocusValue ref) {
		foci.remove(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the generationMode
	 */
	public boolean isGenerationMode() {
		return generationMode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param generationMode the generationMode to set
	 */
	public void setGenerationMode(boolean generationMode) {
		this.generationMode = generationMode;
	}

	//-------------------------------------------------------------------
	public void setEssenceHole(int essenceHole) {
		this.essenceHole = essenceHole;
	}

	//-------------------------------------------------------------------
	public int getEssenceHole() {
		return essenceHole;
	}

	//-------------------------------------------------------------------
	public float getEssence() {
		return unusedEssence;
	}

	//-------------------------------------------------------------------
	public void setEssence(float unusedEssence) {
		this.unusedEssence = unusedEssence;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the debt
	 */
	public int getDebt() {
		return debt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param debt the debt to set
	 */
	public void setDebt(int debt) {
		this.debt = debt;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}

	//--------------------------------------------------------------------
	/**
	 * @param persona the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	//--------------------------------------------------------------------
	public void decide(ModificationChoice choice, Modification mod) {
		int idx = choice.indexOf(mod);
		if (idx<0)
			throw new IllegalArgumentException("Modification is not an option of choice");
		decisions.put(choice.getUUID(), choice.indexOf(mod));
	}

	//--------------------------------------------------------------------
	public Modification getDecision(ModificationChoice choice) {
		Integer idx = decisions.get(choice.getUUID());
		if (idx==null)
			return null;
		return choice.get(idx);
	}

	//-------------------------------------------------------------------
	public void clearRelevanceModifications() {
		relevanceMods.clear();
	}

	//-------------------------------------------------------------------
	public void addRelevanceModification(RelevanceModification mod) {
		relevanceMods.add(mod);
	}

	//-------------------------------------------------------------------
	public List<RelevanceModification> getRelevanceModifications(RelevanceType type) {
		return relevanceMods.stream().filter( mod -> (mod.getType()==type)).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	public void clearEdgeModifications() {
		edgeMods.clear();
	}

	//-------------------------------------------------------------------
	public void addEdgeModification(EdgeModification mod) {
		edgeMods.add(mod);
	}

	//-------------------------------------------------------------------
	public List<EdgeModification> getEdgeModifications(EdgeModification.ModificationType... types) {
		return edgeMods.stream().filter( mod -> List.of(types).contains(mod.getType())).collect(Collectors.toList());
	}
	
	//---------------------------------------------------------
	public int getCostOfAction(ShadowrunAction action) {
		int cost = action.getCost();
		for (EdgeModification mod : edgeMods) {
			if (mod.getAction()==action && mod.getType()==ModificationType.COST) {
				if (mod.getValue()<0)
					cost += mod.getValue();
				else
					cost = mod.getValue();
			}
		}
		if (cost<1)
			cost=1;
		return cost;
	}
	
	//---------------------------------------------------------
	public boolean isCostModified(ShadowrunAction action) {
		for (EdgeModification mod : edgeMods) {
			if (mod.getAction()==action && mod.getType()==ModificationType.COST) {
				return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenUsed
	 */
	public String getChargenUsed() {
		return chargenUsed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenUsed the chargenUsed to set
	 */
	public void setChargenUsed(String chargenUsed) {
		this.chargenUsed = chargenUsed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenSettings
	 */
	public String getChargenSettings() {
		return chargenSettings;
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenSettings the chargenSettings to set
	 */
	public void setChargenSettings(String chargenSettings) {
		this.chargenSettings = chargenSettings;
	}

	//-------------------------------------------------------------------
	public void setTemporaryChargenSettings(Object settings) {
		this.chargenSettingsObject = settings;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T> T getTemporaryChargenSettings(Class<T> cls) {
		return (T)chargenSettingsObject;
	}

	//-------------------------------------------------------------------
	public void memorize(UUID key, String value) {
		memories.add(key+"|"+value);
	}

	//-------------------------------------------------------------------
	public void forget(UUID key) {
		String search = key+"|";
		for (String mem : new ArrayList<>(memories)) {
			if (mem.startsWith(search))
				memories.remove(mem);
		}
	}

	//-------------------------------------------------------------------
	public String getFromMemory(UUID key) {
		String search = key+"|";
		for (String mem : memories) {
			if (mem.startsWith(search))
				return mem.substring(search.length());
		}
		return null;
	}

	//-------------------------------------------------------------------
	public List<MartialArtsValue> getMartialArts() {
		ArrayList<MartialArtsValue> ret = new ArrayList<>(martialArts);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addMartialArt(MartialArtsValue ref) {
		if (!martialArts.contains(ref))
			martialArts.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeMartialArt(MartialArtsValue ref) {
		martialArts.remove(ref);
		// Remove all techniques belonging to that martial art
		for (TechniqueValue tech : getTechniques()) {
			if (tech.getMartialArt()==ref.getMartialArt())
				removeTechnique(tech);
		}
	}

	//-------------------------------------------------------------------
	public boolean hasTechnique(Technique ref) {
		for (TechniqueValue tech : techniques) {
			if (ref==tech.getTechnique())
				return true;
		}
		// Is it a key technique of a style?
		for (MartialArtsValue style : martialArts) {
			if (style.getMartialArt().getSignatureTechnique()==ref)
				return true;
		}
		
		return false;
	}

	//-------------------------------------------------------------------
	public List<TechniqueValue> getTechniques() {
		ArrayList<TechniqueValue> ret = new ArrayList<>(techniques);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<TechniqueValue> getTechniquesAll() {
		ArrayList<TechniqueValue> ret = new ArrayList<>(techniques);
		// Add signature techniques
		for (MartialArtsValue style : martialArts) {
			ret.add(new TechniqueValue(style.getMartialArt().getSignatureTechnique(), style.getMartialArt()));
		}
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<TechniqueValue> getTechniques(MartialArts learnedIn) {
		ArrayList<TechniqueValue> ret = new ArrayList<>();
		ret.add(new TechniqueValue(learnedIn.getSignatureTechnique(), learnedIn));
		for (TechniqueValue tech : techniques) {
			if (tech.getMartialArt()==learnedIn)
				ret.add(tech);
		}
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addTechnique(TechniqueValue ref) {
		if (!techniques.contains(ref))
			techniques.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeTechnique(TechniqueValue ref) {
		techniques.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<String> getPermittedPlugins() {
		return permittedPlugins;
	}

	//-------------------------------------------------------------------
	public void setPermittedPlugins(List<String> value) {
		if (value==null) 
			value = new ArrayList<String>();
		this.permittedPlugins = value;
	}

	//-------------------------------------------------------------------
	public void addPermittedPlugin(String pluginId){
		if (permittedPlugins==null) {
			permittedPlugins = new ArrayList<String>();
		}
		if (!permittedPlugins.contains(pluginId)){
			permittedPlugins.add(pluginId);
		}
	}

	//-------------------------------------------------------------------
	public void removePermittedPlugin(String pluginId){
		if (permittedPlugins==null) {
			return;
		}
		permittedPlugins.remove(pluginId);
	}

	//-------------------------------------------------------------------
	public boolean isPluginPermitted(String pluginId){
		if (permittedPlugins==null) {
			return true;
		}
		return permittedPlugins.contains(pluginId);
	}

	//-------------------------------------------------------------------
	public boolean isExplicitPluginPermitted(String pluginId){
		if (permittedPlugins==null) {
			return false;
		}
		return permittedPlugins.contains(pluginId);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the pluginMode
	 */
	public PluginMode getPluginMode() {
		return pluginMode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param pluginMode the pluginMode to set
	 */
	public void setPluginMode(PluginMode pluginMode) {
		this.pluginMode = pluginMode;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the traditionAttribute
	 */
	public org.prelle.shadowrun6.Attribute getTraditionAttribute() {
		return traditionAttribute;
	}

	//-------------------------------------------------------------------
	/**
	 * @param traditionAttribute the traditionAttribute to set
	 */
	public void setTraditionAttribute(org.prelle.shadowrun6.Attribute traditionAttribute) {
		this.traditionAttribute = traditionAttribute;
	}

	//-------------------------------------------------------------------
	public void addGearDefinition(ItemTemplate ref) {
		if (!customGear.contains(ref))
			customGear.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeGearDefinition(ItemTemplate ref) {
		customGear.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasGearDefinition(String id) {
		for (ItemTemplate ref : customGear) {
			if (ref.getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public ItemTemplate getGearDefinition(String id) {
		for (ItemTemplate ref : customGear) {
			if (ref.getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Custom Item "+id);
	}

	//-------------------------------------------------------------------
//	public List<ItemTemplate> getGearDefinitions() {
//		return new ArrayList<ItemTemplate>(customGear);
//	}

	//-------------------------------------------------------------------
	public void addSignatureManeuver(SignatureManeuver ref) {
		if (!maneuvers.contains(ref))
			maneuvers.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSignatureManeuver(SignatureManeuver ref) {
		maneuvers.remove(ref);
	}

	//-------------------------------------------------------------------
	public List<SignatureManeuver> getSignatureManeuvers() {
		return new ArrayList<SignatureManeuver>(maneuvers);
	}
	
}
