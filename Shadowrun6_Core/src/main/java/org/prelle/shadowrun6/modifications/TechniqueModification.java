package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.persist.MartialArtsConverter;
import org.prelle.shadowrun6.persist.TechniqueConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class TechniqueModification implements Modification {
	
	@org.prelle.simplepersist.Attribute
	protected int expCost;
	@org.prelle.simplepersist.Attribute
	protected Date date;
	protected transient Object source;

	@Attribute
	@AttribConvert(TechniqueConverter.class)
	private Technique ref;
	@Attribute
	@AttribConvert(MartialArtsConverter.class)
	private MartialArts learnedIn;
	@Attribute
	private boolean removed;
    
    //-----------------------------------------------------------------------
    public TechniqueModification() {
    }
    
    //-----------------------------------------------------------------------
    public TechniqueModification(Technique ref) {
        this();
        this.ref = ref;
    }
    
    //-----------------------------------------------------------------------
    public TechniqueModification(Technique ref, Object src) {
    	this(ref);
    	this.source = src;
    }
   
    //-----------------------------------------------------------------------
    public TechniqueModification(Technique ref, Date date, int expCost) {
        this(ref);
        this.date = date;
        this.expCost = expCost;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
         if (ref==null)
        	return "MartialArtsStyle_NOT_SET";
        return ref.getName();
     }
    
    //-----------------------------------------------------------------------
    public Technique getTechnique() {
        return ref;
    }
    
    //-----------------------------------------------------------------------
    public void setTechnique(Technique val) {
        this.ref = val;
    }
    
//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof TechniqueModification) {
            TechniqueModification amod = (TechniqueModification)o;
             return amod.getTechnique()==ref;
        } else
            return false;
    }

	//-------------------------------------------------------------------
	public TechniqueModification clone() {
    	try {
    		return (TechniqueModification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		return date.compareTo(other.getDate());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {		
		return expCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
		this.expCost = expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the learnedIn
	 */
	public MartialArts getLearnedIn() {
		return learnedIn;
	}

	//-------------------------------------------------------------------
	/**
	 * @param learnedIn the learnedIn to set
	 */
	public void setLearnedIn(MartialArts learnedIn) {
		this.learnedIn = learnedIn;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the removed
	 */
	public boolean isRemoved() {
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param removed the removed to set
	 */
	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

   
}
