/**
 * 
 */
package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class OptionModification implements Modification {
	
	public enum Type {
		NOT_SET,
		SKILL,
		MATRIX_ACTION,
		ATTRIBUTE,
	}
	
	@Attribute
	private Type type;
	private transient Object source;

	//-------------------------------------------------------------------
	public OptionModification() {
		type = Type.NOT_SET;
	}

	//-------------------------------------------------------------------
	public OptionModification(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	public String toString() {
		switch (type) {
		case SKILL: return "Select a skill";
		case ATTRIBUTE: return "Select an attribute";
		case MATRIX_ACTION: return "Select a matrix action";
		case NOT_SET:
		default:
			return "Select something";
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new OptionModification(type);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
