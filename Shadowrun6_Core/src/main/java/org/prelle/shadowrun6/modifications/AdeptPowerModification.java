package org.prelle.shadowrun6.modifications;

import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.persist.AdeptPowerConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class AdeptPowerModification extends ModificationBase<AdeptPower> {

	@Attribute
	@AttribConvert(AdeptPowerConverter.class)
	private AdeptPower ref;
	@Attribute
	private int value;
	@Attribute
	private String choice;
    
    //-----------------------------------------------------------------------
    public AdeptPowerModification() {
    }
    
    //-----------------------------------------------------------------------
    public AdeptPowerModification(AdeptPower ref, int val) {
    	if (ref==null) throw new NullPointerException("Power may not be null");
    	this.ref = ref;
        this.value = val;
    }
    
    //-----------------------------------------------------------------------
    public AdeptPowerModification(AdeptPower ref, int val, String choice) {
    	if (ref==null) throw new NullPointerException("Power may not be null");
    	this.ref = ref;
        this.value = val;
        this.choice = choice;
    }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public ValueModification<AdeptPower> clone() {
		AdeptPowerModification cloned = new AdeptPowerModification(ref, value, choice);
		cloned.setDate(date);
		cloned.expCost = expCost;
		return cloned;
	}
    
    //-----------------------------------------------------------------------
    public String toString() {
          if (ref!=null) {
        	  if (ref.needsChoice())
        		  return ref.getId()+"("+choice+") +"+value;
        	 return ref.getId()+" +"+value;
          }
         return "Any adept power +"+value;
    }
    
    //-----------------------------------------------------------------------
    public AdeptPower getPower() {
        return ref;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AdeptPowerModification) {
        	AdeptPowerModification amod = (AdeptPowerModification)o;
            if (amod.getPower()!=ref)
            	return false;
            if (ref.needsChoice() && !amod.getChoice().equals(choice))
            	return false;
             return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public AdeptPower getModifiedItem() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public String getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(String choice) {
		this.choice = choice;
	}
    
}// AttributeModification
