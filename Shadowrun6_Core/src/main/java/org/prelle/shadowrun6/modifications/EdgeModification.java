package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.persist.ActionConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class EdgeModification implements Modification {

	public enum ModificationType {
		COST,
		BONUS,
		USAGE,
	}
	public enum ResistDamageType {
		PHYSICAL,
		STUN,
		BOTH,
		ANTIMAGIC,
	}

	@Attribute(name="stype")
	private SkillType skillType;
	@Attribute(name="val")
	private int value;
	@Attribute
	private ModificationType type;
	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute(name="cat")
	private Spell.Category spellCategory;
	@Attribute
	private ResistDamageType resist;
	@Attribute(name="attr")
	private org.prelle.shadowrun6.Attribute attrib;
	@Attribute(name="action")
	@AttribConvert(ActionConverter.class)
	private ShadowrunAction action;
	protected transient Object source;

    //-----------------------------------------------------------------------
    public EdgeModification() {
    }

	//-------------------------------------------------------------------
 	public EdgeModification clone() {
    	try {
    		return (EdgeModification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof EdgeModification) {
        	EdgeModification amod = (EdgeModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getSkill()    !=skill) return false;
            if (amod.getAttribute()!=attrib) return false;
           if (amod.getSource()   !=source) return false;
            return true;
        } else
            return false;
    }

	//-------------------------------------------------------------------
	public String toString() {
		if (skill!=null) {
//			switch (type) {
//			case COST: return "EdgeMod(COST, skill="+skill+""
//			}
			return "EdgeMod(skill="+skill+","+type+",src="+source+")";
		}
		if (skillType!=null)
			return "EdgeMod(skillType="+skillType+","+type+",src="+source+")";
		if (spellCategory!=null)
			return "EdgeMod(spellCategory="+spellCategory+","+type+",src="+source+")";
		if (resist!=null)
			return "EdgeMod(resist="+resist+","+type+",src="+source+")";
		return "EdgeMod("+type+",src="+source+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skillType
	 */
	public SkillType getSkillType() {
		return skillType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skillType the skillType to set
	 */
	public void setSkillType(SkillType skillType) {
		this.skillType = skillType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ModificationType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ModificationType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
		// TODO Auto-generated method stub
		
	}

	//-------------------------------------------------------------------
	/**
	 * @return the spellCategory
	 */
	public Spell.Category getSpellCategory() {
		return spellCategory;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the resist
	 */
	public ResistDamageType getResist() {
		return resist;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attrib
	 */
	public org.prelle.shadowrun6.Attribute getAttribute() {
		return attrib;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the action
	 */
	public ShadowrunAction getAction() {
		return action;
	}
	
}// EdgeModification
