package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AllowModification implements Modification {

	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@org.prelle.simplepersist.Attribute
	protected int expCost;
	@org.prelle.simplepersist.Attribute
	protected Date date;
	protected transient Object source;
   
    //-----------------------------------------------------------------------
    public AllowModification() {
        skill = null;
    }
    
    //-----------------------------------------------------------------------
    public AllowModification(Skill type) {
        this();
        this.skill = type;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
          return "Allow skill "+skill;
    }

	//-------------------------------------------------------------------
 	public AllowModification clone() {
    	try {
    		return (AllowModification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//-------------------------------------------------------------------
	public Skill getSkill() {
		return skill;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {		
		return expCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
		this.expCost = expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}
   
}
