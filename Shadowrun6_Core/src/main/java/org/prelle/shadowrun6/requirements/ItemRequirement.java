/**
 * 
 */
package org.prelle.shadowrun6.requirements;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="itemreq")
public class ItemRequirement extends Requirement {
	
	@Attribute
	private String item;
	@Attribute(name="char")
	private boolean mustExistInCharacter;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemRequirement() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public String getItemID() {
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the mustExistInCharacter
	 */
	public boolean isMustExistInCharacter() {
		return mustExistInCharacter;
	}

}
