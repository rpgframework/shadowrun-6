/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author prelle
 *
 */
public class MentorSpirit extends BasePluginData implements Comparable<MentorSpirit>, Modifyable {

	@Attribute
	private String id;
	@Element
	private ModificationList modifications;
	@Element(name="magician")
	private ModificationList magician;
	@Element(name="adept")
	private ModificationList adept;

	//-------------------------------------------------------------------
	/**
	 */
	public MentorSpirit() {
		magician = new ModificationList();
		adept    = new ModificationList();
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MentorSpirit o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null) 
			return id;
		try {
			return i18n.getString("mentorspirit."+id);
		} catch (MissingResourceException e) {
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
			logger.error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "mentorspirit."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "mentorspirit."+id+".desc";
	}

	@Override
	public List<Modification> getModifications() {
		return modifications;
	}

	@Override
	public void setModifications(List<Modification> mods) {
		this.modifications = new ModificationList(mods);		
	}

	@Override
	public void addModification(Modification mod) {
		modifications.add(mod);
	}

	@Override
	public void removeModification(Modification mod) {
		modifications.remove(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the magician
	 */
	public ModificationList getMagicianModifications() {
		return magician;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the adept
	 */
	public ModificationList getAdeptModifications() {
		return adept;
	}

}
