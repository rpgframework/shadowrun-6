/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CapacityDefinitions;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.modifications.AccessoryModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyAccessoryModification implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	private String prefix;
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		prefix = indent+model.getItem().getId()+": ";
		List<Modification> unprocessed = new ArrayList<>();

		try {
			for (Modification tmp : previous) {
				if (tmp instanceof AccessoryModification) {
					AccessoryModification mod = (AccessoryModification)tmp;
					CarriedItem accessory = model.getMemorizedAccessory(mod);
					if (accessory==null) {
						if (mod.getItem()==null) {
							logger.error("No item defined in accessorymod: "+mod+" for slot "+mod.getHook()+" from "+mod.getSource());
							continue;
						}
							accessory = new CarriedItem(mod.getItem(), mod.getRating());
							accessory.setCreatedByModification(true);
							accessory.setSlot(mod.getHook());
							accessory.setEmbeddedIn(model.getUniqueId());
							if (mod.isIncluded()) {
								accessory.setIgnoredForCalculations(true);
							}
							if (mod.getAmount()>0) {
								System.err.println("ApplyAccessoryModification: amount="+mod.getAmount());
								accessory.setCount(mod.getAmount());
							}
								
							ItemRecalculation.recalculate(indent+"   ", accessory);
							model.memorizeAccessory(mod, accessory);
							/*
							 * Also save the UUID of the accessory, if the accessory can
							 * contain accessories itself
							 */
							if (accessory.getSlots().size()>0) {
								if (!model.getGeneratedUUIDs().isEmpty()) {
									// Walk through all stored generated UUIDs until you find the matching one
									boolean notFound = true;
									for (String keyVal : model.getGeneratedUUIDs()) {
										String[] idAndUUID = keyVal.split("\\|");
										if (idAndUUID[0].equals(mod.getItem().getId())) {
											logger.info("Replace generated UUID "+accessory.getUniqueId()+" with memorized "+idAndUUID[1]);
											accessory.setUniqueId(UUID.fromString(idAndUUID[1]));
											notFound=false;
											break;
										}
									}
									if (notFound) {
										logger.warn("Did not find previously generated UUID for item "+mod.getItem().getId());
									}
								} else {
									// Not UUIDs yet - add them
									model.addGeneratedUUIDs(mod.getItem().getId()+"|"+accessory.getUniqueId());
									logger.warn("Fix missing generatedUUID with "+accessory.getUniqueId()+"/"+accessory.getItem()+" - if you had items embedded in your "+mod.getItem().getName()+", they are lost");
								}
							}
					}
					// If item attribute modifications are already included, mark them
					if (mod.isIncluded()) {
						accessory.setIgnoredForCalculations(true);
						// Mark modifications as already included
						for (Modification tmp2 : accessory.getModifications()) {
							if (tmp2 instanceof ItemAttributeModification) {
								((ItemAttributeModification)tmp2).setIncluded(true);
							}
						}
					}
					if (model.hasHook(mod.getHook())) {
						// Accessory added to existing slot
						switch ( (int)accessory.getCapacity(mod.getHook())) {
						case (int)CapacityDefinitions.BODY_DIV_3:
							float cookedCap = (float) Math.round( (float)model.getAsValue(ItemAttribute.BODY).getModifiedValue() / 3.0f);
							logger.debug(prefix+"Add calculated CAPACITY to Body/3 = "+cookedCap);
							accessory.setAttributeOverride(ItemAttribute.CAPACITY, cookedCap);
//							accessory.setAttributeOverride(ItemAttribute.CAPACITY, 0f);
							break;
//						case (int)CapacityDefinitions.BODY_DIV_2:
//						case (int)CapacityDefinitions.ARMOR_DIV_4:
//							accessory.setAttributeOverride(ItemAttribute.CAPACITY, 0f);
//							break;
						}
						
						model.getSlot(mod.getHook()).addEmbeddedItem(accessory, mod.isIncluded(), model);
						if (logger.isInfoEnabled())
							logger.info(prefix+"embedded item "+mod.getItem()+" in existing slot "+accessory.getSlot()+"  - ignore it's modifications = "+mod.isIncluded());
					} else {
						// Accessory add to not existing slot
						logger.warn("Slot "+mod.getHook()+" does not exist -  cannot add "+mod.getItem());
						if (mod.isIncluded()) {
							// Is a stock accessory - it's modifications are usually included
							// in the item stats and must now be removed
							model.addToDo(new ToDoElement(Severity.INFO, 
									Resource.format(ShadowrunCore.getI18nResources(), "todos.carrieditem.slot_removed_by_enhancement", 
											mod.getHook().getName(),
											String.valueOf(tmp.getSource()),
											mod.getItem().getName())));
							for (Modification mod2 : mod.getItem().getModifications()) {
								undoFromVirtual(model, mod2);
							}
						}
						logger.info(prefix+"remove slot "+mod.getHook());
						model.removeHook(mod.getHook());
						
//						// To be more robust against errors in data, create the slot (without capacity)
//						AvailableSlot toAdd = mod.getHook().hasCapacity()?(new AvailableSlot(mod.getHook(),0)):(new AvailableSlot(mod.getHook()));
//						toAdd.addEmbeddedItem(accessory);
//						model.addHook(toAdd);
//						if (logger.isInfoEnabled())
//							logger.info(prefix+"embedded item "+mod.getItem()+" in previously not existing slot "+accessory.getSlot()+"  - ignore it's modifications = "+mod.isIncluded());
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			
		}
		return unprocessed;
	}
	
	//--------------------------------------------------------------------
	private void undoFromVirtual(CarriedItem model, Modification tmp) {
		if (tmp instanceof ItemAttributeModification) {
			ItemAttributeModification mod = (ItemAttributeModification)tmp;
			logger.info(prefix+"Undo "+mod.getAttribute()+" = "+mod.getValue()+" or "+mod.getObjectValue());
			switch (mod.getAttribute()) {
			case ATTACK_RATING:
				int[] moddedAR = (int[]) model.getAsObject(ItemAttribute.ATTACK_RATING).getValue();
				for (int i=0; i<moddedAR.length; i++) {
					int toSub = (mod.getObjectValue()!=null)?((int[])mod.getObjectValue())[i]:mod.getValue();
					moddedAR[i] = Math.max(0, moddedAR[i]-toSub);
				}
				// No need to set again - copied by reference
				logger.info(prefix+"Changed AR: "+Arrays.toString(moddedAR));
				break;
			default:
				logger.error(prefix+"Don't know how to undo modifications for "+mod.getAttribute());
			}
		} else {
			logger.error(prefix+"Don't know how to undo modification "+tmp.getClass()+" = "+tmp);
		}
	}

}
