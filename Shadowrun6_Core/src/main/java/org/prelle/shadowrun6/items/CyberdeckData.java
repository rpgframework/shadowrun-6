/**
 * 
 */
package org.prelle.shadowrun6.items;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class CyberdeckData {

	@Attribute(name="a")
	private int attack;
	@Attribute(name="s")
	private int sleaze;
	@Attribute(name="d")
	private int dataProcessing;
	@Attribute(name="f")
	private int firewall;
	@Attribute
	private int programs;
	@Attribute(name="ini")
	private int matrixIniBonus;

	//-------------------------------------------------------------------
	/**
	 * @return the programs
	 */
	public int getPrograms() {
		return programs;
	}

	//-------------------------------------------------------------------
	/**
	 * @param programs the programs to set
	 */
	public void setPrograms(int programs) {
		this.programs = programs;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attack
	 */
	public int getAttack() {
		return attack;
	}

	//-------------------------------------------------------------------
	/**
	 * @param attack the attack to set
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sleaze
	 */
	public int getSleaze() {
		return sleaze;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sleaze the sleaze to set
	 */
	public void setSleaze(int sleaze) {
		this.sleaze = sleaze;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dataProcessing
	 */
	public int getDataProcessing() {
		return dataProcessing;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dataProcessing the dataProcessing to set
	 */
	public void setDataProcessing(int dataProcessing) {
		this.dataProcessing = dataProcessing;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the firewall
	 */
	public int getFirewall() {
		return firewall;
	}

	//-------------------------------------------------------------------
	/**
	 * @param firewall the firewall to set
	 */
	public void setFirewall(int firewall) {
		this.firewall = firewall;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the matrixIniBonus
	 */
	public int getMatrixIniBonus() {
		return matrixIniBonus;
	}

	//-------------------------------------------------------------------
	/**
	 * @param matrixIniBonus the matrixIniBonus to set
	 */
	public void setMatrixIniBonus(int matrixIniBonus) {
		this.matrixIniBonus = matrixIniBonus;
	}

}
