package org.prelle.shadowrun6.items;

import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

public abstract class ItemAttributeValue extends ModifyableImpl {

	protected ItemAttribute attribute;

	//--------------------------------------------------------------------
	public ItemAttributeValue(ItemAttribute attr, List<Modification> mods) {
		this.attribute = attr;
		super.modifications = mods;
	}

	//--------------------------------------------------------------------
	public ItemAttribute getModifyable() {
		return attribute;
	}

}
