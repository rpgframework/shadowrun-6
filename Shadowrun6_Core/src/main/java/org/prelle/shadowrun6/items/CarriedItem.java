package org.prelle.shadowrun6.items;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PropertyResourceBundle;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.UniqueObject;
import org.prelle.shadowrun6.items.ItemTemplate.Multiply;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;
import org.prelle.shadowrun6.modifications.AccessoryModification;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.CarriedItemModification;
import org.prelle.shadowrun6.modifications.DamageTypeModification;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.HasOptionalCondition;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemEnhancementModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.modifications.ModificationBase.ModificationType;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.persist.ItemConverter;
import org.prelle.shadowrun6.requirements.ItemHookRequirement;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

@Root(name = "itemref")
public class CarriedItem extends UniqueObject implements Comparable<CarriedItem> {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items");

	private static PropertyResourceBundle UI = ShadowrunCore.getI18nResources();

	@Attribute(name="ref",required=true)
	@AttribConvert(ItemConverter.class)
	private ItemTemplate ref;
	@Attribute
	private int rating;
	@Attribute
	private String customName;
	@Attribute(name="count",required=false)
	private int count = 1;
	@Attribute
	private ItemLocationType location;
	/**
	 * Nuyen the character invested here. May be 0 for loot
	 */
	@org.prelle.simplepersist.Attribute(name="price",required=false)
	private Integer price = null;
	@Attribute(name="qual")
	private BodytechQuality quality = BodytechQuality.STANDARD;
	@Attribute(name="slot")
	private ItemHook slot;
	@Attribute(name="type")
	private ItemType usedAsType;
	@Attribute(name="subtype")
	private ItemSubType usedAsSubType;
	@Attribute(name="choice")
	private String choiceReference;
	private transient Object choice;
	private boolean cheapKnockOff;

	/** User added accessories */
	@ElementList(entry="item",type=CarriedItem.class)
	private List<CarriedItem> accessories;
	@ElementList(entry="enhancement",type=ItemEnhancementValue.class)
	private List<ItemEnhancementValue> enhancements;
	
	/*
	 * Relevant for auto-added ites
	 */
	/** UUIDs of auto items to create from this instance */
	@ElementList(entry="uuid",type=String.class,required = false)
	private List<String> generatedUUIDs;
	
	@Attribute(name="embedin")
	private UUID embeddedIn;
	@Element
	private String notes;
	@ElementList(entry="id",type=String.class)
	private List<String> conditions = new ArrayList<String>();
	@Element
	private byte[] image;
	@Element
	private Boolean primary;
	
	/** Calculated version of the item, all modifications regarded */
	private transient Map<ItemAttribute, ItemAttributeValue> attributes;
	private transient Map<ItemHook, AvailableSlot> slots;
	/** Survives ItemRecalculate, but is not used for detection */
	private transient Map<ItemHook, AvailableSlot> slotCache;
	/** Item attributes that have been externally overwritten. */
	private transient Map<ItemAttribute, Object> overwrittenBaseAttributes;
	private transient boolean createdByModification;
	private transient List<Requirement> requirements;
	private transient List<ToDoElement> todos;
	private transient List<Modification> autoMods;
	private transient FocusValue usedFocus;
	private transient MetamagicOrEchoValue attunement;
	
	/** Not used e.g. for defensive rating calculation */
	private transient boolean ignoredForCalculations;
	
	private transient Map<AccessoryModification, CarriedItem> memorizedGeneratedAccessories;
	private transient boolean dirty;
	private transient boolean recalculating;
	private transient List<ItemEnhancementValue> autoEnhancements;

	//--------------------------------------------------------------------
	public CarriedItem() {
		dirty=true;
		modifications = new ArrayList<>();
		count = 1;
		accessories   = new CarriedItemList();
		
		attributes    = new HashMap<>();
		slots         = new HashMap<>();
		slotCache     = new HashMap<>();
		overwrittenBaseAttributes = new HashMap<>();
		enhancements  = new ArrayList<>();
		requirements  = new ArrayList<>();
		todos         = new ArrayList<>();
		autoMods      = new ArrayList<>();
		generatedUUIDs= new ArrayList<>();
		memorizedGeneratedAccessories = new HashMap<>();
		autoEnhancements= new ArrayList<ItemEnhancementValue>();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template) {
		this();
		ref = template;
		this.rating= 0;
		this.dirty = true;
		refreshVirtual();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template, int rating) {
		this();
		if (template==null)
			throw new NullPointerException("ItemTemplate may not be null");
		ref = template;
		this.rating= rating;
		this.dirty = true;
		refreshVirtual();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template, UseAs usage) {
		this();
		ref = template;
		this.usedAsType = usage.getType();
		this.usedAsSubType = usage.getSubtype();
		this.slot  = usage.getSlot();
		this.rating= 0;
		this.dirty = true;
		refreshVirtual();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template, UseAs usage, int rating) {
		this();
		ref = template;
		this.usedAsType = usage.getType();
		this.usedAsSubType = usage.getSubtype();
		this.slot  = usage.getSlot();
		this.rating= rating;
		this.dirty = true;
		refreshVirtual();
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof CarriedItem) {
			CarriedItem other = (CarriedItem)o;
			if (getUniqueId()!=null && getUniqueId().equals(other.getUniqueId())) return true;
			
			if (ref!=other.getItem()) return false;
			//			if (location!=other.getLocation()) return false;
			if (customName!=null && !customName.equals(other.getName())) return false;
			if (choiceReference!=null && !choiceReference.equals(other.getChoiceReference())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void setAttributeOverride(ItemAttribute attr, Object value) {
		overwrittenBaseAttributes.put(attr, value);
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	public void clearAttributeOverride(ItemAttribute attr) {
		overwrittenBaseAttributes.remove(attr);
	}

	//-------------------------------------------------------------------
	public void clearAutoModifications() {
		autoMods.clear();
		dirty = true;
	}

	//-------------------------------------------------------------------
	public void addAutoModification(ItemAttributeModification mod) {
		autoMods.add(mod);
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	public List<Modification> getAutoModifications() {
		return new ArrayList<>(autoMods);
	}

	//--------------------------------------------------------------------
	public void removeAttribute(ItemAttribute attr) {
		attributes.remove(attr);
	}

	//--------------------------------------------------------------------
	public void setAttribute(ItemAttributeValue value) {
//		if (attributes.containsKey(value.attribute))
//			throw new IllegalArgumentException("Attribute "+value.attribute+" already set");
		attributes.put(value.attribute, value);
	}

	//--------------------------------------------------------------------
	public void setAttribute(ItemAttribute attr, int value) {
		if (attributes.containsKey(attr))
			throw new IllegalArgumentException("Attribute "+attr+" already set");
		ItemAttributeNumericalValue val = new ItemAttributeNumericalValue(attr, value);
		attributes.put(attr, val);
	}

	//--------------------------------------------------------------------
	public void setAttribute(ItemAttribute attr, float value) {
		if (attributes.containsKey(attr))
			throw new IllegalArgumentException("Attribute "+attr+" already set to "+attributes.get(attr));
		ItemAttributeFloatValue val = new ItemAttributeFloatValue(attr, value);
		attributes.put(attr, val);
	}

	//--------------------------------------------------------------------
	public void setAttribute(ItemAttribute attr, Object value) {
		ItemAttributeObjectValue val = (ItemAttributeObjectValue) attributes.get(attr);
		if (val==null) {
			val = new ItemAttributeObjectValue(attr, value, this);
			val.setValue(value);
			attributes.put(attr, val);
		} else {
			val.setValue(value);
		}
	}

	//--------------------------------------------------------------------
	public Set<Entry<ItemAttribute,Object>> overwrittenBaseAttributes() {
		return overwrittenBaseAttributes.entrySet();
	}
	
	//--------------------------------------------------------------------
	public void clear() {
		slots.clear();
		// Empty cached slots
		slotCache.values().forEach(avail -> avail.clear());
		modifications.clear();
		autoEnhancements.clear();
		requirements.clear();
		attributes.clear();
		todos.clear();		
	}
	
	//--------------------------------------------------------------------
	public void refreshVirtual() {
		if (!dirty || recalculating)
			return;
		logger.debug("----refresh "+ref.getId()+"----------------");
		recalculating = true;
		try {
			ItemRecalculation.recalculate("", this);
		} catch (Exception e) {
			logger.error("Failed recalculating item",e);
		}
		dirty = false;
		recalculating = false;
	}
	

	//--------------------------------------------------------------------
	public String toString() {
		if (ref==null) return null;
		return "CarriedItem "+ref.getId()
				+((ref.getChoice()!=null)?("/"+choiceReference):"")
				+ ":"+choiceReference
				+" (Mods: "+modifications.size()+")"+" (AutoMods: "+autoMods.size()+")"; //+" (Deprecated: "+ignoreModifications+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemTemplate getItem() {
		return ref;
	}
	public void setItem(ItemTemplate item) {
		this.ref = item;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	public CarriedItem getEmbeddedItem(UUID uuid) {
		for (AvailableSlot slot : slots.values()) {
			for (CarriedItem ref : slot.getAllEmbeddedItems()) {
				if (ref.getUniqueId().equals(uuid))
					return ref;
			}
		}
		// Deprecated, but still valid for old chars
		for (CarriedItem ref : accessories) {
			if (ref.getUniqueId().equals(uuid))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public CarriedItem getEmbeddedItem(String templateID) {
		for (AvailableSlot slot : slots.values()) {
			for (CarriedItem ref : slot.getAllEmbeddedItems()) {
				if (ref.getItem().getId().equals(templateID))
					return ref;
			}
		}
		// Deprecated, but still valid for old chars
		for (CarriedItem ref : accessories) {
			if (ref.getItem().getId().equals(templateID))
				return ref;
		}
		return null;
	}

	//	//--------------------------------------------------------------------
	//	public ItemLocationType getLocation() {
	//		return location;
	//	}
	//
	//	//--------------------------------------------------------------------
	//	public void setItemLocation(ItemLocationType location) {
	//		this.location = location;
	//	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CarriedItem other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//--------------------------------------------------------------------
	public boolean isType(ItemType type) {
		return usedAsType==type;
	}
	public boolean isType(Collection<ItemType> types) {
		for (ItemType type : types) {
			if (usedAsType==type) return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	public boolean isSubType(ItemSubType type) {
		return usedAsSubType==type;
	}

	//--------------------------------------------------------------------
	public ItemSubType getSubType() {
		return usedAsSubType;
	}

	//-------------------------------------------------------------------
	public String getCustomName() {
		return customName;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (customName!=null)
			return customName;
		if (ref.getName()==null) {
			return "Unnamed";
		}
		if (choice!=null) {
			if (choice instanceof BasePluginData)
				return ref.getName()+" ("+((BasePluginData)choice).getName()+")";
			else
				return ref.getName()+" ("+choice+")";
		}
		return ref.getName();
	}

	//-------------------------------------------------------------------
	public String getNameWithRating() {
		if (ref.hasRating())
			return getName()+", "+Resource.get(UI,"label.rating")+" "+rating;
		return getName();
	}

	//-------------------------------------------------------------------
	public String getNameWithCount() {
		if (count>1)
			return getNameWithRating()+" ("+count+"x)";
		return getNameWithRating();
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		customName = name;
		if (customName!=null && customName.isBlank())
			customName = null;
	}

	//--------------------------------------------------------------------
	public Availability getAvailability() {
		Availability ret = new Availability(ref.getAvailability().getValue(), ref.getAvailability().getLegality(), ref.getAvailability().isAddToAvailability());

		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.AVAIL))
			ret.setValue( ref.getAvailability().getValue() * rating);

		/*
		 * If any embedded item has higher values, use them
		 */
		for (CarriedItem access : accessories) {
			Availability foo = access.getAvailability();
			if (foo.isAddToAvailability()) {
				ret.setValue(ret.getValue() + foo.getValue());
			} else {
				ret.setValue( Math.max(ret.getValue(), foo.getValue()));
			}
			if (foo.getLegality().ordinal()>ret.getLegality().ordinal())
				ret.setLegality(foo.getLegality());
		}


		return ret;
	}

//	//--------------------------------------------------------------------
//	public int[] getAttackRating() {
//		// If there is a static rating, use it
//		if (attackRating!=null)
//			return attackRating;
//		
//		int[] ret = new int[5];
//
//		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.ATTACK)) {
//			for (int i=0; i<5; i++) {
//				ret[i] = Math.round(ref.getWeaponData().getAttackRating()[i] * rating);
//			}
//		} else {
//			for (int i=0; i<5; i++) {
//				ret[i] = Math.round(ref.getWeaponData().getAttackRating()[i]);
//			}
//		}
//
//		return ret;
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the characterModifications
	 */
	public List<Modification> getCharacterModifications() {
		boolean multiply = false;
		if (ref.getMultiplyWithRate()!=null)
			multiply = Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.MODIFIER);
		
		// Prepare extra memory for cyber AttributeModifications
		Map<org.prelle.shadowrun6.Attribute, AttributeModification> cyberAttribMods = new HashMap<org.prelle.shadowrun6.Attribute, AttributeModification>();
		
		List<Modification> ret = new ArrayList<Modification>();
		for (Modification tmp : ref.getModifications()) {
//			logger.info("* mod "+tmp);
			if (tmp instanceof AttributeModification) {
				AttributeModification newMod = (AttributeModification)((AttributeModification)tmp).clone();
				if (multiply) 
					newMod.setValue(((AttributeModification)tmp).getValue()*getRating());
				newMod.setSource(this);
				// Special handling for cyber AttributeModifications
				if (newMod.isForCyberware()) {
					org.prelle.shadowrun6.Attribute key = newMod.getAttribute();
					if (newMod.getModificationType()==ModificationType.ABSOLUTE) {
						// Memorize this base attribute
						cyberAttribMods.put(key, newMod);
						ret.add(newMod);
					} else if (cyberAttribMods.containsKey(key)) {
						cyberAttribMods.get(key).setValue(cyberAttribMods.get(key).getValue()+newMod.getValue());
					} else {
						ret.add(newMod);
					}
				} else {
					ret.add(newMod);
				}
				
			} else if (tmp instanceof ItemEnhancementModification) {
			} else if (tmp instanceof ItemHookModification) {
			} else if (tmp instanceof ItemAttributeModification) {
			} else if (tmp instanceof AccessoryModification) {
			} else if (tmp instanceof CarriedItemModification) {
				CarriedItemModification newMod = (CarriedItemModification) ((CarriedItemModification)tmp).clone();
				newMod.setSource(this);
				ret.add(newMod);
			} else if (tmp instanceof SkillModification) {
				SkillModification newMod = (SkillModification) ShadowrunTools.instantiateModification(tmp, choice, 1);
				if (multiply) 
					newMod.setValue(((SkillModification)tmp).getValue()*getRating());
				if (ref.getChoice()==ChoiceType.SKILL || ref.getChoice()==ChoiceType.PHYSICAL_SKILL) {
					newMod.setSkill( ShadowrunCore.getSkill( choiceReference ));
				} else if (ref.getChoice()==ChoiceType.NAME)
					newMod.setName(choiceReference);
				newMod.setSource(this);
				ret.add(newMod);
			} else if (tmp instanceof EdgeModification) {
				ret.add(tmp);
			} else if (tmp instanceof DamageTypeModification) {
				ret.add(tmp);
			} else {
				logger.error("getCharacterModifications() of "+ref);
				logger.error("TODO: process "+tmp.getClass()+" = "+tmp);
			}
		}
		
		// Get modifications from accessories
		for (CarriedItem embedded : getUserAddedAccessories()) {
			// Add all cyber modifications for same attribute
			for (Modification mod : embedded.getCharacterModifications()) {
				if (mod instanceof AttributeModification) {
					AttributeModification newMod = (AttributeModification) ShadowrunTools.instantiateModification(mod, null, 1);
					// Special handling for cyber AttributeModifications
					if (newMod.isForCyberware()) {
						org.prelle.shadowrun6.Attribute key = newMod.getAttribute();
						if (cyberAttribMods.containsKey(key)) {
							cyberAttribMods.get(key).setValue(cyberAttribMods.get(key).getValue()+newMod.getValue());
						} else {
							cyberAttribMods.put(key, newMod);
						}
					} else {
						ret.add(newMod);
					}
				} else
					ret.add(mod);
			}
//			ret.addAll(embedded.getCharacterModifications());
		}
		return ret;
	}

//	//-------------------------------------------------------------------
//	public void addCharacterModification(Modification mod) {
//		//		if (!characterModifications.contains(mod))
//		characterModifications.add(mod);
//	}
//
//	//-------------------------------------------------------------------
//	public void removeCharacterModification(Modification mod) {
//		characterModifications.remove(mod);
//	}

	//-------------------------------------------------------------------
	public float getCapacity(ItemHook hook) {
		float size = 0;
		// Old method
		for (Requirement req : ref.getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement hookReq = (ItemHookRequirement)req;
				if (hookReq.getSlot()==hook) {
					size = hookReq.getCapacity();
					break;
				}
			}
		}
		// New method
		UseAs usage = ref.getDefaultUsage();
		if (slot!=null && ref.getUsageFor(slot)!=null) {
			usage = ref.getUsageFor(hook);
		}
		if (usage!=null)
			size = usage.getCapacity();
		
		// Apply multiplier
		if (ref.multipliesWithRate(Multiply.CAPACITY))
			size *= rating;		
		
		return size;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifyableImpl#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		//		List<Modification> ret = new ArrayList<Modification>(ref.getModifications());
		//		if (ref.getWeaponData()!=null)
		//			ret.addAll(ref.getWeaponData().getModifications());
		//
		//		for (AvailableSlot slot : addedAccessories) {
		//			for (CarriedItem item : slot.getEmbeddedItems()) {
		//				ret.addAll(item.getModifications());
		//			}
		//		}
		//
		//		return ret;
		return new ArrayList<Modification>(modifications);
	}

	//-------------------------------------------------------------------
	public boolean hasAttribute(ItemAttribute attr) {
		return attributes.containsKey(attr);
	}

	//-------------------------------------------------------------------
	public ItemAttributeValue getAttribute(ItemAttribute attr) {
		return attributes.get(attr);
	}

	//-------------------------------------------------------------------
	public ItemAttributeObjectValue getAsObject(ItemAttribute attr) {
		if (!attributes.containsKey(attr)) {
			logger.debug("Attribute "+attr+" not set");
			return new ItemAttributeObjectValue(attr,"-", this);
		}
		
		if (attributes.get(attr) instanceof ItemAttributeNumericalValue) {
			logger.error("Attribute "+attr+" must be obtained via getAsValue()");
			return new ItemAttributeObjectValue(attr,"Error", this);
		}
		
		ItemAttributeObjectValue val = (ItemAttributeObjectValue)attributes.get(attr);
		if (overwrittenBaseAttributes.containsKey(attr)) {
			val = new ItemAttributeObjectValue(attr, overwrittenBaseAttributes.get(attr), this);
			// Should it apply always - not only for overwritten attributes
			for (Modification mod : autoMods) {
				if (mod instanceof ItemAttributeModification) {
					ItemAttributeModification iMod = (ItemAttributeModification)mod;
					if (iMod.getAttribute()==attr)
						val.addModification(iMod);
				}
			}
		}

//		// Add suitable modifications
//		for (Modification tmp : this.getModifications()) {
//			if (tmp instanceof ItemAttributeModification && ((ItemAttributeModification)tmp).getAttribute()==attr)
//				val.addModification(tmp);
//		}

		List<String> ret = new ArrayList<String>();
		try {
			switch (attr) {
			case MODE:
				if (val==null)
					return new ItemAttributeObjectValue(attr,"Error", this);
				if (val.getValue()==null)
					return new ItemAttributeObjectValue(attr,"-", this);
				for (FireMode tmp : (List<FireMode>)val.getValue())
					ret.add(tmp.getName());
				return new ItemAttributeObjectValue(attr, String.join("/",ret), this);
			case AMMUNITION:
				if (val==null)
					return new ItemAttributeObjectValue(attr,"", this);
				for (AmmunitionSlot tmp : (List<AmmunitionSlot>)val.getValue()) 
					ret.add(tmp.toString());
				return new ItemAttributeObjectValue(attr, String.join("/",ret), this);
			case DAMAGE:
				return val;
			default:
				return val;
			}
		} catch (Exception e) {
			logger.error("Error getting ItemAttribute as Object",e);
			return new ItemAttributeObjectValue(attr,"Error", this);
		}
	}

	//-------------------------------------------------------------------
	public ItemAttributeNumericalValue getAsValue(ItemAttribute attr) {
		if (!attributes.containsKey(attr))
			throw new IllegalArgumentException("Attribute "+attr+" not set for "+this);
		switch (attr) {
		case PRICE:
			return new ItemAttributeNumericalValue(attr, getPrice());
//		case ESSENCECOST:
//			return new ItemAttributeNumericalValue(attr, Math.round(getEssence()*1000));
		default:
			if (attributes.get(attr) instanceof ItemAttributeNumericalValue)
				return ((ItemAttributeNumericalValue)attributes.get(attr));
			else {
				logger.error("Expect item attribute "+attr+" to be of type ItemAttributeNumericalValue but it was "+((attributes.get(attr)==null)?"null":(attributes.get(attr).getClass())));
				throw new IllegalArgumentException("ClassCastError: Not numerical: "+attributes.get(attr).getClass());
			}
		}
	}

	//-------------------------------------------------------------------
	public ItemAttributeFloatValue getAsFloat(ItemAttribute attr) {
		if (!attributes.containsKey(attr))
			throw new IllegalArgumentException("Attribute "+attr+" not set for "+this);
		switch (attr) {
		default:
			if (attributes.get(attr) instanceof ItemAttributeFloatValue)
				return ((ItemAttributeFloatValue)attributes.get(attr));
			else {
				logger.error("Expect item attribute "+attr+" to be of type ItemAttributeFloatValue but it was "+((attributes.get(attr)==null)?"null":(attributes.get(attr).getClass())));
				throw new IllegalArgumentException("ClassCastError: Not numerical: "+attributes.get(attr).getClass());
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Give the advantages of this item and recursively walk through all
	 * embedded items and return their advantages as well
	 */
	public Collection<String> getWiFiAdvantageStringRecursivly() {
		List<String> ret = new ArrayList<>();
		ret.addAll(ref.getWiFiAdvantageStrings());
		// Add eventually existing advantages from accessories
//		for (AvailableSlot aVal : accessories) {
//			for (CarriedItem accessory : aVal.getAllEmbeddedItems())
//				ret.addAll(accessory.getWiFiAdvantageStringRecursivly());
//		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Give the advantages of this item and non-recursively walk through all
	 * embedded items and return their advantages as well
	 */
	public Collection<String> getWiFiAdvantageStrings() {
		List<String> ret = new ArrayList<>();
		ret.addAll(ref.getWiFiAdvantageStrings());
		// Add eventually existing advantages from accessories
		//		for (AvailableSlot aVal : accessories) {
		//			for (CarriedItem accessory : aVal.getEmbeddedItems())
		//				ret.addAll(accessory.getItem().getWiFiAdvantageStrings());
		//		}
		return ret;
	}

//	//-------------------------------------------------------------------
//	public void addSlot(AvailableSlot slot) {
//		if (getSlot(slot.getSlot())!=null)
//			throw new IllegalArgumentException("Already have a slot "+slot.getSlot());
//
//		accessories.add(slot);
//	}

	//-------------------------------------------------------------------
	public Collection<AvailableSlot> getSlots() {
		List<AvailableSlot> ret = new ArrayList<>(slots.values());
		Collections.sort(ret, new Comparator<AvailableSlot>() {
			public int compare(AvailableSlot o1, AvailableSlot o2) {
				return o1.getSlot().compareTo(o2.getSlot());
			}
		});
		return ret;
	}

	//-------------------------------------------------------------------
	public AvailableSlot getSlot(ItemHook hook) {
		return slots.get(hook);
	}

	//-------------------------------------------------------------------
	public Collection<CarriedItem> getEffectiveAccessories() {
		List<CarriedItem> ret = new ArrayList<CarriedItem>();
		for (AvailableSlot slot : slots.values()) {
			ret.addAll(slot.getAllEmbeddedItems());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public Collection<CarriedItem> getUserAddedAccessories() {
		List<CarriedItem> ret = new ArrayList<CarriedItem>(accessories);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasHook(ItemHook slot) {
//		for (AvailableSlot avail : accessories) {
//			if (avail.getSlot()==slot)
//				return true;
//		}
//		return defaultAccessories.containsKey(slot);
		return slots.containsKey(slot);
	}

	//-------------------------------------------------------------------
	public void addAccessory(CarriedItem accessory) {
		for (Requirement req : accessory.getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement slotReq = (ItemHookRequirement)req;
				AvailableSlot slot = slots.get( slotReq.getSlot() );
				if (slot==null) {
					throw new IllegalArgumentException("Item "+ref.getId()+" has embedded item "+accessory.getItem().getId()+" that requires non-existent slot "+slotReq.getSlot());
				}  else {
					// Slot exists, add accessory if the capacity is sufficient
					if (slotReq.getCapacity()==0 || slot.getFreeCapacity()>=slotReq.getCapacity()) {
						// Capacity is enough - add accessory to slot
						logger.debug("  add user selected accessory '"+accessory.getItem().getId()+" to slot "+slot);
						slot.addEmbeddedItem(accessory, false, this);
						accessory.setSlot(slot.getSlot());
					} else {
						// Capacity not sufficient
						throw new IllegalStateException("Not enough free capacity in slot "+slotReq.getSlot()+" for embedded item "+accessory.getItem().getId()+" - free="+slot.getFreeCapacity()+"  req="+slotReq.getCapacity());
					}
				}
			}
		}
		// If no slot was choosen, use EXTERNAL
		if (accessory.getSlot()==null) {
			accessory.setSlot(ItemHook.FIREARMS_EXTERNAL);
		}
		ItemHook targetSlot = accessory.getSlot();
		addAccessory(targetSlot, accessory);
	}

	//-------------------------------------------------------------------
	public void addAccessory(ItemHook slot, CarriedItem val) {
		if (!hasHook(slot))
			throw new IllegalArgumentException("Cannot add "+val.getItem().getId()+" to hook "+slot+" in instance of "+ref.getId()+"\nExisting: "+slots.keySet());
		
		val.setSlot(slot);
		if (slots.containsKey(slot))
			slots.get(slot).addEmbeddedItem(val, false, this);
		
		val.setEmbeddedIn(getUniqueId());
		accessories.add(val);
//		logger.warn("Added "+val+" into slot "+slot+" of "+ref);
		dirty = true;
		refreshVirtual();
//		logger.warn("Item now: \n"+dump());
	}

	//-------------------------------------------------------------------
	public boolean removeAccessory(CarriedItem val) {
		boolean removed = false;
		logger.warn("removeAccessory on "+getName()+" checks slots: "+accessories);
		for (AvailableSlot avail : getSlots()) {
			if (avail.removeEmbeddedItem(val)) {
				removed = true;
				logger.debug("Removed "+val+" from slot "+avail);
			} 
		}
		accessories.remove(val);
		val.setEmbeddedIn(null);
		return removed;
	}

	//-------------------------------------------------------------------
	public void setPrice(int price) {
		System.err.println("CarriedItem.setPrice()");
		this.price = price;
	}

	//-------------------------------------------------------------------
	private int getPrice() {
		if (createdByModification)
			return 0;
		if (price!=null) {
			System.err.println("CarriedItem.getPrice(): don't calculate price for "+ref.getId()+" since price is fixed to "+price);
			return price;
		}

		int ret = ref.getPrice();

//		logger.info("......"+ret+"...."+ref.hasRating()+"/"+rating+"/"+Arrays.toString(ref.getMultiplyWithRate()));

		if (ref.hasRating() && ref.getPriceTable()!=null)
			ret = ref.getPriceTable()[rating-1];
		
		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.PRICE)) {
			ret *= rating;
		}
		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.BASEPRICE)) {
			ret *= rating;
		}
		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.PRICE2)) {
			ret *= (rating*rating);
		}

		if (ref.getDefaultUsage()!=null && getSlot()==null)
			ret += ref.getDefaultUsage().getExtraCost();
		if (getSlot()!=null && ref.getUsageFor(getSlot())!=null)
			ret += ref.getUsageFor(getSlot()).getExtraCost();

		// Modify essence by quality
		if (quality!=null) {
			switch (quality) {
			case STANDARD: break;
			case ALPHA: ret *= 1.2f; break;
			case BETA : ret *= 1.5f; break;
			case DELTA: ret *= 2.5f; break;
			case USED : ret *= 0.5f; break;
			}
		}

		// Calculate extra multiplier modifications
		for (CarriedItem access : getUserAddedAccessories()) {
			int price = access.getPrice();
			logger.debug("........access "+access.getItem().getId()+" has Price "+price);
			if (price<0)
				ret *= (price*-1);
		}
		// Calculate extra modifications
		for (CarriedItem access : getUserAddedAccessories()) {
			int price = access.getPrice();
			if (price>0)
				ret += price;
		}
		// Calculate item enhancements
		for (ItemEnhancementValue enhance : getEnhancements()) {
			if (!enhance.isAutoAdded() && enhance.getModifyable().getPrice()>0)
				ret += enhance.getModifyable().getPrice();
		}

		// Ammunition type
		if (ref.getChoice()==ChoiceType.AMMUNITION_TYPE && choice!=null) {
			ret *= ((AmmunitionType)choice).getCostMultiplier();
		}
		
		// Cheap knockoff
		if (isCheapKnockOff()) {
			ret *= 0.5;
		}
		
		ret*=count;

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Check if the item has a specific accessory embedded
	 * @param item
	 * @return
	 */
	public boolean hasEmbedded(ItemTemplate item) {
		// Built-in accessories
		//		if (ref instanceof FirearmWeapon) {
		//			for (AccessoryValue tmp : ((FirearmWeapon)ref).getAccessories()) {
		//				if (tmp.getAccessory().getId().equals(item.getId()))
		//					return true;
		//			}
		//		}

		for (CarriedItem tmp : getUserAddedAccessories()) {
			if (tmp.getItem().getId().equals(item.getId()))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String dump() {
		return dump(0);
	}

	//-------------------------------------------------------------------
	public String dump(int depth) {
		String indent = new String(new char[depth]).replace('\0', ' ');
		StringBuffer buf = new StringBuffer(indent+getName()+((ref.getChoice()!=null)?("/"+choiceReference):"")+"   (Cost "+getPrice()+")");
		for (String wifi : getWiFiAdvantageStrings()) {
			buf.append("\n"+indent+"  WIFI: "+wifi);
		}

		for (AvailableSlot slot : getSlots()) {
			if (slot.getCapacity()==0)
				buf.append("\n"+indent+"- "+slot.getSlot().getName());
			else
				buf.append("\n"+indent+"- "+slot.getSlot().getName()+" (Capacity "+slot.getUsedCapacity()+"/"+slot.getCapacity()+")");

			for (CarriedItem accessory : slot.getAllEmbeddedItems()) {
				// Embedded items
				float size = accessory.getCapacity(slot.getSlot());
				buf.append("\n"+indent+"  +(Capacity "+size+") "+accessory.dump(depth+2));
			}

		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public List<WeaponData> getWeaponDataRecursive() {
		List<WeaponData> ret = new ArrayList<WeaponData>();
		if (ref.getWeaponData()!=null)
			ret.add(ref.getWeaponData());

		// Add weapons from accessories
		for (CarriedItem embedded : getEffectiveAccessories()) {
			ret.addAll(embedded.getWeaponDataRecursive());
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
//		if (ref!=null && !ref.hasRating())
//			throw new IllegalArgumentException("Item "+ref.getId()+" has no rating");
		this.rating = rating;

//		refreshVirtual();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public BodytechQuality getQuality() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(BodytechQuality quality) {
		this.quality = quality;
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the createdByModification
	 */
	public boolean isCreatedByModification() {
		return createdByModification;
	}

	//-------------------------------------------------------------------
	/**
	 * @param createdByModification the createdByModification to set
	 */
	public void setCreatedByModification(boolean createdByModification) {
		this.createdByModification = createdByModification;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoiceReference(String choice) {
		this.choiceReference = choice;
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	public void addAutoEnhancement(ItemEnhancementValue value) {
		autoEnhancements.add(value);
	}

	//-------------------------------------------------------------------
	public List<ItemEnhancementValue> getEnhancements() {
		List<ItemEnhancementValue> ret = new ArrayList<>(autoEnhancements);
		ret.addAll(enhancements);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addEnhancement(ItemEnhancementValue value) {
		enhancements.add(value);
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	public void removeEnhancement(ItemEnhancementValue value) {
		enhancements.remove(value);
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public List<Requirement> getRequirements() {
		return new ArrayList<>(requirements);
	}

	//-------------------------------------------------------------------
	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
		dirty = true;
	}

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

	//-------------------------------------------------------------------
	/**
	 * Set the slot the item is stored in the parent object
	 * @param slot the slot to set
	 */
	public void setSlot(ItemHook slot) {
		this.slot = slot;
		dirty = true;
		refreshVirtual();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the ignoredForCalculations
	 */
	public boolean isIgnoredForCalculations() {
		return ignoredForCalculations;
	}

	//--------------------------------------------------------------------
	/**
	 * @param ignoredForCalculations the ignoredForCalculations to set
	 */
	public void setIgnoredForCalculations(boolean ignoredForCalculations) {
		this.ignoredForCalculations = ignoredForCalculations;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the usedFocus
	 */
	public FocusValue getUsedFocus() {
		return usedFocus;
	}

	//--------------------------------------------------------------------
	/**
	 * @param usedFocus the usedFocus to set
	 */
	public void setUsedFocus(FocusValue usedFocus) {
		this.usedFocus = usedFocus;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the usedFocus
	 */
	public MetamagicOrEchoValue getItemAttunement() {
		return attunement;
	}

	//--------------------------------------------------------------------
	/**
	 * @param usedFocus the usedFocus to set
	 */
	public void setItemAttunement(MetamagicOrEchoValue attunement) {
		this.attunement = attunement;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//--------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
	}

	//--------------------------------------------------------------------
	public void addToDo(ToDoElement toDoElement) {
		todos.add(toDoElement);
	}

	//--------------------------------------------------------------------
	public void removeHook(ItemHook hook) {
		slots.remove(hook);		
	}

	//--------------------------------------------------------------------
	public void addHook(AvailableSlot toAdd) {
		slots.put(toAdd.getSlot(), toAdd);
		slotCache.put(toAdd.getSlot(), toAdd);
	}

	//--------------------------------------------------------------------
	public AvailableSlot _intern_getCachedSlot(ItemHook hook) {
		return slotCache.get(hook);
	}

	//--------------------------------------------------------------------
	public void memorizeAccessory(AccessoryModification mod, CarriedItem accessory) {
		memorizedGeneratedAccessories.put(mod, accessory);
	}
	//--------------------------------------------------------------------
	public CarriedItem getMemorizedAccessory(AccessoryModification mod) {
		return memorizedGeneratedAccessories.get(mod);
	}

	public void addRequirement(Requirement realReq) {
		requirements.add(realReq);	
	}
	
	//--------------------------------------------------------------------
	/**
	 * @return the usedAsType
	 */
	public ItemType getUsedAsType() {
		return usedAsType;
	}

	//--------------------------------------------------------------------
	/**
	 * @param usedAsType the usedAsType to set
	 */
	public void setUsedAsType(ItemType usedAsType) {
		this.usedAsType = usedAsType;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the usedAsSubType
	 */
	public ItemSubType getUsedAsSubType() {
		return usedAsSubType;
	}

	//--------------------------------------------------------------------
	/**
	 * @param usedAsSubType the usedAsSubType to set
	 */
	public void setUsedAsSubType(ItemSubType usedAsSubType) {
		this.usedAsSubType = usedAsSubType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the embeddedIn
	 */
	public UUID getEmbeddedIn() {
		return embeddedIn;
	}

	//-------------------------------------------------------------------
	/**
	 * @param embeddedIn the embeddedIn to set
	 */
	public void setEmbeddedIn(UUID embeddedIn) {
		this.embeddedIn = embeddedIn;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the generatedUUIDs
	 */
	public List<String> getGeneratedUUIDs() {
		return generatedUUIDs;
	}

	//-------------------------------------------------------------------
	/**
	 * @param generatedUUIDs the generatedUUIDs to set
	 */
	public void addGeneratedUUIDs(String toAdd) {
		generatedUUIDs.add(toAdd);
	}

	//-------------------------------------------------------------------
	public int getModificationSlotsUsed() {
		int capacityUsed = 0;
		for (ItemEnhancementValue tmp : getEnhancements()) {
			if (!tmp.isAutoAdded())
				capacityUsed+= tmp.getModifyable().getSize();
		}
		return capacityUsed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	//-------------------------------------------------------------------
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the conditions
	 */
	public List<HasOptionalCondition> getAvailableConditions() {
		List<HasOptionalCondition> condMods = new ArrayList<>();
		for (ItemAttribute attr : ItemAttribute.values()) {
			if (!attributes.containsKey(attr)) {
				continue;
			}
			ItemAttributeValue aVal = getAttribute(attr);
			// Process conditional modifications
			aVal.getModifications().forEach(mod -> {
				if ( ((ItemAttributeModification)mod).isConditional()) {
					condMods.add((ItemAttributeModification) mod);
				}
			});
		}
		
		return condMods;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the conditions
	 */
	public List<String> getConditions() {
		return conditions;
	}

	//-------------------------------------------------------------------
	/**
	 * @param conditions the conditions to set
	 */
	public boolean assumesCondition(HasOptionalCondition mod) {
		return conditions.contains(ref.getConditionKey(mod));
	}

	//-------------------------------------------------------------------
	/**
	 * @param conditions the conditions to set
	 */
	public void addCondition(HasOptionalCondition mod) {
		if (!conditions.contains(ref.getConditionKey(mod))) {
			System.err.println("CarriedItem.addCondition: "+ref.getConditionKey(mod));
			conditions.add(ref.getConditionKey(mod));
		}
		dirty = true;
		refreshVirtual();
	}

	//-------------------------------------------------------------------
	/**
	 * @param conditions the conditions to set
	 */
	public void removeCondition(HasOptionalCondition mod) {
		if (conditions.remove(ref.getConditionKey(mod))) {
			dirty = true;
			refreshVirtual();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cheapKnockOff
	 */
	public boolean isCheapKnockOff() {
		return cheapKnockOff;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cheapKnockOff the cheapKnockOff to set
	 */
	public void setCheapKnockOff(boolean cheapKnockOff) {
		this.cheapKnockOff = cheapKnockOff;
	}

	//-------------------------------------------------------------------
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	public void setImage(byte[] image) {
		this.image = image;
	}

	//-------------------------------------------------------------------
	public void setPrimary(boolean value) {
		if (value)
			this.primary = value;
		else
			this.primary = null;
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		return (primary!=null)?primary:false;
	}
	
}
