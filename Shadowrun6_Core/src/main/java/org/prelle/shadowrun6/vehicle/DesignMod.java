package org.prelle.shadowrun6.vehicle;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class DesignMod extends BasePluginData implements Comparable<DesignMod> {

	@Attribute(required=true)
	private String id;
	@Attribute
	private int max;
	@Element
	private ModificationList modifications;
	@Attribute(name="bp",required=true)
	private int buildPoints;

	//-------------------------------------------------------------------
	public DesignMod() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DESIGNMOD:"+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return null;
		try {
			return i18n.getString("designmod."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "designmod."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "designmod."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(DesignMod other) {
		if (getName()==null) return 0;
		if (other.getName()==null) return 0;
		
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		if (modifications==null)
			return new ArrayList<>();
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the buildPoints
	 */
	public int getBuildPoints() {
		return buildPoints;
	}

}
