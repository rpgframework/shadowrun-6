package org.prelle.shadowrun6.vehicle;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public enum DroneAction {

	EVADE,
	PERCEPTION,
	CRACKING,
	PILOT,
	STEALTH,
	WEAPON,
	DEFENSE_RATING
	;
	public String getName() {
		return Resource.get(ShadowrunCore.getI18nResources(), "droneaction."+name().toLowerCase());
	}
	
}
