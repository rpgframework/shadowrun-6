/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.persist.MetamagicOrEchoConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class MetamagicOrEchoValue extends ModifyableImpl implements Modifyable, Comparable<MetamagicOrEchoValue> {

	@Attribute(name="meta")
	@AttribConvert(MetamagicOrEchoConverter.class)
	private MetamagicOrEcho meta;
	@Attribute(name="lvl")
	private int level;
	@Attribute(name="choice")
	private String choiceReference;
	private transient Object choice;

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue() {
	}

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue(MetamagicOrEcho power) {
		this();
		this.meta = power;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "MetamagicOrEcho("+meta+",lvl="+level+",mods="+modifications+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		StringBuffer ret = new StringBuffer(meta.getName());
		if (choice!=null) {
			if (choice instanceof org.prelle.shadowrun6.Attribute)
				ret.append(" ("+((org.prelle.shadowrun6.Attribute)choice).getName()+")");
			else if (choice instanceof Skill)
				ret.append(" ("+((Skill)choice).getName()+")");
			else if (choice instanceof CarriedItem)
				ret.append(" ("+((CarriedItem)choice).getName()+")");
			else 
				ret.append(" ("+choice+")");
		}
		return ret.toString();
	}

	//-------------------------------------------------------------------
	public String getId() {
		return meta.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MetamagicOrEchoValue other) {
		return meta.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public MetamagicOrEcho getModifyable() {
		return meta;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * Return all modifications without MetamagicOrEchoModification - if existing
	 * @see de.rpgframework.genericrpg.modification.Modifyable#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		return new ArrayList<>(modifications);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}
