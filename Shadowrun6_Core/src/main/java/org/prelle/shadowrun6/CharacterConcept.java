/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="concept")
public class CharacterConcept implements Comparable<CharacterConcept> {

	@org.prelle.simplepersist.Attribute
	private String id;
	@ElementList(type=Recommendation.class,entry="recom",inline=true)
	private List<Recommendation> recommendations;
	
    //-------------------------------------------------------------------
	public CharacterConcept() {
		recommendations = new ArrayList<Recommendation>();
	}
	
    //-------------------------------------------------------------------
    public String getName() {
    	if (id==null)
    		return "CharacterConcept(null)";
        return ShadowrunCore.getI18nResources().getString("concept."+id.toLowerCase()+".title");
    }

    //-------------------------------------------------------------------
    public String toString() {
    	return getName();
    }

    //-------------------------------------------------------------------
	public String getId() {
		return id;
	}

    //-------------------------------------------------------------------
	public void setId(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CharacterConcept o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//--------------------------------------------------------------------
	public List<Recommendation> getRecommendations() {
		return new ArrayList<>(recommendations);
	}
	
}
