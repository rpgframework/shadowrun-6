package org.prelle.shadowrun6;

public enum PriorityType {
	METATYPE,
	ATTRIBUTE,
	MAGIC,
	SKILLS,
	RESOURCES
}