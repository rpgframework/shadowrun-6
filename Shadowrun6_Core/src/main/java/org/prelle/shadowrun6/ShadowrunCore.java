/**
 *
 */
package org.prelle.shadowrun6;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.actions.ActionList;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.items.AmmunitionType;
import org.prelle.shadowrun6.items.AmmunitionTypeList;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementList;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemList;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.items.VehicleData.VehicleType;
import org.prelle.shadowrun6.modifications.AccessoryModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.persist.SkillSpecializationConverter;
import org.prelle.shadowrun6.requirements.ItemHookRequirement;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.shadowrun6.vehicle.ChassisType;
import org.prelle.shadowrun6.vehicle.ChassisTypeList;
import org.prelle.shadowrun6.vehicle.ConsoleType;
import org.prelle.shadowrun6.vehicle.ConsoleTypeList;
import org.prelle.shadowrun6.vehicle.DesignMod;
import org.prelle.shadowrun6.vehicle.DesignModList;
import org.prelle.shadowrun6.vehicle.DesignOption;
import org.prelle.shadowrun6.vehicle.DesignOptionList;
import org.prelle.shadowrun6.vehicle.Powertrain;
import org.prelle.shadowrun6.vehicle.PowertrainList;
import org.prelle.shadowrun6.vehicle.QualityFactor;
import org.prelle.shadowrun6.vehicle.QualityFactorList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Serializer;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ShadowrunCore {

	private final static String PROP_IGNORE_PLUGIN_LANGUAGE = "data.ignore_plugin_language";
	public static ConfigOption<Boolean> DATA_IGNORE_PLUGIN_LANGUAGE;
	
	private static Logger logger = LogManager.getLogger("shadowrun6");

	private static PropertyResourceBundle i18NResources;
	private static PropertyResourceBundle i18NHelpResources;
	private static Serializer serializer;
	private static List<RulePlugin<ShadowrunCharacter>> plugins;
	private static CharacterConceptList concepts;
	private static SkillList skills;
	private static SpellList spells;
	private static TraditionList traditions;
	private static MetaTypeList metatypes;
	private static MagicOrResonanceTypeList magicOrResonanceTypes;
	private static PriorityTable prioTable;
	private static SpellFeatureList spellFeatures;
	private static RitualFeatureList ritualFeatures;
	private static RitualList rituals;
	private static QualityList qualities;
	private static AdeptPowerList adeptPowers;
	private static ComplexFormList complexForms;
	private static ItemList items;
	private static ActionList actions;
	private static LifestyleList lifestyles;
	private static LifestyleOptionList lifestyleOptions;
	private static SpiritList  spirits;
	private static SpriteList sprites;
	private static MentorSpiritList mentorSpirits;
	private static SensorFunctionList sensors;
	private static MetamagicOrEchoList metaOrEchoes;
	private static ItemEnhancementList itemEnhancements;
	private static LicenseTypeList licenseTypes;
	private static FocusList focusList;
	private static AmmunitionTypeList ammoTypeList;
	private static TechniqueList techniques;
	private static MartialArtsList martialArts;
	private static ChassisTypeList chassisTypes;
	private static PowertrainList powertrains;
	private static ConsoleTypeList consoleTypes;
	private static DesignOptionList designOptions;
	private static DesignModList designMods;
	private static QualityFactorList qualityFactors;

	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/shadowrun6/i18n/core");
		i18NHelpResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/shadowrun6/i18n/core-help");
		serializer = new Persister();
		plugins     = new ArrayList<RulePlugin<ShadowrunCharacter>>();

		metatypes   = new MetaTypeList();
		prioTable   = new PriorityTable();
		magicOrResonanceTypes = new MagicOrResonanceTypeList();
		skills         = new SkillList();
		spellFeatures  = new SpellFeatureList();
		spells         = new SpellList();
		ritualFeatures = new RitualFeatureList();
		rituals        = new RitualList();
		qualities      = new QualityList();
		adeptPowers    = new AdeptPowerList();
		complexForms   = new ComplexFormList();
		items          = new ItemList();
		actions        = new ActionList();
		lifestyles     = new LifestyleList();
		lifestyleOptions = new LifestyleOptionList();
		spirits        = new SpiritList();
		sprites		   = new SpriteList();
		traditions     = new TraditionList();
		mentorSpirits  = new MentorSpiritList();
		sensors        = new SensorFunctionList();
		metaOrEchoes   = new MetamagicOrEchoList();
		itemEnhancements= new ItemEnhancementList();
		licenseTypes   = new LicenseTypeList();
		focusList      = new FocusList();
		ammoTypeList   = new AmmunitionTypeList();
		techniques     = new TechniqueList();
		martialArts    = new MartialArtsList();
		chassisTypes   = new ChassisTypeList();
		powertrains    = new PowertrainList();
		consoleTypes   = new ConsoleTypeList();
		designOptions  = new DesignOptionList();
		designMods     = new DesignModList();
		qualityFactors = new QualityFactorList();
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nHelpResources() {
		return i18NHelpResources;
	}

	//-------------------------------------------------------------------
	public static void initialize(RulePlugin<ShadowrunCharacter> plugin) {
		if (plugin instanceof Shadowrun6CorePlugin) {
			
		}
	}

	//-------------------------------------------------------------------
	public static void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		logger.debug("Add configuration to "+cfgShadowrun);
		DATA_IGNORE_PLUGIN_LANGUAGE = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_IGNORE_PLUGIN_LANGUAGE, Type.BOOLEAN, false);
//		DATA_IGNORE_PLUGIN_LANGUAGE.setResourceBundle(ShadowrunCore.getI18nResources());
	}

	//-------------------------------------------------------------------
	public static void registerPlugin(RulePlugin<ShadowrunCharacter> plugin) {
		if (!plugins.contains(plugin)) {
			plugins.add(plugin);
		}
	}

	//-------------------------------------------------------------------
	public static List<RulePlugin<ShadowrunCharacter>> getPlugins() {
		return new ArrayList<RulePlugin<ShadowrunCharacter>>(plugins);
	}

	//-------------------------------------------------------------------
	public static void initCharacterConcepts(InputStream in) {
		logger.debug("Initialize character concepts");
		try {
			concepts = (CharacterConceptList)serializer.read(CharacterConceptList.class, in);
			logger.info("Successfully loaded "+concepts.size()+" character concepts");

			if (logger.isDebugEnabled()) {
				for (CharacterConcept tmp : concepts)
					logger.debug("* "+tmp+" // "+tmp.getRecommendations());
			}
		} catch (Exception e) {
			logger.fatal("Failed deserializing character concepts",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static CharacterConcept getCharacterConcept(String key) {
		for (CharacterConcept tmp : concepts) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<CharacterConcept> getCharacterConcepts() {
		List<CharacterConcept> ret = new ArrayList<CharacterConcept>(concepts);
		Collections.sort(ret);
		return ret;
	}


	//-------------------------------------------------------------------
	public static void loadMetaTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load metatypes (Plugin="+plugin.getID()+")");
		try {
			MetaTypeList toAdd = (MetaTypeList)serializer.read(MetaTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" metatypes");

			// Set translation
			for (MetaType tmp : toAdd) {
				tmp.setResourceBundle(resrc);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getHelpText();
				tmp.getPage();

				// Mark modifications
				for (Modification mod : tmp.getModifications()) {
					mod.setSource(tmp);
				}
			}

			metatypes.addAll(toAdd);
			logger.info("Total known metatypes now: "+metatypes.size());
		} catch (Throwable e) {
			logger.fatal("Failed deserializing metatypes",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing metatypes: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static MetaType getMetaType(String key) {
		for (MetaType tmp : metatypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<MetaType> getMetaTypes() {
		List<MetaType> ret = new ArrayList<MetaType>(metatypes);
		Collections.sort(ret);
		return ret;
	}


	//-------------------------------------------------------------------
	public static void loadMagicOrResonanceTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load magic or resonance types (Plugin="+plugin.getID()+")");
		try {
			MagicOrResonanceTypeList toAdd = (MagicOrResonanceTypeList)serializer.read(MagicOrResonanceTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" magic or resonance types");

			// Set translation
			for (MagicOrResonanceType tmp : toAdd) {
				tmp.setResourceBundle(resrc);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled())
					logger.debug("* "+tmp.getName());
			}

			magicOrResonanceTypes.addAll(toAdd);
		} catch (Exception e) {
			logger.fatal("Failed deserializing magic or resonance types",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing magic/resonance types: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static MagicOrResonanceType getMagicOrResonanceType(String key) {
		for (MagicOrResonanceType tmp : magicOrResonanceTypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<MagicOrResonanceType> getMagicOrResonanceTypes() {
		List<MagicOrResonanceType> ret = new ArrayList<MagicOrResonanceType>(magicOrResonanceTypes);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static void loadPriorityTableEntries(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load priority table entries (Plugin="+plugin.getID()+")");
		try {
			PriorityTableEntryList toAdd = (PriorityTableEntryList)serializer.read(PriorityTableEntryList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" priority table entries");

			// Set translation
			for (PriorityTableEntry tmp : toAdd) {
//				tmp.setResourceBundle(resrc);
//				tmp.setHelpResourceBundle(helpResources);
//				tmp.setPlugin(plugin);
//				if (logger.isDebugEnabled())
//					logger.debug("* "+tmp.getName());

				PriorityTableEntry mergeTo = prioTable.get(tmp.getType()).get(tmp.getPriority());
				mergeTo.mergeFrom(tmp);
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing priority table entries",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing priority table entries: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static PriorityTableEntry getPriorityTableEntry(PriorityType type, Priority prio) {
		return prioTable.get(type).get(prio);
	}

	//-------------------------------------------------------------------
	public static List<Skill> getSkills() {
		List<Skill> ret = new ArrayList<>(skills);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Skill getSkill(String key) {
		for (Skill tmp : skills) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * Get the skills of a specific type (e.g. combat)
	 * @see Skill.SkillType
	 */
	public static List<Skill> getSkills(SkillType type) {
		List<SkillType> validTypes = new ArrayList<SkillType>();
		if (type==SkillType.ACTION) {
			validTypes.add(SkillType.COMBAT);
			validTypes.add(SkillType.PHYSICAL);
			validTypes.add(SkillType.SOCIAL);
			validTypes.add(SkillType.TECHNICAL);
			validTypes.add(SkillType.VEHICLE);
		} else
			validTypes.add(type);

		List<Skill> typedSkills = new ArrayList<Skill>();
		for (Skill skill : skills)
			if (validTypes.contains(skill.getType()))
				typedSkills.add(skill);
		return typedSkills;
	}


	//-------------------------------------------------------------------
	public static void loadSkills(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load skills (Plugin="+plugin.getID()+")");
		try {
			SkillList addSkills = serializer.read(SkillList.class, in);
			logger.info("Successfully loaded "+addSkills.size()+" skills");

			// Set translation
			for (Skill tmp : addSkills) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			skills.addAll(addSkills);
			Collections.sort(skills);
			logger.debug("Total known skills now: "+skills.size());

			// For all skills ...
			for (Skill tmp : addSkills) {
				// Check specializations
				for (SkillSpecialization spec : tmp.getSpecializations()) {
					spec.setSkill(tmp);
				}
				// Check masterships
				for (SkillSpecialization master : tmp.getSpecializations()) {
					master.setSkill(tmp);
					master.setResourceBundle(resources);
					master.setHelpResourceBundle(helpResources);
					master.setPlugin(plugin);
					// Requirements
//					for (Requirement req : master.getPrerequisites()) {
//						if (req instanceof MastershipRequirement)
//							((MastershipRequirement)req).setSkill(tmp);
//						if (req instanceof SpecialRequirement)
//							((SpecialRequirement)req).setSkill(tmp);
//						if (req instanceof AnyRequirement) {
//							for (Requirement req2 : ((AnyRequirement)req).getOptions()) {
//								if (req2 instanceof MastershipRequirement)
//									((MastershipRequirement)req2).setSkill(tmp);
//								if (req2 instanceof SpecialRequirement)
//									((SpecialRequirement)req2).setSkill(tmp);
//							}
//						}
//						if (!req.resolve()) {
//							logger.error(String.format("Skill '%s', Mastership '%s' cannot resolve %s '%s'", tmp.toString(), master.toString(), req.getClass().getSimpleName(), req.toString()));
//							System.exit(0);
//						}
//					}
					// Modifications
//					for (Modification mod : master.getModifications()) {
//						mod.setSource(master);
//					}
					// Verifications
					if (master.getName()==null)
						logger.error("Missing name for "+master.getId());
//					master.getPage();
//					master.getShortDescription();
//					master.getHelpText(); // Triggers error when key is missing
				}
			}
		} catch (Exception e) {
			logger.fatal("Failed deserializing skills",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing skills: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<SpellFeature> getSpellFeatures() {
		List<SpellFeature> ret = new ArrayList<>(spellFeatures);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static SpellFeature getSpellFeature(String key) {
		key = key.toLowerCase();
		for (SpellFeature tmp : spellFeatures) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<Spell> getSpells() {
		List<Spell> ret = new ArrayList<>(spells);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Spell getSpell(String key) {
		for (Spell tmp : spells) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadSpellFeatures(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load spell features (Plugin="+plugin.getID()+")");
		try {
			SpellFeatureList addFeatures = serializer.read(SpellFeatureList.class, in);
			logger.info("Successfully loaded "+addFeatures.size()+" spell features");

			// Set translation
			for (SpellFeature tmp : addFeatures) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
//				tmp.getHelpText();
				tmp.getPage();
				}
			}

			// Add plugin data to complete list
			spellFeatures.addAll(addFeatures);
			Collections.sort(spellFeatures);
			logger.debug("Total known spell features now: "+spellFeatures.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing spell features",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadSpells(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load spells (Plugin="+plugin.getID()+")");
		try {
			SpellList toAdd = serializer.read(SpellList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" spells");

			// Set translation
			for (Spell tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();
				if (tmp.getCategory()==Category.COMBAT)
					tmp.setOpposed(true);
			}

			// Add plugin data to complete list
			spells.addAll(toAdd);
			Collections.sort(spells);
			logger.debug("Total known spells now: "+spells.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing spells",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing spells: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadRitualFeatures(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load ritual features (Plugin="+plugin.getID()+")");
		try {
			RitualFeatureList addFeatures = serializer.read(RitualFeatureList.class, in);
			logger.info("Successfully loaded "+addFeatures.size()+" ritual features");

			// Set translation
			for (RitualFeature tmp : addFeatures) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getName();
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			ritualFeatures.addAll(addFeatures);
			Collections.sort(ritualFeatures);
			logger.debug("Total known ritual features now: "+ritualFeatures.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing ritual features",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<RitualFeature> getRitualFeatures() {
		List<RitualFeature> ret = new ArrayList<>(ritualFeatures);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static RitualFeature getRitualFeature(String key) {
		key = key.toLowerCase();
		for (RitualFeature tmp : ritualFeatures) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadRituals(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load rituals (Plugin="+plugin.getID()+")");
		try {
			RitualList toAdd = serializer.read(RitualList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" rituals");

			// Set translation
			for (Ritual tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getName();
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			rituals.addAll(toAdd);
			Collections.sort(rituals);
			logger.debug("Total known rituals now: "+rituals.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing rituals",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing rituals: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Ritual> getRituals() {
		List<Ritual> ret = new ArrayList<>(rituals);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Ritual getRitual(String key) {
		for (Ritual tmp : rituals) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadQualities(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load qualities (Plugin="+plugin.getID()+")");
		try {
			QualityList toAdd = serializer.read(QualityList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" qualities");

			// Set translation
			for (Quality tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
			}

			// Add plugin data to complete list
			qualities.addAll(toAdd);
			Collections.sort(qualities);
			logger.debug("Total known qualities now: "+qualities.size());

			// Now validate
			for (Quality qual : toAdd) {
				// Validate requirements
				for (Requirement req : qual.getPrerequisites()) {
					try {
						if (!req.resolve()) {
							logger.error("Plugin: "+plugin.getID()+",  quality="+qual.getId()+"  has an unresolvable requirement "+req);
						}
					} catch (Exception e) {
						logger.error("Plugin: "+plugin.getID()+",  quality="+qual.getId()+"  has an unresolvable reference to "+e.getMessage());
						System.exit(1);
					}
				}
			}

		} catch (Exception e) {
			logger.fatal("Failed deserializing qualities",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing qualities: "+e);
			return;
		}

//		logger.fatal("Stop here");
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	public static List<Quality> getQualities() {
		List<Quality> ret = new ArrayList<>(qualities);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static List<Quality> getQualities(Quality.QualityType type) {
		List<Quality> ret = qualities.stream().filter(q -> q.getType()==type).collect(Collectors.toList());
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Quality getQuality(String key) {
		for (Quality tmp : qualities) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadAdeptPowers(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load adept powers (Plugin="+plugin.getID()+")");
		try {
			AdeptPowerList toAdd = serializer.read(AdeptPowerList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" adept powers");

			// Set translation
			for (AdeptPower tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();

				// Mark source for modifications
				for (Modification mod : tmp.getModifications())
					mod.setSource(tmp);
			}

			// remove eventually existing duplicates
			for (AdeptPower tmp : toAdd) {
				for (AdeptPower exist : adeptPowers) {
					if (tmp.getId().equals(exist.getId())) {
						logger.warn("Remove adept power "+tmp.getId()+" from "+exist.getProductName()+" to replace it with update");
						adeptPowers.remove(exist);
						break;
					}
				}
			}
			
			// Add plugin data to complete list
			adeptPowers.addAll(toAdd);
			Collections.sort(adeptPowers);
			logger.debug("Total known adept powers now: "+adeptPowers.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing adept powers",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing adept powers: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<AdeptPower> getAdeptPowers() {
		List<AdeptPower> ret = new ArrayList<>(adeptPowers);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static AdeptPower getAdeptPower(String key) {
		for (AdeptPower tmp : adeptPowers) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static byte[] save(ShadowrunCharacter character) throws IOException {
		try {
			StringWriter out = new StringWriter();
			serializer.write(character, out);
			return out.toString().getBytes(Charset.forName("UTF-8"));
		} catch (Exception e) {
			logger.error("Failed generating XML for char",e);
			StringWriter mess = new StringWriter();
			mess.append("Failed saving character\n\n");
			e.printStackTrace(new PrintWriter(mess));
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, mess.toString());
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static ShadowrunCharacter load(byte[] raw) throws IOException {
		String buf = new String(raw, "UTF-8");
		Source source = new StreamSource( new StringReader( buf ) );
		try {
			ShadowrunCharacter ret = serializer.read(ShadowrunCharacter.class, source);
			ret.replaceCustomSpellDummys();
			logger.info("Character successfully loaded: "+ret);

			ShadowrunTools.fixItemTypes(ret);
			ShadowrunTools.recalculateCharacter(ret);
//			ShadowrunTools.loadEquipmentModifications(ret);
//			ShadowrunTools.calculateDerived(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public static ShadowrunCharacter load(Path path) throws IOException {
		String buf = new String(Files.readAllBytes(path), "UTF-8");
		Source source = new StreamSource( new StringReader( buf ) );
		try {
			ShadowrunCharacter ret = serializer.read(ShadowrunCharacter.class, source);
			ret.replaceCustomSpellDummys();
			logger.info("Character successfully loaded: "+ret);

			ShadowrunTools.fixItemTypes(ret);
			ShadowrunTools.recalculateCharacter(ret);
//			ShadowrunTools.loadEquipmentModifications(ret);
//			ShadowrunTools.calculateDerived(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public static ShadowrunCharacter load(InputStream source) throws IOException {
		try {
			ShadowrunCharacter ret = serializer.read(ShadowrunCharacter.class, source);
			ret.replaceCustomSpellDummys();
			logger.info("Character successfully loaded: "+ret);
			ShadowrunTools.fixItemTypes(ret);
			ShadowrunTools.recalculateCharacter(ret);
//			ShadowrunTools.loadEquipmentModifications(ret);
//			ShadowrunTools.calculateDerived(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("incomplete-switch")
	public static void loadEquipment(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load equipment (Plugin="+plugin.getID()+")");

		try {
			Skill exotic = getSkill("exotic_weapons");
			ItemList toAdd = serializer.read(ItemList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" equipments");

			// Set translation
			for (ItemTemplate tmp : new ArrayList<>(toAdd)) {
				tmp.setResourceBundle(resrc);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
				}
				tmp.getPage();
				tmp.getHelpText();
				// Add enhancement as modification source
				for (Modification mod : tmp.getModifications())
					mod.setSource(tmp);
				
				// Prevent loading from non-matching language
				if (tmp.getLanguage()!=null) {
					if (!Locale.getDefault().getLanguage().equals(tmp.getLanguage())) {
						toAdd.remove(tmp);
						continue;
					} else {
						for (ItemTemplate tmp2 : toAdd) {
							if (tmp2.getId().equals(tmp.getId()) && tmp2.getLanguage()==null) {
								logger.info("Ignore original definition "+tmp2.getId()+"(default) over "+tmp.getId()+"("+tmp.getLanguage()+")");
								toAdd.remove(tmp2);
								break;
							}
						}
					}
				}
				
				ItemTemplate prev = getItem(tmp.getId());
				if (prev!=null) {
						// Replace old
						items.remove(prev);
						logger.debug("  Replace "+prev.getName()+" from "+prev.getPlugin().getID()+" with definition from "+plugin.getID());
				}
				/*
				 * Exotic weapons must be added as specializations to skill
				 */
				if (tmp.getWeaponData()!=null && tmp.getWeaponData().getSkill()==exotic) {
					SkillSpecialization spec = new SkillSpecialization(exotic, tmp.getId());
					spec.setExoticItem(tmp);
					spec.setDeprecated(true);
					exotic.addSpecialization(spec);
					logger.debug("  Added specialization "+spec);
					if (tmp.getId().endsWith("_deprecated")) {
						spec.setId(tmp.getId().substring(0, tmp.getId().lastIndexOf("_")));
						logger.info("  Added specialization "+spec);
					}
				}
				/*
				 * Ensure the new UseAs is filled
				 */
				if (tmp.getUseAs().isEmpty()) {
					UseAs generated  = null;
					ItemHookRequirement slotReq = null;
					for (Requirement req : tmp.getRequirements()) {
						if (req instanceof ItemHookRequirement) slotReq = (ItemHookRequirement) req;
					}
					if (slotReq!=null) {
						generated = new UseAs(ItemType.ACCESSORY, tmp.getSubtype(ItemType.ACCESSORY));
						generated.setSlot(slotReq.getSlot());
						generated.setCapacity(slotReq.getCapacity());
						tmp.addUseAs(generated);
					} else {

						generated = new UseAs(tmp.getNonAccessoryType(), tmp.getSubtype(null));
						tmp.addUseAs(generated);
					}
					logger.debug(tmp.getId()+" is missing <useas> - generate "+generated);
				}
				/*
				 * Accessories for the TOP or UNDER slot, can also be used in SIDE_R and SIDE_L slots
				 */
				if (tmp.isType(ItemType.ACCESSORY) && (tmp.getUsageFor(ItemHook.TOP)!=null || tmp.getUsageFor(ItemHook.UNDER)!=null)) {
					if (tmp.getUsageFor(ItemHook.SIDE_L)==null) {
						UseAs generated = new UseAs(ItemType.ACCESSORY, ItemSubType.ACCESSORY, ItemHook.SIDE_L);
						tmp.addUseAs(generated);
					}
					if (tmp.getUsageFor(ItemHook.SIDE_R)==null) {
						UseAs generated = new UseAs(ItemType.ACCESSORY, ItemSubType.ACCESSORY, ItemHook.SIDE_R);
						tmp.addUseAs(generated);
					}
				}
				
				/*
				 * Special additions depending on item type
				 */
				if (tmp.isType(ItemType.WEAPON_CLOSE_COMBAT)) {
					tmp.setModificationSlots(2);
					tmp.addSlot(new ItemHookModification(ItemHook.MELEE_EXTERNAL, 0));				
					tmp.addUseAs(new UseAs(ItemType.ACCESSORY,tmp.getSubtype(ItemType.WEAPON_CLOSE_COMBAT), ItemHook.MOUNTED_MELEE));
				} else if (tmp.isType(ItemType.WEAPON_RANGED)) {
					if (tmp.getSubtype(ItemType.WEAPON_RANGED)==ItemSubType.THROWING) {
						tmp.setModificationSlots(0);
						tmp.addSlot(new ItemHookModification(ItemHook.MELEE_EXTERNAL, 0));				
					} else {
						tmp.setModificationSlots(2);	// This affects WEAPON_RANGED which are not THROWING, thus BOWS and CROSSBOWS
						tmp.addSlot(new ItemHookModification(ItemHook.RANGED_EXTERNAL, 0));				
					}
					tmp.addUseAs(new UseAs(ItemType.ACCESSORY,tmp.getSubtype(ItemType.WEAPON_RANGED), ItemHook.MOUNTED_RANGED));
				} else if (tmp.isType(ItemType.WEAPON_FIREARMS)) {
					switch (tmp.getSubtype(ItemType.WEAPON_FIREARMS)) {
					case TASERS:
						tmp.setModificationSlots(2);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.TASERS, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.TASERS, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.TASERS, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.TASERS, ItemHook.INSTRUMENT_WEAPON, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case HOLDOUTS:
						tmp.setModificationSlots(0);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.HOLDOUTS, ItemHook.MOUNTED_HOLDOUT, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.HOLDOUTS, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.HOLDOUTS, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.HOLDOUTS, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY, ItemSubType.HOLDOUTS, ItemHook.INSTRUMENT_WEAPON, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case PISTOLS_LIGHT:
						tmp.setModificationSlots(3);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_LIGHT, ItemHook.MOUNTED_PISTOL_LIGHT, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_LIGHT, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_LIGHT, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_LIGHT, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case PISTOLS_HEAVY:
						tmp.setModificationSlots(3);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_HEAVY, ItemHook.MOUNTED_PISTOL_HEAVY, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_HEAVY, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_HEAVY, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.PISTOLS_HEAVY, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case MACHINE_PISTOLS:
						tmp.setModificationSlots(3);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MACHINE_PISTOLS, ItemHook.MOUNTED_PISTOL_MACHINE, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MACHINE_PISTOLS, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MACHINE_PISTOLS, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MACHINE_PISTOLS, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case SUBMACHINE_GUNS:
						tmp.setModificationSlots(4);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SUBMACHINE_GUNS, ItemHook.MOUNTED_SMG, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SUBMACHINE_GUNS, ItemHook.VEHICLE_WEAPON_SMALL, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SUBMACHINE_GUNS, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SUBMACHINE_GUNS, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case SHOTGUNS:
						tmp.setModificationSlots(5);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SHOTGUNS, ItemHook.MOUNTED_SHOTGUN, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SHOTGUNS, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.SHOTGUNS, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case RIFLE_SNIPER:
						tmp.setModificationSlots(5);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_SNIPER, ItemHook.MOUNTED_RIFLES, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_SNIPER, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case RIFLE_HUNTING:
						tmp.setModificationSlots(5);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_HUNTING, ItemHook.MOUNTED_RIFLES, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_HUNTING, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_HUNTING, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case RIFLE_ASSAULT:
						tmp.setModificationSlots(6);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_ASSAULT, ItemHook.MOUNTED_RIFLES, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_ASSAULT, ItemHook.VEHICLE_WEAPON, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.RIFLE_ASSAULT, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case LMG:
						tmp.setModificationSlots(4);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.LMG, ItemHook.MOUNTED_MACHINEGUN, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.LMG, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case MMG:
						tmp.setModificationSlots(4);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MMG, ItemHook.MOUNTED_MACHINEGUN, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.MMG, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case HMG:
						tmp.setModificationSlots(4);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.HMG, ItemHook.MOUNTED_MACHINEGUN, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.HMG, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					case ASSAULT_CANNON:
						tmp.setModificationSlots(4);
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.ASSAULT_CANNON, ItemHook.MOUNTED_MACHINEGUN, 1));
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.ASSAULT_CANNON, ItemHook.VEHICLE_WEAPON_LARGE, 1));
						tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
						break;
					}
				} else if (tmp.isType(ItemType.WEAPON_SPECIAL)) {
					switch (tmp.getSubtype(ItemType.WEAPON_SPECIAL)) {
					case LAUNCHERS:
						tmp.addUseAs(new UseAs(ItemType.ACCESSORY,ItemSubType.LAUNCHERS, ItemHook.MOUNTED_LAUNCHER, 1));
						break;
					}
					tmp.setModificationSlots(2);
					tmp.addUseAs(new UseAs(ItemType.ACCESSORY,tmp.getSubtype(ItemType.WEAPON_SPECIAL), ItemHook.VEHICLE_WEAPON_LARGE, 1));
					tmp.addSlot(new ItemHookModification(ItemHook.FIREARMS_EXTERNAL, 0));
				} else if (tmp.isType(ItemType.AMMUNITION)) {
					switch (tmp.getSubtype(ItemType.AMMUNITION)) {
					case GRENADES:
					case AMMUNITION:
						tmp.setModificationSlots(1);
						break;
					}
				} else if (tmp.isType(Arrays.asList(ItemType.vehicleTypes()))) {
					// Add missing vehicle type
					if (tmp.getVehicleData().getType()==null) {
						// SR6-700 To allow sensors in vehicles, copy vehicles sensor value to attribute
						tmp.getModifications().add(new ItemAttributeModification(ItemAttribute.MAX_SENSOR_RATING, tmp.getVehicleData().getSensor()));
						
						switch (tmp.getNonAccessoryUsage().getSubtype()) {
						case BIKES:
						case ATVS:
						case CARS:
						case TRUCKS:
						case VANS:
						case BUS:
						case GROUND:
						case SPECIAL_VEHICLES:
						case ANTHRO:
							tmp.getVehicleData().setType(VehicleType.GROUND);
							break;
						case FIXED_WING:
						case ROTORCRAFT:
						case VTOL:
						case GRAV:
						case AIR:
							tmp.getVehicleData().setType(VehicleType.AIR);
							break;
						case HOVERCRAFT:
						case PWC:
						case BOATS:
						case SHIPS:
						case SUBMARINES:
						case AQUATIC:
							tmp.getVehicleData().setType(VehicleType.WATER);
							break;
						}
					}
					
					int body = tmp.getVehicleData().getBody();
					int pt_slots = body;
					if (tmp.isSubtype(ItemSubType.MOD_TRAILER, ItemType.VEHICLES)) {
						pt_slots = 0;
					}
					int cf_tmp = tmp.getVehicleData().getCargoFactor();
					// If not given explicitly, calculate CF by rules from DC 126
					if (cf_tmp == 0) {
						cf_tmp = tmp.getVehicleData().getSeats(); // CF is "seats" *1,25 for subtype BIKES, "seats" *1,5
																	// for all others, +4 for subtype TRUCKS 
						if (tmp.isSubtype(ItemSubType.BIKES, ItemType.VEHICLES)) {
							if (Locale.getDefault().getLanguage().equals("de")) {
								cf_tmp = (int) Math.round(cf_tmp * 0.25);
							} else {
								cf_tmp = (int) Math.round(cf_tmp * 1.25);
							}
						} else {
							if (Locale.getDefault().getLanguage().equals("de")) {
								cf_tmp = (int) Math.round(cf_tmp * 0.5);
							} else {
								cf_tmp = (int) Math.round(cf_tmp * 1.5);
							}
							if (tmp.isSubtype(ItemSubType.TRUCKS, ItemType.VEHICLES)) {
								cf_tmp += 4;
							}
						}
					}
					tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_ACCESSORY, 99));
					tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_CHASSIS, body));				
					tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_CF, cf_tmp));	
					tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_POWERTRAIN, pt_slots));				
					tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_ELECTRONICS, body));	
					if (tmp.getVehicleData().getType()==VehicleType.GROUND && tmp.getNonAccessoryUsage().getSubtype()!=ItemSubType.ANTHRO) {
						tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_TIRES, 1));	
					}
					// Add calculated hardpoints, if necessary
						if (tmp.isType(ItemType.VEHICLES) // Define capacity to be "Body/3 (round down)" if type is Vehicle OR Medium Drone OR Large Drone
								|| tmp.getNonAccessoryUsage().getType()==ItemType.DRONE_MEDIUM
								|| tmp.getNonAccessoryUsage().getType()==ItemType.DRONE_LARGE
								) {
							float capacity = (float)Math.floor(tmp.getVehicleData().getBody()/3); // "Math.Floor" = Round down = One Hardpoint per full 3 body
							ItemHookModification mod = new ItemHookModification(ItemHook.VEHICLE_BODY, capacity);
							tmp.addSlot(mod);
						} else if ( tmp.getNonAccessoryUsage().getType()==ItemType.DRONE_SMALL) {
							// Define Capactiy to be 0,5 (small hardpoint) if type is Small Drones AND body >3
							if (tmp.getVehicleData().getBody()>3 ) {
								tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_BODY, 0.5f));
							} else {
								tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_BODY, 0));
							}
						} else if ( tmp.getNonAccessoryUsage().getType()==ItemType.DRONE_MINI) {
							tmp.addSlot(new ItemHookModification(ItemHook.VEHICLE_BODY, 0));
						}
					
//				} else  {
//					logger.warn("Don't know what to do with "+tmp);
//					System.err.println("Don't know what to do with "+tmp);
				}
				
				
//				/*
//				 * Add weapon mount slot
//				 */
//				if (tmp.isType(ItemType.VEHICLES) || tmp.isType(Arrays.asList(ItemType.droneTypes()))) {
//					int capacity = tmp.getVehicleData().getBody()/3;
//					if (capacity>0) {
//						ItemHookModification mod = new ItemHookModification(ItemHook.VEHICLE_BODY, capacity);
//						tmp.addSlot(mod);
//					}
//				}
				/*
				 * Add autosoft slot
				 */
				if (tmp.isType(Arrays.asList(ItemType.droneTypes())) || tmp.isType(ItemType.VEHICLES)) {
					int capacity = tmp.getVehicleData().getPilot()/2;
					if ((tmp.getVehicleData().getPilot()%2)>0)
						capacity++;
					if (capacity>0) {
						ItemHookModification mod = new ItemHookModification(ItemHook.SOFTWARE_DRONE, capacity);
						tmp.addSlot(mod);
					}
					// Add rigger interace - for drones only
					if (tmp.isType(Arrays.asList(ItemType.droneTypes()))) {
						AccessoryModification mod = new AccessoryModification(ItemHook.VEHICLE_ELECTRONICS, getItem("rigger_interface"), 0, null);
						tmp.getModifications().add(mod);
						
						// Make drones countable
						tmp.setCountable(true);
					}
				}
			}

			/*
			 * Sanity checks
			 */
			for (ItemTemplate item : new ArrayList<ItemTemplate>(toAdd)) {
				if (item.getWeaponData()!=null)
					item.getWeaponData().setParent(item);
				if (item.getVehicleData()!=null) {
					int rating = item.getVehicleData().getBody();
//					item.addSlot(new ItemHookModification(ItemHook.VEHICLE_COSMETICS, rating));
//					item.addSlot(new ItemHookModification(ItemHook.VEHICLE_DRIVE, rating));
					item.addSlot(new ItemHookModification(ItemHook.VEHICLE_ELECTRONICS, rating));
//					item.addSlot(new ItemHookModification(ItemHook.VEHICLE_PROTECTION, rating));
//					item.addSlot(new ItemHookModification(ItemHook.VEHICLE_WEAPON, rating));
				}
			}
			/*
			 * Custom data sanity check
			 */
			if (plugin.getID().toLowerCase().equals("custom")) {
				for (ItemTemplate item : new ArrayList<ItemTemplate>(toAdd)) {
					// Custom data
					if (item.getTypes().contains(ItemType.WEAPON)) {
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, ResourceI18N.format(i18NResources, "error.customdata.deprecated_definition", item.getId()));
						break;
					}
				}
			}

//			serializer.write(toAdd, new PrintWriter("/projects/workspaces/RPGFramework/Shadowrun_Core/data/shadowrun/core/equipment2.xml"));

			items.addAll(toAdd);
			Collections.sort(items);

		} catch (Exception e) {
			logger.fatal("Failed loading equipment: "+e,e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing gear: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getItems() {
        Collections.sort(items);
		return items;
	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getItems(ItemType type) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		for (ItemTemplate tmp : items)
			if (tmp.isType(type))
				ret.add(tmp);

		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getItems(ItemType type, ItemSubType... subtypes) {
		List<ItemSubType> allowed = Arrays.asList(subtypes);
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		for (ItemTemplate tmp : items) {
			for (UseAs use : tmp.getUseAs()) {
				if (use.getType()==type && allowed.contains(use.getSubtype())) {
					ret.add(tmp);
					break;
				}
			}
		}

		Collections.sort(ret);
		return ret;
	}

//	//-------------------------------------------------------------------
//	public static List<ItemTemplate> getItemsByPlugins(List<String> permittedPlugins, ItemType type, ItemSubType... subTypes) {
//		List<ItemTemplate> itemTemplates = getItems(type, subTypes);
//		if (permittedPlugins == null || permittedPlugins.isEmpty()) {
//			return itemTemplates;
//		}
//
//		List<String> extendedPermittedPlugins = new ArrayList<>(permittedPlugins);
//		extendedPermittedPlugins.add("core");
//		extendedPermittedPlugins.add("custom)");
//
//		List<ItemTemplate> ret = new ArrayList<>();
//		for (ItemTemplate itemTemplate : itemTemplates) {
//			if (itemTemplate.getPlugin() != null && (extendedPermittedPlugins.contains(itemTemplate.getPlugin().getID()))){
//				ret.add(itemTemplate);
//			}
//		}
//
//		Collections.sort(ret);
//		return ret;
//	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getAccessoryItems(ItemHook hook, ItemTemplate embedIn) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		outer:
		for (ItemTemplate tmp : items) {
			if (!tmp.isType(ItemType.ACCESSORY))
				continue;
			// New way
			UseAs usage = tmp.getUsageFor(hook);
			if (usage==null)
				continue;
			
			// Old way
			for (Requirement req : tmp.getRequirements()) {
				if (req instanceof ItemHookRequirement) {
					if (((ItemHookRequirement)req).getSlot()!=hook)
						continue outer;
				} else if (!ShadowrunTools.isRequirementMet(req, embedIn))
					continue outer;
			}
			logger.debug(tmp+" can be embedded in "+hook+" in item "+embedIn);
			ret.add(tmp);
		}

		Collections.sort(ret);
		return ret;
	}

//	//-------------------------------------------------------------------
//	public static List<ItemTemplate> getItems(Class<? extends ItemTypeData> cls) {
//		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
//		for (ItemTemplate tmp : items) {
//			for (ItemTypeData data : tmp.getTypeData()) {
//				if (data.getClass() == cls)
//					ret.add(tmp);
//			}
//		}
//
//		Collections.sort(ret);
//		return ret;
//	}

	//-------------------------------------------------------------------
	public static ItemTemplate getItem(String key) {
		for (ItemTemplate tmp : items)
			if (tmp.getId().equals(key))
				return tmp;

		return null;
	}

//	//-------------------------------------------------------------------
//	public static List<ItemTemplate> getWeaponItems(Skill skill) {
//		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
//		for (ItemTemplate tmp : items) {
//			if (tmp.isType(ItemType.WEAPON) && tmp.getType(Weapon.class).getSkill()==skill)
//				ret.add(tmp);
//			else if (tmp.isType(ItemType.LONG_RANGE_WEAPON) && tmp.getType(LongRangeWeapon.class).getSkill()==skill)
//				ret.add(tmp);
//		}
//
//		Collections.sort(ret);
//		return ret;
//	}

	//-------------------------------------------------------------------
	public static void addCustomItem(ItemTemplate item) {
		logger.info("Add custom item: "+item);
		if (!item.getId().startsWith("custom"))
			throw new IllegalArgumentException("Not a custom item");

		if (!items.contains(item)) {
			items.add(item);
			Collections.sort(items);
		}

		try {
			saveCustomItems();
		} catch (IOException e) {
			logger.error("Failed saving custom items",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Failed writing custom item data: "+e.getMessage());
		}
	}

	//-------------------------------------------------------------------
	public static void removeCustomItem(ItemTemplate item) {
		logger.info("Remove custom item: "+item);
		if (!item.getId().startsWith("custom"))
			throw new IllegalArgumentException("Not a custom item");

		items.remove(item);
		Collections.sort(items);

		try {
			saveCustomItems();
		} catch (IOException e) {
			logger.error("Failed saving custom items",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Failed writing custom item data: "+e.getMessage());
		}
	}

	//-------------------------------------------------------------------
	public static void saveCustomItems() throws SerializationException, IOException {
		ItemList list = new ItemList();
		for (ItemTemplate item : items) {
//			logger.debug(item.toString());
			if (item.getId().startsWith("custom")) {
				logger.debug("Found "+item.getId());
				list.add(item);
			}
		}

		String dataDir = RPGFrameworkLoader.getInstance().getConfiguration().getOption(RPGFramework.PROP_DATADIR).getStringValue();
//		Preferences usr = Preferences.userRoot().node("/de/rpgframework");
//		String defaultDir =
//				System.getProperty("user.home")
//				+System.getProperty("file.separator")
//				+"rpgframework"
//				+System.getProperty("file.separator")
//				+"custom";
//		String localDirName = usr.get("custom.datadir", defaultDir);
//		usr.put("pluginDir.rules", localDirName);
//
		Path path = FileSystems.getDefault().getPath(dataDir, "custom", RoleplayingSystem.SHADOWRUN.name().toLowerCase(),"items.xml");
		Files.createDirectories(path.getParent());
		logger.info("Save "+list.size()+" custom items to "+path);

		serializer.write(list, path.toFile());
	}

	//-------------------------------------------------------------------
	public static void loadCustomItems() throws SerializationException, IOException {
		String dataDir = RPGFrameworkLoader.getInstance().getConfiguration().getOption(RPGFramework.PROP_DATADIR).getStringValue();
//		Preferences usr = Preferences.userRoot().node("/de/rpgframework");
//		String defaultDir =
//				System.getProperty("user.home")
//				+System.getProperty("file.separator")
//				+"rpgframework"
//				+System.getProperty("file.separator")
//				+"custom";
//		String localDirName = usr.get("custom.datadir", defaultDir);
//		usr.put("pluginDir.rules", localDirName);
//
//		Path path = FileSystems.getDefault().getPath(localDirName, "splittermond","items.xml");
		Path path = FileSystems.getDefault().getPath(dataDir, "custom", RoleplayingSystem.SHADOWRUN.name().toLowerCase(),"items.xml");
		if (!Files.exists(path)) {
			logger.info("No custom items found at "+path);
			return;
		}
		logger.debug("Load custom items to "+path);

		ItemList list = serializer.read(ItemList.class, new FileReader(path.toFile()));
		items.addAll(list);
		logger.info("Loaded "+list.size()+" custom items from "+path);

		Collections.sort(items);
	}

	//-------------------------------------------------------------------
	public static void loadActions(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load actions (Plugin="+plugin.getID()+")");

		try {
			ActionList toAdd = serializer.read(ActionList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" actions");

			// Set translation
			for (ShadowrunAction tmp : new ArrayList<>(toAdd)) {
				tmp.setResourceBundle(resrc);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				if (tmp.getType()==ShadowrunAction.Type.BOOST || tmp.getType()==ShadowrunAction.Type.EDGE)
					tmp.getShortDescription();

				ShadowrunAction prev = getAction(tmp.getId());
				if (prev!=null) {
						// Replace old
						actions.remove(prev);
						logger.debug("  Replace "+prev.getName()+" with definition from "+plugin);
				}
			}

			actions.addAll(toAdd);
			Collections.sort(actions);
			logger.debug("Total known actions now: "+actions.size());

		} catch (Exception e) {
			logger.fatal("Failed loading action: "+e,e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<ShadowrunAction> getActions() {
        Collections.sort(actions);
		return actions;
	}

	//-------------------------------------------------------------------
	public static List<ShadowrunAction> getActions(ShadowrunAction.Type type) {
		List<ShadowrunAction> ret = new ArrayList<ShadowrunAction>();
		for (ShadowrunAction tmp : actions)
			if (tmp.getType()==type)
				ret.add(tmp);

		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static List<ShadowrunAction> getActions(ShadowrunAction.Category... cat) {
		List<ShadowrunAction> ret = new ArrayList<ShadowrunAction>();
		List<ShadowrunAction.Category> allowed = Arrays.asList(cat);
		for (ShadowrunAction tmp : actions)
			if (allowed.contains(tmp.getCategory()))
				ret.add(tmp);

		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static ShadowrunAction getAction(String key) {
		for (ShadowrunAction tmp : actions)
			if (tmp.getId().equals(key))
				return tmp;

		return null;
	}


	//-------------------------------------------------------------------
	public static void loadComplexForms(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load complex forms (Plugin="+plugin.getID()+")");
		try {
			ComplexFormList toAdd = serializer.read(ComplexFormList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" complex forms");

			// Set translation
			for (ComplexForm tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
//					if (logger.isTraceEnabled())
//						logger.trace(tmp.dump());
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			// Add plugin data to complete list
			complexForms.addAll(toAdd);
			Collections.sort(complexForms);
			logger.debug("Total known complex forms now: "+complexForms.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing complex forms",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing complex forms: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<ComplexForm> getComplexForms() {
		List<ComplexForm> ret = new ArrayList<>(complexForms);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static ComplexForm getComplexForm(String key) {
		for (ComplexForm tmp : complexForms) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadLifestyles(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load lifestyles (Plugin="+plugin.getID()+")");
		try {
			LifestyleList toAdd = serializer.read(LifestyleList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" lifestyles");

			// Set translation
			for (Lifestyle tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list. Overwrite previously existing lifestyles with same id
			for (Lifestyle tmp : toAdd) {
				Lifestyle prev = getLifestyle(tmp.getId());
				if (prev!=null) {
					logger.debug("Replace lifestyle "+tmp.getId()+" from "+prev.getProductNameShort()+" with the one from "+tmp.getProductNameShort());
					lifestyles.remove(prev);
				}
			}
			lifestyles.addAll(toAdd);
			Collections.sort(lifestyles);
			logger.debug("Total known lifestyles now: "+lifestyles.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing lifestyles",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing lifestyles: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Lifestyle> getLifestyles() {
		List<Lifestyle> ret = new ArrayList<>(lifestyles);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Lifestyle getLifestyle(String key) {
		for (Lifestyle tmp : lifestyles) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadLifestyleOptions(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load lifestyle options (Plugin="+plugin.getID()+")");
		try {
			LifestyleOptionList toAdd = serializer.read(LifestyleOptionList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" lifestyle options");

			// Set translation
			for (LifestyleOption tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
//					if (logger.isTraceEnabled())
//						logger.trace(tmp.dump());
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			// Add plugin data to complete list. Overwrite previously existing lifestyles with same id
			for (LifestyleOption tmp : toAdd) {
				LifestyleOption prev = getLifestyleOption(tmp.getId());
				if (prev!=null) {
					logger.debug("Replace lifestyle option "+tmp.getId()+" from "+prev.getProductNameShort()+" with the one from "+tmp.getProductNameShort());
					lifestyleOptions.remove(prev);
				}
			}

			// Add plugin data to complete list
			lifestyleOptions.addAll(toAdd);
			Collections.sort(lifestyleOptions);
			logger.debug("Total known lifestyle options now: "+lifestyleOptions.size());

		} catch (Exception e) {
			logger.fatal("Failed deserializing lifestyle options",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<LifestyleOption> getLifestyleOptions() {
		List<LifestyleOption> ret = new ArrayList<>(lifestyleOptions);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static LifestyleOption getLifestyleOption(String key) {
		for (LifestyleOption tmp : lifestyleOptions) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadSpirits(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load spirits (Plugin="+plugin.getID()+")");
		try {
			SpiritList toAdd = serializer.read(SpiritList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" spirits");

			// Set translation
			for (Spirit tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			spirits.addAll(toAdd);
			Collections.sort(spirits);
		} catch (Exception e) {
			logger.fatal("Failed deserializing spirits",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing spirits: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Spirit> getSpirits() {
		List<Spirit> ret = new ArrayList<>(spirits);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Spirit getSpirit(String key) {
		for (Spirit tmp : spirits) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadSprites(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load sprites (Plugin="+plugin.getID()+")");
		try {
			SpriteList toAdd = serializer.read(SpriteList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" sprites");

			// Set translation
			for (Sprite tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			sprites.addAll(toAdd);
			Collections.sort(sprites);
		} catch (Exception e) {
			logger.fatal("Failed deserializing sprites",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing sprites: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Sprite> getSprites() {
		List<Sprite> ret = new ArrayList<>(sprites);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Sprite getSprite(String key) {
		for (Sprite tmp : sprites) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadTraditions(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load traditions (Plugin="+plugin.getID()+")");
		try {
			TraditionList toAdd = serializer.read(TraditionList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" traditions");

			// Set translation
			for (Tradition tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			traditions.addAll(toAdd);
			Collections.sort(traditions);
		} catch (Exception e) {
			logger.fatal("Failed deserializing traditions",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing traditions: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Tradition> getTraditions() {
		List<Tradition> ret = new ArrayList<>(traditions);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Tradition getTradition(String key) {
		for (Tradition tmp : traditions) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadMentorSpirits(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load mentor spirits (Plugin="+plugin.getID()+")");
		try {
			MentorSpiritList toAdd = serializer.read(MentorSpiritList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" mentor spirits");

			// Set translation
			for (MentorSpirit tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
					logger.debug("* "+tmp.getName());
					tmp.getHelpText();
					tmp.getPage();
			}

			// Add plugin data to complete list
			mentorSpirits.addAll(toAdd);
			Collections.sort(spirits);
		} catch (Exception e) {
			logger.fatal("Failed deserializing mentor spirits",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing mentor spirits: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<MentorSpirit> getMentorSpirits() {
		List<MentorSpirit> ret = new ArrayList<>(mentorSpirits);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static MentorSpirit getMentorSpirit(String key) {
		for (MentorSpirit tmp : mentorSpirits) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadSensorFunctions(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load sensor functions (Plugin="+plugin.getID()+")");
		try {
			SensorFunctionList toAdd = serializer.read(SensorFunctionList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" sensor functions");

			// Set translation
			for (SensorFunction tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			// Add plugin data to complete list
			sensors.addAll(toAdd);
			Collections.sort(spirits);
		} catch (Exception e) {
			logger.fatal("Failed deserializing sensor functions",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<SensorFunction> getSensorFunctions() {
		List<SensorFunction> ret = new ArrayList<>(sensors);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static SensorFunction getSensorFunction(String key) {
		for (SensorFunction tmp : sensors) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadMetamagicOrEchos(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load metamagic or echos (Plugin="+plugin.getID()+")");
		try {
			MetamagicOrEchoList toAdd = serializer.read(MetamagicOrEchoList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" metamagics or echoes");

			// Set translation
			for (MetamagicOrEcho tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getHelpText();
				tmp.getPage();
			}

			// Add plugin data to complete list
			metaOrEchoes.addAll(toAdd);
			Collections.sort(metaOrEchoes);
			logger.debug("Total known metamagics or echoes now: "+metaOrEchoes.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing  metamagics or echoes",e);
			return;
		}

//		logger.fatal("Stop here");
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	public static List<MetamagicOrEcho> getMetamagicOrEchoes() {
		List<MetamagicOrEcho> ret = new ArrayList<>(metaOrEchoes);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static MetamagicOrEcho getMetamagicOrEcho(String key) {
		for (MetamagicOrEcho tmp : metaOrEchoes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static SkillSpecialization getSkillSpecialization(String idref) {
		try {
			return (new SkillSpecializationConverter()).read(idref);
		} catch (Exception e) {
			logger.error("Error getting specialization: "+idref,e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	public static void loadItemEnhancement(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resrc, ResourceBundle helpResources) {
		logger.debug("Load item enhancements (Plugin="+plugin.getID()+")");

		try {
			ItemEnhancementList toAdd = serializer.read(ItemEnhancementList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" item modifications (ItemEnhancement)");

			// Set translation
			for (ItemEnhancement tmp : new ArrayList<>(toAdd)) {
				tmp.setResourceBundle(resrc);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName());
				tmp.getPage();
				tmp.getHelpText();
				// Add enhancement as modification source
				for (Modification mod : tmp.getModifications())
					mod.setSource(tmp);
				
				ItemEnhancement prev = getItemEnhancement(tmp.getId());
				if (prev!=null) {
						// Replace old
						itemEnhancements.remove(prev);
						logger.debug("  Replace "+prev.getName()+" from "+prev.getPlugin().getID()+" with definition from "+plugin.getID());
				}
			}

			itemEnhancements.addAll(toAdd);
			Collections.sort(itemEnhancements);

		} catch (Exception e) {
			logger.fatal("Failed loading equipment: "+e,e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing item modifications: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static ItemEnhancement getItemEnhancement(String key) {
		for (ItemEnhancement tmp : itemEnhancements)
			if (tmp.getId().equals(key))
				return tmp;

		return null;
	}

	//-------------------------------------------------------------------
	public static List<ItemEnhancement> getItemEnhancements() {
        Collections.sort(itemEnhancements);
		return itemEnhancements;
	}

	//-------------------------------------------------------------------
	public static List<ItemEnhancement> getItemEnhancements(ItemTemplate target) {
		List<ItemEnhancement> ret = new ArrayList<ItemEnhancement>();
		outer:
		for (ItemEnhancement tmp : itemEnhancements) {
//			logger.info("Check "+tmp.getName());
			for (Requirement req : tmp.getRequirements()) {
//				logger.info("  Requires "+req);
				if (!ShadowrunTools.isRequirementMet(req, target)) {
//					logger.warn("  Not met: "+req);
					continue outer;
				}
			}
			ret.add(tmp);
		}

		Collections.sort(ret);
		return ret;
	}

//	//-------------------------------------------------------------------
//	public static List<ItemEnhancement> getItemEnhancements(ItemType type) {
//		List<ItemEnhancement> ret = new ArrayList<ItemEnhancement>();
//		for (ItemEnhancement tmp : itemEnhancements)
//			if (tmp.getType()==type)
//				ret.add(tmp);
//
//		Collections.sort(ret);
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	public static List<ItemEnhancement> getItemEnhancements(ItemType type, ItemSubType subtype) {
//		List<ItemEnhancement> ret = new ArrayList<ItemEnhancement>();
//		for (ItemEnhancement tmp : itemEnhancements) {
//			if (tmp.getType()==type && tmp.getSubtype()==subtype)
//				ret.add(tmp);
//		}
//
//		Collections.sort(ret);
//		return ret;
//	}

	//-------------------------------------------------------------------
	public static void loadLicenseTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load license types (Plugin="+plugin.getID()+")");
		try {
			LicenseTypeList toAdd = serializer.read(LicenseTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" license types");

			// Set translation
			for (LicenseType tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			// Add plugin data to complete list
			licenseTypes.addAll(toAdd);
			Collections.sort(licenseTypes);
		} catch (Exception e) {
			logger.fatal("Failed deserializing license types",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<LicenseType> getLicenseTypes() {
		List<LicenseType> ret = new ArrayList<>(licenseTypes);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static LicenseType getLicenseType(String key) {
		for (LicenseType tmp : licenseTypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadFoci(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load foci (Plugin="+plugin.getID()+")");
		try {
			FocusList toAdd = serializer.read(FocusList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" foci");

			// Set translation
			for (Focus tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getHelpText();
				tmp.getPage();
				logger.debug("* "+tmp.getName());
			}

			// Add plugin data to complete list
			focusList.addAll(toAdd);
			Collections.sort(focusList);
		} catch (Exception e) {
			logger.fatal("Failed deserializing foci",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing foci: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Focus> getFoci() {
		List<Focus> ret = new ArrayList<>(focusList);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Focus getFocus(String key) {
		for (Focus tmp : focusList) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadAmmoTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load ammunition types (Plugin="+plugin.getID()+")");
		try {
			AmmunitionTypeList toAdd = serializer.read(AmmunitionTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" ammunition types");

			// Set translation
			for (AmmunitionType tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getHelpText();
				logger.debug("* "+tmp.getName());
				tmp.getPage();
			}

			// Add plugin data to complete list
			ammoTypeList.addAll(toAdd);
			Collections.sort(ammoTypeList);
		} catch (Exception e) {
			logger.fatal("Failed deserializing ammunition types",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<AmmunitionType> getAmmoTypes() {
		List<AmmunitionType> ret = new ArrayList<>(ammoTypeList);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static AmmunitionType getAmmoType(String key) {
		for (AmmunitionType tmp : ammoTypeList) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadTechniques(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load techniques (Plugin="+plugin.getID()+")");
		try {
			TechniqueList toAdd = serializer.read(TechniqueList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" techniques");

			// Set translation
			for (Technique tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				tmp.getName();
				tmp.getPage();
				tmp.getHelpText();
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
			}

			// Add plugin data to complete list
			techniques.addAll(toAdd);
			Collections.sort(techniques);
			logger.debug("Total known techniques now: "+techniques.size());

			// Now validate
			for (Technique tech : toAdd) {
				// Validate requirements
				for (Requirement req : tech.getRequirements()) {
					try {
						if (!req.resolve()) {
							logger.error("Plugin: "+plugin.getID()+",  technique="+tech.getId()+"  has an unresolvable requirement "+req);
						}
					} catch (Exception e) {
						logger.error("Plugin: "+plugin.getID()+",  technique="+tech.getId()+"  has an unresolvable reference to "+e.getMessage());
						System.exit(1);
					}
				}
			}

		} catch (Exception e) {
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing techniques: "+e);
			logger.fatal("Failed deserializing techniques",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Technique> getTechniques() {
		List<Technique> ret = new ArrayList<>(techniques);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Technique getTechnique(String key) {
		for (Technique tmp : techniques) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadMartialArts(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load martial arts (Plugin="+plugin.getID()+")");
		try {
			MartialArtsList toAdd = serializer.read(MartialArtsList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" martial arts");

			// Set translation
			for (MartialArts tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			martialArts.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known martial arts now: "+martialArts.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing martial arts",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "Failed deserializing martial arts: "+e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<MartialArts> getMartialArt() {
		List<MartialArts> ret = new ArrayList<>(martialArts);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static MartialArts getMartialArts(String key) {
		for (MartialArts tmp : martialArts) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadChassisTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load chassis types (Plugin="+plugin.getID()+")");
		try {
			ChassisTypeList toAdd = serializer.read(ChassisTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" chassis types");

			// Set translation
			for (ChassisType tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			chassisTypes.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known chassis types now: "+chassisTypes.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing chassis types",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<ChassisType> getChassisTypes() {
		List<ChassisType> ret = new ArrayList<>(chassisTypes);
//		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static ChassisType getChassisType(String key) {
		for (ChassisType tmp : chassisTypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadPowertrains(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load powertrains (Plugin="+plugin.getID()+")");
		try {
			PowertrainList toAdd = serializer.read(PowertrainList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" powertrains");

			// Set translation
			for (Powertrain tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			powertrains.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known powertrains now: "+powertrains.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing powertrains",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<Powertrain> getPowertrains() {
		List<Powertrain> ret = new ArrayList<>(powertrains);
//		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static Powertrain getPowertrain(String key) {
		for (Powertrain tmp : powertrains) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadConsoleTypes(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load console types (Plugin="+plugin.getID()+")");
		try {
			ConsoleTypeList toAdd = serializer.read(ConsoleTypeList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" console types");

			// Set translation
			for (ConsoleType tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			consoleTypes.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known console types now: "+consoleTypes.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing console types",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<ConsoleType> getConsoleTypes() {
		List<ConsoleType> ret = new ArrayList<>(consoleTypes);
//		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static ConsoleType getConsoleType(String key) {
		for (ConsoleType tmp : consoleTypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadDesignOption(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load design options(Plugin="+plugin.getID()+")");
		try {
			DesignOptionList toAdd = serializer.read(DesignOptionList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" design options");

			// Set translation
			for (DesignOption tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
				// Add modification source
				for (Modification mod : tmp.getModifications())
					mod.setSource(tmp);
			}

			// Add plugin data to complete list
			designOptions.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known design options now: "+consoleTypes.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing design options",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<DesignOption> getDesignOptions() {
		List<DesignOption> ret = new ArrayList<>(designOptions);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static DesignOption getDesignOption(String key) {
		for (DesignOption tmp : designOptions) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadDesignMods(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load design options(Plugin="+plugin.getID()+")");
		try {
			DesignModList toAdd = serializer.read(DesignModList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" design options");

			// Set translation
			for (DesignMod tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
				// Add modification source
				for (Modification mod : tmp.getModifications()) 
					mod.setSource(tmp);
			}

			// Add plugin data to complete list
			designMods.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known design mods now: "+designMods.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing design mods",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<DesignMod> getDesignMods() {
		List<DesignMod> ret = new ArrayList<>(designMods);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static DesignMod getDesignMod(String key) {
		for (DesignMod tmp : designMods) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void loadQualityFactors(RulePlugin<? extends ShadowrunCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load quality factors (Plugin="+plugin.getID()+")");
		try {
			QualityFactorList toAdd = serializer.read(QualityFactorList.class, in);
			logger.info("Successfully loaded "+toAdd.size()+" quality factors");

			// Set translation
			for (QualityFactor tmp : toAdd) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				logger.debug("* "+tmp.getName()+"     ("+tmp.getId()+")");
				tmp.getPage();
				tmp.getHelpText();
			}

			// Add plugin data to complete list
			qualityFactors.addAll(toAdd);
//			Collections.sort(martialArts);
			logger.debug("Total known quality factors now: "+qualityFactors.size());
		} catch (Exception e) {
			logger.fatal("Failed deserializing quality factors",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static List<QualityFactor> getQualityFactors() {
		List<QualityFactor> ret = new ArrayList<>(qualityFactors);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public static QualityFactor getQualityFactor(String key) {
		for (QualityFactor tmp : qualityFactors) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

}

