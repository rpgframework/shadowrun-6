/**
 *
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public class Spell extends BasePluginData implements Comparable<Spell> {

	private static Logger logger = LogManager.getLogger("shadowrun");

	public enum Category {
		COMBAT,
		DETECTION,
		HEALTH,
		ILLUSION,
		MANIPULATION
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("spellcategory."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("spellcategory."+name().toLowerCase()+".short"); }
	}

	public enum Type {
		PHYSICAL,
		MANA
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("spelltype."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("spelltype."+name().toLowerCase()+".short"); }
	}

	public enum Range {
		LINE_OF_SIGHT,
		LINE_OF_SIGHT_AREA,
		TOUCH,
		SELF,
		SELF_AREA,
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("spellrange."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("spellrange."+name().toLowerCase()+".short"); }
	}

	public enum Duration {
		INSTANTANEOUS,
		SUSTAINED,
		PERMANENT,
		LIMITED,
		SPECIAL
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("spellduration."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("spellduration."+name().toLowerCase()+".short"); }
	}

//	public enum Damage {
//		PHYSICAL,
//		STUN,
//		SPECIAL
//		;
//		public String getName() { return ShadowrunCore.getI18nResources().getString("spelldamage."+name().toLowerCase()); }
//		public String getShortName() { return ShadowrunCore.getI18nResources().getString("spelldamage."+name().toLowerCase()+".short"); }
//	}

	@Attribute
	private String id;
	@Attribute(name="cat")
	private Category category;
	@Attribute
	private Type   type;
	@Attribute
	private Range  range;
	@Attribute(name="dmg")
	private DamageType damage;
	@Attribute(name="dur")
	private Duration duration;
	@Attribute
	private int  drain;
	@Attribute
	private ChoiceType choice;
	@Attribute
	private boolean multi;
	@Element(name = "description")
	private String customDescription;
	@Element(name = "customName")
	private String customName;
	@Attribute
	private boolean wild;
	@Attribute
	private boolean opposed;
	@Attribute
	private boolean essence;

	@ElementList(entry="spellfeature",type=SpellFeatureReference.class, inline=true)
	private List<SpellFeatureReference> features;

	//-------------------------------------------------------------------
	public Spell() {
		features = new ArrayList<SpellFeatureReference>();
	}

	//-------------------------------------------------------------------
	public Spell(String id) {
		this();
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append(id+"\n");
		buf.append(features+"\n");
		buf.append("Type:"+type+"  \tRange:"+range+"   Damage:"+damage+"\n");
		buf.append("Duration:"+duration+"  \tDrain:F"+drain);

		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (customName!=null)
			return customName;
		if (i18n==null)
			return "spell."+id;
		try {
			return i18n.getString("spell."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "spell."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "spell."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Spell o) {
		if (o==null || category==null) return 0;
		int foo = category.compareTo(o.getCategory());
		if (foo!=0)
			return foo;
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public Range getRange() {
		return range;
	}

	//-------------------------------------------------------------------
	/**
	 * @param range the range to set
	 */
	public void setRange(Range range) {
		this.range = range;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public DamageType getDamage() {
		return damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(DamageType damage) {
		this.damage = damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drain
	 */
	public int getDrain() {
		return drain;
	}

	//-------------------------------------------------------------------
	/**
	 * @param drain the drain to set
	 */
	public void setDrain(int drain) {
		this.drain = drain;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<SpellFeatureReference> getFeatures() {
		return features;
	}

	//-------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<SpellFeatureReference> features) {
		this.features = features;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multi
	 */
	public boolean canBeLearnedMultiple() {
		return multi;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public ChoiceType getChoiceType() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the customDescription
	 */
	public String getCustomDescription() {
		return customDescription;
	}

	//-------------------------------------------------------------------
	/**
	 * @param customDescription the customDescription to set
	 */
	public void setCustomDescription(String customDescription) {
		this.customDescription = customDescription;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpText()
	 */
	@Override
	public String getHelpText() {
		if (customDescription!=null)
			return customDescription;
		return super.getHelpText();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the customName
	 */
	public String getCustomName() {
		return customName;
	}

	//-------------------------------------------------------------------
	/**
	 * @param customName the customName to set
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getProductName()
	 */
	@Override
	public String getProductName() {
		if (customName!=null)
			return "";
		return super.getProductName();
	}

	//-------------------------------------------------------------------
	@Override
	public String getProductNameShort() {
		if (customName!=null)
			return "";
		return super.getProductNameShort();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the wild
	 */
	public boolean usesWildDie() {
		return wild;
	}

	//-------------------------------------------------------------------
	/**
	 * @param wild the wild to set
	 */
	public void setWildDie(boolean wild) {
		this.wild = wild;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the opposed
	 */
	public boolean isOpposed() {
		return opposed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param opposed the opposed to set
	 */
	public void setOpposed(boolean opposed) {
		this.opposed = opposed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the wild
	 */
	public boolean isWild() {
		return wild;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the essence
	 */
	public boolean isEssence() {
		return essence;
	}

}
