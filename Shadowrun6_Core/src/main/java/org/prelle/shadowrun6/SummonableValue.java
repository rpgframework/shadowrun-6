/**
 *
 */
package org.prelle.shadowrun6;

import org.prelle.shadowrun6.persist.SummonableConverter;
import org.prelle.simplepersist.AttribConvert;

/**
 * @author Stefan
 *
 */
public class SummonableValue {

	@org.prelle.simplepersist.Attribute
	private int services;
	@org.prelle.simplepersist.Attribute
	private int level;
	@org.prelle.simplepersist.Attribute
	@AttribConvert(SummonableConverter.class)
	private Summonable summonable;

	//--------------------------------------------------------------------
	public SummonableValue() {
	}

	//--------------------------------------------------------------------
	public SummonableValue(Summonable ref, int rating, int services) {
		this.services = services;
		this.level = rating;
		this.summonable = ref;
	}

	//--------------------------------------------------------------------
	public String getName() {
		return summonable.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the services
	 */
	public int getServices() {
		return services;
	}


	//--------------------------------------------------------------------
	/**
	 * @param services the services to set
	 */
	public void setServices(int services) {
		this.services = services;
	}


	//--------------------------------------------------------------------
	public int getRating() {
		return level;
	}


	//--------------------------------------------------------------------
	public void setRating(int level) {
		this.level = level;
	}


	//--------------------------------------------------------------------
	public Summonable getSummonable() {
		return summonable;
	}


	//--------------------------------------------------------------------
	public void setSummonable(Summonable summonable) {
		this.summonable = summonable;
	}

}
