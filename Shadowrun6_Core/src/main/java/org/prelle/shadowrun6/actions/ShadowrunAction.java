/**
 * 
 */
package org.prelle.shadowrun6.actions;

import java.util.List;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.persist.AccessConverter;
import org.prelle.shadowrun6.persist.ActionConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.ResourceI18N;

/**
 * @author Stefan
 *
 */
public class ShadowrunAction extends BasePluginData implements Comparable<ShadowrunAction> {

	public enum Category {
		ANY,   // Temporary - Remove this
		BOOST,
		COMBAT,
		POSITION,
		MATRIX,
		MAGIC,
		SOCIAL,
		DRIVING,
		CHASE,
		OTHER
		;
		public String getName() { return ResourceI18N.get(ShadowrunCore.getI18nResources(), "action.category."+name().toLowerCase()); }
	}
	
	public enum Type {
		MINOR,
		MAJOR,
		SPECIAL,
		EDGE,
		BOOST
		;
		public String getNameLong() { return ResourceI18N.get(ShadowrunCore.getI18nResources(), "action.type."+name().toLowerCase()); }
		public String getNameShort() { return ResourceI18N.get(ShadowrunCore.getI18nResources(), "action.type."+name().toLowerCase()+".short"); }
	}
	
	public enum Time {
		INITIATIVE,
		ANYTIME,
		/** Pre dice roll */
		PRE,
		/** Post dice roll */
		POST
		;
		public String getNameLong() { return ResourceI18N.get(ShadowrunCore.getI18nResources(), "action.time."+name().toLowerCase()); }
		public String getNameShort() { return ResourceI18N.get(ShadowrunCore.getI18nResources(), "action.time."+name().toLowerCase()+".short"); }
	}
	
	public enum Access {
		OUTSIDER,
		USER,
		ADMIN
	}
	
	public enum Situation {
		MELEE,
		RANGED,
		ATTACK,
		SKILL,
		ACTION,
		MATRIX,
		VEHICLE,
		CHASE
	}
	
	@Attribute(name="id", required=true)
	protected String id;
	@Attribute(name="cat",required=false)
	protected Category category;
	@Attribute(name="type",required=true)
	protected Type type;
	@Attribute
	private Time time;
	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute
	@AttribConvert(ActionConverter.class)
	private ShadowrunAction action;
	
	// For edge actions
	@Attribute
	private int cost;
	@Attribute(name="apply")
	private Situation apply;

	// For matrix actions
	@Attribute
	private boolean illegal;
	@Attribute
	@AttribConvert(AccessConverter.class)
	private List<Access> access;
	@Attribute(name="attr")
 	private org.prelle.shadowrun6.Attribute attribute;
	@Attribute(name="iattr")
	private ItemAttribute itemAttribute;

	//--------------------------------------------------------------------
	public ShadowrunAction() {
		time = Time.INITIATIVE;
	}

	//--------------------------------------------------------------------
	public ShadowrunAction(String id) {
		this.id = id;
		time = Time.INITIATIVE;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		return ResourceI18N.get(i18n, "action."+id);
	}

	//--------------------------------------------------------------------
	public String getPrintName() {
		if (time==null)
			return getName();
		else
			return getName()+" ("+time.getNameShort()+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "action."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "action."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ShadowrunAction other) {
		int cmp = ((Integer)type.ordinal()).compareTo(other.getType().ordinal());
		if (cmp!=0)
			return cmp;
		Integer c1 = cost;
		Integer c2 = other.getCost();
		cmp = c1.compareTo(c2);
		
		if (cmp!=0)
			return cmp;
		return getName().compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	public Category getCategory() {
		return category;
	}

	//--------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//--------------------------------------------------------------------
	public Time getTime() {
		return time;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	public String getApplyString() {
		if (apply==null)
			return "";
		switch (apply) {
		case MELEE:
			return Resource.get(getResourceBundle(), "actiontype.edge.apply.melee");
		case RANGED:
			return Resource.get(getResourceBundle(), "actiontype.edge.apply.ranged");
		case ATTACK:
			return Resource.get(getResourceBundle(), "actiontype.edge.apply.attack");
		case SKILL:
			if (skill==null)
				return Resource.get(getResourceBundle(), "actiontype.edge.apply.skill");
			return skill.getName();
		case ACTION:
			if (action==null)
				return "(Action)";
			return action.getName();
		case MATRIX:
			return "";
//			return Resource.get(getResourceBundle(), "actiontype.edge.apply.matrix");
		}
		return "?"+apply+"?";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the apply
	 */
	public Situation getApply() {
		return apply;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the action
	 */
	public ShadowrunAction getAction() {
		return action;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the illegal
	 */
	public boolean isIllegal() {
		return illegal;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the access
	 */
	public List<Access> getAccess() {
		return access;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the attribute
	 */
	public org.prelle.shadowrun6.Attribute getAttribute() {
		return attribute;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the itemAttribute
	 */
	public ItemAttribute getItemAttribute() {
		return itemAttribute;
	}

	//--------------------------------------------------------------------
	public String getShortDescription() {
		if (i18n==null)
			return id;
		return Resource.get(i18n, "action."+id+".desc");
	}

}
