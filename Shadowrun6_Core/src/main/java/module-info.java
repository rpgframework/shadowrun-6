module shadowrun6.core {
	exports org.prelle.shadowrun6;
	exports org.prelle.shadowrun6.persist;
	exports org.prelle.shadowrun6.requirements;
	exports org.prelle.shadowrun6.modifications;
	exports org.prelle.shadowrun6.proc;
	exports org.prelle.shadowrun6.actions;
	exports org.prelle.shadowrun6.items;
	exports org.prelle.shadowrun6.items.proc;
	exports org.prelle.shadowrun6.vehicle;

	opens org.prelle.shadowrun6 to simple.persist;
	opens org.prelle.shadowrun6.actions to simple.persist;
	opens org.prelle.shadowrun6.items to simple.persist;
	opens org.prelle.shadowrun6.vehicle to simple.persist;
	opens org.prelle.shadowrun6.modifications to simple.persist;
	opens org.prelle.shadowrun6.requirements to simple.persist;

	provides de.rpgframework.character.RulePlugin with org.prelle.shadowrun6.Shadowrun6CorePlugin;

	requires java.datatransfer;
	requires java.mail;
	requires java.naming;
	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires transitive de.rpgframework.products;
	requires simple.persist;
	requires java.activation;
}