package org.prelle.shadowrun6.vehiclegen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Legality;
import org.prelle.shadowrun6.items.OnRoadOffRoadValue;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.items.VehicleData;
import org.prelle.shadowrun6.items.VehicleData.VehicleType;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.vehicle.ChassisType;
import org.prelle.shadowrun6.vehicle.ConsoleType;
import org.prelle.shadowrun6.vehicle.DesignMod;
import org.prelle.shadowrun6.vehicle.DesignOption;
import org.prelle.shadowrun6.vehicle.Powertrain;
import org.prelle.shadowrun6.vehicle.QualityFactor;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class VehicleGenerator {
	
	private final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(VehicleGenerator.class.getName());
	
	protected final static Logger logger = LogManager.getLogger("shadowrun6.vehiclegen");
	protected final static float[] CF_BY_SIZE = new float[] {0,0,0,0,0.25f,0.5f,1,4,6,10,15,20,50,100,150,250,500,1000,2000,4000,10000};
	
	private ItemTemplate model;
	private ChassisType chassis;
	private Powertrain  powertrain;
	private ConsoleType console;
	private List<DesignOptionValue> designOptions;
	private List<DesignModValue> designMods;
	private List<QualityFactorValue> qualityFactors;
	private int buildPoints;
	private float cargo;
	
	private Map<ItemAttribute, List<ItemAttributeModification>> modsByAttr = new HashMap<>();
	
	//-------------------------------------------------------------------
	public VehicleGenerator() {
		model = new ItemTemplate();
		model.setVehicleData(new VehicleData());
		designOptions = new ArrayList<>();
		designMods = new ArrayList<>();
		qualityFactors = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	private boolean hasDesignMod(String key) {
		return designMods.stream().anyMatch(mod -> key.equals(mod.getDesignMod().getId()));
	}

	//-------------------------------------------------------------------
	private boolean hasQuality(String key) {
		return qualityFactors.stream().anyMatch(mod -> key.equals(mod.getQualityFactor().getId()));
	}

	//-------------------------------------------------------------------
	private void recalculate() {
		logger.info("-----------recalculate-------------------------");
		String pattern = "%3d: %s";
		VehicleData vehicle = model.getVehicleData();
		vehicle.clear();
		model.getUseAsReference().clear();
		model.getModifications().clear();
		modsByAttr.clear();
		buildPoints = 0;
		cargo = 0f;
		model.getModifications().clear();
		
		StringBuffer name = new StringBuffer();
		
		if (chassis!=null) {
			name.append(chassis.getName());
			vehicle.setBody(chassis.getBody());
			vehicle.setArmor(chassis.getArmor());
			vehicle.setHandling(new OnRoadOffRoadValue());
			vehicle.getHandling().set(chassis.getHandling());
			vehicle.setSeats(chassis.getSeats());
			UseAs usage = new UseAs(chassis.getType(), chassis.getSubtype());
			model.getUseAsReference().add(usage);
//			model.setType(chassis.getType());
//			model.setSubtype(chassis.getSubtype());
			buildPoints += chassis.getBuildPoints();
			logger.info(String.format(pattern, chassis.getBuildPoints(), "Chassis "+chassis.getId()));
			vehicle.setSize(chassis.getSize());	
		}
		
		if (powertrain!=null) {
			name.insert(0,powertrain.getName()+" ");
			vehicle.setSpeedInterval(new OnRoadOffRoadValue(powertrain.getSpeedInterval()));
			vehicle.setTopSpeed(powertrain.getTopSpeed());
			if (powertrain.getSubtype()!=null)
				model.setSubtype(powertrain.getSubtype());
			vehicle.setType(powertrain.getType());
			buildPoints += powertrain.getBuildPoints();
			logger.info(String.format(pattern, powertrain.getBuildPoints(), "Powertrain "+powertrain.getId()));
			vehicle.setAcceleration(new OnRoadOffRoadValue(
					 (int) ( powertrain.getAcceleration().getOnRoad()),
					 (int) ( powertrain.getAcceleration().getOffRoad())));
		}
		
		if (console!=null) {			
			name.append(" "+ResourceI18N.get(RES, "label.with")+" "+console.getName()+" "+ResourceI18N.get(RES, "label.electronics"));
			vehicle.setPilot(console.getPilot());
			vehicle.setSensor(console.getSensor());
			buildPoints += console.getBuildPoints();
			logger.info(String.format(pattern, console.getBuildPoints(), "Console "+console.getId()));
		}
		
		vehicle.setSize(Math.min(vehicle.getSize(), 20));
		cargo = CF_BY_SIZE[vehicle.getSize()];
		logger.debug("Start CF for size "+vehicle.getSize()+": "+cargo);
		
		// Design options
		for (DesignOptionValue dMod : designOptions) {
			int cost = dMod.getCost();
			logger.info(String.format(pattern, cost, "Design Option "+dMod));
			buildPoints += cost;
			List<Modification> modList = null;
			if (dMod.getDesignOption()!=null) {
				DesignOption mod = dMod.getDesignOption();
				modList = mod.getModifications();
			} else {
				ItemTemplate mod = dMod.getModificationItem();
				modList = mod.getModifications();
			}

			for (Modification tmp : modList) {
				if (tmp instanceof ItemAttributeModification) {
					applyMod((ItemAttributeModification) tmp, dMod.getLevel());
				} else if (tmp instanceof ItemHookModification) {
					applyMod((ItemHookModification) tmp, dMod.getLevel());
				} else if (tmp instanceof EdgeModification) {
					model.getModifications().add(tmp);
				} else {
					logger.warn("ToDo: Unsupported modification: "+tmp.getClass());
				}
			}
		}
		// Recheck CF
		if (chassis!=null && vehicle.getSize()!=chassis.getSize()) {
			float calcCF = CF_BY_SIZE[chassis.getSize()];
			float diffCF = cargo - calcCF;
			cargo = CF_BY_SIZE[vehicle.getSize()]  + diffCF;
			logger.info("Base size CF="+calcCF+", new size CF="+CF_BY_SIZE[vehicle.getSize()]+"  modified by "+diffCF);
		}
		
		
		// Design modifications
		for (DesignModValue dMod : designMods) {
			DesignMod mod = dMod.getDesignMod();
			int cost = dMod.getCost();
			logger.info(String.format(pattern, cost, "Design Modification "+mod.getId()));
			buildPoints += cost;
			
			for (Modification tmp : mod.getModifications()) {
				if (tmp instanceof ItemAttributeModification) {
					applyMod((ItemAttributeModification) tmp, dMod.getLevel());
				} else if (tmp instanceof ItemHookModification) {
					applyMod((ItemHookModification) tmp, dMod.getLevel());
				} else if (tmp instanceof EdgeModification) {
					model.getModifications().add(tmp);
				} else {
					logger.warn("ToDo: Unsupported modification: "+tmp.getClass());
				}
			}
		}
		
		if (hasDesignMod("heavy_duty")) {
			vehicle.setBody( Math.round(vehicle.getBody()*1.5f));
			logger.debug("+50% Body due to Heavy Duty");
		}
		if (hasDesignMod("light_weight")) {
			vehicle.setBody( Math.round(vehicle.getBody()*0.5f));
			logger.debug("-50% Body due to Lightweight");
		}

		/*
		 * 
		 */
		float speed_modifier = (vehicle.getBody()/2) + vehicle.getArmor();
		if (hasDesignMod("integrated_armor") || hasDesignMod("cargo_hauler")) {
			speed_modifier = (vehicle.getBody()/2) + (vehicle.getArmor()/2);
			logger.info("Speed modifier    : ("+vehicle.getBody()+"/2 + "+vehicle.getArmor()+"/2) = "+speed_modifier);
		} else {
			logger.info("Speed modifier    : ("+vehicle.getBody()+"/2 + "+vehicle.getArmor()+") = "+speed_modifier);
		}
		if (powertrain!=null) {
			// Acceleration
			OnRoadOffRoadValue finalAccel = new OnRoadOffRoadValue(
					 (int) ( powertrain.getAcceleration().getOnRoad() - (speed_modifier/2)),
					 (int) ( powertrain.getAcceleration().getOffRoad() - (speed_modifier/2)));
			logger.debug("Final acceleration base: "+powertrain.getAcceleration()+"- ("+speed_modifier+"/2) = "+finalAccel);
			if (modsByAttr.containsKey(ItemAttribute.ACCELERATION)) {
				for (ItemAttributeModification mod : modsByAttr.get(ItemAttribute.ACCELERATION)) {
					OnRoadOffRoadValue tmp = (OnRoadOffRoadValue) mod.getObjectValue();
					logger.debug("   Add: "+tmp+" from "+mod.getSource());
					finalAccel.add(tmp);
				}
			}
			logger.info("Final acceleration base: "+finalAccel);
			vehicle.setAcceleration(finalAccel);
			
			// Top speed
			int topSpeed = (int) ( powertrain.getTopSpeed() - speed_modifier*10 );
			if (powertrain.getSubtype()==ItemSubType.WALKER)
				topSpeed = powertrain.getTopSpeed();
			logger.info("Top Speed base: "+powertrain.getTopSpeed()+" - ("+speed_modifier+"*10) = "+topSpeed);
			if (modsByAttr.containsKey(ItemAttribute.SPEED)) {
				for (ItemAttributeModification mod : modsByAttr.get(ItemAttribute.SPEED)) {
					logger.debug("   Add: "+mod.getValue()+" from "+mod.getSource());
					topSpeed += mod.getValue();
				}
			}
			// Top speed for drones
			if (chassis!=null && chassis.getType()!=null) {
				switch (chassis.getType()) {
				case DRONE_SMALL: topSpeed *= 0.75; break;
				case DRONE_MINI : topSpeed *= 0.50; break;
				case DRONE_MICRO: topSpeed *= 0.25; break;
				default:
				}
			}
			
			logger.info("Final top speed: "+topSpeed);
			vehicle.setTopSpeed(topSpeed);
		}
		
		/* Hardcoded design modifications */
		if (hasDesignMod("cargo_hauler")) {
			if (vehicle.getSeats()>2) {
				vehicle.setSeats( Math.max(2,vehicle.getSeats()/2));
				logger.debug("-50% Seating due to Cargo Hauler");
			}
			logger.debug("+100% CF due to Cargo Hauler");
			cargo *= 2;
		}
		if (hasDesignMod("mass_transit")) {
			vehicle.setSeats(vehicle.getSeats()*3);
			logger.debug("Triple Seating due to Mass Transit");
		}
		if (hasDesignMod("multi_engine")) {
			vehicle.setTopSpeed(vehicle.getTopSpeed()*2);
			logger.debug("Double speed due to Multi Engine");
		}
		if (hasDesignMod("off_road_chassis")) {
			OnRoadOffRoadValue handling = vehicle.getHandling();
			handling.set(handling.getOnRoadFloat()-1, handling.getOffRoadFloat()+1);
			vehicle.setTopSpeed(vehicle.getTopSpeed()-5);
			logger.debug("Apply Off-Road chassis");
		}
		if (hasDesignMod("underpowered")) {
			vehicle.setTopSpeed(vehicle.getTopSpeed()/2);
			logger.debug("Half speed due to Underpowered");
		}
		
		//logger.info("Cargo Factor: "+model.get)
		logger.info("Total build points: "+buildPoints);
		model.setPrice(buildPoints*1000);
		model.setCustomName(name.toString());
		
		// Apply quality factors
		float allFactors = 1;
		for (QualityFactorValue tmp: qualityFactors) {
			allFactors *= tmp.getMultiplier();
			logger.info("Factor "+tmp.getMultiplier()+" due to "+tmp.getQualityFactor().getId());
		}
		model.setPrice(Math.round(model.getPrice()*allFactors));
		buildPoints *= allFactors;
		
		// Availability
		model.setAvailability(new Availability(2, false));
		if (hasQuality("luxury")) {
			model.getAvailability().setValue(model.getAvailability().getValue()+1);
			logger.debug("+1 availability for Luxury");
		}
		if (!model.getUseAsReference().isEmpty() && model.getUseAsReference().get(0)!=null) {
			switch (model.getUseAsReference().get(0).getSubtype()) {
			case FIXED_WING:
			case ROTORCRAFT:
			case VTOL:
			case LAV:
			case LTAV:
			case GRAV:
				model.getAvailability().setValue(model.getAvailability().getValue()+1);
				model.getAvailability().setLegality(Legality.RESTRICTED);
				logger.debug("+1L availability for aircraft");

			}
		}
		if (hasQuality("military")) {
			model.getAvailability().setValue(model.getAvailability().getValue()+2);
			model.getAvailability().setLegality(Legality.FORBIDDEN);
			logger.debug("+2I availability for Military");
		}
		if (model.getId()==null || model.getId().startsWith("custom")) {
			model.setId("custom_"+UUID.randomUUID());
		}
		logger.info("Final CF: "+cargo);
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_CF, cargo));
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_ACCESSORY, 99));	
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_CHASSIS, vehicle.getBody()));				
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_POWERTRAIN, vehicle.getBody()));				
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_ELECTRONICS, vehicle.getBody()));	
		model.addSlot(new ItemHookModification(ItemHook.VEHICLE_BODY, (float)Math.floor(vehicle.getBody()/3) + vehicle.getHardpoints()));	
		if (model.getVehicleData().getType()==VehicleType.GROUND && chassis!=null && chassis.getId().equals("wheeled")) {
			model.addSlot(new ItemHookModification(ItemHook.VEHICLE_TIRES, 1));	
		}
		int capacity = vehicle.getPilot()/2;
		if ((vehicle.getPilot()%2)>0)
			capacity++;
		if (capacity>0) {
			model.addSlot(new ItemHookModification(ItemHook.SOFTWARE_DRONE, capacity));	
		}

	}

	//-------------------------------------------------------------------
	private void applyMod(ItemAttributeModification mod, int level) {
		logger.info("  apply "+mod+" with level "+level);
		VehicleData vehicle = model.getVehicleData();
		List<ItemAttributeModification> list = modsByAttr.get(mod.getAttribute());
		if (list==null) {
			list = new ArrayList<>();
			modsByAttr.put(mod.getAttribute(), list);
		}
		int multi = level;
		if (level==0) multi=1;
		
		switch (mod.getAttribute()) {
		case ACCELERATION:
			OnRoadOffRoadValue oldVal = (OnRoadOffRoadValue) mod.getObjectValue();
			if (oldVal==null)
				oldVal = new OnRoadOffRoadValue(mod.getValue());			
			OnRoadOffRoadValue newVal = new OnRoadOffRoadValue(oldVal.getOnRoadFloat()*multi, oldVal.getOffRoadFloat()*multi);
			ItemAttributeModification newMod = new ItemAttributeModification(mod.getAttribute(), newVal);
			newMod.setSource(mod.getSource());
			list.add(newMod);
			logger.debug("     Add "+mod+" with level "+level+" => "+newMod);
			break;
		case ARMOR:
			vehicle.setArmor(vehicle.getArmor() + mod.getValue()*multi);
			logger.debug("  New armor = "+vehicle.getArmor());
			break;
		case BODY:
			vehicle.setBody(vehicle.getBody() + mod.getValue()*multi);
			logger.debug("  New body = "+vehicle.getBody());
			break;
		case CARGO:
			cargo += mod.getValue()*multi;
			logger.debug("  New CF = "+cargo);
			break;
		case HANDLING:
			oldVal = (OnRoadOffRoadValue) mod.getObjectValue();
			if (oldVal==null)
				oldVal = new OnRoadOffRoadValue(mod.getValue());
			newVal = new OnRoadOffRoadValue(oldVal.getOnRoadFloat()*multi, oldVal.getOffRoadFloat()*multi);
			vehicle.getHandling().add(newVal);
			logger.debug("     Add "+mod.getAttribute()+":"+newVal);
			break;
		case SPEED_INTERVAL:
			oldVal = (OnRoadOffRoadValue) mod.getObjectValue();
			if (oldVal==null)
				oldVal = new OnRoadOffRoadValue(mod.getValue());
			newVal = new OnRoadOffRoadValue(oldVal.getOnRoadFloat()*multi, oldVal.getOffRoadFloat()*multi);
			vehicle.getSpeedInterval().add(newVal);
			logger.debug("     Add "+mod.getAttribute()+":"+newVal);
			break;
		case SPEED:
			// Add via mod for final calculation
			newMod = new ItemAttributeModification(mod.getAttribute(), mod.getValue()*multi);
			newMod.setSource(mod.getSource());
			list.add(newMod);
			logger.debug("     Add "+mod+" with level "+level+" => "+newMod);
			break;
		case SEATS:
			int plusSeat = 0;			
			if (mod.getValue()>=50) {
				// Percent value
				float current = vehicle.getSeats();
				for (int i=0; i<Math.abs(multi); i++) {
					logger.info("...."+current+" * "+mod.getValue()+" / 100  = "+current+" * "+(mod.getValue() / 100.0f));
					if (multi>0)
						current += current * mod.getValue() / 100.0f;
					else
						current -= current * mod.getValue() / 100.0f;
				}
				plusSeat = Math.round(current) - vehicle.getSeats();
			} else {
				// Fixed value - multiplied
				plusSeat = mod.getValue() * multi;
			}
			vehicle.setSeats(vehicle.getSeats() + plusSeat);
			logger.info("     Add "+mod.getAttribute()+":"+plusSeat);
//			newMod = new ItemAttributeModification(mod.getAttribute(), plusSeat);
//			newMod.setSource(mod.getSource());
//			list.add(newMod);
//			logger.debug("     Add "+mod+" with level "+level+" => "+newMod);
			break;
		case CAPACITY:
			vehicle.setHardpoints(vehicle.getHardpoints() + mod.getValue()*multi);
			logger.debug("  New hardpoints = "+vehicle.getHardpoints());
			break;
		case SIZE:
			vehicle.setSize(vehicle.getSize() + mod.getValue()*multi);
			logger.debug("  New size = "+vehicle.getSize());
			break;
		default:
			logger.error("ToDo: attribute "+mod.getAttribute());
		}
	}

	//-------------------------------------------------------------------
	private void applyMod(ItemHookModification mod, int multi) {
		VehicleData vehicle = model.getVehicleData();
		if (mod.isRemove()) {
			logger.error("Not implemented yet: removing hooks");
		} else {
			model.addSlot(mod);
		}
	}

	//-------------------------------------------------------------------
	public ItemTemplate getVehicle() {
		return model;
	}

	//-------------------------------------------------------------------
	public int getBuildPoints() {
		return buildPoints;
	}

	//-------------------------------------------------------------------
	public float getCargoFactor() {
		return cargo;
	}

	//-------------------------------------------------------------------
	public int getSizeClass() {
		return model.getVehicleData().getSize();
	}

	//-------------------------------------------------------------------
	public void selectChassis(ChassisType value) {
		logger.info("selectChassis("+value+")");
		chassis = value;
		recalculate();
	}

	//-------------------------------------------------------------------
	public void selectPowertrain(Powertrain value) {
		logger.info("selectPowertrain("+value+")");
		powertrain = value;
		recalculate();
	}

	//-------------------------------------------------------------------
	public void selectConsole(ConsoleType value) {
		logger.info("selectConsole("+value+")");
		console = value;
		recalculate();		
	}

	//-------------------------------------------------------------------
	public List<DesignOptionValue> getSelectedDesignOptions() {
		return new ArrayList<DesignOptionValue>(designOptions);
	}

	//-------------------------------------------------------------------
	public List<DesignOption> getAvailableDesignOptions() {
		List<DesignOption> ret = new ArrayList<>(ShadowrunCore.getDesignOptions());
		designOptions.forEach(tmp -> ret.remove(tmp.getDesignOption()));
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean canBeSelected(DesignOption dOpt, int level) {
		logger.debug("canBeSelected("+dOpt.getId()+", "+level+")");
		int maxInt = 1;
		try {
			maxInt = Integer.parseInt( dOpt.getMax() );
		} catch (NumberFormatException e) {
			switch (dOpt.getMax()) {
			case "DOUBLE_BODY":
				maxInt = getVehicle().getVehicleData().getBody()*2;
				break;
			case "150PERCENT":
				maxInt = Math.round(getVehicle().getVehicleData().getSeats()*1.5f);
				break;
			case "MAXSIZE":
				maxInt = 20 - getVehicle().getVehicleData().getSeats();
				break;
			default:
				logger.error("Don't understand '"+dOpt.getMax()+"'");
			}
		}
		
		if (level>maxInt) return false;
		if (level<-maxInt) return false;
		return true;
	}

	//-------------------------------------------------------------------
	public DesignOptionValue selectDesignOption(DesignOption value) {
		if (!canBeSelected(value, 1)) {
			logger.error("Trying to select '"+value+"' which cannot be selected");
			return null;
		}
		DesignOptionValue tmp = new DesignOptionValue(value);
		tmp.setLevel(1);
		tmp.setCost(value.getBuildPoints());
		designOptions.add(tmp);
		recalculate();	
		
		return tmp;
	}

	//-------------------------------------------------------------------
	public DesignOptionValue selectDesignOption(DesignOption value, int level) {
		if (!canBeSelected(value, level)) {
			logger.warn("Cannot select "+value);
			return null;
		}
		
		DesignOptionValue tmp = new DesignOptionValue(value);
		tmp.setLevel(level);
		tmp.setCost(value.getBuildPoints()*level);
		designOptions.add(tmp);
		recalculate();	
		
		return tmp;
	}

	//-------------------------------------------------------------------
	public DesignOptionValue selectModificationAsDesignOption(ItemTemplate item) {
		float cost = 99;
		UseAs use = null;
		for (UseAs useAs : item.getUseAs()) {
			if (useAs.getSlot().name().startsWith("VEHICLE_") && useAs.getCapacity()<cost) {
				use  = useAs;
				cost = useAs.getCapacity();
			}
		}
		
		if (use==null)
			throw new IllegalArgumentException(item.getId()+" cannot be used as vehicle accessory");
		
		DesignOptionValue tmp = new DesignOptionValue(item, use);
		designOptions.add(tmp);
		logger.info("Add modification "+tmp);
		recalculate();
		
		return tmp;
	}

	//-------------------------------------------------------------------
	public void deselect(DesignOptionValue selectedItem) {
		logger.info("Deselect "+selectedItem);
		designOptions.remove(selectedItem);
		recalculate();
	}

	//-------------------------------------------------------------------
	public List<DesignMod> getAvailableDesignMods() {
		List<DesignMod> ret = new ArrayList<>(ShadowrunCore.getDesignMods());
		designMods.forEach(tmp -> ret.remove(tmp.getDesignMod()));
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean canBeSelected(DesignMod value) {
		logger.info("canBeSelected("+value.getId()+")");
		// Check if already selected
		boolean already = false;
		for (DesignModValue tmp : designMods) {
			if (tmp.getDesignMod()==value) {
				already=true;
				break;
			}
		}
		return !already;
	}

	//-------------------------------------------------------------------
	public DesignModValue selectDesignMod(DesignMod value) {
		DesignModValue tmp = new DesignModValue(value);
		designMods.add(tmp);
		recalculate();	
		
		return tmp;
	}

	//-------------------------------------------------------------------
	public List<DesignModValue> getSelectedDesignMods() {
		return new ArrayList<DesignModValue>(designMods);
	}

	//-------------------------------------------------------------------
	public void deselect(DesignModValue selectedItem) {
		logger.info("Deselect "+selectedItem);
		if (designMods.remove(selectedItem))
			recalculate();
	}

	//-------------------------------------------------------------------
	public List<QualityFactor> getAvailableQualityFactors() {
		List<QualityFactor> ret = new ArrayList<>(ShadowrunCore.getQualityFactors());
		qualityFactors.forEach(tmp -> ret.remove(tmp.getQualityFactor()));
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean canBeSelected(QualityFactor value) {
		logger.info("canBeSelected("+value.getId()+")");
		// Check if already selected
		boolean already = false;
		for (QualityFactorValue tmp : qualityFactors) {
			if (tmp.getQualityFactor()==value) {
				already=true;
				break;
			}
		}
		return !already;
	}

	//-------------------------------------------------------------------
	public QualityFactorValue selectQualityFactor(QualityFactor value) {
		QualityFactorValue tmp = new QualityFactorValue(value);
		qualityFactors.add(tmp);
		recalculate();	
		
		return tmp;
	}

	//-------------------------------------------------------------------
	public List<QualityFactorValue> getSelectedQualityFactors() {
		return new ArrayList<QualityFactorValue>(qualityFactors);
	}

	//-------------------------------------------------------------------
	public void deselect(QualityFactorValue selectedItem) {
		logger.info("Deselect "+selectedItem);
		if (qualityFactors.remove(selectedItem))
			recalculate();
	}

}
