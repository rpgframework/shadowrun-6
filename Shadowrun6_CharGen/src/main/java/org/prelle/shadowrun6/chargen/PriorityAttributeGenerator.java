/**
 *
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.PriorityAttributeController;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationBase.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityAttributeGenerator implements PriorityAttributeController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen.attrib");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR6CharacterGenerator parent;
	private ShadowrunCharacter model;

	private int pointsAllowed;
	private int pointsSpecial;
	private List<ToDoElement> todos;
	private List<Attribute> metatypeAttribute;
	
	private List<Modification> unitTestMods;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityAttributeGenerator(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		if (model==null)
			throw new NullPointerException("Model may not be null");
		todos = new ArrayList<>();
		metatypeAttribute = new ArrayList<>();
		unitTestMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * Only for unit tests
	 */
	public PriorityAttributeGenerator(ShadowrunCharacter model) {
		this.model = model;
		todos = new ArrayList<>();
		metatypeAttribute = new ArrayList<>();
		unitTestMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return pointsAllowed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPointsLeftSpecial()
	 */
	@Override
	public int getPointsLeftSpecial() {
		return pointsSpecial;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPerAttribute(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public PerAttributePoints getPerAttribute(Attribute key) {
		return model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#mightReach(org.prelle.shadowrun6.Attribute, int)
	 */
	@Override
	public ValueState getValueState(Attribute key, int value) {
		if (value<1) return ValueState.BELOW_MINIMUM;
		AttributeValue val = model.getAttribute(key);
		if (value<=val.getModifier()) return ValueState.WITHIN_MODIFIER;

		if (value>val.getMaximum()) return ValueState.ABOVE_MAX;

		if (value<=val.getModifiedValue()) return ValueState.SELECTED;

		// Not selected yet.
		if (value==val.getMaximum()) {
			// If another attribute is already maxed, this attribute may not
			List<Attribute> maxed = getMaximizedAttributes();
			if (!maxed.isEmpty())
				return ValueState.UNATAINABLE_MAX;
		}
		return ValueState.UNSELECTED;
	}

	//-------------------------------------------------------------------
	public int getMaximumValue(Attribute key) {
		return model.getAttribute(key).getMaximum();
	}

	//-------------------------------------------------------------------
	private List<Attribute> getMaximizedAttributes() {
		List<Attribute> maxed = new ArrayList<Attribute>();
		for (Attribute key : Attribute.primaryValues()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per.getSum() >= model.getAttribute(key).getMaximum())
				maxed.add(key);
		}
		return maxed;
	}
	
	//-------------------------------------------------------------------
	private boolean isAnotherAttributeAlreadyMaxed(Attribute key) {
		if (key.isSpecial())
			return false;

		Collection<Attribute> alreadyMaxed = getMaximizedAttributes();
		PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
		// Only allow to max an attribute, if there isn't one already
		if ((per.getSum()+1)>=getMaximumValue(key) && key.isPrimary()) {
			if (logger.isTraceEnabled())
				logger.trace("Increasing "+key+" would reach maximum of "+getMaximumValue(key)+".  Is already one maxed = "+alreadyMaxed);
			return !alreadyMaxed.isEmpty();
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#canBeDecreased(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute key) {
		return canDecreaseKarma(key);
		//		AttributeValue val = model.getAttribute(key);
		//		if (key==Attribute.MAGIC||key==Attribute.RESONANCE)
		//			return val.getPoints()>0;
		//			return val.getPoints()>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		return canIncreaseKarma(key);
		//		AttributeValue val = model.getAttribute(key);
		//		boolean enoughKarma = model.getKarmaFree() >= ((val.getModifiedValue()+1)*5);
		//		if (key.isPrimary()) {
		//			if (getPointsLeft()<1 && !enoughKarma)
		//				return false;
		//		} else {
		//			if (getPointsLeftSpecial()<1 && !enoughKarma)
		//				return false;
		//		}
		//
		//		Collection<Attribute> alreadyMaxed = getMaximizedAttributes();
		//
		//		// Only allow to max an attribute, if there isn't one already
		//		if ((val.getModifiedValue()+1)>=getMaximumValue(key) && key.isPrimary()) {
		//			if (logger.isTraceEnabled())
		//				logger.trace("Increasing "+key+" would reach maximum of "+getMaximumValue(key)+".  Is already one maxed = "+alreadyMaxed);
		//			return alreadyMaxed.isEmpty();
		//		}
		//		return val.getModifiedValue()<getMaximumValue(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getIncreaseCost(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#increase(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean increase(Attribute key) {
		return increaseKarma(key);
		//		if (!canBeIncreased(key)) {
		//			logger.warn("Trying to increase "+key+" which cannot be increased");
		//			return false;
		//		}
		//		AttributeValue val = model.getAttribute(key);
		//		val.setPoints(val.getPoints()+1);
		//		logger.info("Increase "+key+" to "+val.getPoints()+"/"+val.getModifiedValue()+"/max="+val.getMaximum());
		//
		//		parent.runProcessors();
		//		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#decrease(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean decrease(Attribute key) {
		return decreaseKarma(key);
		//		if (!canBeDecreased(key))
		//			return false;
		//		logger.info("Decrease "+key);
		//		AttributeValue val = model.getAttribute(key);
		//		val.setPoints(val.getPoints()-1);
		//
		//		parent.runProcessors();
		//		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		return canBeIncreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		return canBeDecreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean increase(AttributeValue value) {
		return increase(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean decrease(AttributeValue value) {
		return decrease(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//--------------------------------------------------------------------
	private void dump(Level level, String message) {
		logger.log(level, message);
		List<AttributeValue> allValues = new ArrayList<>();
		for (Attribute key : Attribute.attributesToSave()) {
			allValues.add(model.getAttribute(key));
		}
		Map<Attribute,PerAttributePoints> perAttrib = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib;
		for (AttributeValue val : allValues) {
			PerAttributePoints payed = perAttrib.get(val.getAttribute());
			if (payed!=null)
				logger.log(level, String.format("  %9s(%d): %d base  ", val.getAttribute().name(), val.getPoints(), payed.base)+payed.adjust+" adjustment, "+payed.regular+" regular, "+payed.karma+" with karma ("+payed.getKarmaInvest()+" Karma)  "+payed.listCost);
			else
				logger.log(level, String.format("  %9s(%d):  %d mod", val.getAttribute().name(), val.getPoints(), val.getGenerationModifier()));
		}

	}

	//--------------------------------------------------------------------
	private void ensureFilledPerAttrib() {
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per==null) {
				model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.put(key, new PerAttributePoints());
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * Validate that the maximum value of each attribute is not exceeded.
	 * If it should be the case, reduce in the following order:
	 * - Ratings gained from Karma
	 * - Ratings gained from attribute points
	 * - Ratings gained from adjustment points
	 */
	private void ensureMaximumNotExceeded() {
		for (Attribute key : Attribute.specialAttributes()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per!=null) {
				while (per.karma>0 && per.getSum()>getMaximumValue(key))
					per.karma--;
				while (per.regular>0 && per.getSum()>getMaximumValue(key))
					per.regular--;
				while (per.adjust>0 && per.getSum()>getMaximumValue(key))
					per.adjust--;
			} 
		}
	}

	//--------------------------------------------------------------------
	/**
	 * Validate that no more adjustment points than allowed are distributed.
	 * If necessary remove points
	 */
	private void validateAdjustmentPoints() {
		for (Attribute key : Attribute.specialAttributes()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per!=null && per.adjust>0) {
				while (per.karma>0 && per.getSum()>getMaximumValue(key))
					per.karma--;
				if (per.adjust<=pointsSpecial) {
					pointsSpecial -= per.adjust;
				} else {
					logger.info("Reduce adjustment points for "+key+" to remaining "+pointsSpecial);
					per.adjust = pointsSpecial;
					pointsSpecial = 0;
				}
			}
		}
		for (Attribute key : Attribute.primaryValues()) {
			if (!metatypeAttribute.contains(key))
				continue;
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per.adjust>0) {
				if (per.adjust<=pointsSpecial) {
					pointsSpecial -= per.adjust;
				} else {
					logger.info("Reduce adjustment points for "+key+" to remaining "+pointsSpecial);
					per.adjust = pointsSpecial;
					pointsSpecial = 0;
				}
			}
		}

	}

	//--------------------------------------------------------------------
	/**
	 * Validate that no more attribute points than allowed are distributed.
	 * If necessary remove points
	 */
	private void validateAttributePoints() {
		for (Attribute key : Attribute.primaryValues()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			if (per.regular>0) {
				if (per.regular<=pointsAllowed) {
					pointsAllowed -= per.regular;
				} else {
					logger.warn("Reduce attribute points for "+key+" to remaining "+pointsAllowed);
					per.regular = pointsAllowed;
					pointsAllowed = 0;
				}
			}
		}

	}

	//--------------------------------------------------------------------
	/**
	 * Validate that no more attribute points than allowed are distributed.
	 * If necessary remove points
	 */
	private void validateKarma() {
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);						
			if (per.karma>0) {
				model.setKarmaFree(model.getKarmaFree() - per.getKarmaInvest());
				logger.info("Pay "+per.getKarmaInvest()+" Karma    / per.karma="+per.karma+"     getKarmaFree()="+model.getKarmaFree());
			}
		}
	}

	//--------------------------------------------------------------------
	private void copyResultsToAttributes() {
		for (Attribute key : Attribute.attributesToSave()) {
			PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
			AttributeValue aVal = model.getAttribute(key);
			if (per!=null) {
				if (Arrays.asList(Attribute.primaryAndSpecialValues()).contains(key))
					aVal.setPoints(per.getSum() - aVal.getGenerationModifier());
				else
					aVal.setPoints(per.getSum());
				logger.debug("Set "+key+" to "+aVal.getPoints()+" + "+aVal.getGenerationModifier());
			} else {
				logger.debug("Keep "+key+" at "+aVal.getPoints()+" + "+aVal.getGenerationModifier());
			}
		}
		
		// Set maximum power points to current magic
		logger.info("Set maximum POWER_POINTS to "+model.getAttribute(Attribute.MAGIC).getModifiedValue());
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(ModificationValueType.MAX, Attribute.POWER_POINTS, model.getAttribute(Attribute.MAGIC).getModifiedValue()));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		try {
			metatypeAttribute.clear();
			Map<Attribute,PerAttributePoints> perAttrib = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib;
//			logger.warn("perAttrib = "+perAttrib);

			// Clear all attribute modifications in character
			pointsAllowed = 0;
			pointsSpecial = 0;
			todos.clear();
			// Prepare calculation matrix. Regular attributes and edge start with 1
			ensureFilledPerAttrib();
			for (Attribute key : Attribute.primaryAndSpecialValues()) {
				if (key.isPrimary() || key==Attribute.EDGE)
					perAttrib.get(key).base=1;
				else
					perAttrib.get(key).base=0;
				// Set base from modifier
				//				logger.info("Mods ? "+model.getAttribute(key).getModifications());
				for (Modification mod : model.getAttribute(key).getModifications()) {
					if (mod instanceof AttributeModification) {
						AttributeModification aMod = (AttributeModification)mod;
						if (aMod.getType()==ModificationValueType.NATURAL) {
							perAttrib.get(key).base+= aMod.getValue();
							logger.info("set base of "+key+" to "+perAttrib.get(key).base);
						}
					}
				}
			}

			// Apply attribute modifications
			if (!unitTestMods.isEmpty()) {
				logger.info("Add unit-test modifications");
				previous.addAll(0, unitTestMods);
			}
			
			for (Modification _mod : previous) {
				if (_mod instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)_mod;
					AttributeValue val = model.getAttribute(mod.getAttribute());
					if (mod.getType()==ModificationValueType.NATURAL && Arrays.asList(Attribute.primaryAndSpecialValues()).contains(mod.getAttribute())) {
						perAttrib.get(val.getAttribute()).base+=mod.getValue();
						logger.info("set base of "+mod.getAttribute()+" to "+perAttrib.get(mod.getAttribute()).base);
					} 
					logger.debug("  Add "+mod);
					val.addModification(mod);
					if (mod.getType()==ModificationValueType.MAX && mod.getValue()>6)
						metatypeAttribute.add(mod.getAttribute());
				} else if (_mod instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)_mod;
					logger.debug("Found "+mod);
					if (mod.getType()==Type.ATTRIBUTES) {
						pointsAllowed = mod.getCount();
					} else if (mod.getType()==Type.ADJUSTMENT_POINTS) {
						pointsSpecial = mod.getCount();
					} else {
						unprocessed.add(_mod);						
					}
				} else
					unprocessed.add(_mod);
			}
			logger.debug("  Metatype attributes are: "+metatypeAttribute);

			/*
			 * Only allow resonance or magic for specific types
			 */
			if (model.getMagicOrResonanceType()!=null) {
				if (!model.getMagicOrResonanceType().usesMagic()) {
					model.getAttribute(Attribute.MAGIC).setPoints(0);
				}
				if (!model.getMagicOrResonanceType().usesResonance()) {
					model.getAttribute(Attribute.RESONANCE).setPoints(0);
				}
			}

			// Ensure maximum and minimum values
			boolean alreadyAnAttributeMaxed = false;
			for (Attribute key : Attribute.primaryAndSpecialValues()) {
				AttributeValue val = model.getAttribute(key);
				if (val.getGenerationValue()>=val.getMaximum() && val.getMaximum()>1) {
					if (alreadyAnAttributeMaxed) {
						int maxAllow = val.getMaximum()-val.getModifier()-1;
						logger.warn("Reduce points for "+key+" to "+maxAllow+" since another attribute was already maxed");
						val.setPoints(maxAllow);
					} else {
						alreadyAnAttributeMaxed = key.isPrimary();
						while (val.getGenerationValue()>val.getMaximum()) {
							val.setPoints(val.getPoints()-1);
							logger.warn("Reduce points for "+key+" since attribute was above maximum");
						}
					}
				}
			}

			ensureMaximumNotExceeded();
			validateAdjustmentPoints();
			validateAttributePoints();
			int karmaBefore = model.getKarmaFree();
			validateKarma();
			copyResultsToAttributes();

			int magicBoughtWithKarma = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(Attribute.MAGIC).karma;
			boolean isRegularAdept = model.getMagicOrResonanceType().getId().equals("adept");
			boolean isMysticAdept = model.getMagicOrResonanceType().getId().equals("mysticadept");
			boolean isMysticAdeptGainsPPOnKarmaForMagic = isMysticAdept && SR6ConfigOptions.MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP.getValue();
			if (magicBoughtWithKarma>0 && model.getMagicOrResonanceType().usesPowers() && (isRegularAdept || isMysticAdeptGainsPPOnKarmaForMagic)) {
				logger.info("Add "+magicBoughtWithKarma+" power points for MAGIC bought with karma");
				AttributeModification aMod = new AttributeModification(ModificationValueType.NATURAL, Attribute.POWER_POINTS, magicBoughtWithKarma, ModificationType.RELATIVE, Attribute.MAGIC);
				aMod.setExpCost(perAttrib.get(Attribute.MAGIC).getKarmaInvest());
				aMod.setSource(Attribute.MAGIC);
				AttributeValue aVal = model.getAttribute(Attribute.POWER_POINTS); 
				aVal.addModification(aMod);
			}


			/*
			 * Log output
			 */
			if (logger.isDebugEnabled())
				dump(Level.DEBUG, "Final\n=============================");

			logger.info("  free normal: "+pointsAllowed+"/"+getPointsLeft()+"    free special: "+pointsSpecial);
			logger.debug("  karmaBefore: "+karmaBefore+"    model.getKarmaFree: "+model.getKarmaFree());
			//			investedKarma = (karmaBefore - model.getKarmaFree());
			logger.info(" invested "+(karmaBefore - model.getKarmaFree())+" karma for attributes");
			if (pointsAllowed>0) {
				todos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.points.attribute"), pointsAllowed)));
			}
			if (pointsAllowed<0) {
				todos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.reduce.attribute"), Math.abs(pointsAllowed))));
			}
			if (pointsSpecial>3) {
				todos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.points.adjustment"), pointsSpecial)));
			} else if (pointsSpecial>0) {
				todos.add(new ToDoElement(Severity.WARNING, String.format(Resource.get(RES, "priogen.todo.points.adjustment"), pointsSpecial)));
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#isRacialAttribute(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean isRacialAttribute(Attribute key) {
		return metatypeAttribute.contains(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.PriorityAttributeController#canDecreaseAdjust(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canDecreaseAdjust(Attribute key) {
		return model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).adjust>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.PriorityAttributeController#canIncreaseAdjust(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canIncreaseAdjust(Attribute key) {
		if (pointsSpecial<1) {
			logger.trace("Cannot increase "+key+" because no adjustment points");
			return false;
		}

		if (key==Attribute.RESONANCE && !model.getMagicOrResonanceType().usesResonance()) {
			logger.trace("Cannot increase "+key+" because resonance not used");
			return false;
		}

		if (key==Attribute.MAGIC && !model.getMagicOrResonanceType().usesMagic()) {
			logger.trace("Cannot increase "+key+" because magic not used");
			return false;
		}

		if (key.isPrimary() && !metatypeAttribute.contains(key)) {
			logger.trace("Cannot increase "+key+" because not a metatype attribute");
			return false;
		}

		if (isAnotherAttributeAlreadyMaxed(key)) {
			logger.trace("Cannot increase "+key+" because another attribute is already maxed: "+getMaximizedAttributes());
			return false;
		}

		PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
		if (per.getSum()>=getMaximumValue(key)) {
			logger.debug("Cannot increase "+key+" because attribute "+per.getSum()+" is already higher than maximum "+getMaximumValue(key));
		}
		return per.getSum()<getMaximumValue(key);
	}

	//-------------------------------------------------------------------
	@Override
	public boolean increaseAdjust(Attribute key) {
		if (!canIncreaseAdjust(key))
			return false;
		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).adjust++;
		logger.info("Increased "+key+" with adjustment points");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean decreaseAdjust(Attribute key) {
		if (!canDecreaseAdjust(key))
			return false;

		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).adjust--;
		logger.info("Decreased "+key+" with adjustment points");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.PriorityAttributeController#canDecreaseAttrib(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canDecreaseAttrib(Attribute key) {
		return model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).regular>0;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean canIncreaseAttrib(Attribute key) {
		if (pointsAllowed<1)
			return false;

		if (key.isSpecial()) {
			return false;
		}

		if (isAnotherAttributeAlreadyMaxed(key))
			return false;

		PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
		return per.getSum()<getMaximumValue(key);
	}

	//-------------------------------------------------------------------
	@Override
	public boolean increaseAttrib(Attribute key) {
		if (!canIncreaseAttrib(key))
			return false;
		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).regular++;
		logger.info("Increased "+key+" with attribute points");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean decreaseAttrib(Attribute key) {
		if (!canDecreaseAttrib(key))
			return false;

		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).regular--;
		logger.info("Decreased "+key+" with attribute points");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.PriorityAttributeController#canDecreaseKarma(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canDecreaseKarma(Attribute key) {
		return model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).karma>0;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean canIncreaseKarma(Attribute key) {
		if (key==Attribute.RESONANCE && !model.getMagicOrResonanceType().usesResonance())
			return false;
		if (key==Attribute.MAGIC && !model.getMagicOrResonanceType().usesMagic()) {
			return false;
		}

		PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key);
		if (per.getSum()>=getMaximumValue(key))
			return false;

		if (isAnotherAttributeAlreadyMaxed(key))
			return false;
		
		int requiredKarma = (per.getSum()+1)*5;
		if (model.getKarmaFree()<requiredKarma) {
			return false;
		}
		return true;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean increaseKarma(Attribute key) {
		if (!canIncreaseKarma(key))
			return false;
		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).karma++;
		logger.info("Increased "+key+" with karma");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean decreaseKarma(Attribute key) {
		if (!canDecreaseKarma(key))
			return false;

		model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(key).karma--;
		logger.info("Decreased "+key+" with karma");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	public void setUnitTestMods(List<Modification> unitTestMods) {
		this.unitTestMods = unitTestMods;
	}
}
