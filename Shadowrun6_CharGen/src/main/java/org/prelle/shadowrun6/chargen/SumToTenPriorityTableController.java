package org.prelle.shadowrun6.chargen;

import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.gen.CharacterGenerator;

/**
 * @author Stefan Prelle
 *
 */
public class SumToTenPriorityTableController extends PriorityTableController {

	//--------------------------------------------------------------------
	public SumToTenPriorityTableController(CharacterGenerator parent) {
		super(parent);
	}

	//--------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		logger.warn("setPriority("+option+" = "+prio+")");
		
		int invested = Math.max(0,4-prio.ordinal());
		settings.priorities.put(option, prio);
		
		Priority oldPriority = null;
		PriorityType oldOption = null;
		for (PriorityType opt : PriorityType.values()) {
			if (option==opt)
				continue;
			Priority tmpPrio = getPriority(opt);
			int toAdd = Math.max(0,4-tmpPrio.ordinal());
			int allow = 10 - invested;
			if (toAdd<=allow) {
				// No change necessary
				invested += toAdd;
			} else {
				// Priority must be reduced
				oldPriority = tmpPrio;
				oldOption   = opt;
				settings.priorities.put(opt, Priority.values()[5-(allow+1)]);
				invested += allow;
			}
		}
		logger.info("Set "+option+" to "+prio+" and change "+oldOption+" to "+oldPriority);

		// If points remain, distribute them
		while (invested<10) {
			inner:
			for (int i=4; i>=0; i--) {
				PriorityType tmpType = PriorityType.values()[i];
				if (tmpType==option)
					continue;
				Priority tmpPrio = getPriority(tmpType);
				if (tmpPrio.ordinal()>0) {
					Priority setTo = Priority.values()[tmpPrio.ordinal()-1];
					logger.info("Priority points left. Set "+tmpType+" from "+settings.priorities.get(tmpType)+" to "+setTo);
					settings.priorities.put(tmpType, setTo);
					invested++;
					break inner;
				}
			}
		}
		
		parent.runProcessors();
	}

}
