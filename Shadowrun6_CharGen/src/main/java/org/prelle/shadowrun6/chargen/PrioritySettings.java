package org.prelle.shadowrun6.chargen;

import java.util.HashMap;
import java.util.Map;

import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.charctrl.AttributeController.PerAttributePoints;

/**
 * @author Stefan Prelle
 *
 */
public class PrioritySettings {

	public PriorityVariant variant;
	
//	Map<Priority,PriorityType> priorities = new HashMap<Priority, PriorityType>();
	public Map<PriorityType, Priority> priorities = new HashMap<PriorityType, Priority>();
	public int mysticAdeptMaxPoints;
	public int mysticAdeptPowerPoints;
	/**
	 * Karma points converted to Nuyen
	 */
	public int usedKarma;
	public Map<Attribute, PerAttributePoints> perAttrib = new HashMap<>();
	
	/** Modifier to apply to customization karma */
	public int karmaMod;
	
}
