/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.chargen.proc.LatePlausabilityCheck;
import org.prelle.shadowrun6.common.CommonLifestyleController;
import org.prelle.shadowrun6.common.FocusGenerator;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.gen.WizardPageType;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.proc.CalculateDerivedAttributes;
import org.prelle.shadowrun6.proc.CalculateEssence;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import com.google.gson.Gson;

import de.rpgframework.ConfigChangeListener;
import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;

/**
 * @author prelle
 *
 */
public class NewPriorityCharacterGenerator extends CommonSR6CharacterGenerator {
	
	private enum Mode {
		FINISHED,
		CREATION,
		TUNING
	}
	
	protected final static Logger logger = LogManager.getLogger("shadowrun6.gen");

	private Mode mode;
	
	private boolean dontProcess;
	private boolean recalcuateHasEnoughData;
	private boolean hasEnoughData;
	private boolean setupDone;
	
	private PriorityTableController prioCtrl;
	
	//-------------------------------------------------------------------
	public NewPriorityCharacterGenerator() {
		super();
		
		mode = Mode.FINISHED;
		i18n = ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName());
		i18nHelp = ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName());
	}

	//-------------------------------------------------------------------
	public String toString() {
		return super.toString()+"/"+model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getID()
	 */
	@Override
	public String getID() {
		return "prio";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		cfgShadowrun.addListener(new ConfigChangeListener() {
			public void configChanged(ConfigContainer source, Collection<ConfigOption<?>> options) {
				logger.info("Changed config option "+source.getLocalId()+" in "+this);
				if (mode!=Mode.FINISHED)
					runProcessors();
			}
		});
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		if (dontProcess)
			return;
		
		try {
			dontProcess = true;
			model.setKarmaFree(50);
			logger.debug("Variant = "+model.getTemporaryChargenSettings(PrioritySettings.class).variant);
			if (model.getTemporaryChargenSettings(PrioritySettings.class).variant==PriorityVariant.PRIME_RUNNER)
				model.setKarmaFree(100);
			model.setKarmaInvested(0);
			model.setNuyen(0);
			model.setEssence(0f);
			logger.debug("runProcessors----------------------------------");
			super.runProcessors();
			recalcuateHasEnoughData = true;
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model ));
		} finally {
			dontProcess = false;
		}
	}

	//-------------------------------------------------------------------
	public Priority getPriority(PriorityType option) {
		return prioCtrl.getPriority(option);
	}

	//-------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		logger.info("Set "+option+" to priority "+prio+" in Gen "+this);
		prioCtrl.setPriority(option, prio);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "chargen.priority.page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "chargen.priority.desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return "No i18n resources for "+this.getClass();
		return Resource.get(i18n,"chargen.priority.title");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
				WizardPageType.PRIORITIES,
				WizardPageType.METATYPE,
				WizardPageType.MAGIC_OR_RESONANCE,
				WizardPageType.QUALITIES,
				WizardPageType.ATTRIBUTES,
				WizardPageType.SKILLS,
				WizardPageType.SPELLS,
				WizardPageType.ALCHEMY,
				WizardPageType.RITUALS,
				WizardPageType.POWERS,
				WizardPageType.COMPLEX_FORMS,
				WizardPageType.NAME,
		};
	}

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model==null)
			return ret;
		
		if (model.getMetatype()==null) {
			ret.add(new ToDoElement(Severity.STOPPER, Resource.get(RES, "priogen.todo.no_metatype_selected")));
		}
		
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		ret.addAll(quality.getToDos());
		ret.addAll(equip.getToDos());
		ret.addAll(connections.getToDos());
		ret.addAll(lifestyles.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}
		ret.addAll(martialArts.getToDos());
		ret.addAll(nuyen.getToDos());

		return ret;
	}

	//-------------------------------------------------------------------
	private List<ToDoElement> getToDosWithoutMoney() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model==null)
			return ret;
		
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		ret.addAll(quality.getToDos());
		ret.addAll(connections.getToDos());
		ret.addAll(lifestyles.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}
		ret.addAll(nuyen.getToDos());
		ret.addAll(martialArts.getToDos());

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		if (!recalcuateHasEnoughData)
			return hasEnoughData;
		
		List<ToDoElement> ret = getToDos();
		
		boolean noStopper = true;
		for (ToDoElement todo : ret) {
			if (todo.getSeverity()==Severity.STOPPER)
				noStopper=false;
		}
		
		logger.info("hasEnoughData = "+noStopper+"    mode="+mode);
		logger.info("ToDos = "+ret);
		
		hasEnoughData = noStopper;
		recalcuateHasEnoughData = false;
		return noStopper;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughDataIgnoreMoney()
	 */
	@Override
	public boolean hasEnoughDataIgnoreMoney() {
		List<ToDoElement> ret = getToDosWithoutMoney();
		
		boolean noStopper = true;
		for (ToDoElement todo : ret) {
			if (todo.getSeverity()==Severity.STOPPER)
				noStopper=false;
		}
		
		return noStopper;
	}

	//--------------------------------------------------------------------
	protected PriorityTableController getPriorityTableController() {
		return new PriorityTableController(this);
	}

	//--------------------------------------------------------------------
	private void setupProcessChain() {
		if (setupDone) {
			System.err.println("NewPriorityCharacterGenerator.setupProcessChain: already set up");
			return;
		}
		
		prioCtrl = getPriorityTableController();
		magOrRes = new PriorityMagicOrResonanceController(this);
		meta     = new PriorityMetatypeController(this);
		attrib   = new PriorityAttributeGenerator(this);
		skill    = new PrioritySkillController(this);
//		((PrioritySkillController)skill).getCostCalculator().setKarmaOptimization(OPTIMIZE_BUILD_POINTS);
		spell    = new PrioritySpellController(this);
		alchemy  = spell;
		ritual   = (PrioritySpellController)spell;
		quality  = new CommonQualityGenerator(this, CharGenMode.CREATING);
		power    = new CommonPowerGenerator(this);
		cforms   = new PriorityComplexFormController(this);
		equip    = new PriorityEquipmentController(this);
		connections = new PriorityConnectionController(this);
		sins        = new CommonSINController(this);
		lifestyles  = new LifestyleGenerator(this);
//		summonables = new KarmaSummonableController(this);
		metaEchoes  = new CommonMetamagicOrEchoGenerator(this);
		foci        = new FocusGenerator(this);
		martialArts = new MartialArtsGenerator(this);
		
		processChain.clear();
		processChain.addAll(ShadowrunTools.RECALCULATE_STEPS);
		// Remove CalculateDerivedAttributes to be added later
		for (CharacterProcessor step : new ArrayList<CharacterProcessor>(processChain)) {
			if (step.getClass()==CalculateDerivedAttributes.class) {
				processChain.remove(step);
			}
		}
		processChain.add( prioCtrl);
		processChain.add( new ApplyKarmaModifications());
		processChain.add( (PriorityMetatypeController) meta );
		processChain.add( (PriorityMagicOrResonanceController)magOrRes ); // Get special mods from priority option
		processChain.add( (CharacterProcessor)quality ); 
		processChain.add( (CharacterProcessor) attrib );
		processChain.add( (CharacterProcessor) connections );  
		processChain.add( (PrioritySkillController)skill );
		processChain.add( (CharacterProcessor) metaEchoes );
		processChain.add( (CommonSINController)sins );
		processChain.add( (CommonLifestyleController)lifestyles );
		processChain.add( martialArts );
		processChain.add( (CharacterProcessor) equip);
		processChain.add( (CharacterProcessor) power );
		processChain.add( (CharacterProcessor) spell );
		processChain.add( (CharacterProcessor) cforms );
		processChain.add( (CharacterProcessor) foci );
		processChain.add( nuyen);
		processChain.add( new LatePlausabilityCheck(this));
		processChain.add( new CalculateDerivedAttributes());
		
		setupDone = true;
	}
	
	//-------------------------------------------------------------------
	public void saveCreation() throws IOException {
//		System.err.println("Save PrioSettings "+model.getTemporaryChargenSettings(PrioritySettings.class)+" of "+model);
//		System.err.println("saveCreation "+ (new Gson()).toJson(model.getTemporaryChargenSettings(PrioritySettings.class)) );
		model.setChargenSettings( (new Gson()).toJson(model.getTemporaryChargenSettings(PrioritySettings.class)) );
		model.setGenerationMode(true);
		super.saveCreation();
	}

	//-------------------------------------------------------------------
	private String applyOldGenesisFix(String json) {
		Pattern pattern = Pattern.compile(".*(\".\"):(\"[A-Z]*\").*");
		boolean keepGoing = true;
		do {
			Matcher matcher = pattern.matcher(json);
			if (matcher.matches()) {
				String toReplace = matcher.group(1)+":"+matcher.group(2);
				String replaceWith = matcher.group(2)+":"+matcher.group(1);
				json = json.replace(toReplace, replaceWith);
			} else
				keepGoing = false;
		} while (keepGoing);
		
		return json;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#continueCreation(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void continueCreation(ShadowrunCharacter model) {
		mode = Mode.CREATION;
		this.model = model;
		logger.warn("START: continueCreation "+super.toString());

		String json = applyOldGenesisFix(model.getChargenSettings());
		PrioritySettings settings = (new Gson()).fromJson(json, PrioritySettings.class);
		model.setTemporaryChargenSettings(settings);
		logger.warn("PrioSettings= "+settings.perAttrib);
		
		setupProcessChain();
		prioCtrl.setToSave(settings.priorities);
		runProcessors();
		logger.debug("STOP : continueCreation");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#start(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		mode = Mode.CREATION;
		this.model = model;
		this.setupDone = false;
		PrioritySettings settings = new PrioritySettings();
		settings.variant = PriorityVariant.STANDARD;
		settings.priorities = new HashMap<PriorityType,Priority>();
		settings.priorities.put(PriorityType.METATYPE , Priority.B);
		settings.priorities.put(PriorityType.ATTRIBUTE, Priority.A);
		settings.priorities.put(PriorityType.MAGIC    , Priority.C);
		settings.priorities.put(PriorityType.SKILLS   , Priority.D);
		settings.priorities.put(PriorityType.RESOURCES, Priority.E);
		model.setChargenUsed(getID());
		model.setTemporaryChargenSettings(settings);
		model.setKarmaFree(50);
		logger.info("----------------Start generator-----------------------"+toString()+"\n\n\n");
		
		setupProcessChain();

//		dontProcess = true;
//		setPriority(PriorityType.METATYPE , Priority.B);
//		setPriority(PriorityType.ATTRIBUTE, Priority.A);
//		setPriority(PriorityType.MAGIC    , Priority.C);
//		setPriority(PriorityType.SKILLS   , Priority.D);
//		setPriority(PriorityType.RESOURCES, Priority.E);
//		dontProcess = false;

//		((MetatypeGenerator)meta).updateAvailable();
		runProcessors();
		logger.debug("start done");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		logger.info("Stop generation\n\nFinalizing character creation now\n\n");

		if (model==null)
			return;

		model.setChargenSettings( (new Gson()).toJson(model.getTemporaryChargenSettings(PrioritySettings.class)) );
		model.setGenerationMode(false);
		
		
		/*
		 * Fix attributes
		 */
		logger.debug("Sum up attribute values before finalizing");
		for (Attribute key : Attribute.values()) {
			if (key.isPrimary() || key.isSpecial()) {
				AttributeValue aVal = model.getAttribute(key);
				aVal.setStart(aVal.getGenerationValue());
				aVal.setPoints(aVal.getGenerationValue());
				aVal.clearModifications();
				logger.info("..."+aVal);
			}
		}

		/*
		 * Fix skills
		 */
		logger.debug("Sum up skill values before finalizing");
		for (SkillValue val : model.getSkillValues()) {
			int modified = val.getPoints();
			val.clearModifications();
			val.setPoints(modified);
			val.setStart(modified);
		}

		// Start-Nuyen
		int nuyen= Math.min(5000, model.getNuyen());
		model.setNuyen(nuyen);

		model.setKarmaInvested(0);
		if (SR6ConfigOptions.THIRD_PRINTING_ERRATA.getValue()) {
			model.setKarmaFree(Math.min(5, model.getKarmaFree()));
		} else {
			model.setKarmaFree(0);
		}
		
//		Reward creationReward = new RewardImpl(0, RES.getString("reward.creation"));
//		creationReward.addModification(new AddNuyenModification(nuyen));
//		model.addReward(creationReward);
		
		mode = Mode.FINISHED;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return "priority";
	}

	//-------------------------------------------------------------------
	public void setVariant(PriorityVariant value) {
		if (value!=null) {
			logger.info("Set generation variant to "+value);
			model.getTemporaryChargenSettings(PrioritySettings.class).variant = value;
			
			runProcessors();
		}
	}

	//-------------------------------------------------------------------
	public PriorityVariant getVariant() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).variant;
	}

	//-------------------------------------------------------------------
	public void setKarmaModifier(int value) {
		logger.info("Set karma modifier to "+value);
		model.getTemporaryChargenSettings(PrioritySettings.class).karmaMod = value;
		
		runProcessors();
	}

}
