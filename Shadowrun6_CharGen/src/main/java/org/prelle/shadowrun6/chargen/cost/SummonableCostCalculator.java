/**
 * 
 */
package org.prelle.shadowrun6.chargen.cost;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.SummonableValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SummonableCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	//-------------------------------------------------------------------
	public SummonableCostCalculator() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {

			// Walk through all SINs - ignore real SINs / criminals
			for (SummonableValue tmp : model.getSummonables()) {
				logger.debug("Pay "+tmp.getServices()+" karma for "+tmp);
				model.setKarmaFree( model.getKarmaFree() - tmp.getServices() );
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
