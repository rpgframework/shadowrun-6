/**
 *
 */
package org.prelle.shadowrun6.gen.event;

/**
 * @author prelle
 *
 */
public enum GenerationEventType {
	
	/** Character data has changed */
	CHARACTER_CHANGED,

	/** Name, Gender, Weight, Size or colors changed. Value is NULL*/
	BASE_DATA_CHANGED,

	/** Key is Attribute, Value is AttributeValue */
	ATTRIBUTE_CHANGED,
	/** Key is special attributes, value is an Integer */
	POINTS_LEFT_ATTRIBUTES,
	/** Key is an Integer */
	POINTS_LEFT_CONNECTIONS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_SKILLS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_KNOWLEDGE_SKILLS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_SKILLGROUPS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_SPELLS_RITUALS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_QUALITIES,
	/** Key is null, value is an Double */
	POINTS_LEFT_POWERS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_COMPLEX_FORMS,

	/** Key is Construction Kit */
	CONSTRUCTIONKIT_CHANGED,

	/** Fired when underlying controller classes where changed */
	CONTROLLER_CHANGED,

	/** Key is the connection */
	CONNECTION_ADDED,
	/** Key is the connection */
	CONNECTION_CHANGED,
	/** Key is the connection */
	CONNECTION_REMOVED,

	/** Key is Character concept */
	CHARACTERCONCEPT_ADDED,
	/** Key is Character concept */
	CHARACTERCONCEPT_REMOVED,

	/** Key is ComplexFormValue */
	COMPLEX_FORM_ADDED,
	/** Key is ComplexFormValue */
	COMPLEX_FORM_CHANGED,
	/** Key is ComplexFormValue */
	COMPLEX_FORM_REMOVED,
	/** Key is a list of ComplexForms */
	COMPLEX_FORMS_AVAILABLE_CHANGED,

	/** Key is null, Value is Array [ExpFree, ExpInvested] */
	EXPERIENCE_CHANGED,
	/** Key is used essence, Value is unused essence */
	ESSENCE_CHANGED,

	/** Key is LicenseValue */
	LICENSE_ADDED,
	/** Key is LicenseValue */
	LICENSE_CHANGED,
	/** Key is LicenseValue */
	LICENSE_REMOVED,

	/** Key is LifestyeValue */
	LIFESTYLE_ADDED,
	/** Key is LifestyeValue */
	LIFESTYLE_CHANGED,
	/** Key is LifestyeValue */
	LIFESTYLE_REMOVED,

	/** Key is List of MagicOrResonanceOptions */
	MAGICORRESONANCE_AVAILABLE_CHANGED,
	/** Key is the MagicOrResonanceOption */
	MAGICORRESONANCE_CHANGED,

	/** Key is List of MetaTypeOptions, value is List of MetaTypes */
	METATYPES_AVAILABLE_CHANGED,
	/** Key is the metatype or metavariant, value is special attribute points */
	METATYPE_CHANGED,

	/**  */
	NUYEN_RECALC_NECESSARY,
	/** Key=Available, Value=Invested */
	NUYEN_CHANGED,

	/** Key is PowerValue */
	POWER_ADDED,
	/** Key is PowerValue */
	POWER_CHANGED,
	/** Key is PowerValue */
	POWER_REMOVED,
	/** Key is a list of Powers */
	POWERS_AVAILABLE_CHANGED,

	/** Key is PriorityType, value is an Integer */
	PRIORITY_CHANGED,

	/** Key is SIN */
	SIN_ADDED,
	/** Key is SIN */
	SIN_CHANGED,
	/** Key is SIN */
	SIN_REMOVED,

	/** Key is SkillValue */
	SKILL_ADDED,
	/** Key is SkillValue */
	SKILL_CHANGED,
	/** Key is SkillValue */
	SKILL_REMOVED,
	/** Key is a list of Skill, Value a List of SkillGroup */
	SKILLS_AVAILABLE_CHANGED,
	/** Key is SkillGroupValue */
	SKILLGROUP_ADDED,
	/** Key is SkillGroupValue */
	SKILLGROUP_CHANGED,
	/** Key is SkillGroupValue */
	SKILLGROUP_REMOVED,

	/** Key is SpellValue */
	SPELL_ADDED,
	/** Key is SpellValue */
	SPELL_CHANGED,
	/** Key is SpellValue */
	SPELL_REMOVED,
	/** Key is a list of Spells, Value list of alchemy spells*/
	SPELLS_AVAILABLE_CHANGED,

	/** Key is RitualValue */
	RITUAL_ADDED,
	/** Key is RitualValue */
	RITUAL_CHANGED,
	/** Key is RitualValue */
	RITUAL_REMOVED,
	/** Key is a list of Rituals */
	RITUALS_AVAILABLE_CHANGED,

	/** Key is QualityValue */
	QUALITY_ADDED,
	/** Key is QualityValue */
	QUALITY_CHANGED,
	/** Key is QualityValue */
	QUALITY_REMOVED,
	/** Key is a list of Qualities */
	QUALITY_AVAILABLE_CHANGED,

	/** Key is SummonableValue */
	SUMMONABLE_ADDED,
	/** Key is SummonableValue */
	SUMMONABLE_CHANGED,
	/** Key is SummonableValue */
	SUMMONABLE_REMOVED,
	/** Key is List of Summonable */
	SUMMONABLES_AVAILABLE_CHANGED,

	/** Key is the Tradition */
	TRADITION_CHANGED,

	/** Key is CarriedItem */
	EQUIPMENT_ADDED,
	/** Key is CarriedItem */
	EQUIPMENT_CHANGED,
	/** Key is CarriedItem */
	EQUIPMENT_REMOVED,
	/** Key is a list of ItemTemplates */
	EQUIPMENT_AVAILABLE_CHANGED,
	
	NOTES_CHANGED,
	
	FINISH_REQUESTED

}
