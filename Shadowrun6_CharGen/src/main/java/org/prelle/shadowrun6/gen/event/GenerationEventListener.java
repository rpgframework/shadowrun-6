/**
 * 
 */
package org.prelle.shadowrun6.gen.event;

/**
 * @author prelle
 *
 */
public interface GenerationEventListener {

	//-------------------------------------------------------------------
	public void handleGenerationEvent(GenerationEvent event);
	
}
