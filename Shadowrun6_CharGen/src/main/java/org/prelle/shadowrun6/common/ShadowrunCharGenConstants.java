/**
 * 
 */
package org.prelle.shadowrun6.common;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface ShadowrunCharGenConstants {

	public final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/shadowrun6/chargen/i18n/chargen");

}
