/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public interface FocusController extends Controller, CharacterProcessor {

	//--------------------------------------------------------------------
	/**
	 * You cannot bind more than MAGICx5 Force points
	 */
	public int getFocusPointsLeft();

	//-------------------------------------------------------------------
	public boolean canBeSelected(Focus data, int rating);

	//-------------------------------------------------------------------
	public boolean canBeSelected(Focus data, int rating, Object choice);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(FocusValue data);

	//-------------------------------------------------------------------
	public FocusValue select(Focus data, int rating);

	//-------------------------------------------------------------------
	public FocusValue select(Focus data, int rating, Object choice);

	//-------------------------------------------------------------------
	public boolean deselect(FocusValue data);

}
