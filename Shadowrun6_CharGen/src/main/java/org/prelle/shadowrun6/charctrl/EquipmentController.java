/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;
import java.util.Map;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.items.AmmunitionType;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.BodytechQuality;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;

/**
 * @author Stefan
 *
 */
public interface EquipmentController extends Controller {
	
	public enum SelectionOptionType {
		AMOUNT,
		BODYTECH_QUALITY,
		RATING,
		SKILL,
		PHYSICAL_SKILL,
		AMMOTYPE,
		NAME,
		WEAPON,
//		/** E.g. used for cyber implant weapons */
//		ACCESSORY,
		ITEMTYPE,
//		ITEMSUBTYPE,
		CHEAP_KNOCKOFF,
	}
	
	public class SelectionOption {
		SelectionOptionType type;
		Object value;
		public SelectionOption(SelectionOptionType type, Object val) {
			this.type = type;
			this.value= val;
		}
		public String toString() { return type+"="+value; }
		public void setValue(Object value) { this.value = value; }
		public void setType(SelectionOptionType type) { this.type = type; }
		public SelectionOptionType getType() { return type; }
		public int getAsAmount() { return (Integer)value; }
		public int getAsRating() { return (Integer)value; }
		public BodytechQuality getAsBodytechQuality() { return (BodytechQuality)value; }
		public Skill getAsSkill() { return (Skill)value; }
		public AmmunitionType getAsAmmoType() { return (AmmunitionType)value; }
		public String getAsName() { return (String)value; }
		public ItemTemplate getAsItem() { return (ItemTemplate)value; }
		public ItemType getAsItemType() { return (ItemType)value; }
//		public ItemSubType getAsItemSubType() { return (ItemSubType)value; }
		public boolean getAsCheapKnockOff() { return (Boolean)value; }
	}
	
	public enum EmbedOption {
		NOT_POSSIBLE,
		AFTER_REMOVING,
		POSSIBLE
	}

	//-------------------------------------------------------------------
	public List<SelectionOptionType> getOptions(ItemTemplate item, UseAs usage);

	//-------------------------------------------------------------------
	/**
	 * Calculate cost for buying an item
	 * @param item
	 * @param rating Item rating. Ignored for items without rating
	 * @param quality Bodytech quality. Ignored for non-bodytech
	 * @return Price in nuyen
	 */
	public int getCost(ItemTemplate item, UseAs usage, SelectionOption...options);

	//-------------------------------------------------------------------
	/**
	 * Calculate availability for an item
	 * @param item
	 * @param rating Item rating. Ignored for items without rating
	 * @param quality Bodytech quality. Ignored for non-bodytech
	 * @return 
	 */
	public Availability getAvailability(ItemTemplate item, UseAs usage, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean canBuyLevel(CarriedItem item, int newLevel);

	//-------------------------------------------------------------------
//	public boolean changeRating(CarriedItem item, int newRating);

	//-------------------------------------------------------------------
	public boolean canBuyQuality(CarriedItem item, BodytechQuality newQuality);

	//-------------------------------------------------------------------
	public boolean changeQuality(CarriedItem item, BodytechQuality newQuality);

	//-------------------------------------------------------------------
	/*
	 * TODO: take usage model into account - some items may have different costs
	 * depending on if they are used direct or as an accessory.
	 */
	public boolean canBePayed(ItemTemplate item, boolean asAccessory, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean canBeSelected(ItemTemplate item, UseAs useAs, SelectionOption...options);

	//-------------------------------------------------------------------
	public CarriedItem select(ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean deselect(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * This method sells the item for a factor of the value. In case of 
	 * bodytech, the essence hole is growing.
	 * @param ref Item to sell
	 * @param factor 0.5 = 50% 1.0=100%
	 */
	public boolean sell(CarriedItem ref, float factor);

	//-------------------------------------------------------------------
	public boolean canBeEmbedded(CarriedItem container, ItemTemplate item, ItemHook slot, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed);

	//-------------------------------------------------------------------
	public Map<ItemHook, EmbedOption> getEmbeddingSlotState(CarriedItem container, ItemTemplate n);

	//-------------------------------------------------------------------
//	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed, ItemHook slot);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embeddable in the given object
	 */
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embeddable in the given object
	 */
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref, ItemHook slot);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embedded in the given object
	 */
	public List<CarriedItem> getEmbeddedIn(CarriedItem ref);

	//-------------------------------------------------------------------
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	/**
	 * Use with items that can be embedded in multiple slots
	 */
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook slot, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean remove(CarriedItem container, CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all accessories available for the given object
	 */
	public List<ItemTemplate> getAvailableAccessoriesFor(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all enhancements available for the given object
	 */
	public List<ItemEnhancement> getAvailableEnhancementsFor(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all enhancements present in the object
	 */
	public List<ItemEnhancement> getEnhancementsIn(CarriedItem ref);

	//--------------------------------------------------------------------
	public boolean canBeModifiedWith(CarriedItem container, ItemEnhancement mod);

	//-------------------------------------------------------------------
	public ItemEnhancementValue modify(CarriedItem container, ItemEnhancement mod);

	//-------------------------------------------------------------------
	public boolean remove(CarriedItem container, ItemEnhancementValue mod);

	
	//-------------------------------------------------------------------
	public boolean increase(CarriedItem data);

	//-------------------------------------------------------------------
	public boolean decrease(CarriedItem data);

	//-------------------------------------------------------------------
	public void markUpdated(CarriedItem data);

	//-------------------------------------------------------------------
	public int getCountChangeCost(CarriedItem data, int newCount);

	//-------------------------------------------------------------------
	public boolean canChangeCount(CarriedItem data, int newCount);
	

	//--------------------------------------------------------------------
	public boolean canIncreaseBoughtNuyen();

	//--------------------------------------------------------------------
	public boolean canDecreaseBoughtNuyen();

	//--------------------------------------------------------------------
	public boolean increaseBoughtNuyen();

	//--------------------------------------------------------------------
	public boolean decreaseBoughtNuyen();

	//-------------------------------------------------------------------
	public int getBoughtNuyen();

	//-------------------------------------------------------------------
	public void markPrimary(CarriedItem ref);
	
}
