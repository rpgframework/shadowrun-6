/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.LifestyleValue;

/**
 * @author prelle
 *
 */
public interface LifestyleController extends Controller {

	public int getLifestyleCost(LifestyleValue lifestyle);
	
	public void addLifestyle(LifestyleValue toAdd);

	public void removeLifestyle(LifestyleValue lifestyle);
	
	public boolean canIncreaseMonths(LifestyleValue data);
	
	public boolean canDecreaseMonths(LifestyleValue data);
	
	public void increaseMonths(LifestyleValue data);
	
	public void decreaseMonths(LifestyleValue data);
	
}
