/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.MetaTypeOption;

/**
 * @author Stefan
 *
 */
public interface MetatypeController extends Controller {

	//--------------------------------------------------------------------
	/**
	 * Returns all metatypes that may be selected.
	 * @return
	 */
	public List<MetaTypeOption> getAvailable();
	
	//--------------------------------------------------------------------
	public int getKarmaCost(MetaType type);
	
	//--------------------------------------------------------------------
	public boolean canBeSelected(MetaType type);
	
	//--------------------------------------------------------------------
	public boolean select(MetaType value);
	
}
