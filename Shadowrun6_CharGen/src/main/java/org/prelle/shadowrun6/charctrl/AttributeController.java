package org.prelle.shadowrun6.charctrl;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.charctrl.AttributeController.RaiseCost.State;

import de.rpgframework.genericrpg.NumericalValueController;

public interface AttributeController extends Controller, NumericalValueController<Attribute,AttributeValue> {

	public enum ValueState {
		BELOW_MINIMUM,
		WITHIN_MODIFIER,
		UNSELECTED,
		SELECTED,
		UNATAINABLE_MAX,
		ABOVE_MAX
	}
	
	class RaiseCost {
		public enum State { UNPAID,REGULAR,ADJUST,KARMA }
		public Attribute key;
		public int adjust;
		public int attrib;
		public int karma;
		public State state = State.UNPAID;
		public RaiseCost(Attribute key) { this.key = key; }
		public String toString() { return key+":"+adjust+"/"+attrib+"/"+karma+":"+state; }
	}
	
	public class PerAttributePoints {
		public transient int base;
		public int adjust;
		public int regular;
		public int karma;
		public transient List<RaiseCost> listCost = new ArrayList<>();
		public int getSumBeforeKarma() {return base+adjust+regular;}
		public int getSum() {return base+adjust+regular+karma;}
		public int getKarmaInvest() {
			int start = base+adjust+regular;
			int invest = 0;
			for (int i=1; i<=karma; i++)
				invest+=(start+i)*5;
			return invest;
		}
		public RaiseCost getHighestPaid() {
			RaiseCost cost = null;
			for (int i=listCost.size()-1; i>=0; i--) {
				if (listCost.get(i).state==State.ADJUST || listCost.get(i).state==State.REGULAR)
					return listCost.get(i);
			}
			return cost;
		}
		public int getKarmaStart() {
			int i=0;
			for (RaiseCost cost : listCost) {
				i++;
				if (cost.state==State.KARMA)
					return i;
			}
			return 0;
		}
		public String toString() {return base+"/"+adjust+"/"+regular+"/"+karma+"|"+getKarmaInvest();}
	}

	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeft();

	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeftSpecial();

	//-------------------------------------------------------------------
	public PerAttributePoints getPerAttribute(Attribute key);

	//-------------------------------------------------------------------
	public ValueState getValueState(Attribute key, int value);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(Attribute key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(Attribute key);

	//-------------------------------------------------------------------
	public int getIncreaseCost(Attribute key);

	//-------------------------------------------------------------------
	public boolean increase(Attribute key);

	//-------------------------------------------------------------------
	public boolean decrease(Attribute key);

	public boolean isRacialAttribute(Attribute key);
}