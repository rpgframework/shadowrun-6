package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.charctrl.MartialArtsController;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.charctrl.SummonableController;
import org.prelle.shadowrun6.chargen.CommonPowerGenerator;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.Damage;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.RelevanceModification;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.proc.GetModificationsFromPowers;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PowerGenTest {

	private CommonPowerGenerator control;
	private ShadowrunCharacter model;
	private List<Modification> mods;
	private int baseKarma;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
		SR6ConfigOptions.attachConfigurationTree(new ConfigContainerImpl(Preferences.userRoot(), "foo"));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		GenerationEventDispatcher.clear();
		mods = new ArrayList<>();
		CharacterController charGen = new CharacterController() {
			public void runProcessors() {
				model.setKarmaFree(baseKarma);
				control.process(model, mods);
			}
			public WizardPageType[] getWizardPages() {return new WizardPageType[0];}
			public CharGenMode getMode() {return CharGenMode.CREATING;}
			public String getName() { return null;}
			public ShadowrunCharacter getCharacter() {return model;}
			public AttributeController getAttributeController() { return null;}
			public SkillController getSkillController() { return null;}
			public SpellController getSpellController() { return null;}
			public SpellController getAlchemyController() { return null;}
			public RitualController getRitualController() { return null;}
			public QualityController getQualityController() { return null;}
			public AdeptPowerController getPowerController() { return control;}
			public ComplexFormController getComplexFormController() { return null;}
			public EquipmentController getEquipmentController() { return null;}
			public ConnectionsController getConnectionController() { return null;}
			public SINController getSINController() {return null;}
			public LifestyleController getLifestyleController() {return null;}
			public SummonableController getSummonableController() {return null;}
			public MetamagicOrEchoController getMetamagicOrEchoController() {return null;}
			public FocusController getFocusController() {return null;}
			public MartialArtsController getMartialArtsController() {return null;}
			public void attachConfigurationTree(ConfigContainer addBelow) {}
			};
		model.getAttribute(Attribute.MAGIC).clearModifications();
		control = new CommonPowerGenerator(charGen);
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		control.process(model, mods);
		assertEquals(0f, control.getPowerPointsLeft(), 0);
		assertFalse(control.canBeSelected(ShadowrunCore.getAdeptPower("combat_sense")));
		assertNull(control.select(ShadowrunCore.getAdeptPower("combat_sense")));
		AdeptPowerValue power = new AdeptPowerValue(ShadowrunCore.getAdeptPower("combat_sense")); 
		assertFalse(control.canBeDeselected(power));
		assertFalse(control.canBeDecreased(power));
		assertFalse(control.canBeIncreased(power));
		assertEquals(0, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertFalse(control.canIncreasePowerPoints());
		assertFalse(control.increasePowerPoints());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimplePointSelection() {
//		model.getAttribute(Attribute.MAGIC).setPoints(2);
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 2));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		assertEquals(2f, control.getPowerPointsLeft(), 0);
		
		assertTrue(control.canBeSelected(ShadowrunCore.getAdeptPower("astral_perception")));
		assertNotNull(control.select(ShadowrunCore.getAdeptPower("astral_perception")));
		control.process(model, mods);
		assertEquals(1f, control.getPowerPointsLeft(), 0);
		
		// Power with level
		assertTrue(control.canBeSelected(ShadowrunCore.getAdeptPower("combat_sense")));
		AdeptPowerValue power =  control.select(ShadowrunCore.getAdeptPower("combat_sense"));
		assertNotNull(power);
		control.process(model, mods);
		assertEquals(0.5f, control.getPowerPointsLeft(), 0);
		// Increase
		assertTrue(control.canBeDecreased(power));
		assertTrue(control.canBeIncreased(power));
		assertTrue(control.increase(power));
		control.process(model, mods);
		// Should fail to increase any further
		assertTrue(control.canBeDecreased(power));
		assertFalse(control.canBeIncreased(power));
		assertFalse(control.increase(power));
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleKarmaSelection() {
		baseKarma = 50;
		model.setKarmaFree(baseKarma);
//		model.getAttribute(Attribute.MAGIC).setPoints(2);
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 2));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		assertEquals(50, model.getKarmaFree());
		assertEquals(2.0f, control.getPowerPointsLeft(), 0);
		assertFalse(control.canIncreasePowerPoints());
		assertFalse(control.canDecreasePowerPoints());
		
		assertTrue(control.canBeSelected(ShadowrunCore.getAdeptPower("astral_perception")));
		assertNotNull(control.select(ShadowrunCore.getAdeptPower("astral_perception")));
		control.process(model, mods);
		assertEquals(1f, control.getPowerPointsLeft(), 0);
		
		// Power with level
		assertTrue(control.canBeSelected(ShadowrunCore.getAdeptPower("combat_sense")));
		AdeptPowerValue power =  control.select(ShadowrunCore.getAdeptPower("combat_sense"));
		assertNotNull(power);
		control.process(model, mods);
		assertEquals(0.5f, control.getPowerPointsLeft(), 0);
		// Increase
		assertTrue(control.canBeDecreased(power));
		assertTrue(control.canBeIncreased(power));
		assertTrue(control.increase(power));
		assertEquals(50, model.getKarmaFree());
//		control.process(model, mods);
		// Should fail to increase any further
		assertTrue(control.canBeDecreased(power));
		assertFalse(control.canBeIncreased(power));
		assertFalse(control.increase(power));

		List<Modification> unproc = (new GetModificationsFromPowers()).process(model, new ArrayList<>());
		assertEquals(4, unproc.size());
		assertTrue(unproc.get(0) instanceof AllowModification);
		assertNotNull(unproc.get(0).getSource());
		assertTrue(unproc.get(1) instanceof RelevanceModification);
		assertNotNull(unproc.get(1).getSource());
		assertTrue(unproc.get(3) instanceof EdgeModification);
		assertNotNull(unproc.get(3).getSource());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleAutoSelection() {
		AdeptPower combatSense = ShadowrunCore.getAdeptPower("combat_sense");
		assertNotNull(combatSense);
		
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 3));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 3));
//		model.getAttribute(Attribute.MAGIC).setPoints(3);
		mods.add(new AdeptPowerModification(combatSense, 2));
		control.process(model, mods);
		assertEquals(3f, control.getPowerPointsLeft(), 0);
		
		// Increase auto selected power
		AdeptPowerValue ref = model.getAdeptPower("combat_sense");
		assertTrue("Cannot increase auto selected", control.canBeIncreased(ref));
		assertTrue("Failed increase auto selected", control.increase(ref));
	}

	//-------------------------------------------------------------------
	@Test
	public void testMergeUserAndAutoSelection() {
		// Add adept power with level 1
		AdeptPower combatSense = ShadowrunCore.getAdeptPower("combat_sense");
		AdeptPowerValue existing = new AdeptPowerValue(combatSense);
		model.addAdeptPower(existing);
		
		// Add modification +2 for same adept power
		mods.add(new AdeptPowerModification(combatSense, 2));
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 2));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		assertSame(existing, model.getAdeptPower("combat_sense"));
		assertEquals(3, existing.getModifiedLevel());
		assertEquals(1.5f, control.getPowerPointsLeft(), 0);

		List<Modification> unproc = (new GetModificationsFromPowers()).process(model, new ArrayList<>());
		assertEquals(3, unproc.size());
		assertTrue(unproc.get(1) instanceof AttributeModification);
		assertEquals(3, ((AttributeModification)unproc.get(1)).getValue() );
		assertNotNull(unproc.get(1).getSource());
		assertTrue(unproc.get(2) instanceof EdgeModification);
		assertNotNull(unproc.get(2).getSource());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMergeUserAndAutoSelectionWithChoice() {
		try {
			model.getAdeptPower("attribute_boost","AGILITY");
			fail("Should have thrown an exception");
		} catch (Exception e) {}
		AdeptPower attBoost = ShadowrunCore.getAdeptPower("attribute_boost");
		AdeptPowerValue existing = new AdeptPowerValue(attBoost);
		existing.setChoiceReference("REACTION");
		existing.setChoice(Attribute.REACTION);
		model.addAdeptPower(existing);
		try {
			model.getAdeptPower("attribute_boost","AGILITY");
			fail("Should have thrown an exception");
		} catch (Exception e) {}
		
		mods.add(new AdeptPowerModification(attBoost, 2, "AGILITY"));
		mods.add(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		assertNotNull(model.getAdeptPower("attribute_boost","AGILITY"));
		// Should have 2 powers now
		assertEquals(2, model.getAdeptPowers().size());
		assertSame(existing, model.getAdeptPower("attribute_boost","REACTION"));
		assertNotSame(existing, model.getAdeptPower("attribute_boost","AGILITY"));
		assertEquals(1, existing.getModifiedLevel());
		assertEquals(2, model.getAdeptPower("attribute_boost","AGILITY").getModifiedLevel());
		assertEquals(1.75f, control.getPowerPointsLeft(), 0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testSelectingWithoutChoice() {
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 2));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 2));
		AdeptPower attBoost = ShadowrunCore.getAdeptPower("attribute_boost");
		
		control.process(model, mods);
		try {
			assertNull(control.select(attBoost));
			fail("Failed to detect missing choice");
		} catch (IllegalArgumentException e) {
		}
		AdeptPowerValue added = control.select(attBoost, Attribute.AGILITY);
		assertNotNull(added);
		assertNotNull(added.getChoice());
		assertNotNull(added.getChoiceReference());
		assertEquals(1.75f, control.getPowerPointsLeft(), 0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testRequirements() {
//		mods.add(new AttributeModification(Attribute.POWER_POINTS, 2));
//		model.getAttribute(Attribute.MAGIC).setPoints(2);
		model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, 2));
		model.getAttribute(Attribute.POWER_POINTS).addModification(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		Skill con = ShadowrunCore.getSkill("con");

		AdeptPower imprAbil = ShadowrunCore.getAdeptPower("improved_ability");
		// Requirement (SkillValue 1) not given
		assertNull("Failed to respect requirements", control.select(imprAbil, con));
		model.addSkill( new SkillValue(con,1) );
		AdeptPowerValue ref = control.select(imprAbil, con);
		assertNotNull("Given requirements fulfillment not recognized", ref);
		
		// Increase up to 2 should be possible (1.5 x Skill-Rating)
		assertTrue(control.canBeIncreased(ref));
		assertTrue(control.increase(ref));
		assertFalse("Should not be able to raise above the 1.5x rating", control.canBeIncreased(ref));
		model.getSkillValue(con).setPoints(2);
		assertFalse(control.canBeIncreased(ref));
		model.getAttribute(Attribute.MAGIC).setPoints(3);
		assertTrue(control.canBeIncreased(ref));
	}

	//-------------------------------------------------------------------
	@Test
	public void testSelectingSkill() {
		control.process(model, mods);
		assertEquals(0, control.getPowerPointsLeft(), 0);
		mods.add(new AttributeModification(Attribute.POWER_POINTS, 2));
		control.process(model, mods);
		assertEquals(2f, control.getPowerPointsLeft(), 0);

		AdeptPower imprAbil = ShadowrunCore.getAdeptPower("improved_ability");
		try {
			assertNull(control.select(imprAbil));
			fail("Failed to detect missing choice");
		} catch (IllegalArgumentException e) {
		}
		assertNull("Should not be able to use on combat skill", control.select(imprAbil, ShadowrunCore.getSkill("firearms")));
		model.addSkill( new SkillValue(ShadowrunCore.getSkill("con"),1) );
		mods.clear();
		AdeptPowerValue added = control.select(imprAbil, ShadowrunCore.getSkill("con"));
		assertNotNull("Should have been able on non-combat skill", added);
		assertNotNull(added.getChoice());
		assertNotNull(added.getChoiceReference());
		assertEquals(1.5f, control.getPowerPointsLeft(), 0);

		List<Modification> unproc = (new GetModificationsFromPowers()).process(model, new ArrayList<>());
		assertEquals(1, unproc.size());
		assertTrue(unproc.get(0) instanceof SkillModification);
		assertEquals(1, ((SkillModification)unproc.get(0)).getValue() );
		assertEquals(ShadowrunCore.getSkill("con"), ((SkillModification)unproc.get(0)).getSkill() );
	}

	//-------------------------------------------------------------------
	@Test
	public void testCriticalStrike() {
		Skill combat = ShadowrunCore.getSkill("close_combat");
		SkillValue sVal = new SkillValue(combat, 4);
		model.addSkill(sVal);
		model.getAttribute(Attribute.REACTION).setPoints(3);
		model.getAttribute(Attribute.AGILITY).setPoints(6);
		model.getAttribute(Attribute.STRENGTH).setPoints(5);
		
		ShadowrunTools.recalculateCharacter(model);
		
		assertNotNull(model.getItem("unarmed"));
		assertEquals(10, ShadowrunTools.getWeaponPool(model, model.getItem("unarmed"))); // AGILITY + Skill Value "Unarmed"
		Object ar = model.getItem("unarmed").getAsObject(ItemAttribute.ATTACK_RATING).getValue();
		assertEquals(8, ((int[])ar)[0]);  // Reaction + Strength
		sVal.addSpecialization(new SkillSpecializationValue(combat.getSpecialization("unarmed")));
		assertEquals(12, ShadowrunTools.getWeaponPool(model, model.getItem("unarmed"))); // AGILITY + Skill Value "Unarmed" + Specialization
		assertEquals(8, ((int[])model.getItem("unarmed").getAsObject(ItemAttribute.ATTACK_RATING).getValue())[0]);  // Reaction + Strength
//		System.err.println(ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")));
		assertEquals( 2, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getValue());
		assertEquals( 2, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getModifiedValue());
		assertEquals( Damage.Type.STUN, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getType());

		// Add adept power
//		System.out.println("--------Add critical_strike ---------------");
		AdeptPowerValue crit = new AdeptPowerValue(ShadowrunCore.getAdeptPower("critical_strike"));
		crit.setLevel(2);
		model.addAdeptPower(crit);
		
		ShadowrunTools.recalculateCharacter(model);
		assertEquals(12, ShadowrunTools.getWeaponPool(model, model.getItem("unarmed"))); // AGILITY + Skill Value "Unarmed" + Specialization
		assertEquals(8, ((int[])model.getItem("unarmed").getAsObject(ItemAttribute.ATTACK_RATING).getValue())[0]);  // Reaction + Strength
		assertEquals(2, ((Damage)model.getItem("unarmed").getAsValue(ItemAttribute.DAMAGE)).getValue());  // (STR/2) 
//		assertEquals( 2, ((Damage)model.getItem("unarmed").getAsValue(ItemAttribute.DAMAGE)).getModifiedValue());  // (STR/2) +2
		assertEquals( 2, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getValue()); // (STR/2) 
		assertEquals( 4, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getModifiedValue()); // (STR/2) +2 (critical strike 2)
		assertEquals( Damage.Type.STUN, ShadowrunTools.getWeaponDamage(model, model.getItem("unarmed")).getType());

	}
	
}
