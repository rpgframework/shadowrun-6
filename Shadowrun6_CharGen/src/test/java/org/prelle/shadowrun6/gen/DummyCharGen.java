package org.prelle.shadowrun6.gen;

import java.util.List;

import org.prelle.shadowrun6.ShadowrunCharacter;

import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author prelle
 *
 */
public class DummyCharGen extends CommonSR6CharacterGenerator {
	
	public DummyCharGen(ShadowrunCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getID()
	 */
	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#start(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#continueCreation(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void continueCreation(ShadowrunCharacter model) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughDataIgnoreMoney()
	 */
	@Override
	public boolean hasEnoughDataIgnoreMoney() {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
