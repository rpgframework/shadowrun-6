package de.rpgframework.shadowrun6.export.foundry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.RitualFeatureReference;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellFeatureReference;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.Damage;
import org.prelle.shadowrun6.items.Damage.Type;
import org.prelle.shadowrun6.items.FireMode;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.OnRoadOffRoadValue;
import org.prelle.shadowrun6.items.WeaponData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.shadowrun6.foundry.ActionSkills.ActionSkillValue;
import de.rpgframework.shadowrun6.foundry.Actor;
import de.rpgframework.shadowrun6.foundry.FVTTAdeptPower;
import de.rpgframework.shadowrun6.foundry.FVTTArmor;
import de.rpgframework.shadowrun6.foundry.FVTTBodyware;
import de.rpgframework.shadowrun6.foundry.FVTTComplexForm;
import de.rpgframework.shadowrun6.foundry.FVTTContact;
import de.rpgframework.shadowrun6.foundry.FVTTEcho;
import de.rpgframework.shadowrun6.foundry.FVTTFocus;
import de.rpgframework.shadowrun6.foundry.FVTTGear;
import de.rpgframework.shadowrun6.foundry.FVTTLifestyle;
import de.rpgframework.shadowrun6.foundry.FVTTMAStyle;
import de.rpgframework.shadowrun6.foundry.FVTTMATechnique;
import de.rpgframework.shadowrun6.foundry.FVTTMetamagic;
import de.rpgframework.shadowrun6.foundry.FVTTQuality;
import de.rpgframework.shadowrun6.foundry.FVTTRitual;
import de.rpgframework.shadowrun6.foundry.FVTTSIN;
import de.rpgframework.shadowrun6.foundry.FVTTSkill;
import de.rpgframework.shadowrun6.foundry.FVTTSpell;
import de.rpgframework.shadowrun6.foundry.FVTTVehicle;
import de.rpgframework.shadowrun6.foundry.FVTTWeapon;
import de.rpgframework.shadowrun6.foundry.Item;
import de.rpgframework.shadowrun6.foundry.Shadowrun6FoundryCharacter;

public class FoundryExportService {
	
	public final static String VERSION = "0.8.9";

	//-------------------------------------------------------------------
	public String exportCharacter(ShadowrunCharacter character) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Actor actor = new Actor(character.getName(), "Player", getJSONCharacter(character));
//		actor.exportVersion = VERSION;
		addFoundryItems(actor, character);
		return gson.toJson(actor);
	}

	//-------------------------------------------------------------------
	private Shadowrun6FoundryCharacter getJSONCharacter(ShadowrunCharacter character) {
		Shadowrun6FoundryCharacter jsonCharacter = new Shadowrun6FoundryCharacter();
		jsonCharacter.nuyen = character.getNuyen();
		jsonCharacter.mortype  = character.getMagicOrResonanceType().getId();
		setAttributes(jsonCharacter, character);
		setDerivedAttributes(jsonCharacter, character); 
		setSkills(jsonCharacter, character);
		jsonCharacter.metatype = character.getMetatype().getName();
		jsonCharacter.gender   = character.getGender().toString();
		return jsonCharacter;
	}


	//-------------------------------------------------------------------
	private void setAttributes(Shadowrun6FoundryCharacter json, ShadowrunCharacter model) {
		for (Attribute attribute : Attribute.values()) {
			switch (attribute) {
			case CHARISMA : 
				json.attributes.cha.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.cha.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.cha.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.cha.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case AGILITY  : 
				json.attributes.agi.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.agi.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.agi.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.agi.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case INTUITION: 
				json.attributes.inn.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.inn.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.inn.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.inn.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case BODY: 
				json.attributes.bod.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.bod.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.bod.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.bod.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case REACTION   : 
				json.attributes.rea.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.rea.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.rea.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.rea.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case STRENGTH : 
				json.attributes.str.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.str.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.str.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.str.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case LOGIC     : 
				json.attributes.log.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.log.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.log.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.log.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case WILLPOWER: 
				json.attributes.wil.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.wil.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.wil.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.wil.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case MAGIC:
				json.attributes.mag.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.mag.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.mag.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.mag.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case RESONANCE:
				json.attributes.res.base = model.getAttribute(attribute).getPoints(); 
				json.attributes.res.mod   = model.getAttribute(attribute).getModifier(); 
				json.attributes.res.pool = model.getAttribute(attribute).getModifiedValue(); 
				json.attributes.res.modString = ShadowrunTools.getAttributeModifierExplanation(model, attribute); 
				break;
			case EDGE:
				json.edge.max = model.getAttribute(attribute).getPoints(); 
				json.edge.value = model.getAttribute(attribute).getPoints(); 
				break;
			default:
			}
		}
	}

	//-------------------------------------------------------------------
	private void setDerivedAttributes(Shadowrun6FoundryCharacter json, ShadowrunCharacter model) {
		json.physical.base = model.getAttribute(Attribute.PHYSICAL_MONITOR).getPoints();
		json.physical.mod = model.getAttribute(Attribute.PHYSICAL_MONITOR).getModifier();
		json.physical.modString = ShadowrunTools.getAttributeModifierExplanation(model, Attribute.PHYSICAL_MONITOR); 
		json.physical.value = model.getAttribute(Attribute.PHYSICAL_MONITOR).getModifiedValue();

		json.stun.base = model.getAttribute(Attribute.STUN_MONITOR).getPoints();
		json.stun.mod = model.getAttribute(Attribute.STUN_MONITOR).getModifier();
		json.stun.modString = ShadowrunTools.getAttributeModifierExplanation(model, Attribute.STUN_MONITOR); 
		json.stun.value = model.getAttribute(Attribute.STUN_MONITOR).getModifiedValue();

		json.initiative.physical.dice = model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifiedValue();
		json.initiative.physical.mod = 0; //model.getAttribute(Attribute.INITIATIVE_PHYSICAL).getModifier();
		json.initiative.astral.dice = model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getModifiedValue();
		json.initiative.astral.mod = 0; //model.getAttribute(Attribute.INITIATIVE_ASTRAL).getModifier();
		
		json.derived.attack_rating.base = model.getAttribute(Attribute.ATTACK_RATING).getPoints();
		json.derived.attack_rating.mod  = model.getAttribute(Attribute.ATTACK_RATING).getModifier();
		json.derived.attack_rating.pool = model.getAttribute(Attribute.ATTACK_RATING).getModifiedValue();
		
		json.derived.defense_rating.base = model.getAttribute(Attribute.DEFENSE_RATING).getPoints();
		json.derived.defense_rating.mod  = model.getAttribute(Attribute.DEFENSE_RATING).getModifier();
		json.derived.defense_rating.pool = model.getAttribute(Attribute.DEFENSE_RATING).getModifiedValue();
		
		json.derived.composure.base = model.getAttribute(Attribute.COMPOSURE).getModifiedValue();
		
		json.derived.judge_intentions.base = model.getAttribute(Attribute.JUDGE_INTENTIONS).getModifiedValue();
		
		json.derived.memory.base = model.getAttribute(Attribute.MEMORY).getModifiedValue();
		
		json.derived.lift_carry.base = model.getAttribute(Attribute.LIFT_CARRY).getModifiedValue();
		
		json.resist.attacks.base = model.getAttribute(Attribute.DAMAGE_RESISTANCE).getModifier();
		json.resist.attacks.pool = model.getAttribute(Attribute.DAMAGE_RESISTANCE).getModifiedValue();
		
		json.resist.toxin.base = model.getAttribute(Attribute.DEFENSIVE_POOL_RESIST_TOXIN_DAMAGE).getModifier();
		json.resist.toxin.pool = model.getAttribute(Attribute.DEFENSIVE_POOL_RESIST_TOXIN_DAMAGE).getModifiedValue();
	}

	//-------------------------------------------------------------------
	private void setSpecialization(ActionSkillValue target, SkillValue val) {
		List<String> spec = new ArrayList<>();
		List<String> exp  = new ArrayList<>();
		for (SkillSpecializationValue tmp : val.getSkillSpecializations()) {
			if (tmp.isExpertise())
				exp.add(tmp.getSpecial().getId());
			else
				spec.add(tmp.getSpecial().getId());
		}
		if (!spec.isEmpty()) target.specialization = String.join(",", spec);
		if (!exp.isEmpty()) target.expertise = String.join(",", exp);
	}

	//-------------------------------------------------------------------
	private void setSkills(Shadowrun6FoundryCharacter json, ShadowrunCharacter model) {
		for (SkillValue val : model.getSkillValues()) {
			switch (val.getModifyable().getId()) {
			case "astral" : 
				json.skills.astral.points = val.getPoints(); 
				json.skills.astral.modifier = val.getModifier();
				json.skills.astral.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.astral, val);
				break;
			case "athletics" : 
				json.skills.athletics.points = val.getPoints(); 
				json.skills.athletics.modifier = val.getModifier();
				json.skills.athletics.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.athletics, val);
				break;
			case "biotech" : 
				json.skills.biotech.points = val.getPoints(); 
				json.skills.biotech.modifier = val.getModifier();
				json.skills.biotech.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.biotech, val);
				break;
			case "close_combat" : 
				json.skills.close_combat.points = val.getPoints(); 
				json.skills.close_combat.modifier = val.getModifier();
				json.skills.close_combat.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.close_combat, val);
				break;
			case "con" : 
				json.skills.con.points = val.getPoints(); 
				json.skills.con.modifier = val.getModifier();
				json.skills.con.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.con, val);
				break;
			case "conjuring" : 
				json.skills.conjuring.points = val.getPoints(); 
				json.skills.conjuring.modifier = val.getModifier();
				json.skills.conjuring.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.conjuring, val);
				break;
			case "cracking" : 
				json.skills.cracking.points = val.getPoints(); 
				json.skills.cracking.modifier = val.getModifier();
				json.skills.cracking.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.cracking, val);
				break;
			case "electronics" : 
				json.skills.electronics.points = val.getPoints(); 
				json.skills.electronics.modifier = val.getModifier();
				json.skills.electronics.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.electronics, val);
				break;
			case "enchanting" : 
				json.skills.enchanting.points = val.getPoints(); 
				json.skills.enchanting.modifier = val.getModifier();
				json.skills.enchanting.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.enchanting, val);
				break;
			case "engineering" : 
				json.skills.engineering.points = val.getPoints(); 
				json.skills.engineering.modifier = val.getModifier();
				json.skills.engineering.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.engineering, val);
				break;
			case "exotic_weapons" : 
				json.skills.exotic_weapons.points = val.getPoints(); 
				json.skills.exotic_weapons.modifier = val.getModifier();
				json.skills.exotic_weapons.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.exotic_weapons, val);
				break;
			case "firearms" : 
				json.skills.firearms.points = val.getPoints(); 
				json.skills.firearms.modifier = val.getModifier();
				json.skills.firearms.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.firearms, val);
				break;
			case "influence" : 
				json.skills.influence.points = val.getPoints(); 
				json.skills.influence.modifier = val.getModifier();
				json.skills.influence.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.influence, val);
				break;
			case "outdoors" : 
				json.skills.outdoors.points = val.getPoints(); 
				json.skills.outdoors.modifier = val.getModifier();
				json.skills.outdoors.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.outdoors, val);
				break;
			case "perception" : 
				json.skills.perception.points = val.getPoints(); 
				json.skills.perception.modifier = val.getModifier();
				json.skills.perception.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.perception, val);
				break;
			case "piloting" : 
				json.skills.piloting.points = val.getPoints(); 
				json.skills.piloting.modifier = val.getModifier();
				json.skills.piloting.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.piloting, val);
				break;
			case "sorcery" : 
				json.skills.sorcery.points = val.getPoints(); 
				json.skills.sorcery.modifier = val.getModifier();
				json.skills.sorcery.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.sorcery, val);
				break;
			case "stealth" : 
				json.skills.stealth.points = val.getPoints(); 
				json.skills.stealth.modifier = val.getModifier();
				json.skills.stealth.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.stealth, val);
				break;
			case "tasking" : 
				json.skills.tasking.points = val.getPoints(); 
				json.skills.tasking.modifier = val.getModifier();
				json.skills.tasking.modString = ShadowrunTools.getSkillPoolExplanation(model, val.getModifyable()); 
				setSpecialization(json.skills.tasking, val);
				break;
			default:
			}
		}
	}

	//-------------------------------------------------------------------
	private void addFoundryItems(Actor actor, ShadowrunCharacter character) {
		addSkills(actor, character);
		addQualities(actor, character);
		addGear(actor, character);	
		addMartialArts(actor, character);
		addSpells(actor, character);
		addAdeptPowers(actor, character);
		addRituals(actor, character);
		addMetamagics(actor, character);
		addFoci(actor, character);
		addComplexForms(actor, character);
		addEchoes(actor, character);
		addSINs(actor, character);
		addLifestyles(actor, character);
		addContacts(actor, character);
	}

	//-------------------------------------------------------------------
	private void addSkills(Actor actor, ShadowrunCharacter model) {
		for (SkillValue val : model.getSkillValues()) {
			if (!(val.getModifyable().getId().equals("language") || val.getModifyable().getId().equals("knowledge"))) {
				continue;
			} else {
				FVTTSkill fVal = new FVTTSkill();
				fVal.genesisID = val.getModifyable().getId();
				fVal.points    = val.getPoints();
				fVal.modifier  = val.getModifier();

				Item<FVTTSkill> item = new Item<FVTTSkill>(val.getName(), "skill", fVal);
				actor.addItems(item);
			}
		}
	}

	//-------------------------------------------------------------------
	private void addQualities(Actor actor, ShadowrunCharacter model) {
		for (QualityValue val : model.getQualities()) {
			FVTTQuality fVal = new FVTTQuality();
			// Definition fields
			fVal.genesisID = val.getModifyable().getId();
			fVal.category  = val.getModifyable().getType().name();
			fVal.level     = val.getModifyable().getMax()>0;
			// Value fields
			fVal.value = val.getPoints();
			fVal.explain = val.getDescription();

			Item<FVTTQuality> item = new Item<FVTTQuality>(val.getName(), "quality", fVal);
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private static FVTTWeapon.FireMode getFireModes(WeaponData weapon) {
		FVTTWeapon.FireMode modes = new FVTTWeapon.FireMode();
		if (weapon.getFireModes()!=null) {
			for (FireMode mode : weapon.getFireModes()) {
				switch (mode) {
				case BURST_FIRE: modes.BF=true; break;
				case FULL_AUTO : modes.FA=true; break;
				case SEMI_AUTOMATIC: modes.SA=true; break;
				case SINGLE_SHOT: modes.SS=true; break;
				}
			}
		}
		return modes;
	}

	//-------------------------------------------------------------------
	private void addGear(Actor actor, ShadowrunCharacter character) {
		//		WeaponDamageConverter dmgConv = new WeaponDamageConverter();
		for (CarriedItem item : character.getItems(true)) {
			ItemType type = item.getUsedAsType();
			FVTTGear gear = new FVTTGear();
			switch (type) {
			case WEAPON_CLOSE_COMBAT:
			case WEAPON_FIREARMS:
			case WEAPON_RANGED:
			case WEAPON_SPECIAL:
				gear = new FVTTWeapon();
				break;
			case ARMOR:
				gear = new FVTTArmor();
				break;
			case BIOWARE:
			case CYBERWARE:
			case NANOWARE:
				gear = new FVTTBodyware();
				break;
			case VEHICLES: 
			case DRONES: case DRONE_LARGE: case DRONE_MEDIUM: case DRONE_MICRO: case DRONE_MINI: case DRONE_SMALL:
				gear = new FVTTVehicle();
				break;
			}

			gear.genesisID = item.getItem().getId();
			gear.type = type.name();
			gear.subtype = item.getUsedAsSubType().name();
			gear.availDef = item.getItem().getAvailability().toString();
			if (item.hasAttribute(ItemAttribute.AVAILABILITY))
				gear.avail = ((Availability)item.getAsObject(ItemAttribute.AVAILABILITY).getValue()).getValue();
			gear.price = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
			gear.notes = item.getNotes();
			gear.customName = item.getCustomName();
			gear.countable  = item.getItem().isCountable();
			if (item.getItem().isCountable())
				gear.count = item.getCount();
			gear.needsRating = item.getItem().hasRating();
			if (item.getItem().hasRating())
				gear.rating = item.getRating();
			
			// Accessories
			List<String> accList = new ArrayList<>();
			item.getEffectiveAccessories().forEach( ci -> accList.add(ci.getNameWithRating()));
			if (!accList.isEmpty()) {
				gear.accessories = String.join(", ", accList);
			}

			if (!item.getWeaponDataRecursive().isEmpty()) {
				WeaponData weapon = item.getWeaponDataRecursive().get(0);
				if (weapon.getSkill()!=null)
					gear.skill = weapon.getSkill().getId();
				if (weapon.getSpecialization()!=null)
					gear.skillSpec = weapon.getSpecialization().getId();
				Damage dmg = new Damage(weapon.getDamage(), new ArrayList<>());
				if (item.hasAttribute(ItemAttribute.DAMAGE))
					dmg.setValue(item.getAsValue(ItemAttribute.DAMAGE).getModifiedValue());
				if (gear instanceof FVTTWeapon) {
					if (item.hasAttribute(ItemAttribute.ATTACK_RATING))
						((FVTTWeapon)gear).attackRating = (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue();
					((FVTTWeapon)gear).dmg    = dmg.getModifiedValue();
					((FVTTWeapon)gear).dmgDef = dmg.toString();
					((FVTTWeapon)gear).stun   = dmg.getType()==Type.STUN;
					((FVTTWeapon)gear).modes  = getFireModes(weapon);
				}
			}
			if (gear instanceof FVTTArmor) {
				if (item.hasAttribute(ItemAttribute.ARMOR))
					((FVTTArmor)gear).defense = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.SOCIAL))
					((FVTTArmor)gear).social = item.getAsValue(ItemAttribute.SOCIAL).getModifiedValue();				
			}
			if (gear instanceof FVTTBodyware) {
				if (item.hasAttribute(ItemAttribute.ESSENCECOST))
					((FVTTBodyware)gear).essence = item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue()/1000.0f;
				if (item.hasAttribute(ItemAttribute.CAPACITY))
					((FVTTBodyware)gear).capacity = (int)item.getAsFloat(ItemAttribute.CAPACITY).getModifiedValue();				
			}
			if (gear instanceof FVTTVehicle) {
				if (item.hasAttribute(ItemAttribute.ACCELERATION)) {
					((FVTTVehicle)gear).accOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad();				
					((FVTTVehicle)gear).accOn  = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad();
				}
				if (item.hasAttribute(ItemAttribute.HANDLING)) {
					((FVTTVehicle)gear).handlOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad();				
					((FVTTVehicle)gear).handlOn  = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad();
				}
				if (item.hasAttribute(ItemAttribute.SPEED_INTERVAL))
					((FVTTVehicle)gear).spdiOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOffRoad();				
					((FVTTVehicle)gear).spdiOn  = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOnRoad();
				if (item.hasAttribute(ItemAttribute.SPEED))
					((FVTTVehicle)gear).tspd = item.getAsValue(ItemAttribute.SPEED).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.BODY))
					((FVTTVehicle)gear).bod = item.getAsValue(ItemAttribute.BODY).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.ARMOR))
					((FVTTVehicle)gear).arm = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.PILOT))
					((FVTTVehicle)gear).pil = item.getAsValue(ItemAttribute.PILOT).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.SENSORS))
					((FVTTVehicle)gear).sen = item.getAsValue(ItemAttribute.SENSORS).getModifiedValue();
				if (item.hasAttribute(ItemAttribute.SEATS))
					((FVTTVehicle)gear).sea = item.getAsValue(ItemAttribute.SEATS).getModifiedValue();
				if (item.getItem().getVehicleData()!=null)
					((FVTTVehicle)gear).vtype = item.getItem().getVehicleData().getType().name();
			}

			Item<FVTTGear> foundry = new Item<FVTTGear>(item.getName(), "gear", gear);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addMartialArts(Actor actor, ShadowrunCharacter model) {
		for (MartialArtsValue val : model.getMartialArts()) {
			FVTTMAStyle fVal = new FVTTMAStyle();
			// Definition fields
			fVal.genesisID = val.getMartialArt().getId();
			List<String> names = val.getMartialArt().getCategories().stream().map( (c) -> c.name().toLowerCase()).collect(Collectors.toList());
			fVal.category  = String.join(", ", names);

			Item<FVTTMAStyle> item = new Item<FVTTMAStyle>(val.getMartialArt().getName(), "martialartstyle", fVal);
			actor.addItems(item);
			
			for (TechniqueValue tech : model.getTechniques(val.getMartialArt())) {
				FVTTMATechnique toAdd = new FVTTMATechnique();
				toAdd.genesisID = tech.getTechnique().getId();
				if (tech.getMartialArt()!=null)
					toAdd.style = tech.getMartialArt().getId();
				if (tech.getTechnique().getChoice()!=null) {
					toAdd.explain = tech.getChoice()+"";
				}
				Item<FVTTMATechnique> item2 = new Item<FVTTMATechnique>(tech.getTechnique().getName(), "martialarttech", toAdd);
				actor.addItems(item2);
			}
		}
	}

	//-------------------------------------------------------------------
	private void addSpells(Actor actor, ShadowrunCharacter character) {
		for (SpellValue item : character.getSpells()) {
			FVTTSpell spell = new FVTTSpell();

			spell.genesisID = item.getModifyable().getId();
			spell.category  = item.getModifyable().getCategory().name().toLowerCase();
			spell.duration  = item.getModifyable().getDuration().name().toLowerCase();
			spell.drain     = item.getModifyable().getDrain();
			spell.range     = item.getModifyable().getRange().name().toLowerCase();
			spell.type      = item.getModifyable().getType().name().toLowerCase();
			if (item.getModifyable().getDamage()!=null)
				spell.damage    = item.getModifyable().getDamage().name().toLowerCase();
			spell.isOpposed = item.getModifyable().isOpposed();
			spell.withEssence = item.getModifyable().isEssence();
			for (SpellFeatureReference ref : item.getModifyable().getFeatures()) {
				switch (ref.getFeature().getId()) {
				case "sense_multi": spell.multiSense=true; break; 
				}
			}

			Item<FVTTSpell> foundry = new Item<FVTTSpell>(item.getName(), "spell", spell);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addAdeptPowers(Actor actor, ShadowrunCharacter character) {
		for (AdeptPowerValue item : character.getAdeptPowers()) {
			FVTTAdeptPower spell = new FVTTAdeptPower();

			spell.genesisID = item.getModifyable().getId();
			spell.cost      = (float)item.getCost();
			spell.activation= item.getModifyable().getActivation().name().toLowerCase();

			Item<FVTTAdeptPower> foundry = new Item<FVTTAdeptPower>(item.getName(), "adeptpower", spell);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addRituals(Actor actor, ShadowrunCharacter character) {
		for (RitualValue item : character.getRituals()) {
			FVTTRitual spell = new FVTTRitual();

			spell.genesisID = item.getModifyable().getId();
			spell.threshold = item.getModifyable().getThreshold();
			for (RitualFeatureReference ref : item.getModifyable().getFeatures()) {
				switch (ref.getFeature().getId()) {
				case "material_link": spell.features.material_link=true; break; 
				case "anchored": spell.features.anchored=true; break; 
				case "minion": spell.features.minion=true; break; 
				case "spell": spell.features.spell=true; break; 
				case "spotter": spell.features.spotter=true; break; 
				}
			}

			Item<FVTTRitual> foundry = new Item<FVTTRitual>(item.getModifyable().getName(), "ritual", spell);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addMetamagics(Actor actor, ShadowrunCharacter model) {
		for (MetamagicOrEchoValue val : model.getMetamagicOrEchoes()) {
			if (val.getModifyable().getType()==MetamagicOrEcho.Type.ECHO)
				continue;
			
			FVTTMetamagic fVal = new FVTTMetamagic();
			// Definition fields
			fVal.genesisID = val.getModifyable().getId();
			fVal.level     = val.getModifyable().hasLevels();
			fVal.adepts    = (val.getModifyable().getType()==MetamagicOrEcho.Type.METAMAGIC_ADEPT);
			fVal.mages     = true;

			Item<FVTTMetamagic> item = new Item<FVTTMetamagic>(val.getName(), "metamagic", fVal);
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addFoci(Actor actor, ShadowrunCharacter character) {
		for (FocusValue item : character.getFoci()) {
			FVTTFocus focus = new FVTTFocus();

			focus.genesisID = item.getModifyable().getId();
			focus.rating    = item.getLevel();
			focus.choice    = item.getChoiceText();

			Item<FVTTFocus> foundry = new Item<FVTTFocus>(item.getModifyable().getName(), "focus", focus);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addComplexForms(Actor actor, ShadowrunCharacter character) {
		for (ComplexFormValue item : character.getComplexForms()) {
			FVTTComplexForm cplx = new FVTTComplexForm();

			cplx.genesisID = item.getModifyable().getId();
			cplx.duration  = item.getModifyable().getDuration().name().toLowerCase();
			cplx.fading    = item.getModifyable().getFading();
//			spell.isOpposed = item.getModifyable().isOpposed();
			if (item.getChoiceReference()!=null) {
				cplx.choice = item.getChoiceReference();
			}

			Item<FVTTComplexForm> foundry = new Item<FVTTComplexForm>(item.getName(), "complexform", cplx);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addEchoes(Actor actor, ShadowrunCharacter model) {
		for (MetamagicOrEchoValue val : model.getMetamagicOrEchoes()) {
			if (val.getModifyable().getType()==MetamagicOrEcho.Type.METAMAGIC)
				continue;
			
			FVTTEcho fVal = new FVTTEcho();
			// Definition fields
			fVal.genesisID = val.getModifyable().getId();
			fVal.level     = val.getModifyable().hasLevels();
			if (val.getChoiceReference()!=null) {
				fVal.choice = val.getChoiceReference();
			}

			Item<FVTTEcho> item = new Item<FVTTEcho>(val.getName(), "echo", fVal);
			actor.addItems(item);
		}
	}

	//-------------------------------------------------------------------
	private void addSINs(Actor actor, ShadowrunCharacter character) {
		for (SIN item : character.getSINs()) {
			FVTTSIN data = new FVTTSIN();

			data.quality = item.getQuality().name();
			data.description = item.getDescription();

			Item<FVTTSIN> foundry = new Item<FVTTSIN>(item.getName(), "sin", data);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addLifestyles(Actor actor, ShadowrunCharacter character) {
		for (LifestyleValue item : character.getLifestyle()) {
			FVTTLifestyle data = new FVTTLifestyle();

			data.genesisID = item.getLifestyle().getId();
			data.type      = item.getLifestyle().getId();
			data.paid      = item.getPaidMonths();
			data.cost      = item.getCost();
			data.description = item.getDescription();
			if (item.getSIN()!=null)
			data.sin       = item.getSIN().toString();

			Item<FVTTLifestyle> foundry = new Item<FVTTLifestyle>(item.getName(), "lifestyle", data);
			actor.addItems(foundry);
		}
	}

	//-------------------------------------------------------------------
	private void addContacts(Actor actor, ShadowrunCharacter character) {
		for (Connection item : character.getConnections()) {
			FVTTContact data = new FVTTContact();

			data.rating      = item.getInfluence();
			data.loyalty     = item.getLoyalty();
			data.type        = item.getType();
			data.description = item.getDescription();

			Item<FVTTContact> foundry = new Item<FVTTContact>(item.getName(), "contact", data);
			actor.addItems(foundry);
		}
	}

}
