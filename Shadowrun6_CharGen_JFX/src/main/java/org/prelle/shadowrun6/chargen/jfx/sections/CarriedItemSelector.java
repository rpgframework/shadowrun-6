package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.Collection;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.items.CarriedItem;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

/**
 * @author Stefan Prelle
 *
 */
public class CarriedItemSelector extends ListView<CarriedItem> implements IListSelector<CarriedItem> {
	
	//-------------------------------------------------------------------
	public CarriedItemSelector(Collection<CarriedItem> data) {
		getItems().addAll(data);
		this.setCellFactory(lv -> new ListCell<CarriedItem>() {
			public void updateItem(CarriedItem item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setText(null);
				} else {
					setText(item.getName());
				}
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

}
