package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.ShadowrunCharacter;

import de.rpgframework.character.RulePlugin;
import javafx.beans.property.BooleanProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class PluginListCell extends ListCell<RulePlugin<ShadowrunCharacter>> {

	private ShadowrunCharacter model;
	private CheckBox cbSelected;
	private HBox languages;
	private HBox layout;
	
	private boolean updating;

	//--------------------------------------------------------------------
	public PluginListCell(ShadowrunCharacter model) {
		this.model = model;
		cbSelected = new CheckBox();
		languages  = new HBox(5);
		cbSelected.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(cbSelected, Priority.ALWAYS);
		layout = new HBox(5, cbSelected, languages);
	}

	//--------------------------------------------------------------------
	public BooleanProperty selectedCheckboxProperty() {
		return cbSelected.selectedProperty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(RulePlugin<ShadowrunCharacter> item, boolean empty) {
		super.updateItem(item, empty);
		updating = true;
		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			cbSelected.setText(item.getReadableName());
			cbSelected.setUserData(item.getID());
			cbSelected.setSelected(model.isExplicitPluginPermitted(item.getID()));
			cbSelected.selectedProperty().addListener( (ov,o,n) -> {
				if (updating)
					return;
				if (n) {
					model.addPermittedPlugin(PluginListCell.this.getItem().getID());
				} else {
					model.removePermittedPlugin(PluginListCell.this.getItem().getID());
				}
			});
			languages.getChildren().clear();
			for (String lang : item.getLanguages()) {
				if (lang.equals("de")) {
					Label icon = new Label("  "+String.valueOf(Character.toChars(127465))+String.valueOf(Character.toChars(127466)));
					icon.setStyle("-fx-font-family: \"Segoe UI Symbol\"; -fx-font-size: 150%");
					languages.getChildren().add(icon);
				} else if (lang.equals("en")) {
					StringBuffer sb = new StringBuffer();
				    sb.append(Character.toChars(127482));
				    sb.append(Character.toChars(127480));
					Label icon = new Label("   "+String.valueOf(Character.toChars(127482))+String.valueOf(Character.toChars(127480)));
					icon.setStyle("-fx-font-family: \"Segoe UI Symbol\"; -fx-font-size: 150%");
					languages.getChildren().add(icon);
				} else {
					languages.getChildren().add(new Label("  "+lang));
				}
			}
		}
		updating = false;
	}
}