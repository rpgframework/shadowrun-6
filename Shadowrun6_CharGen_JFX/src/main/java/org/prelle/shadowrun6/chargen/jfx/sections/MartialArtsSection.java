package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.MartialArtsValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class MartialArtsSection extends GenericListSection<MartialArtsValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(MartialArtsSection.class.getName());
	
	private Label note;
//	private Label ptsTotal;
//	private Button btnDec;
//	private Button btnInc;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public MartialArtsSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> {
			MartialArtsValueListCell cell = new MartialArtsValueListCell(ctrl, provider);
//			cell.setOnMouseClicked(ev -> {
//				if (ev.getClickCount()==2)
//					onEdit(cell.getItem(), cell);
//			});
			return cell;
		});
		initPlaceholder();
		setSettingsButton( new Button(null, new SymbolIcon("setting")) );
		getSettingsButton().setOnAction(ev -> onSettings());
	
		refresh();
		list.setStyle("-fx-pref-height: 25em; -fx-min-width: 22em; -fx-pref-width: 36em");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(Resource.get(RES, "martialartssection.listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.warn("onAdd");
//		if (!control.getSINController().canCreateNewLicense(SIN.Quality.ANYONE)) {
//			logger.warn("UI connection creation initated, but generator does not allow it");
//			return;
//		}
		
		MartialArtsSelector selector = new MartialArtsSelector(control.getMartialArtsController());
		SelectorWithHelp<MartialArts> pane = new SelectorWithHelp<MartialArts>(selector);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "martialartsection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
//		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(RES, "martialartsection.selector.title"), selector);
		logger.debug("Adding license dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			logger.info("Adding martial art: "+selector.getSelected());
			MartialArtsValue val = control.getMartialArtsController().select(selector.getSelected());
			if (val!=null) {
				logger.debug("Created martial art was "+val);
			} else {
				logger.warn("Failed to select");
			}
		}
	}

	//-------------------------------------------------------------------
	protected void onDelete() {
		MartialArtsValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Martial art to deselect: "+selected);
		control.getMartialArtsController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<MartialArtsValue> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		todos.addAll(control.getSINController().getToDos());
		
//		ptsToSpend.setText( String.valueOf( ctrl.getPowerController().getPowerPointsLeft()));
//		ptsTotal.setText( String.valueOf( ctrl.getCharacter().getPowerPoints()));
//		btnDec.setDisable(!ctrl.getPowerController().canDecreasePowerPoints());
//		btnInc.setDisable(!ctrl.getPowerController().canIncreasePowerPoints());
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getMartialArts());
	}

	//-------------------------------------------------------------------
	private void onSettings() {
		
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : control.getMartialArtsController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

}
