/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.chargen.jfx.listcells.RitualListCell;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualSelectionPane;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class RitualSelector extends OptionalDescriptionPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(RitualSelectionPane.class.getName());
	
	private RitualController ctrl;

	private ListView<Ritual> lvAvailable;
	private RitualDescriptionPane description;
	private Label hdAvailable;
	private Label hdInfo;
	private Button btnDelKnow;

	//-------------------------------------------------------------------
	public RitualSelector(RitualController ctrl) {
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdInfo  = new Label(Resource.get(UI, "label.info"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdInfo.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdInfo.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		lvAvailable = new ListView<Ritual>();
		lvAvailable.setCellFactory(new Callback<ListView<Ritual>, ListCell<Ritual>>() {
			public ListCell<Ritual> call(ListView<Ritual> param) {
				RitualListCell cell =  new RitualListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		
		description = new RitualDescriptionPane();
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		
		Label phAvailable = new Label(Resource.get(UI, "ritualselector.placeholder.available"));
		phAvailable.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 24em;");
		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);

		column1.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);

		setChildren(column1, description);

		description.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(description, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> description.setData(n));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableRituals());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Ritual getSelected() {
		return lvAvailable.getSelectionModel().getSelectedItem();
	}

}
