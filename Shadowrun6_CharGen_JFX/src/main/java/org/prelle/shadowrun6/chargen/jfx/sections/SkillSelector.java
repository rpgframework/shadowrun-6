package org.prelle.shadowrun6.chargen.jfx.sections;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.chargen.jfx.listcells.SkillListCell;

import javafx.scene.Node;
import javafx.scene.control.ListView;

/**
 * @author Stefan Prelle
 *
 */
public class SkillSelector extends ListView<Skill> implements IListSelector<Skill> {
	
	//-------------------------------------------------------------------
	public SkillSelector(SkillController ctrl, SkillType...types) {
		getItems().addAll(ctrl.getAvailableSkills(types));
		this.setCellFactory(lv -> new SkillListCell(ctrl));
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

}
