package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditConnectionDialog;
import org.prelle.shadowrun6.chargen.jfx.listcells.ConnectionListCell;

import javafx.scene.control.Label;

/**
 * @author Stefan Prelle
 *
 */
public class ConnectionSection extends GenericListSection<Connection> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ConnectionSection.class.getName());

	//-------------------------------------------------------------------
	public ConnectionSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> {
			ConnectionListCell cell = new ConnectionListCell(ctrl, provider);
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2)
					onEdit(cell.getItem(), cell);
			});
			return cell;
		});
		initPlaceholder();
		
		refresh();
		list.setStyle("-fx-pref-height: 25em; -fx-min-width: 22em; -fx-pref-width: 36em");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(RES.getString("connectionsection.listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.warn("onAdd");
		if (!control.getConnectionController().canCreateConnection()) {
			logger.warn("UI connection creation initated, but generator does not allow it");
			return;
		}
		
		Connection toAdd = new Connection();
		EditConnectionDialog dialog = new EditConnectionDialog(toAdd, false);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
		logger.debug("Adding Connection dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			logger.info("Adding Contact: "+toAdd);
			Connection added = control.getConnectionController().createConnection();
			added.setName(toAdd.getName());
			added.setType(toAdd.getType());
			added.setDescription(toAdd.getDescription());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		Connection toDelete = list.getSelectionModel().getSelectedItem();
		if (toDelete!=null) {
			logger.info("Try remove Contact: "+toDelete);
			control.getConnectionController().removeConnection(toDelete);
			list.getItems().remove(toDelete);
			list.getSelectionModel().clearSelection();
		}
	}

	//-------------------------------------------------------------------
	protected void onEdit(Connection data, ConnectionListCell cell) {
		logger.debug("onEdit");

		EditConnectionDialog dialog = new EditConnectionDialog(data, true);
		provider.getScreenManager().showAndWait(dialog);
		logger.info("Edited connection "+data);
		cell.updateItem(data, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getCharacter().getConnections());
		getAddButton().setDisable(!control.getConnectionController().canCreateConnection());
	}

}
