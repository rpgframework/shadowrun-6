package org.prelle.shadowrun6.chargen.jfx.sections;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.PluginListCell;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RulePlugin;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class CharPluginListSection extends SingleSection {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(CharPluginListSection.class.getName());

	private CharacterController ctrl;
	private VBox layout;
	private CheckBox cbShowOnlyMyLanguagePlugins;
	private ListView<RulePlugin<ShadowrunCharacter>> options;
	private Instant last = Instant.now();
	private boolean runProcPending;

	//-------------------------------------------------------------------
	/**
	 * @param provider
	 */
	public CharPluginListSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, new VBox(20));
		this.ctrl = ctrl;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbShowOnlyMyLanguagePlugins = new CheckBox(ResourceI18N.get(RES, "label.showOnlyMyLanguage"));
		
		options= new ListView<RulePlugin<ShadowrunCharacter>>();
		options.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		options.setCellFactory(new Callback<ListView<RulePlugin<ShadowrunCharacter>>, ListCell<RulePlugin<ShadowrunCharacter>>>() {
			public ListCell<RulePlugin<ShadowrunCharacter>> call(ListView<RulePlugin<ShadowrunCharacter>> arg0) {
				PluginListCell cell = new PluginListCell(ctrl.getCharacter());
				cell.selectedCheckboxProperty().addListener( (ov,o,n) -> selectionChanged());
				return cell;
			}
		});

		// Fill with data
		options.getItems().addAll(ShadowrunCore.getPlugins());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		options.setStyle("-fx-max-width: 25em");
		layout = (VBox) getContent(); 
		layout.setStyle("-fx-padding: 1em");
		layout.getChildren().addAll(cbShowOnlyMyLanguagePlugins, options);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbShowOnlyMyLanguagePlugins.selectedProperty().addListener( (ov,o,n) -> refreshContent());
	}

	//-------------------------------------------------------------------
	private void refreshContent() {
		List<RulePlugin<ShadowrunCharacter>> list = ShadowrunCore.getPlugins();
		if (cbShowOnlyMyLanguagePlugins.isSelected()) {
			for (RulePlugin<ShadowrunCharacter> plugin : new ArrayList<RulePlugin<ShadowrunCharacter>>(list)) {
				if (!plugin.getLanguages().contains(Locale.getDefault().getLanguage())) {
					list.remove(plugin);
					ctrl.getCharacter().removePermittedPlugin(plugin.getID());
				}
			}
		}
		options.getItems().setAll(list);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
	}

	//-------------------------------------------------------------------
	/**
	 * Refresh after selected plugins changed, but ensure 2 seconds between
	 * to re-runs
	 */
	protected void selectionChanged() {
		if (last.plusSeconds(2).isBefore(Instant.now())) {
			// More than 2 seconds ago
			ctrl.runProcessors();
			last = Instant.now();
			runProcPending = false;
		} else {
			if (!runProcPending) {
				runProcPending = true;
				Thread thread = new Thread( () -> {
					try { Thread.sleep(2000); } catch (InterruptedException e) {}
					ctrl.runProcessors();
					last = Instant.now();
					runProcPending = false;
				} );
				thread.start();
			}
		}
		
	}

}
