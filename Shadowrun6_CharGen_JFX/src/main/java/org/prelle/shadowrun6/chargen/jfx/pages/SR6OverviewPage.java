package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.AttributeSection;
import org.prelle.shadowrun6.chargen.jfx.sections.BasicDataSection;
import org.prelle.shadowrun6.chargen.jfx.sections.PortraitSection;
import org.prelle.shadowrun6.chargen.jfx.sections.QualitySection;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;

/**
 * @author Stefan Prelle
 *
 */
public class SR6OverviewPage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6OverviewPage.class.getName());

	private BasicDataSection basic;
	private PortraitSection     portrait;
	private AttributeSection    attributes;
	private QualitySection      qualities;

	private Section secBasic;
	private Section secAttr;

	//-------------------------------------------------------------------
	public SR6OverviewPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-overview");
		this.setTitle(control.getCharacter().getName());
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;

		initComponents();
		initLayout();
//		initCommandBar();

		try {
			descrBtnEdit.setVisible(false);
		} catch (Throwable e) {
		}
		refresh();
	}

//	//-------------------------------------------------------------------
//	private void initCommandBar() {
//		/*
//		 * Command bar
//		 */
//		firstLine = new SR6FirstLine();
//		getCommandBar().setContent(firstLine);
//		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
//		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
//		if (handle!=null)
//			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
//		
//	}

	//-------------------------------------------------------------------
	private void initBasicData() {
		basic = new BasicDataSection(UI.getString("section.basedata"), control, mode, provider);
		portrait = new PortraitSection(UI.getString("section.portrait"), control, provider);

		secBasic = new DoubleSection(basic, portrait);
		getSectionList().add(secBasic);

		// Interactivity
		basic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initAttributes() {
		attributes = new AttributeSection(UI.getString("section.attributes"), control, mode, provider);
		qualities = new QualitySection(UI.getString("section.qualities"), control, provider);

		secAttr = new DoubleSection(attributes, qualities);
		getSectionList().add(secAttr);

		// Interactivity
		attributes.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		qualities.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initBasicData();
		initAttributes();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (getManager()==null)
			setManager(provider.getManager());
		try { descrBtnEdit.setVisible(data!=null); } catch (Throwable e) {}
		this.helpData = data;
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh  "+control.getCharacter().getName());
		super.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.overview"));
//		setPointsFree(control.getCharacter().getKarmaFree());
//		firstLine.setData(control.getCharacter());

		attributes.getToDoList().clear();
		attributes.getToDoList().addAll(control.getAttributeController().getToDos());
		qualities.getToDoList().clear();
		qualities.getToDoList().addAll(control.getQualityController().getToDos());

		basic.refresh();
		
		if (attributes!=null) attributes.refresh();
		if ( qualities!=null)  qualities.refresh();
	}

}
