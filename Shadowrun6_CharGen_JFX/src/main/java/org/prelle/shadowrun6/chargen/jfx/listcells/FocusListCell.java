package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.chargen.jfx.sections.FociSection;
import org.prelle.shadowrun6.items.ItemTemplate.Legality;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class FocusListCell extends ListCell<Focus> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(FociSection.class.getName());

	private final static String NORMAL_STYLE = "focus-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private FocusController control;
	private Focus data;

	private Label lblName;
	private Label lblAvail;
	private Label lblPrice;
	private Label lblKarma;
	private HBox  layout;

	//-------------------------------------------------------------------
	public FocusListCell(FocusController ctrl) {
		this.control = ctrl;
		initComponents();
		initLayout();
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("base");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);

		lblKarma = new Label();
		lblKarma.getStyleClass().add("title");

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");

		lblAvail = new Label();
		lblAvail.getStyleClass().add("itemprice-label");		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdAvail = new Label(Resource.get(UI, "label.availability"));
		Label hdPrice = new Label(Resource.get(UI, "label.cost"));
		HBox bxDetails = new HBox(5);
		bxDetails.getChildren().addAll(hdPrice, lblPrice, hdAvail, lblAvail);
		HBox.setMargin(hdAvail, new Insets(0, 0, 0, 20));
		
		VBox col1 = new VBox(lblName, bxDetails);
		col1.setMaxWidth(Double.MAX_VALUE);
		
		layout = new HBox(10, col1, lblKarma);
		HBox.setHgrow(col1, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Focus item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			String rating = Resource.get(UI, "label.rating");
			lblName.setText(((Focus)item).getName());
			lblPrice.setText(rating+"*"+item.getNuyenCost()+"\u00A5");
			lblKarma.setText("x"+item.getBondingMultiplier());
			// Availability
			if (item.getAvailabilityPlus()>0)
				lblAvail.setText("("+rating+"+"+item.getAvailabilityPlus()+") "+Legality.LEGAL.getShortCode());
			else
				lblAvail.setText(rating+" "+Legality.LEGAL.getShortCode());
//			if (control.canBeSelected(item, 1)) {
//				lblPrice.getStyleClass().remove(NOT_MET_STYLE);
//				lblAvail.getStyleClass().remove(NOT_MET_STYLE);
//			} else {
//				lblPrice.getStyleClass().add(NOT_MET_STYLE);
//				lblAvail.getStyleClass().add(NOT_MET_STYLE);
//			}
			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("gear:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}


}
