/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.SignatureManeuver;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.ConnectionSection;
import org.prelle.shadowrun6.chargen.jfx.sections.SignatureManeuverSection;

import de.rpgframework.ResourceI18N;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SignatureManeuverListCell extends ListCell<SignatureManeuver> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SignatureManeuverSection.class.getName());

	private final static String NORMAL_STYLE = "maneuver-cell";

	private CharacterController charGen;
	private ScreenManagerProvider provider;
	
	private Label lbName;
	private Label lbSkill;
	private Label lbActions;
	private Label lbCost;
	private Label lbWarning;
	private VBox bxNameSkill;
	private HBox layout;

	//---------------------------------------------------------
	public SignatureManeuverListCell(CharacterController control, ScreenManagerProvider provider) {
		this.charGen = control;
		this.provider= provider;
		
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbSkill= new Label();
		lbActions = new Label();
		lbWarning = new Label("Not connected with quality");
		lbWarning.setText(ResourceI18N.format(UI, "label.warning.format", ShadowrunCore.getQuality("signature_maneuver").getName()));
		lbWarning.setStyle("-fx-text-fill: red");
		bxNameSkill = new VBox(5, new HBox(5,lbName, lbSkill), lbActions, lbWarning);
		bxNameSkill.setMaxWidth(Double.MAX_VALUE);
		lbCost = new Label();
		lbCost.setStyle("-fx-font-size: 200%; -fx-font-weight: bold");
		layout = new HBox(10, bxNameSkill, lbCost);
		layout.setAlignment(Pos.BASELINE_LEFT);
		HBox.setHgrow(bxNameSkill, Priority.ALWAYS);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SignatureManeuver item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			lbSkill.setText("("+item.getSkill().getName()+")");
			lbActions.setText(item.getAction1().getName()+" + "+item.getAction2().getName());
			int cost = item.getAction1().getCost() + item.getAction2().getCost();
			lbCost.setText(String.valueOf(cost));
			boolean warn = item.getQuality()==null;
			lbWarning.setVisible(warn);
			lbWarning.setManaged(warn);
			setGraphic(layout);
		}
	}
}
