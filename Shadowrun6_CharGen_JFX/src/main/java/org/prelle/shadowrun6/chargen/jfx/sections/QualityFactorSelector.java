package org.prelle.shadowrun6.chargen.jfx.sections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.DesignOptionAndModListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.vehicle.DesignMod;
import org.prelle.shadowrun6.vehicle.QualityFactor;
import org.prelle.shadowrun6.vehiclegen.VehicleGenerator;

import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class QualityFactorSelector extends VBox implements IListSelector<BasePluginData> {
	
	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController charGen;
	private VehicleGenerator control;
	private NavigButtonControl btnControl;

	private ListView<BasePluginData> list;

	//-------------------------------------------------------------------
	/**
	 */
	public QualityFactorSelector(CharacterController charGen, VehicleGenerator control) {
		this.charGen = charGen;
		this.control = control;
		btnControl   = new NavigButtonControl();
		btnControl.setDisabled(CloseType.OK, true);

		initComponents();
		initLayout();
		initInteractivity();

		list.getItems().addAll(control.getAvailableQualityFactors());
		list.setCellFactory(lv -> new DesignOptionAndModListCell());
//		cbSubTypes.getSelectionModel().select(0);
		refreshOkayButton();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<BasePluginData>();
		list.setStyle("-fx-pref-width: 26em");
		list.setCellFactory( lv -> new DesignOptionAndModListCell());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setSpacing(10);
		getChildren().addAll(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ovo,o,n) -> refreshOkayButton());
	}

	//-------------------------------------------------------------------
	private void refreshOkayButton() {
		QualityFactor selected = (QualityFactor) list.getSelectionModel().getSelectedItem();
		if (selected==null) {
			btnControl.setDisabled(CloseType.OK, false);
			return;
		}
		
		
		boolean canBeSelected = false;

		canBeSelected = control.canBeSelected(selected);
		logger.info("canBeSelected("+selected+") = "+canBeSelected);

		btnControl.setDisabled(CloseType.OK, 
				list.getSelectionModel().getSelectedItem()==null || 
				!canBeSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<BasePluginData> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public QualityFactor getSelected() {
		return (QualityFactor) list.getSelectionModel().getSelectedItem();
	}

}
