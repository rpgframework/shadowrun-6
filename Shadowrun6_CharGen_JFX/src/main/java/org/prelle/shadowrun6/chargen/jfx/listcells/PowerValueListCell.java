package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.chargen.jfx.panels.PowerSpecializationPane;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class PowerValueListCell extends ListCell<AdeptPowerValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".powers");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "power-cell";

	private AdeptPowerController charGen;
	private ListView<AdeptPowerValue> parent;
	private ScreenManagerProvider provider;

	private transient AdeptPowerValue data;

	private HBox layout;
	private Label name;
	private Label attrib;
	private Label total;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	private VBox  bxSpecial;

	//-------------------------------------------------------------------
	public PowerValueListCell(AdeptPowerController charGen, ListView<AdeptPowerValue> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content		
		layout  = new HBox(5);
		name    = new Label();
		attrib  = new Label();
		total   = new Label();
		bxSpecial = new VBox(1);
		btnEdit = new Button("\uE1C2");
		btnDec = new Button("\uE0C6");
		btnDec.getStyleClass().addAll("mini-button");
		btnInc = new Button("\uE0C5");
		btnInc.getStyleClass().addAll("mini-button");
		lblVal  = new Label("?");

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent");
		btnEdit.setVisible(false);

		//		setStyle("-fx-pref-width: 24em");
		layout.getStyleClass().add("content");		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name, attrib);		

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, bxSpecial);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		VBox foo = new VBox(2);
		foo.getChildren().addAll(total, tiles);
		layout.getChildren().addAll(bxCenter, foo);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			charGen.increase(data);
			updateItem(data, false); 
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			charGen.decrease(data);
			updateItem(data, false); 
			parent.refresh();
		});
		btnEdit.setOnAction(event -> {
			editClicked(data);
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		String id = "power:"+data.getModifyable().getId()+":"+data.getUniqueId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(AdeptPowerValue item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty || item==null) {
			setText(null);
			setGraphic(null);			
		} else {
			data = item;
			name.setText(item.getName());
			lblVal.setText(String.valueOf(item.getLevel()));
			total.setText(String.valueOf(item.getCost()));
			btnDec.setVisible(item.getModifyable().hasLevels());
			lblVal.setVisible(item.getModifyable().hasLevels());
			btnInc.setVisible(item.getModifyable().hasLevels());
			btnDec.setDisable(!charGen.canBeDecreased(data));
			btnInc.setDisable(!charGen.canBeIncreased(data));
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			bxSpecial.getChildren().clear();
			//				if (item.getSkillSpecializations().isEmpty()) {
			//					Label phSpecial = new Label(UI.getString("skillvaluelistview.skillvaluelistcell.placeholder"));
			//					phSpecial.setStyle("-fx-font-style: italic");
			//					bxSpecial.getChildren().add(phSpecial);
			//				} else {
			//					for (SkillSpecializationValue spec : item.getSkillSpecializations()) {
			//						Label phSpecial = new Label(spec.getName());
			//						//					phSpecial.setStyle("-fx-font-style: italic");
			//						bxSpecial.getChildren().add(phSpecial);
			//					}
			//				}
			imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			//				lblType.setText(null);
			//				lblVal.setText(" "+String.valueOf(item.getPoints())+" ");
			//				int sum = charGen.getModel().getAttribute(attr).getModifiedValue() + item.getModifiedValue();
			//				total.setText(String.format(UI.getString("skillvaluelistview.skillvaluelistcell.total"), sum));
			//				btnEdit.setText("\uE1C2");
			//				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
			//				btnDec.setDisable(!charGen.canBeDecreased(item));
			//				btnInc.setDisable(!charGen.canBeIncreased(item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(AdeptPowerValue data) {
		logger.debug("editClicked");

		PowerSpecializationPane dia = new PowerSpecializationPane(charGen, data);

		provider.getScreenManager().showAlertAndCall(
				AlertType.NOTIFICATION, 
				UI.getString("powerselectpane.specialization.dialog.header"), 
				dia);
		GenerationEventDispatcher.removeListener(dia);
		updateItem(data, false);
	}

}