/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.PowerListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.PowerValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class PowerSelectionPane extends HBox {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(PowerSelectionPane.class.getName());
	
	private CharacterController charGen;
	private AdeptPowerController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<AdeptPower> lvAvailable;
	private ListView<AdeptPowerValue> lvSelected;
	private Button btnDelKnow;
	
	private AdeptPower selectedAvailable;

	private ObjectProperty<AdeptPower> showHelpFor = new SimpleObjectProperty<AdeptPower>();

	//-------------------------------------------------------------------
	/**
	 */
	public PowerSelectionPane(CharacterController charGen, ScreenManagerProvider provider) {
		this.ctrl = charGen.getPowerController();
		this.model = charGen.getCharacter();
		this.charGen = charGen;
		this.provider = provider;

		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		/* Column 1 */
		lvAvailable = new ListView<AdeptPower>();
		lvAvailable.setCellFactory(new Callback<ListView<AdeptPower>, ListCell<AdeptPower>>() {
			public ListCell<AdeptPower> call(ListView<AdeptPower> param) {
				PowerListCell cell =  new PowerListCell(ctrl);
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) {
						AdeptPower data = cell.getItem();
						if (data.needsChoice()) {
							Platform.runLater(new Runnable() {
								public void run() {
									logger.debug("To select "+data+", a further choice is necessary");
									Object selection = ShadowrunJFXUtils.choose(charGen, provider, model, data.getSelectFrom(), data.getName());
									ctrl.select(data, selection);
								}});
						} else {
							ctrl.select(data);
						}
					}
				});
				return cell;
			}
		});
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		lvSelected  = new ListView<AdeptPowerValue>();
		lvSelected.setCellFactory(new Callback<ListView<AdeptPowerValue>, ListCell<AdeptPowerValue>>() {
			public ListCell<AdeptPowerValue> call(ListView<AdeptPowerValue> param) {
				PowerValueListCell cell =  new PowerValueListCell(ctrl, lvSelected, provider);
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		
		Label phAvailable = new Label(UI.getString("placeholder.available"));
		Label phSelected  = new Label(UI.getString("placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdAvailable = new Label(Resource.get(UI, "label.available"));
		Label hdSelected  = new Label(Resource.get(UI, "label.selected"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdSelected.setStyle("-fx-font-size: 130%");

		lvAvailable.setStyle("-fx-pref-width: 20em;");
		lvSelected .setStyle("-fx-pref-width: 21em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);

		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		Region grow2 = new Region();
		grow2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow2, Priority.ALWAYS);
		HBox lineKnow = new HBox(5, grow2, btnDelKnow);
		column2.getChildren().addAll(hdSelected, lineKnow, lvSelected);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected .setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set((n!=null)?n.getModifyable():null));
		lvSelected.setOnDragDropped (event -> droppedOnSelected(event));
		lvSelected.setOnDragOver    (event -> dragOverSelected(event));
		lvAvailable.setOnDragDropped(event -> droppedOnAvailable(event));
		lvAvailable.setOnDragOver   (event -> dragOverAvailable(event));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailablePowers());

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getAdeptPowers());

	}

	//-------------------------------------------------------------------
	private void droppedOnSelected(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("power")) {
					AdeptPower data = ShadowrunCore.getAdeptPower(tail);
					if (data==null) {
						logger.warn("Cannot find adept power for dropped power id '"+tail+"'");						
					} else {
						event.setDropCompleted(success);
						event.consume();
						if (data.needsChoice()) {
							Platform.runLater(new Runnable() {
								public void run() {
									logger.debug("To select "+data+", a further choice is necessary");
									Object selection = ShadowrunJFXUtils.choose(charGen, provider, model, data.getSelectFrom(), data.getName());
									ctrl.select(data, selection);
								}});
						} else {
							ctrl.select(data);
						}
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private AdeptPowerValue getAdeptPowerValueByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.equals("power")) {
				StringTokenizer tok = new StringTokenizer(tail,":");
				tail = tok.nextToken();
				String uuid = (tok.hasMoreTokens())?tok.nextToken():null;
				AdeptPowerValue pVal = null;
				for (AdeptPowerValue tmp : model.getAdeptPowers()) {
					if (!tmp.getModifyable().getId().equals(tail))
						continue;
					if (uuid==null || tmp.getUniqueId().toString().equals(uuid)) {
						pVal = tmp;
						break;
					}
				}
				return pVal;
			} 
		}
		return null;
	}

	//-------------------------------------------------------------------
	private AdeptPower getAdeptPowerByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("power")) {
				return ShadowrunCore.getAdeptPower(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			AdeptPower qual = getAdeptPowerByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void droppedOnAvailable(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			logger.debug("Dropped "+db.getString());
			AdeptPowerValue data = getAdeptPowerValueByEvent(event);
			if (data==null) {
				logger.warn("Cannot find power for dropped power");						
//			} else if (!data.isFreeSelectable()) {
//				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
//				if (model.hasAdeptPower(data.getId())) {
//					AdeptPowerValue ref = model.getAdeptPower(data.getId());
				logger.info("Deselect "+data);
					ctrl.deselect(data);
//				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			AdeptPowerValue qual = getAdeptPowerValueByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public AdeptPower getSelectedAvailable() {
		return selectedAvailable;
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<AdeptPower> showHelpForProperty() {
		return showHelpFor;
	}

}
