package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ComponentElementView;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemEnhancementListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemEnhancementValueObjectListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemTemplateListCell2;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * @author Stefan Prelle
 *
 */
public class NewEditCarriedItemDialog extends ManagedDialog implements GenerationEventListener {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(NewEditCarriedItemDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController control;
	private ScreenManagerProvider provider;

	private NavigButtonControl btnControl;

	private ComponentElementView view;
	private TextField tfCustomName;
	private TextArea  taCustomDesc;
	private ValueInfo priceAndCount;
	
	private OptionalDescriptionPane content;
	private DescriptionPane description;
	private CarriedItem selectedItem;
	
	private Map<ItemHook, Integer> positionCache;

	//--------------------------------------------------------------------
	public NewEditCarriedItemDialog(CharacterController ctrl, CarriedItem data, ScreenManagerProvider prov) {
		super(ResourceI18N.get(UI,"dialog.title"), null, CloseType.APPLY);
		if (prov==null)
			throw new NullPointerException("ScreenManagerProvider");
		this.control = ctrl;
		this.selectedItem    = data;
		this.provider = prov;
		this.positionCache = new HashMap<ItemHook, Integer>();
		btnControl = new NavigButtonControl();
		
		initCompoments();
		initLayout();
		initInteractivity();
		refresh();
		
		description.setText(data.getItem());
		
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		taCustomDesc = new TextArea();
		taCustomDesc.setPromptText(ResourceI18N.get(UI, "label.custom.description"));
		tfCustomName = new TextField();
		tfCustomName.setPromptText(selectedItem.getItem().getName());
		
		priceAndCount = new ValueInfo(selectedItem, control);
		
		view = new ComponentElementView();
		view.setTitle(selectedItem.getName());
		view.setSubtitle(selectedItem.getUsedAsSubType().getName());
		description = new DescriptionPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Layout
		content = new OptionalDescriptionPane(view, description);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		taCustomDesc.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setNotes(null);
			} else {
				selectedItem.setNotes(n);
			}
		});
		tfCustomName.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setName(null);
			} else {
				selectedItem.setName(n);
			}
		});
		
		// Requires update inn JavaFXExtension
//		for (int i=3; i<=12; i++) {
//			view.getListView(i);
//		}
	
		try {
			view.setOnEditAction(ev -> showImageDialog());
		} catch (Throwable e) {
			logger.error("Cannot listen to image change requests: "+e);
		}
	}

	//--------------------------------------------------------------------
	private void positionSlot(AvailableSlot slot) {
		boolean large = false;
		int index = -1;
		switch (slot.getSlot()) {
		// Available slots: 2-6, 8-11 (1 = data, 7 = name and notes, 12 = cost and number)

		// Melee weapons
		// Slot 2 is used for modifications
		case MELEE_EXTERNAL    : index= 9; break; 
		// Firearms
		// Slot 2 is used for modifications
		case UNDER             : index= 3; break;
		case UNDER_WEAPON_MOUNT: index= 3; break; // Ok to overlap with UNDER as UNDER_WEAPON_MOUNT replaces UNDER
		case STOCK             : index= 4; break;
		// case INTERNAL          : index= 4; break; // This slot is no longer used
		case WEAPON_SECURITY   : index= 5; break; // Overlaps with SIDE_R, ideally choice of WEAPON_SECURITY should be a mod inside the modification, not a new slot
		case SIDE_R            : index= 5; break; 
		case SIDE_L            : index= 6; break;
		// case SMARTGUN          : index= 6; break; // This slot is no longer used
		case OPTICAL           : index= 8; break; // Created by smartgun_system, also affects ELECTRONICS OPTICAL
		case RANGED_EXTERNAL   : index= 9; break;
		case FIREARMS_EXTERNAL : index= 9; break;
		case TOP               : index=10; break;
		case BARREL            : index=11; break;

		// Armor
		case ARMOR             : index= 2; break;		
		case ARMOR_REACTIVE    : index= 3; break;		
		case ARMOR_ADDITION    : index= 4; break;		
		case ARMOR_MEMS        : index= 8; break;		
		case HELMET_ACCESSORY  : index= 9; break;		

		// Cyberware
		case HEADWARE_IMPLANT   : index= 2; break;		
		case SKILLJACK          : index= 3; break;		
		case CYBERLIMB_IMPLANT  : index= 4; break;		
		case CYBEREYE_IMPLANT   : index= 8; break;		
		case CYBEREAR_IMPLANT   : index= 9; break;		

		// Vehicles
//		case VEHICLE_PROTECTION : index= 3; break; 
		case VEHICLE_CHASSIS    : index= 2; break;
		case VEHICLE_ELECTRONICS: index= 3; break;
		case VEHICLE_POWERTRAIN : index= 4; break;
		// note: SOFTWARE_DRONE uses slot 5, see Kommlinks
		case VEHICLE_TIRES      : index= 6; break;
		case VEHICLE_ACCESSORY  : index= 8; break;
		case VEHICLE_BODY       : index= 9; break;
		case VEHICLE_CF         : index=10; break; // place for VEHICLE_WEAPON not needed, as those are inside hardpoints (VEHICLE_BODY)
//		case VEHICLE_COSMETICS  : index=11; break;

		// Kommlinks/Rigger Consoles/Decks/Tac-Nets
		case ELECTRONIC_ACCESSORY: index= 3; break;
		case SOFTWARE_DRONE      : index= 5; break; 
		case SOFTWARE            : index= 8; break;
		case SOFTWARE_RIGGER     : index= 9; break;
		case SOFTWARE_TACNET     : index=10; break;

		// Devices
		// note: OPTICAL uses slot 8, see Firearms
		case AUDIO             : index= 9; break;		
		case SENSOR_HOUSING    : index= 2; break;
		case SENSOR_FUNCTION   : index= 3; break; 
		
		// Instruments
		case INSTRUMENT_SLOT  : index= 2; break;
		case INSTRUMENT_WEAPON: index= 9; break;

		// Procam
		// note: OPTICAL uses slot 8, see Firearms
		case PROCAM_SLOT      : index= 3; break;
		}
		
		// Consult cache, if necessary
		if (index<0 && positionCache.containsKey(slot.getSlot())) {
			index = positionCache.get(slot.getSlot());
		}
		
		
		
		if (index<0) {
			logger.error("No instruction where to position slot "+slot.getSlot());
			boolean found = false;
			for (int i=2; i<=12; i++) {
				if (view.getList(i).isEmpty()) {
					logger.error("Guess slot "+i+" for "+slot.getSlot());
					if (slot.getSlot().hasCapacity()) {
						view.setName(i, slot.getSlot().getName()+"  "+slot.getFreeCapacity()+"/"+slot.getCapacity());  // Eventually add capacity here
					} else {
						view.setName(i, slot.getSlot().getName());  // Eventually add capacity here
					}
					view.getList(i).setAll(slot.getAllEmbeddedItems());
					final int staticI = i;
//					view.setOnAddAction(i-1, addClicked(slot.getSlot()));
					view.setCellFactory(i, (lv)-> new EditDialogCarriedItemListCell(control, view.getList(staticI), provider, selectedItem));
					view.setOnAddAction(i, ev -> addClicked(slot.getSlot()));
					positionCache.put(slot.getSlot(), i);
					found = true;
					break;
				}
			}
			if (!found) {
				logger.error("Could not display list for slot "+slot.getSlot());
			}
		} else  {
			final int staticIndex = index;
			if (slot.getSlot().hasCapacity()) {
				view.setName(index, slot.getSlot().getName()+"  "+slot.getFreeCapacity()+"/"+slot.getCapacity());  // Eventually add capacity here
			} else {
				view.setName(index, slot.getSlot().getName());  // Eventually add capacity here
			}
			view.getList(index).setAll(slot.getAllEmbeddedItems());
			view.setCellFactory(index, (lv)-> new EditDialogCarriedItemListCell(control, view.getList(staticIndex), provider, selectedItem));
			view.setOnAddAction(index, ev -> addClicked(slot.getSlot()));
		}
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		
		logger.info("refresh");
		if (selectedItem.getImage() != null) {
			Image img = new Image(new ByteArrayInputStream(selectedItem.getImage()));
			view.setImage(img);
			view.setEditButton(true);
		} else {
			String imgName = "Placeholder_" + selectedItem.getUsedAsSubType() + ".png";
			switch (selectedItem.getUsedAsSubType()) {
			case BLADES:
			case WHIPS:
			case PISTOLS_HEAVY:
			case PISTOLS_LIGHT:
			case MACHINE_PISTOLS:
			case THROWERS:
				view.setEditButton(true);
				break;
			case LMG:
			case MMG:
			case HMG:
				imgName = "Placeholder_Machine_Guns.png";
				view.setEditButton(true);
				break;
			case MICRODRONES:
			case MINIDRONES:
			case SMALL_DRONES:
			case MEDIUM_DRONES:
			case LARGE_DRONES:
				imgName = "Placeholder_Drone.png";
				view.setEditButton(true);
				break;
			default:
				try {
					if (ItemType.isDrone(selectedItem.getUsedAsType())) {
						imgName = "Placeholder_Drone.png";
						view.setEditButton(true);
					}
					if (List.of(ItemType.vehicleTypes()).contains(selectedItem.getUsedAsType())) {
						view.setEditButton(true);
					}
					if (List.of(ItemType.weaponTypes()).contains(selectedItem.getUsedAsType())) {
						view.setEditButton(true);
					}
				} catch (NoSuchMethodError e) {
					// Catch error, that older Genesis starter cannot find "view.setEditButton()"
				}
			}
			InputStream ins = NewEditCarriedItemDialog.class.getResourceAsStream(imgName);
			if (ins != null) {
				view.setImage(new Image(ins));
			} else {
				view.setImage(null);
			}
		}
		
		switch (selectedItem.getUsedAsType()) {
		case WEAPON_CLOSE_COMBAT:
		case WEAPON_RANGED:
		case WEAPON_FIREARMS:
		case AMMUNITION: // to enable modifications for grenades
		case WEAPON_SPECIAL:
			view.setName(2, ResourceI18N.get(UI, "label.modifications"));
			view.getList(2).setAll(selectedItem.getEnhancements());
			view.setCellFactory(2, (lv)-> new ItemEnhancementValueObjectListCell(selectedItem,control.getEquipmentController()));
			view.setOnAddAction(2, ev -> addModificationClicked());
			break;
		}
		
		// Add fixed components
		// Price/Count
		view.setOverrideComponent(0, priceAndCount);
		priceAndCount.refresh();
		
		view.setOverrideComponent(1, ItemUtilJFX.getFlexItemInfoNode(selectedItem, control));
		
		// Custom
		Label hdName = new Label(ResourceI18N.get(UI, "label.custom.name"));
		tfCustomName.setMaxWidth(Double.MAX_VALUE);
		hdName.getStyleClass().add("base");
		HBox bxLine = new HBox(5, hdName, tfCustomName);
		HBox.setHgrow(tfCustomName, Priority.ALWAYS);
		bxLine.setAlignment(Pos.CENTER_LEFT);
		VBox bxCustom = new VBox(5, bxLine, taCustomDesc);
		// Don't show customization options for auto-created items
		if (!selectedItem.isCreatedByModification() && selectedItem.getUsedFocus()==null) {
			view.setOverrideComponent(7, bxCustom);
		}
		
		
		
		for (AvailableSlot slot : selectedItem.getSlots()) {
			positionSlot(slot);
		}
		
		tfCustomName.setText(selectedItem.getCustomName());
		taCustomDesc.setText(selectedItem.getNotes());
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		logger.warn("RCV "+event+"-----------------------------------");
		switch (event.getType()) {
		case EQUIPMENT_ADDED:
		case EQUIPMENT_CHANGED:
		case EQUIPMENT_REMOVED:
		case CHARACTER_CHANGED:
			refresh();
			break;
		default:
		}
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedDialog#onClose(org.prelle.javafx.CloseType)
	 */
	@Override
	public void onClose(CloseType type) {
		logger.debug("close");
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	private void afterTryingToAdd(ItemTemplate master, CarriedItem added) {
		if (added==null) {
			provider.getScreenManager().showAlertAndCall(AlertType.ERROR, 
					Resource.get(UI, "selectiondialog.failed.title"), Resource.format(UI, "selectiondialog.failed.format",master.getName()));
		}
		logger.info("embedding "+master+" in "+selectedItem+" returned "+added);
		if (added!=null) { 
			logger.info("explicitly call refresh independent from event");
			refresh();
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @param slot
	 * @return
	 */
	private void addClicked(ItemHook slot) {
		logger.info("addClicked("+slot+")");
		
		List<ItemTemplate> data = control.getEquipmentController().getEmbeddableIn(selectedItem, slot);
		logger.info("getEmbeddableIn("+selectedItem+") returns "+data.size()+" elements");
		SelectAccessoryDialog dialog = new SelectAccessoryDialog(
				ResourceI18N.get(UI, "dialog.add.accessory.title"), 
				data, slot,
				lv -> new ItemTemplateListCell2(control.getCharacter(), control.getEquipmentController(), true, selectedItem, ItemType.ACCESSORY),
				CloseType.CANCEL, CloseType.OK);
		CloseType closed = provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
		logger.info("Closed via "+closed);
		if (closed==CloseType.OK) {
			for (ItemTemplate master : dialog.getSelection()) {
     			logger.debug("add accessory "+master+" to "+selectedItem+" in slot "+slot);
     			logger.debug("embed "+master+" in "+selectedItem);
     			UseAs usage = master.getUsageFor(slot);
     			logger.info("use for slot "+slot+" is "+usage);
     			ItemType useAs = (usage==null)?master.getNonAccessoryType():usage.getType();
     			logger.info("ItemType is "+useAs);
     			List<SelectionOptionType> options = control.getEquipmentController().getOptions(master, usage);
				try {
					if (!options.isEmpty()) {
						logger.info("ask options");
						Platform.runLater( () -> {
							SelectionOption[] opts = ItemUtilJFX.askOptionsFor( provider, control.getEquipmentController(), master, selectedItem, useAs, selectedItem.getSlot(slot).getFreeCapacity(), usage);
							CarriedItem added = control.getEquipmentController().embed(selectedItem, master, slot, opts);
							logger.info("After adding =============="+selectedItem.getSlot(slot));
							afterTryingToAdd(master, added);
						});
					} else {         			
						logger.info("dont ask options");
	    				CarriedItem added = control.getEquipmentController().embed(selectedItem, master, slot);
						afterTryingToAdd(master, added);
					}
				} catch (Exception e) {
					logger.error("Failed asking for Options",e);
					StringWriter out = new StringWriter();
					e.printStackTrace(new PrintWriter(out));
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2,out.toString());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @param slot
	 * @return
	 */
	private void addModificationClicked() {
		logger.info("addModificationClicked");
		
		List<ItemEnhancement> data = control.getEquipmentController().getAvailableEnhancementsFor(selectedItem);
		SelectPluginDataDialog<ItemEnhancement> dialog = new SelectPluginDataDialog<ItemEnhancement>(
				ResourceI18N.get(UI, "dialog.add.enhancement.title"), 
				data,
				lv -> new ItemEnhancementListCell(control.getCharacter(), control.getEquipmentController(), selectedItem),
				CloseType.CANCEL, CloseType.OK);
		CloseType closed = provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
		logger.info("Closed via "+closed);
		if (closed==CloseType.OK) {
			for (ItemEnhancement master : dialog.getSelection()) {
     			logger.debug("add modification "+master+" to "+selectedItem);
     			ItemEnhancementValue result = control.getEquipmentController().modify(selectedItem, master);
     			logger.debug("embedding "+master+" in "+selectedItem+" returned "+result);
     			if (result==null) {
     				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, ResourceI18N.format(UI, "dialog.add.enhancement.fail", master.getName()));
     			}
			}
		}
	}


	//-------------------------------------------------------------------
	private void showImageDialog() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open Icon 300x200 Pixel max");
		FileChooser.ExtensionFilter imageFilter
	    = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
		chooser.setSelectedExtensionFilter(imageFilter);
		File file = chooser.showOpenDialog(getScene().getWindow());
		if (file!=null) {
			try {
				Image img = new Image(new FileInputStream(file));
				if (img.getWidth()>300 || img.getHeight()>200) {
					Alert alert = new Alert(Alert.AlertType.WARNING, "Max. 300x200 Pixel", ButtonType.CLOSE);
					alert.showAndWait();
					return;
				}
				logger.info("Custom image set");
				selectedItem.setImage(Files.readAllBytes(file.toPath()));
				view.setImage(img);
			} catch (Exception e) {
				logger.error("Failed on image",e);
			}
		} 
	}

}

class EditDialogCarriedItemListCell extends ListCell<Object> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(NewEditCarriedItemDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private CharacterController control;
	private ObservableList<Object> parent;
	private ScreenManagerProvider provider;
	private CarriedItem container;
	
	private Label lbName;
	private Button btnDel;
	private Button btnEdit;
	private MenuItem menUndo;
	private MenuItem menSell;
	private MenuItem menDelete;
	
	private HBox layout;

	//-------------------------------------------------------------------
	public EditDialogCarriedItemListCell(CharacterController charGen, ObservableList<Object> parent, ScreenManagerProvider provider, CarriedItem container) {
		this.control = charGen;
		this.container = container;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;
		getStyleClass().add(NORMAL_STYLE);
		
		FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
		btnDel = new Button(null, icoPopup);
		btnDel.getStyleClass().add("mini-button");
//		btnDel.setOnAction(ev -> {
//			logger.debug("remove accessory "+item+" from "+container.getName());
//			if (control.remove(container, item)) {
//				slot.removeEmbeddedItem(item);
//				refresh();
//			}
//		});
		// Context menu
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
		FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
		icoDelete.setStyle("-fx-padding: 3px");
		icoSell.setStyle("-fx-padding: 3px");
		icoUndo.setStyle("-fx-padding: 3px");
		menUndo = new MenuItem(ResourceI18N.get(UI, "label.removeitem.undo"), icoUndo);
		menSell = new MenuItem(ResourceI18N.get(UI, "label.removeitem.sell"), icoSell);
		menDelete = new MenuItem(ResourceI18N.get(UI, "label.removeitem.trash"), icoDelete);
		ContextMenu menu = new ContextMenu();
		menu.getItems().add(menUndo);
		menu.getItems().add(menSell);
		menu.getItems().add(menDelete);
		btnDel.setContextMenu(menu);

		btnDel.setOnAction(event -> menu.show(btnDel, Side.BOTTOM, 0, 0));
		
		// Edit button
		FontIcon icoEdit  = new FontIcon("\uE17E\uE104");
		btnEdit = new Button(null, icoEdit);
		btnEdit.getStyleClass().add("mini-button");
		btnEdit.setManaged(false);
		btnEdit.setVisible(false);

		
		lbName = new Label();
		lbName.setMaxWidth(Double.MAX_VALUE);
		lbName.setStyle("-fx-font-size: 120%");
		
		layout = new HBox(15, lbName, btnEdit, btnDel);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(lbName, Priority.ALWAYS);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	public void updateItem(Object oitem, boolean empty) {
		super.updateItem(oitem, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			CarriedItem item = (CarriedItem)oitem;
			lbName.setText(item.getNameWithRating());
			btnDel.setVisible(!item.isCreatedByModification());
			menDelete.setOnAction(event -> { logger.info("Trash "+item); control.getEquipmentController().sell(item, 0f); });
			menSell  .setOnAction(event -> { logger.info("Sell  "+item); control.getEquipmentController().sell(item, 0.5f);});
			menUndo  .setOnAction(event -> { logger.info("Undo  "+item); control.getEquipmentController().deselect(item);});
			if (item.getItem().getSlots().size()>0) {
				btnEdit.setManaged(true);
				btnEdit.setVisible(true);
				btnEdit.setOnAction(ev -> {
					logger.info("edit accessory "+item+" in "+container.getName());
					EditCarriedItemDialog dialog = new EditCarriedItemDialog(control, item, provider);
					CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
					if (result==CloseType.OK) {
						this.requestLayout();
					}
				});
			}

			setGraphic(layout);
		}
	}
}

class ValueInfo extends VBox {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(NewEditCarriedItemDialog.class.getName());
	
	private CarriedItem item;
	private CharacterController control;
	
	private Button btnDec;
	private Button btnInc;
	private Label  lbCount;
	private Label  lbValue;
	
	//-------------------------------------------------------------------
	public ValueInfo(CarriedItem item, CharacterController control) {
		this.item = item;
		this.control = control;
		initComponents();		
		initLayout();
		initInteractivity();
		refresh();
	}
	
	//-------------------------------------------------------------------
	public void initComponents() {
		lbValue = new Label();
		lbValue.setStyle("-fx-font-weight: bold; -fx-font-size: 120%");

		lbCount = new Label();
		lbCount.setStyle("-fx-font-weight: bold; -fx-font-size: 120%");
		
		btnDec = new Button(null, new SymbolIcon("remove"));
		btnInc = new Button(null, new SymbolIcon("add"));
		btnDec.getStyleClass().addAll("mini-icon","extended-list-view-button");
		btnInc.getStyleClass().addAll("mini-icon","extended-list-view-button");
	}
	
	//-------------------------------------------------------------------
	public void initLayout() {
		setAlignment(Pos.TOP_CENTER);
		Label hdValue = new Label(ResourceI18N.get(RES, "label.value"));
		hdValue.getStyleClass().add("table-head");
		hdValue.setMaxWidth(Double.MAX_VALUE);
		hdValue.setAlignment(Pos.CENTER);
		getChildren().addAll(hdValue, lbValue);
		
		TilePane bxButtons = new TilePane();
		bxButtons.setVgap(5);
		bxButtons.getChildren().addAll(btnDec, lbCount, btnInc);
		bxButtons.setAlignment(Pos.CENTER);
		Label hdCount = new Label(ResourceI18N.get(RES, "label.count"));
		hdCount.getStyleClass().add("table-head");
		hdCount.setMaxWidth(Double.MAX_VALUE);
		hdCount.setAlignment(Pos.CENTER);
		getChildren().addAll(hdCount, bxButtons);
		
		VBox.setMargin(hdCount, new Insets(20, 0, 0, 0));
		VBox.setMargin(bxButtons, new Insets(5, 0, 0, 0));
	}
	
	//-------------------------------------------------------------------
	public void initInteractivity() {
		btnDec.setOnAction(ev -> control.getEquipmentController().decrease(item));
		btnInc.setOnAction(ev -> control.getEquipmentController().increase(item));
	}
	
	//-------------------------------------------------------------------
	public void refresh() {
		lbValue.setText(item.getAsValue(ItemAttribute.PRICE).getModifiedValue()+" Nuyen");
		lbCount.setText(String.valueOf(item.getCount()));
		btnDec.setDisable(!control.getEquipmentController().canChangeCount(item, item.getCount()-1));
		btnInc.setDisable(!control.getEquipmentController().canChangeCount(item, item.getCount()+1));
	}

}