package org.prelle.shadowrun6.chargen.jfx.sections;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.chargen.jfx.listcells.MetamagicOrEchoListCell;

import javafx.scene.Node;
import javafx.scene.control.ListView;

/**
 * @author Stefan Prelle
 *
 */
public class MetamagicOrEchoSelector extends ListView<MetamagicOrEcho> implements IListSelector<MetamagicOrEcho> {
	
	//-------------------------------------------------------------------
	public MetamagicOrEchoSelector(MetamagicOrEchoController ctrl) {
		getItems().addAll(ctrl.getAvailableMetamagics());
		setCellFactory(lv -> new MetamagicOrEchoListCell());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

}
