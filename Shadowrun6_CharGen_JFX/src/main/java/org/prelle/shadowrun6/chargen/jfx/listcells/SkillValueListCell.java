package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.chargen.jfx.panels.SkillSelectionPane;
import org.prelle.shadowrun6.chargen.jfx.sections.SkillSection;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

public class SkillValueListCell extends ListCell<SkillValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController parentCtrl;
	private SkillController charGen;
	private ListView<SkillValue> parent;

	private transient SkillValue data;

	private HBox layout;
	private Label name;
	private Label attrib;
	private Label total;
	
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane stack;
	private Label lblType;

	private ChoiceBox<SkillSpecialization>  bxSpecial;
	
	private SkillValue previous;
	private boolean isUpdating;

	//-------------------------------------------------------------------
	public SkillValueListCell(CharacterController charGen, ListView<SkillValue> parent) {
		this.parentCtrl = charGen;
		this.charGen = charGen.getSkillController();
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content
		layout  = new HBox(5);
		name    = new Label();
		attrib  = new Label();
		total   = new Label();
		bxSpecial = new ChoiceBox<>();
		bxSpecial.setConverter(new StringConverter<SkillSpecialization>() {
			public String toString(SkillSpecialization val) {
				if (val==null) return "";
				if (val==SkillSpecialization.NONE)
					return Resource.get(UI, "skillspecialization.none");
				return val.getName(); 
			}
			public SkillSpecialization fromString(String string) {return null;}
		});
		btnDec  = new Button("\uE0C6");
		btnDec.getStyleClass().add("mini-button");
		lblVal  = new Label("?");
		btnInc  = new Button("\uE0C5");
		btnInc.getStyleClass().add("mini-button");

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout);
//		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		//		setStyle("-fx-pref-width: 24em");
		layout.getStyleClass().add("content");
//		layout.setStyle("-fx-min-width: 19em; -fx-pref-width: 23em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1Col1 = new HBox(10);
		line1Col1.getChildren().addAll(name, attrib);

//		HBox line2Col1 = new HBox(5);
//		line2Col1.getChildren().addAll(btnEdit, bxSpecial);
//		line2Col1.setMaxWidth(Double.MAX_VALUE);
//		line2Col1.setAlignment(Pos.CENTER_LEFT);

		VBox col1 = new VBox(2);
		col1.getChildren().addAll(line1Col1, bxSpecial);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		HBox line2Col2 = new HBox(4, btnDec, lblVal, btnInc);
		line2Col2.setAlignment(Pos.CENTER_LEFT);
		VBox col2 = new VBox(2);
		col2.getChildren().addAll(total, line2Col2);
		
		layout.getChildren().addAll(col1, col2);

		col1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(col1, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			if (parent.getUserData()!=null && parent.getUserData() instanceof SkillSelectionPane)
				((SkillSelectionPane)parent.getUserData()).setIgnoreRefresh(true);
			charGen.increase((SkillValue)data);
			updateItem(data, false);
			if (parent.getUserData()!=null && parent.getUserData() instanceof SkillSelectionPane)
				((SkillSelectionPane)parent.getUserData()).setIgnoreRefresh(false);
//			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			if (data instanceof SkillValue)
				charGen.decrease((SkillValue)data);
			updateItem(data, false);
//			parent.refresh();
		});
//		btnEdit.setOnAction(event -> {
//			if (data instanceof SkillValue)
//				editClicked((SkillValue)data);
//		});
		bxSpecial.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (isUpdating)
				return;
			if (n==null || n==SkillSpecialization.NONE) {
				logger.debug("Deselect specialization "+o+" - call "+charGen.getClass()+".deselect");
				boolean deselected = charGen.deselect(getItem(), o, false);
				logger.debug("Deselect result was "+deselected);
				if (deselected)
					bxSpecial.getSelectionModel().clearSelection();
				
			} else {
				logger.debug("Select specialization");
				charGen.select(getItem(), n, false);
			}
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = null;
		if (data instanceof SkillValue)
			id = "skill:"+((SkillValue)data).getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	private static SkillSpecialization getFirstSpecialization(SkillValue item) {
		for (SkillSpecializationValue spec : item.getSkillSpecializations()) {
			return spec.getSpecial();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SkillValue item, boolean empty) {
		super.updateItem(item, empty);

		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			data = item;

			Attribute attr = item.getModifyable().getAttribute1();
			name.setText(item.getName());
			lblVal.setText(String.valueOf(item.getPoints()));
			if (item.getModifier()>0)
				lblVal.setText(item.getPoints()+"+"+item.getModifier());
			attrib.setText(attr.getShortName()+" "+parentCtrl.getCharacter().getAttribute(attr).getModifiedValue());
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			
			SkillSpecialization special =getFirstSpecialization(item);
			SkillSpecialization current = bxSpecial.getSelectionModel().getSelectedItem();
			boolean needsChange = 
					(current==SkillSpecialization.NONE) && (special!=null)
					||
					(current!=SkillSpecialization.NONE) && (special!=current);
			if (item!=previous || needsChange) {
				isUpdating = true;
				bxSpecial.getItems().clear();
				bxSpecial.getItems().add(SkillSpecialization.NONE);
				bxSpecial.getItems().addAll(item.getModifyable().getSpecializations().stream().filter(s -> !s.isDeprecated()).collect(Collectors.toList()));
				bxSpecial.getSelectionModel().select(special);
				if (bxSpecial.getValue()==null)
					bxSpecial.setValue(SkillSpecialization.NONE);
				isUpdating = false;
			}
			previous = item;
			
//				imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			lblType.setText(null);
//			lblVal.setText(" "+String.valueOf(item.getModifiedValue())+" ");
			int sum = parentCtrl.getCharacter().getAttribute(attr).getModifiedValue() + item.getModifiedValue();
			total.setText(String.format(Resource.get(UI, "skillvaluelistview.skillvaluelistcell.total"), sum));
//				btnEdit.setText("\uE1C2");
//				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
//				btnEdit.setVisible(item.getModifyable().getType()!=SkillType.KNOWLEDGE && item.getModifyable().getType()!=SkillType.LANGUAGE);
////				btnEdit.setDisable(!charGen.canSpecializeIn(item));
			btnDec.setDisable(!charGen.canBeDecreased(item));
			btnInc.setDisable(!charGen.canBeIncreased(item));
			bxSpecial.setVisible(!item.getModifyable().isToSpecify());
			setGraphic(stack);
		}
	}

}