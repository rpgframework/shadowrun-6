/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.KnowledgeSkillValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class KnowledgeSkillSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController control;
	private SkillType type;
	private ScreenManagerProvider provider;

	private ListView<SkillValue> list;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public KnowledgeSkillSection(String title, CharacterController ctrl, ScreenManagerProvider provider, SkillType type) {
		super(provider, title.toUpperCase(), null);
		this.type = type;
		this.provider = provider;
		control = ctrl;

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<SkillValue>();
		list.setStyle("-fx-min-height: 8em; -fx-pref-height: 20em"); 
		list.setCellFactory(cell -> new KnowledgeSkillValueListCell(control, getManagerProvider()));
		
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				showHelpFor.set(n.getModifyable());
				getDeleteButton().setDisable(!control.getSkillController().canBeDeselected(n));
			} else {
				showHelpFor.set(null);
				getDeleteButton().setDisable(true);
			}
		});
		
		getDeleteButton().setOnAction(ev -> {
			logger.debug("Delete selected "+list.getSelectionModel().getSelectedItem());
			control.getSkillController().deselect(list.getSelectionModel().getSelectedItem());
			list.getSelectionModel().clearSelection();
		});
		getAddButton().setOnAction(ev -> {
			String name = askForName();
			if (name!=null) {
				Skill skill = ShadowrunCore.getSkills(type).get(0);
				control.getSkillController().selectKnowledgeOrLanguage(skill, name);
			}
		});
	}

	//-------------------------------------------------------------------
	private String askForName() {
		Label explain = new Label(Resource.get(RES, "namedialog.explain"));
		
		TextField tfName = new TextField();
		tfName.setPrefColumnCount(20);
		VBox layout = new VBox(20, explain, tfName);
		
		CloseType closed = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(RES, "namedialog.title"), layout);
		if (closed==CloseType.OK) {
			return tfName.getText();
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");

		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getSkillValues(type));

		getAddButton().setDisable(!control.getSkillController().canBeSelected(ShadowrunCore.getSkill("knowledge")));
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
