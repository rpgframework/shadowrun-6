/**
 *
 */
package org.prelle.shadowrun6.jfx;

import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class UserChoiceDialog extends ManagedDialog implements ChangeListener<Boolean> {

	private static final ResourceBundle res = SR6Constants.RES;

	private String reason;
	private ModificationChoice choice;
	private VBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public UserChoiceDialog(String choiceReason, ModificationChoice choice) {
		super("Dummy", new Label("Dummy"), CloseType.OK);
		this.reason = choiceReason;
		this.choice = choice;

		initComponents();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		/*
		 * Heading
		 */
		String heading = (choice.getNumberOfChoices()>1)?
				String.format(res.getString("wizard.selectMod.multiple"), choice.getNumberOfChoices())
				:
				res.getString("wizard.selectMod.single");

		/*
		 * Content
		 */
		ToggleGroup group = new ToggleGroup();
		content = new VBox();
		content.setSpacing(10);
		for (Modification mod : choice.getOptions()) {
			if (choice.getNumberOfChoices()<2) {
				RadioButton box = new RadioButton(ShadowrunTools.getModificationString(mod));
				box.setToggleGroup(group);
				box.setUserData(mod);
				box.getStyleClass().add("text-body");
				content.getChildren().add(box);
			} else {
				CheckBox check = new CheckBox(ShadowrunTools.getModificationString(mod));
				check.setUserData(mod);
				check.selectedProperty().addListener(this);
				content.getChildren().add(check);
			}
		}
		if (!group.getToggles().isEmpty())
			group.getToggles().get(0).setSelected(true);


		setTitle(heading);

		Label lblReason = new Label(String.format(res.getString("wizard.selectMod.reasonString"),reason));
		ScrollPane scroll = new ScrollPane(content);
		VBox metaContent = new VBox(20, lblReason, scroll);
		setContent(metaContent);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setButtonPredicateCheck( closeType -> canBeClosed());
	}

	//-------------------------------------------------------------------
	private boolean canBeClosed() {
		int numSelected = 0;
		for (Node node : content.getChildren()) {
			ButtonBase tmp = (ButtonBase)node;
			boolean isSelected = (tmp instanceof Toggle)?((Toggle)tmp).isSelected():((CheckBox)tmp).isSelected();
			if (isSelected)
				numSelected++;
		}

		for (Node node : content.getChildren()) {
			ButtonBase tmp = (ButtonBase)node;
			boolean isSelected = (tmp instanceof Toggle)?((Toggle)tmp).isSelected():((CheckBox)tmp).isSelected();
			if (!isSelected)
				tmp.setDisable(numSelected>=choice.getNumberOfChoices());
		}

		return numSelected==choice.getNumberOfChoices();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends Boolean> button, Boolean old,
			Boolean newVal) {
//		this.refreshButtons();
	}

	//-------------------------------------------------------------------
	public Modification[] getChoice() {
		Modification[] ret = new Modification[choice.getNumberOfChoices()];
		int pos=0;
		for (Node node : content.getChildren()) {
			ButtonBase toggle = (ButtonBase)node;
			boolean isSelected = (toggle instanceof Toggle)?((Toggle)toggle).isSelected():((CheckBox)toggle).isSelected();
			if (isSelected) {
				ret[pos++] = (Modification) toggle.getUserData();
			}
			if (pos==choice.getNumberOfChoices())
				break;
		}
		return ret;
	}

}
