/**
 * 
 */
package org.prelle.shadowrun6.jfx.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributePaneSecondary extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;
	
	class AttributeField extends HBox {
		private Button dec, inc;
		private TextField value;
		
		public AttributeField() {
			dec  = new Button("<");
			inc  = new Button(">");
			value = new TextField();
			value.setPrefColumnCount(1);
			this.getChildren().addAll(dec, value, inc);
		}
		public void setText(String val) {
			this.value.setText(val);
		}
	}

	private AttributeController control;
	private ShadowrunCharacter  model;
//	private CharGenMode         mode;

	private Label headDeri, headDPoints, headDMod, headDValue;
	private Map<Attribute, Label> modification;
	private Map<Attribute, Label> derived;
	private Map<Attribute, Label> finalValue;
	private Map<Attribute, Label> startValue;

	//--------------------------------------------------------------------
	public AttributePaneSecondary(AttributeController cntrl, CharGenMode mode) {
		this.control = cntrl;
//		this.mode    = mode;
		assert control!=null;
		assert mode   !=null;

		modification = new HashMap<Attribute, Label>();
		finalValue   = new HashMap<Attribute, Label>();
		startValue   = new HashMap<Attribute, Label>();
		derived      = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setMaxWidth(Double.MAX_VALUE);
		super.getStyleClass().add("content");
		
		headDeri   = new Label(Resource.get(UI,"label.derived_values"));
		headDPoints= new Label(Resource.get(UI,"label.points"));
		headDMod   = new Label(Resource.get(UI,"label.modified.short"));
		headDValue = new Label(Resource.get(UI,"label.value"));
		headDeri.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDMod.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headDeri.getStyleClass().add("table-head");
		headDPoints.getStyleClass().add("table-head");
		headDMod.getStyleClass().add("table-head");
		headDValue.getStyleClass().add("table-head");
		
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label value = new Label();
			Label modVal= new Label();
			Label finVal= new Label();
			Label startVal= new Label();
			
			finalValue  .put(attr, finVal);
			derived     .put(attr, value);
			modification.put(attr, modVal);
			startValue  .put(attr, startVal);

			finVal.getStyleClass().add("result");
		}
		
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		super.setVgap(2);
		super.setHgap(0);
		super.setMaxWidth(Double.MAX_VALUE);

		this.add(headDeri   , 0,0);
		this.add(headDPoints, 1,0);
		this.add(headDMod   , 2,0);
		this.add(headDValue , 3,0);

		/*
		 * Derived attributes
		 */
		int y=0;
		for (final Attribute attr : Attribute.derivedValues()) {
			y++;
			Label longName  = new Label(attr.getName());
			Label modVal    = modification.get(attr);
			Label points    = derived.get(attr);
			Label finVal    = finalValue.get(attr);
			
			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName.getStyleClass().add(lineStyle);
			modVal.getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			finVal.getStyleClass().addAll(lineStyle);
			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			GridPane.setVgrow(longName, Priority.ALWAYS);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			modVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setAlignment(Pos.CENTER);
			modVal.setAlignment(Pos.CENTER);
			finVal.setAlignment(Pos.CENTER);
			
			this.add(longName , 0, y);
			this.add(points   , 1, y);
			this.add(modVal   , 2, y);
			this.add(finVal   , 3, y);
		}
		
		
//		// Column alignment
//		ColumnConstraints nameCol = new ColumnConstraints();
//		nameCol.setHgrow(Priority.ALWAYS);
//		ColumnConstraints rightAlign = new ColumnConstraints();
//		rightAlign.setHalignment(HPos.RIGHT);
//		ColumnConstraints centerAlign = new ColumnConstraints();
//		centerAlign.setHalignment(HPos.CENTER);
//
//		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
//		constraints.add(nameCol);
//		constraints.add(new ColumnConstraints());
//		switch (mode) {
//		case CREATING:
//			constraints.add(centerAlign);  // Final value
//			break;
//		case LEVELING:
//			constraints.add(centerAlign);  // Equipment modifier
//			constraints.add(centerAlign);  // Final value
//			break;
//		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
	}

	//-------------------------------------------------------------------
	public void updateController(AttributeController ctrl) {
		this.control = ctrl;
		updateContent();
	}

	//-------------------------------------------------------------------
	public void updateContent() {
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label modif_l = modification.get(attr);
			Label deriv_l = derived.get(attr);
			Label final_l = finalValue.get(attr);

			AttributeValue data = model.getAttribute(attr);
			
			if (data.getAugmentedModifier()==0) {
				modif_l.setTooltip(null);
			} else {
				logger.debug("Set tooltip for "+data+"  mods = "+data.getModifications());
				modif_l.setTooltip(new Tooltip(ShadowrunJFXUtils.getModificationTooltip(data)));
			}

			deriv_l.setText(Integer.toString(data.getPoints()));
			if (data.getAugmentedModifier()!=0)
				modif_l.setText(Integer.toString(data.getAugmentedModifier()));
			else
				modif_l.setText(null);
			final_l.setText(Integer.toString(data.getAugmentedValue()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case ATTRIBUTE_CHANGED:
			Attribute      attribute = (Attribute) event.getKey();
			if (attribute.isPrimary()) 
				return;
		case CHARACTER_CHANGED:
		case POINTS_LEFT_ATTRIBUTES:
		case EXPERIENCE_CHANGED:
			updateContent();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
		doInteractivity();
	}

}
