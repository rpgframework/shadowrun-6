/**
 * 
 */
package org.prelle.shadowrun6.jfx.newwizard;

import org.prelle.shadowrun6.BasePluginData;

import javafx.scene.Node;

/**
 * Called from the CharacterDataNodes when an item is selected for description
 * @author prelle
 *
 */
public interface SelectionCallback {

	public void showDescription(Node node, BasePluginData value);
	
}
