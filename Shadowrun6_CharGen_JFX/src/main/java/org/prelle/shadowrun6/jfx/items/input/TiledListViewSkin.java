/**
 * 
 */
package org.prelle.shadowrun6.jfx.items.input;

import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Skin;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
public class TiledListViewSkin<T> implements Skin<ListView<T>> {

	private ListView<T> list;
	
	private ScrollPane scroll;
	private TilePane tile;
	
	//-------------------------------------------------------------------
	/**
	 */
	public TiledListViewSkin(ListView<T> list) {
		this.list = list;
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		tile = new TilePane();
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tile = new TilePane();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getSkinnable()
	 */
	@Override
	public ListView<T> getSkinnable() {
		return list;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getNode()
	 */
	@Override
	public Node getNode() {
		// TODO Auto-generated method stub
		return tile;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
