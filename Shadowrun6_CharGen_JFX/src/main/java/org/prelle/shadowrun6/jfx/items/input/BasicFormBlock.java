package org.prelle.shadowrun6.jfx.items.input;

import java.util.Collections;
import java.util.Comparator;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Legality;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock;
import org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormEventType;
import org.prelle.shadowrun6.items.ItemType;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class BasicFormBlock implements FormBlock {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private ItemDataForm parent;
	private ChoiceBox<ItemType> cbType;
	private ChoiceBox<ItemSubType> cbSubType;
	private ComboBox<Integer> cbDeviceRating;
	private ComboBox<Integer> cbConceal;
	private ComboBox<Integer> cbAvail;
	private ComboBox<Legality> cbLegality;
	private TextField tfPrice;
	
	private VBox layout;
	
	//-------------------------------------------------------------------
	public BasicFormBlock(ItemDataForm parent) {
		this.parent = parent;
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(ItemType.values());
		cbType.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType object) { return object.getName(); }
			public ItemType fromString(String string) { return null; }
		});
		cbSubType = new ChoiceBox<>();
		cbSubType.getItems().addAll(ItemSubType.values());
		Collections.sort(cbSubType.getItems(), new Comparator<ItemSubType>() {
			public int compare(ItemSubType o1, ItemSubType o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbSubType.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType object) { return object.getName(); }
			public ItemSubType fromString(String string) { return null; }
		});

		cbDeviceRating = new ComboBox<>();
		cbDeviceRating.setEditable(true);
		cbDeviceRating.getItems().addAll(0,1,2,3,4,5,6);
		cbDeviceRating.setConverter(new StringConverter<Integer>() {
			public String toString(Integer val) {
				try {
					return UI.getString("label.devicerating."+val);
				} catch (MissingResourceException e) {
					return String.valueOf(val);
				}
			}
			public Integer fromString(String string) {
				for (Integer tmp : cbDeviceRating.getItems()) {
					try {
						if (string.equals(UI.getString("label.devicerating."+tmp)))
							return tmp;
					} catch (MissingResourceException e) {
					}
				}
				try {
					return Integer.parseInt(string);
				} catch (NumberFormatException e) {
					logger.warn("No valid device rating: "+string);
					return 0;
				} 
			}
		});
//		cbDeviceRating.valueProperty().addListener( (ov,o,n) -> cbDeviceRating.);
		cbConceal      = new ComboBox<>();
		cbConceal.setEditable(true);
		cbConceal.getItems().addAll(-10,-8,-6,-4,-2,0,2,4,6,8,10);
		cbConceal.setConverter(new StringConverter<Integer>() {
			public String toString(Integer val) {
				try {
					return UI.getString("label.concealability."+val);
				} catch (MissingResourceException e) {
					return String.valueOf(val);
				}
			}
			public Integer fromString(String string) { 
				for (Integer tmp : cbConceal.getItems()) {
					try {
						if (string.equals(UI.getString("label.concealability."+tmp)))
							return tmp;
					} catch (MissingResourceException e) {
					}
				}
				try {
					return Integer.parseInt(string);
				} catch (NumberFormatException e) {
					logger.warn("No valid concealability: "+string);
					return 0;
				} 
			}
		});
		cbAvail        = new ComboBox<>();
		for (int i=0; i<=24; i++)
			cbAvail.getItems().add(i);
		cbAvail.setConverter(new StringConverter<Integer>() {
			public String toString(Integer val) {return String.valueOf(val);}
			public Integer fromString(String string) {  return Integer.parseInt(string); }
		});
		cbLegality     = new ComboBox<>();
		cbLegality.getItems().addAll(Legality.values());
		cbLegality.setConverter(new StringConverter<Legality>() {
			public String toString(Legality val) {return val.getShortCode();}
			public Legality fromString(String string) { 
				for (Legality tmp : cbLegality.getItems()) {
					try {
						if (string.equals(tmp.getShortCode()))
							return tmp;
					} catch (MissingResourceException e) {
					}
				}
				return Legality.LEGAL;
			}
		});
		tfPrice        = new TextField();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaType    = new Label(UI.getString("label.itemtype"));
		Label heaSubType = new Label(UI.getString("label.itemsubtype"));
		Label heaDevice  = new Label(UI.getString("label.devicerating"));
		Label heaConceal = new Label(UI.getString("label.concealability"));
		Label heaAvail   = new Label(ItemAttribute.AVAILABILITY.getName());
		Label heaPrice   = new Label(ItemAttribute.PRICE.getName());
		
		HBox baseLine1 = new HBox();
//		HBox baseLine2 = new HBox();
//		HBox baseLine3 = new HBox();
		baseLine1.getChildren().addAll(cbAvail, cbLegality, heaPrice, tfPrice);
//		baseLine2.getChildren().addAll(heaDevice, cbDeviceRating);
//		baseLine3.getChildren().addAll(heaConceal, cbConceal);
//		HBox.setMargin(cbAvail , new Insets(0,0,0,10));
		HBox.setMargin(heaPrice, new Insets(0,0,0,20));
//		HBox.setMargin(cbDeviceRating, new Insets(0,0,0,10));
		HBox.setMargin(heaConceal, new Insets(0,0,0,20));
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-hgap: 0.3em; -fx-vgap: 0.4em;");
		grid.add(heaType   , 0, 1);
		grid.add(cbType    , 1, 1);
		grid.add(heaSubType, 0, 2);
		grid.add(cbSubType , 1, 2);
		grid.add(heaAvail  , 0, 3);
		grid.add(baseLine1 , 1, 3);
		grid.add(heaDevice , 0, 4);
		grid.add(cbDeviceRating , 1, 4);
		grid.add(heaConceal, 0, 5);
		grid.add(cbConceal , 1, 5);
		
		layout = new VBox(10);
		layout.setStyle("-fx-spacing: 0.8em");
		layout.getChildren().addAll(grid);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				cbSubType.getItems().clear();
				if (n!=null && n.getSubTypes()!=null)
					cbSubType.getItems().addAll(n.getSubTypes());
				
				parent.handleFormBlockEvent(FormEventType.TYPE_CHANGED, n);
			}
		});
	}
	
	//-------------------------------------------------------------------
	public void initStartData() {
		cbType.getSelectionModel().select(ItemType.WEAPON_FIREARMS);
		cbSubType.getSelectionModel().select(0);
		cbAvail.getSelectionModel().select(0);
		cbLegality.getSelectionModel().select(0);
		cbConceal.getSelectionModel().select(0);
		cbDeviceRating.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		item.setType(cbType.getValue());
		item.setSubtype(cbSubType.getValue());
		item.setAvailability(new Availability(cbAvail.getValue(), cbLegality.getValue(), false));
		item.setDeviceRating(cbDeviceRating.getValue());
		item.setConcealability(cbConceal.getValue());
		item.setPrice(Integer.parseInt(tfPrice.getText()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		cbType.getSelectionModel().select(item.getNonAccessoryType());
		if (item.getSubtype(item.getNonAccessoryType())!=null)
			cbSubType.getSelectionModel().select(item.getSubtype(item.getNonAccessoryType()));
		
		cbAvail.getSelectionModel().select(item.getAvailability().getValue());
		cbLegality.getSelectionModel().select(item.getAvailability().getLegality());
		cbDeviceRating.getSelectionModel().select(item.getDeviceRating());
		cbConceal.setValue(item.getConcealability());
		
		tfPrice.setText(String.valueOf(item.getPrice()));
	}
	
	//-------------------------------------------------------------------
	public void preselectTypes(ItemType type, ItemSubType sub) {
		if (type!=null)
			cbType.setValue(type);
		if (sub!=null)
			cbSubType.setValue(sub);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		logger.debug("checkInput");
		boolean isOkay = true;
		if (cbType.getValue()==null) {
			if (!cbType.getStyleClass().contains(ERROR_CLASS))
				cbType.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		} else
			cbType.getStyleClass().remove(ERROR_CLASS);
		
		// Subtype
		if (cbSubType.getValue()==null) {
			if (!cbSubType.getStyleClass().contains(ERROR_CLASS))
				cbSubType.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		} else
			cbSubType.getStyleClass().remove(ERROR_CLASS);
		
		// Price
		boolean isInteger = true;
		try {
			Integer.parseInt( tfPrice.getText() );
		} catch (Exception e) {
			isInteger = false;
		}
		if (!isInteger) {
			if (!tfPrice.getStyleClass().contains(ERROR_CLASS))
				tfPrice.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		} else
			tfPrice.getStyleClass().remove(ERROR_CLASS);
		
		logger.debug("isOkay = "+isOkay);		
		return isOkay;
	}

}