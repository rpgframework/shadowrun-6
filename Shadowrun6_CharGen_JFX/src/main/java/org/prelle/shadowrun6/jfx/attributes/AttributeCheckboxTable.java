/**
 * 
 */
package org.prelle.shadowrun6.jfx.attributes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.AttributeController.ValueState;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class AttributeCheckboxTable extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	
	private Map<Attribute, CheckBox[]> fields;
	private Map<Attribute, Button> decButtons;
	private Map<Attribute, Button> incButtons;
	private Map<Attribute, Label>  finalPrim;
	private Map<Attribute, Label>  secondary;
	
	private AttributeController attrGen;
	private ShadowrunCharacter model;
	private EventHandler<MouseEvent> onMouse;
	private int zeroPoint;
	
	//-------------------------------------------------------------------
	public AttributeCheckboxTable(AttributeController gen) {
		this.attrGen = gen;
		if (gen==null)
			throw new NullPointerException();
		
		initCompontents();
		initLayout();
		initInteractivity();
		
	}
	
	//-------------------------------------------------------------------
	private void initCompontents() {
		setVgap(10);
		getStyleClass().add("priority-table");
		
		finalPrim = new HashMap<Attribute, Label>();
		secondary = new HashMap<Attribute, Label>();
		fields = new HashMap<Attribute, CheckBox[]>();
		fields.put(Attribute.BODY     , new CheckBox[10]);
		fields.put(Attribute.AGILITY  , new CheckBox[10]);
		fields.put(Attribute.REACTION , new CheckBox[10]);
		fields.put(Attribute.STRENGTH , new CheckBox[10]);
		fields.put(Attribute.WILLPOWER, new CheckBox[10]);
		fields.put(Attribute.LOGIC    , new CheckBox[10]);
		fields.put(Attribute.INTUITION, new CheckBox[10]);
		fields.put(Attribute.CHARISMA , new CheckBox[10]);

		for (Attribute attrib : Attribute.primaryValues()) {
			Label result = new Label();
			result.getStyleClass().add("result");
			finalPrim.put(attrib, result);
			for (int x=0; x<fields.get(attrib).length; x++) {
				fields.get(attrib)[x] = new CheckBox();
			}
//			fields.get(attrib)[0].setDisable(true);
//			if (attrib!=Attribute.LOGIC)
//				fields.get(attrib)[0].selectedProperty().set(true);
		}

		/* 
		 * Buttons
		 */
		incButtons = new HashMap<Attribute, Button>();
		decButtons = new HashMap<Attribute, Button>();
		for (Attribute key : Attribute.values()) {
			Button dec = new Button("<");
			Button inc = new Button(">");
			decButtons.put(key, dec);
			incButtons.put(key, inc);
		}
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Create attribute labels
		 */
		Label[] lblAttrib = new Label[Attribute.primaryValues().length];
		int i=0;
		for (Attribute attrib : Attribute.primaryValues()) {
			lblAttrib[i] = new Label(attrib.getName());
			// Geht nur in Java 8
//			lblAttrib[i].setPadding(new Insets(0,10,0,0));;
			i++;
		}
		
		
		i=0;
		setGridLinesVisible(true);
		for (Attribute attrib : Attribute.primaryValues()) {
			GridPane.setMargin(decButtons.get(attrib), new Insets(0,10,0,0));
			
			add(lblAttrib[i], 0,i);
			add(decButtons.get(attrib), 1,i);
			CheckBox[] boxes = fields.get(attrib);
			for (int x=0; x<boxes.length; x++) {
				add(boxes[x], 2+x, i);
			}
			add(incButtons.get(attrib), 13,i);
			GridPane.setMargin(decButtons.get(attrib), new Insets(0,10,0,20));
			
			add(finalPrim.get(attrib), 14, i);
			GridPane.setMargin(finalPrim.get(attrib), new Insets(0,10,0,20));
			i++;
		}
		
		/*
		 * Create derived attribute labels
		 */
		i=0;
		for (Attribute attrib : Attribute.secondaryValues()) {
			Label name  = new Label(attrib.getName());
			Label value = new Label("-");
			value.getStyleClass().add("result");
			name.setUserData(attrib);
			value.setUserData(attrib);
			name.setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			value.setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			secondary.put(attrib, value);
//			add(name , 15,i);
//			add(value, 16,i);
//			GridPane.setMargin(name, new Insets(0,10,0,40));
			i++;
		}
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Entry<Attribute, Button> entry : decButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.decrease(entry.getKey()));
		}
		for (Entry<Attribute, Button> entry : incButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.increase(entry.getKey()));
		}
		
		// Mouse over
		for (Attribute attrib : Attribute.primaryValues()) {
			decButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			incButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
		}

		for (Attribute attrib : Attribute.primaryValues()) {
			for (CheckBox box : fields.get(attrib))
				box.setOnAction(event -> {
//				box.selectedProperty().addListener((ov,o,n) -> {
					int index = Arrays.asList(fields.get(attrib)).indexOf(box);
					setAttributeTo(attrib, index+1);
				});
		}
	}

	//-------------------------------------------------------------------
	private void setAttributeTo(Attribute attrib, int val) {
		int current = model.getAttribute(attrib).getPoints();
		logger.debug("Set "+attrib+" to "+val);
		// Loop until requested value reached
		while (current!=val) {
			if (current>val) {
				// Must be decreased
				if (attrGen.canBeDecreased(attrib)) {
					attrGen.decrease(attrib);
				} else {
					logger.warn("Cannot decrease from "+current+" to "+val);
					refresh();
					break;
				}
			} else {
				// Must be increased
				if (attrGen.canBeIncreased(attrib)) {
					attrGen.increase(attrib);
				} else {
					logger.warn("Cannot increase from "+current+" to "+val);
					refresh();
					break;
				}
			}
			current = model.getAttribute(attrib).getPoints();
		}
		logger.debug("Done");
	}
	
	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		
//		int i=0;
//		for (Attribute attrib : Attribute.primaryValues()) {
//			int max = model.getAttribute(attrib).getMaximum();
//			CheckBox[] boxes = fields.get(attrib);
//			for (int x=0; x<boxes.length; x++) {
//				if (x<max) {
//					if (!getChildren().contains(boxes[x]))
//						add(boxes[x], 2+x, i);
//				} else {
////					getChildren().remove(boxes[x]);
//				}
//			}
//			i++;
//		}
//		
		GenerationEventDispatcher.addListener(this);
		refresh();
	}
	
	//-------------------------------------------------------------------
	public void refresh() {
		// Get highest start modifier
		zeroPoint=1;
		for (Attribute key : Attribute.primaryValues()) {
		   zeroPoint = Math.max(zeroPoint, model.getAttribute(key).getNaturalModifier());
		}
		logger.debug("Zero point of "+model.getMetatype()+" is "+zeroPoint);
		
		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue val = model.getAttribute(key);
			set(key, val);
			decButtons.get(key).setDisable(!attrGen.canBeDecreased(key));
			incButtons.get(key).setDisable(!attrGen.canBeIncreased(key));
			finalPrim.get(key).setText(String.valueOf(val.getModifiedValue()));
		}
		for (Attribute key : Attribute.secondaryValues()) {
			AttributeValue val = model.getAttribute(key);
			secondary.get(key).setText(String.valueOf(val.getModifiedValue()));
		}
	}
	
	//-------------------------------------------------------------------
	public void set(Attribute key, AttributeValue val) {
		logger.debug("set "+key+" to "+val+"  MAX="+val.getMaximum());
		CheckBox[] boxes = fields.get(key);
		// Calculate modifier boxes
//		for (int i=0; i<zeroPoint; i++) {
//			boxes[i].setDisable(true);
//			boxes[i].setSelected(true);
//			boxes[i].setVisible( (val.getModifier()-(zeroPoint-i-1))>0 );
//		}
//		
//		for (int i=zeroPoint; i<boxes.length; i++) {
//			int attrVal = i-(zeroPoint-val.getModifier());
//			logger.debug("  attrVal = "+attrVal);
//			boxes[i].setSelected( attrVal<val.getModifiedValue());
//			boxes[i].setVisible( (attrVal+1)<=val.getMaximum());
//		}
		for (int i=0; i<boxes.length; i++) {
			int value = (i+1)-zeroPoint+val.getNaturalModifier();
			ValueState state = attrGen.getValueState(key, value);
			switch (state) {
			case BELOW_MINIMUM:
			case ABOVE_MAX:
				boxes[i].setVisible(false);
				break;
			case WITHIN_MODIFIER:
				boxes[i].setVisible(true);
				boxes[i].setSelected(true);
				boxes[i].setDisable(true);
				break;
			case SELECTED:
				boxes[i].setVisible(true);
				boxes[i].setSelected(true);
				boxes[i].setDisable(false);
				break;
			case UNSELECTED:
				boxes[i].setVisible(true);
				boxes[i].setSelected(false);
				boxes[i].setDisable(false);
				break;
			case UNATAINABLE_MAX:
				boxes[i].setVisible(true);
				boxes[i].setSelected(false);
				boxes[i].setDisable(true);
				break;
			}
		}
	}
	
	//-------------------------------------------------------------------
	public void setOnAttributeSelected(EventHandler<MouseEvent> handler) {
		onMouse = handler;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case ATTRIBUTE_CHANGED:
		case EXPERIENCE_CHANGED:
			refresh();
			break;
		default:
		}
	}
	
}
