/**
 *
 */
package org.prelle.shadowrun6.jfx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.GeneratorWrapper;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.SumToTenCharacterGenerator;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;
import org.prelle.shadowrun6.jfx.items.input.ShadowrunDataInputScreen;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class SR6CharGenJFXPlugin implements RulePlugin<ShadowrunCharacter>, CommandBusListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;
	private static PropertyResourceBundle HELP =  SR6Constants.HELP;

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.CHARACTER_CREATION);
		FEATURES.add(RulePluginFeatures.DATA_INPUT);
	}

	private ConfigContainer cfgShadowrun;

	//-------------------------------------------------------------------
	/**
	 */
	public SR6CharGenJFXPlugin() {
//		DirectInputCharacterGenerator input = new DirectInputCharacterGenerator();
//		input.setResourceBundle(UI);
//		input.setHelpResourceBundle(HELP);
//		input.setPlugin(this);
//		CharacterGeneratorRegistry.register(input);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CHARGEN";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 6 Character Generator";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		List<RulePluginFeatures> ret = new ArrayList<>(FEATURES);
//		if (developerMode!=null && !(Boolean)developerMode.getValue()) {
//			ret.remove(RulePluginFeatures.DATA_INPUT);
//			ret.clear();
//		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		logger.debug("attachConfigurationTree");
		cfgShadowrun = (ConfigContainer) addBelow.getChild("shadowrun6");
		cfgShadowrun.setResourceBundle(SR6ConfigOptions.CONFIG_RES);
		CharacterGeneratorRegistry.attachConfigurationTree(cfgShadowrun);
		
//		logger.info("attach for everyone");
//		for (CharacterGenerator charGen : CharacterGeneratorRegistry.getGenerators()) {
//			logger.info("YYY");
//			charGen.attachConfigurationTree(cfgShadowrun);
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			logger.debug("Check "+Arrays.toString(values));
			if (values[0]!=RoleplayingSystem.SHADOWRUN6) return false;
			if (!(values[1] instanceof ShadowrunCharacter)) return false;
			if (!(values[2] instanceof CharacterHandle)) return false;
			if (!(values[4] instanceof ScreenManager)) return false;
			return true;
		case SHOW_CHARACTER_CREATION_GUI:
			if (values[0]!=RoleplayingSystem.SHADOWRUN6) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
		case SHOW_DATA_INPUT_GUI:
			if (values[0]!=RoleplayingSystem.SHADOWRUN6) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
		default:
			return false;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		logger.debug("Handle "+type);
		if (!willProcessCommand(src, type, values))
			return new CommandResult(type, false, null, false);

		logger.debug("Handle "+type);
		ScreenManager manager;
		CharacterController control;
		ShadowrunCharacter model;
		GenerationEventDispatcher.clear();
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			model = (ShadowrunCharacter)values[1];
			if (model.isGenerationMode()) {
				logger.warn("start character creation continuation");
				control = CharacterGeneratorRegistry.get(model.getChargenUsed());
				control.attachConfigurationTree(cfgShadowrun);
				control = new GeneratorWrapper((CharacterGenerator)control);				
				CharacterGeneratorRegistry.selectAndContinue(((CharacterGenerator)control), model);
//				control =  new GeneratorWrapper(CharacterGeneratorRegistry.getSelected());
			} else {
				logger.info("start character modification");
				control = new CharacterLeveller(model);
				control.attachConfigurationTree(cfgShadowrun);
			}
			CharacterHandle handle = (CharacterHandle)values[2];
			manager = (ScreenManager)values[4];

			CharacterViewScreenSR6Fluent screen;
			logger.warn("control = "+control);
			try {
				screen = new CharacterViewScreenSR6Fluent(control, model.isGenerationMode()?ViewMode.GENERATION:ViewMode.MODIFICATION, handle);
//				screen.setData(model, handle);
				manager.navigateTo(screen);

				// Eventually inform screen that this is a continued generation
				if (model.isGenerationMode()) {
					logger.info("Continue generation");
					screen.continueGeneration();
				}
			} catch (Exception e) {
				logger.error("Failed opening character screen",e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2, e.toString());
			}

			return new CommandResult(type, true);
		case SHOW_CHARACTER_CREATION_GUI:
			logger.debug("start character creation");
			model = new ShadowrunCharacter();
			model.setGenerationMode(true);
			manager = (ScreenManager)values[2];
			CharacterGenerator defaultCharGen = CharacterGeneratorRegistry.getGenerators().get(0);
			control =  new GeneratorWrapper();
			CharacterGeneratorRegistry.select(defaultCharGen, model);
			if (model!=CharacterGeneratorRegistry.getSelected().getCharacter())
				throw new IllegalStateException("Expected model "+model.getName()+" but found "+CharacterGeneratorRegistry.getSelected().getCharacter().getName());

			screen = new CharacterViewScreenSR6Fluent(control, ViewMode.GENERATION, null);
			manager.navigateTo(screen);

			logger.warn("You could give a warning here");
//			manager.showAlertAndCall(AlertType.NOTIFICATION, "Achtung!", "Zwar werden 'Schattenläufer' und 'Straßengrimoire' unterstützt, aber Metagenetik, Initiation sowie Erschaffung außer Prioriäten- und Karmasystem noch nicht - das kommt erst in späteren Versionen.\n\n"+
//			"Wer Fehler in bestehenden Funktionen findet, kann uns dies melden. Siehe hierzu: http://www.rpgframework.de/index.php/de/support/fehler-melden/\n"+
//			"Wer Java-Entwickler ist und mitmachen möchte, kann uns an  genesis@rpgframework.de  eine Mail schreiben.\n"
//			);

			screen.startGeneration();

			CommandResult result = new CommandResult(type, true);
			result.setReturnValue(model);
			return result;
		case SHOW_DATA_INPUT_GUI:
			logger.debug("start data input");
			manager = (ScreenManager)values[2];

			ShadowrunDataInputScreen screen2 = new ShadowrunDataInputScreen();
//			manager.navigateTo(screen2);
//			manager.show(screen2);
			result = new CommandResult(type, true);
//			result.setReturnValue(model);
			return result;
		default:
			return new CommandResult(type, false);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		logger.debug("Prepare construction kits");
		CharacterGeneratorRegistry.register(NewPriorityCharacterGenerator.class, this);

//		SumToTenCharacterGenerator sumToTen = new SumToTenCharacterGenerator();
		CharacterGeneratorRegistry.register(SumToTenCharacterGenerator.class, this);

//		NewKarmaGenerator charGen2 = new NewKarmaGenerator();
////		charGen2.setResourceBundle(UI);
////		charGen2.setHelpResourceBundle(HELP);
//		charGen2.setPlugin(this);
////		CharacterGeneratorRegistry.register(charGen2);

		CommandBus.registerBusCommandListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return ClassLoader.getSystemResourceAsStream(SR6Constants.PREFIX+"/i18n/shadowrun/chargenui.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
