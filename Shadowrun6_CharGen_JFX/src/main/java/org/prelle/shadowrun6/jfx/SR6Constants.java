/**
 * 
 */
package org.prelle.shadowrun6.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface SR6Constants {
	
	public final static String PREFIX = "org/prelle/shadowrun6/jfx";
	public final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(PREFIX+"/i18n/shadowrun/chargenui");
	public final static PropertyResourceBundle HELP = (PropertyResourceBundle) ResourceBundle.getBundle(PREFIX+"/i18n/shadowrun/chargenui-help");
	
	public final static String BASE_LOGGER_NAME = "shadowrun6.jfx";

}
