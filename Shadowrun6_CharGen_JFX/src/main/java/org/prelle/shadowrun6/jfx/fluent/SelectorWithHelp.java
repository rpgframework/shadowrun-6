package org.prelle.shadowrun6.jfx.fluent;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.BasePluginData;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SelectorWithHelp<T extends BasePluginData> extends HBox {
	
	private IListSelector<T> selector;
	private VBox colDescription;
	private Label lblName;
	private Label lblPage;
	private Label lblDesc;

	//-------------------------------------------------------------------
	public SelectorWithHelp(IListSelector<T> selector) {
		this.setStyle("-fx-spacing: 2em");
		this.selector = selector;
		
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("text-small-secondary");
		lblName.setStyle("-fx-font-weight: bold");
		lblPage = new Label();
		lblPage.setStyle("-fx-font-style: italic");
		lblDesc = new Label();
		lblDesc.setWrapText(true);

		colDescription = new VBox(5);
		colDescription.getStyleClass().add("character-document-view-description");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		colDescription.setStyle("-fx-pref-width: 30em");
		colDescription.getChildren().addAll(lblName, lblPage, lblDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);
		
		getChildren().addAll(selector.getNode(), colDescription);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selector.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				lblName.setText(null);
				lblPage.setText(null);
				lblDesc.setText(null);
			} else {
				lblName.setText(n.getName());
				lblPage.setText(n.getProductName()+" "+n.getPage());
				lblDesc.setText(n.getHelpText());
			}
		});
	}


}
