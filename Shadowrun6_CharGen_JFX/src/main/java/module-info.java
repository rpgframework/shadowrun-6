/**
 * @author Stefan Prelle
 *
 */
module shadowrun6.chargen.jfx {
//	exports org.prelle.shadowrun6.jfx;

	provides de.rpgframework.character.RulePlugin with org.prelle.shadowrun6.jfx.SR6CharGenJFXPlugin;

	requires java.prefs;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.graphics;
	requires javafx.web;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.chars;
	requires transitive de.rpgframework.javafx;
	requires simple.persist;
	requires shadowrun6.core;
	requires shadowrun6.chargen;
	requires shadowrun6.data;
}