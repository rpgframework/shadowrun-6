package de.rpgframework.shadowrun6.print.json.model;

public class JSONItemAccessory {
    public String name;
    public String type;
    public String subType;
    public int rating;
    public String description;
}
