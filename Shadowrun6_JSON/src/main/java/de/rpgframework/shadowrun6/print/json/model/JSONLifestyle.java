package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONLifestyle {
    public String customName;
    public String name;
    public int cost;
    public int paidMonths;
    public String sin;
    public List<String> options;
    public String description;
}
