package de.rpgframework.shadowrun6.print.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.rpgframework.shadowrun6.print.json.model.*;
import org.prelle.shadowrun6.*;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.OnRoadOffRoadValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class JSONSR6ExportService {

    public String exportCharacter(ShadowrunCharacter character) {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return gson.toJson(getJSONCharacter(character));
    }

    private JSONCharacter getJSONCharacter(ShadowrunCharacter character) {
        JSONCharacter jsonCharacter = new JSONCharacter();
        setGeneralInfo(jsonCharacter, character);
        setAttributes(jsonCharacter, character);
        setInitiatives(jsonCharacter, character);
        setSkills(jsonCharacter, character);
        setSpells(jsonCharacter, character);
        setComplexForms(jsonCharacter, character);
        setAdeptPowers(jsonCharacter, character);
        setConnections(jsonCharacter, character);
        setQualities(jsonCharacter, character);
        setLongRangeWeapons(jsonCharacter, character);
        setCloseCombatWeapons(jsonCharacter, character);
        setArmors(jsonCharacter, character);
        setItems(jsonCharacter, character);
        setAugmentations(jsonCharacter, character);
        setVehicles(jsonCharacter, character);
        setDrones(jsonCharacter, character);
        setRituals(jsonCharacter, character);
        setSINs(jsonCharacter, character);
        setLifestyles(jsonCharacter, character);
        setLicenses(jsonCharacter, character);
        setMatrixItems(jsonCharacter, character);
        setMartialArts(jsonCharacter, character);
        setSignatureMoves(jsonCharacter, character);
        return jsonCharacter;
    }

    private void setSignatureMoves(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSignatureManeuver> jsonSignatureMoves = new ArrayList<>();
        for (SignatureManeuver signatureManeuver : character.getSignatureManeuvers()) {
            JSONSignatureManeuver jsonSignatureManeuver = new JSONSignatureManeuver();
            jsonSignatureManeuver.name = signatureManeuver.getName();
            if (signatureManeuver.getSkill()!=null)
            	jsonSignatureManeuver.skill = signatureManeuver.getSkill().getName();
            else
            	jsonSignatureManeuver.skill = "No skill set";

            jsonSignatureManeuver.action1Id = signatureManeuver.getAction1().getId();
            jsonSignatureManeuver.action1Name = signatureManeuver.getAction1().getName();
            jsonSignatureManeuver.action2Id = signatureManeuver.getAction2().getId();
            jsonSignatureManeuver.action2Name = signatureManeuver.getAction2().getName();
            jsonSignatureManeuver.cost = signatureManeuver.getAction1().getCost() + signatureManeuver.getAction2().getCost();
            jsonSignatureMoves.add(jsonSignatureManeuver);
        }
        jsonCharacter.signatureManeuvers = jsonSignatureMoves;
    }

    private void setMartialArts(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONMartialArts> jsonMartialArts = new ArrayList<>();
        for (MartialArtsValue martialArt : character.getMartialArts()) {
            JSONMartialArts ma = new JSONMartialArts();
            ma.name = martialArt.getMartialArt().getName();
            ma.page = getPageString(martialArt.getMartialArt().getPage(), martialArt.getMartialArt().getProductNameShort());
            ma.techniques = character.getTechniques(martialArt.getMartialArt()).stream().map(t -> t.getTechnique().getName()).collect(Collectors.toList());
            ma.description = getDescription(martialArt.getMartialArt());
            jsonMartialArts.add(ma);
        }
        jsonCharacter.martialArts = jsonMartialArts;
    }

    private void setInitiatives(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONInitiative> jsonInitiativeList = new ArrayList<>();
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_ASTRAL, Attribute.INITIATIVE_DICE_ASTRAL));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_PHYSICAL, Attribute.INITIATIVE_DICE_PHYSICAL));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX, Attribute.INITIATIVE_DICE_MATRIX));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX_VR_COLD, Attribute.INITIATIVE_DICE_MATRIX_VR_COLD));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX_VR_HOT, Attribute.INITIATIVE_DICE_MATRIX_VR_HOT));
        jsonCharacter.initiatives = jsonInitiativeList;
    }

    private JSONInitiative getJsonInitiative(ShadowrunCharacter character, Attribute a, Attribute diceAttribute) {
        AttributeValue attribute = character.getAttribute(a);
        JSONInitiative i = new JSONInitiative();
        i.name = attribute.getAttribute().getName();
        i.id = a.name();
        i.value = attribute.getModifiedValue();
        i.dice = "+" +character.getAttribute(diceAttribute).getModifiedValue() + "D6";
        return i;
    }

    private void setLicenses(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLicense> jsonLicenseList = new ArrayList<>();
        for (LicenseValue license : character.getLicenses()) {
            JSONLicense jsonLicense = new JSONLicense();
            jsonLicense.name = license.getName();
            UUID sin = license.getSIN();
            if (sin != null) {
                jsonLicense.sin =  character.getSIN(sin).getName();
            }
            jsonLicense.type = license.getType().getName();
            jsonLicense.rating = license.getRating().name();
            jsonLicenseList.add(jsonLicense);
        }
        jsonCharacter.licenses = jsonLicenseList;
    }

    private void setLifestyles(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLifestyle> jsonLifestyleList = new ArrayList<>();
        for (LifestyleValue lifestyleValue : character.getLifestyle()) {
            JSONLifestyle jsonLifestyle = new JSONLifestyle();
            jsonLifestyle.customName = lifestyleValue.getName();
            jsonLifestyle.name = lifestyleValue.getLifestyle().getName();
            jsonLifestyle.cost = ShadowrunTools.getLifestyleCost(character, lifestyleValue);
            jsonLifestyle.paidMonths = lifestyleValue.getPaidMonths();
            jsonLifestyle.sin = lifestyleValue.getSIN() != null ? character.getSIN(lifestyleValue.getSIN()).getName() : null;
            jsonLifestyle.options = lifestyleValue.getOptions().stream().map(o -> o.getOption().getName()).collect(Collectors.toList());
            jsonLifestyle.description = getDescription(lifestyleValue.getLifestyle());
            jsonLifestyleList.add(jsonLifestyle);
        }
        jsonCharacter.lifestyles = jsonLifestyleList;
    }

    private void setSINs(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSin> jsonSinList = new ArrayList<>();
        for (SIN sin : character.getSINs()) {
            JSONSin jsonSin = new JSONSin();
            jsonSin.name = sin.getName();
            jsonSin.description = sin.getDescription();
            jsonSin.quality = sin.getQualityValue();
            jsonSinList.add(jsonSin);
        }
        jsonCharacter.sins = jsonSinList;
    }

    private void setRituals(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONRitual> jsonRitualList = new ArrayList<>();
        for (RitualValue ritualValue : character.getRituals()) {
            JSONRitual jsonRitual = new JSONRitual();
            Ritual ritual = ritualValue.getModifyable();
            jsonRitual.name = ritual.getName();
            jsonRitual.features = ritual.getFeatures().stream().map(r -> r.getFeature().getName()).collect(Collectors.toList());
            jsonRitual.threshold = ritual.getThreshold();
            jsonRitual.page = getPageString(ritual.getPage(), ritual.getProductNameShort());
            jsonRitual.description = getDescription(ritual);
            jsonRitualList.add(jsonRitual);
        }
        jsonCharacter.rituals = jsonRitualList;
    }

    private void setDrones(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONDrone> jsonDroneList = new ArrayList<>();
        for (CarriedItem item : character.getItems(true, ItemType.droneTypes())) {
            JSONDrone drone = new JSONDrone();
            drone.name = item.getName();
            drone.count = item.getCount();
            drone.handlOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad();
            drone.handlOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad();
            drone.speed = item.getAsValue(ItemAttribute.SPEED).getModifiedValue();
            drone.accelOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad();
            drone.accelOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad();
            drone.speedIntOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOnRoad();
            drone.speedIntOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOffRoad();
            drone.body = item.getAsValue(ItemAttribute.BODY).getModifiedValue();
            drone.armor = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            drone.pilot = item.getAsValue(ItemAttribute.PILOT).getModifiedValue();
            drone.sensor = item.getAsValue(ItemAttribute.SENSORS).getModifiedValue();
            drone.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            drone.type = item.getUsedAsType().name();
            drone.subtype = item.getUsedAsSubType().name();
            drone.accessories = getJsonItemAccessories(item.getUserAddedAccessories());
            drone.description = getDescription(item.getItem());
            jsonDroneList.add(drone);
        }
        jsonCharacter.drones = jsonDroneList;
    }

    private void setVehicles(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONVehicle> jsonVehicleList = new ArrayList<>();
        for (CarriedItem item : character.getItems(false, ItemType.VEHICLES)) {
            JSONVehicle vehicle = new JSONVehicle();
            vehicle.name = item.getName();
            vehicle.handlOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad();
            vehicle.handlOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad();
            vehicle.pilot = item.getAsValue(ItemAttribute.PILOT).getModifiedValue();
            vehicle.body = item.getAsValue(ItemAttribute.BODY).getModifiedValue();
            vehicle.accelOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad();
            vehicle.accelOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad();
            vehicle.speedIntOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOnRoad();
            vehicle.speedIntOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOffRoad();
            vehicle.speed = item.getAsValue(ItemAttribute.SPEED).getModifiedValue();
            vehicle.armor = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            vehicle.sensor = item.getAsValue(ItemAttribute.SENSORS).getModifiedValue();
            vehicle.seats = item.getAsValue(ItemAttribute.SEATS).getModifiedValue();
            vehicle.type = item.getUsedAsType().name();
            vehicle.subtype = item.getUsedAsSubType().name();
            vehicle.accessories = getJsonItemAccessories(item.getUserAddedAccessories());
            vehicle.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            vehicle.description = getDescription(item.getItem());
            jsonVehicleList.add(vehicle);
        }
        jsonCharacter.vehicles = jsonVehicleList;
    }

    private void setAugmentations(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAugmentation> jsonAugmentationList = new ArrayList<>();
        for (CarriedItem item : character.getItems(true, ItemType.bodytechTypes())) {
            JSONAugmentation jsonAugmentation = new JSONAugmentation();
            jsonAugmentation.name = item.getName();
            jsonAugmentation.level = item.getRating() > 0 ? String.valueOf(item.getRating()) : "-";
            jsonAugmentation.essence = (float) ShadowrunTools.getItemAttribute(character, item, ItemAttribute.ESSENCECOST).getModifiedValue() / 1000.0f;
            jsonAugmentation.quality = item.getQuality().name();
            jsonAugmentation.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            jsonAugmentation.description = getDescription(item.getItem());
            jsonAugmentation.accessories = getJsonItemAccessories(item.getUserAddedAccessories());
            jsonAugmentationList.add(jsonAugmentation);
        }
        jsonCharacter.augmentations = jsonAugmentationList;
    }

    private void setAdeptPowers(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAdeptPower> jsonAdeptPowerList = new ArrayList<>();
        for (AdeptPowerValue adeptPower : character.getAdeptPowers()) {
            JSONAdeptPower jsonAdeptPower = new JSONAdeptPower();
            jsonAdeptPower.name = adeptPower.getNameWithoutRating();
            if (adeptPower.getModifyable().hasLevels()) {
                jsonAdeptPower.level = adeptPower.getModifiedLevel();
            }
            jsonAdeptPower.activation = adeptPower.getModifyable().getActivation().name();
            jsonAdeptPower.cost = adeptPower.getCost();
            jsonAdeptPower.page = getPageString(adeptPower.getModifyable().getPage(), adeptPower.getModifyable().getProductNameShort());
            jsonAdeptPower.description = getDescription(adeptPower.getModifyable());
            jsonAdeptPowerList.add(jsonAdeptPower);
        }
        jsonCharacter.adeptPowers = jsonAdeptPowerList;
    }

    private void setArmors(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONArmor> jsonArmorList = new ArrayList<>();
        for (CarriedItem carriedItem : character.getItemsRecursive(true, ItemType.ARMOR)) {
            JSONArmor jsonArmor = new JSONArmor();
            jsonArmor.name = carriedItem.getName();
            jsonArmor.isIgnored = carriedItem.isIgnoredForCalculations();
            jsonArmor.rating = carriedItem.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            jsonArmor.socialrating =  carriedItem.getAsValue(ItemAttribute.SOCIAL).getModifiedValue();
            jsonArmor.accessories = getItemAccessories(carriedItem);
            jsonArmor.page = getPageString(carriedItem.getItem().getPage(), carriedItem.getItem().getProductNameShort());
            jsonArmor.description = getDescription(carriedItem.getItem());
            jsonArmor.primary = ShadowrunTools.isPrimaryItem(character,carriedItem);
            jsonArmorList.add(jsonArmor);
        }
        jsonCharacter.armors = jsonArmorList;
    }

    private JSONItem getJsonItem(CarriedItem item) {
        JSONItem jsonItem = new JSONItem();
        jsonItem.name = item.getName();
        jsonItem.type = item.getUsedAsType().name();
        jsonItem.subType = item.getUsedAsSubType().name();
        jsonItem.count = item.getCount();
        jsonItem.rating = item.getRating();
        jsonItem.accessories = getItemAccessories(item);
        jsonItem.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
        jsonItem.description = getDescription(item.getItem());
        return jsonItem;
    }

    private void setComplexForms(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONComplexForm> jsonComplexFormList = new ArrayList<>();
        for (ComplexFormValue complexForm : character.getComplexForms()) {
            JSONComplexForm jsonComplexForm = new JSONComplexForm();
            jsonComplexForm.name = complexForm.getName();
            jsonComplexForm.duration = complexForm.getModifyable().getDuration().getName();
            jsonComplexForm.fading = complexForm.getModifyable().getFading();
            jsonComplexForm.influences = complexForm.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
            jsonComplexForm.page = getPageString(complexForm.getModifyable().getPage(), complexForm.getModifyable().getProductNameShort());
            jsonComplexForm.description = getDescription(complexForm.getModifyable());    
            jsonComplexFormList.add(jsonComplexForm);
        }

        jsonCharacter.complexForms = jsonComplexFormList;
    }

    private void setCloseCombatWeapons(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONCloseCombatWeapon> jsonCloseCombatWeaponList = new ArrayList<>();
        for (CarriedItem closeCombatWeapon : getCloseCombatWeapons(character)) {
            JSONCloseCombatWeapon jsonCloseCombatWeapon = new JSONCloseCombatWeapon();
            jsonCloseCombatWeapon.name = closeCombatWeapon.getName();
            jsonCloseCombatWeapon.type = closeCombatWeapon.getUsedAsType().name();
            jsonCloseCombatWeapon.skill = closeCombatWeapon.getItem().getWeaponData().getSkill().getId();
            jsonCloseCombatWeapon.subtype = closeCombatWeapon.getUsedAsSubType().name();
            jsonCloseCombatWeapon.pool = ShadowrunTools.getWeaponPool(character, closeCombatWeapon);
            jsonCloseCombatWeapon.damage = ShadowrunTools.getWeaponDamage(character, closeCombatWeapon).toString();
            jsonCloseCombatWeapon.attackRating = ShadowrunTools.getAttackRatingString((int[]) closeCombatWeapon.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue());
            jsonCloseCombatWeapon.wifi = closeCombatWeapon.getWiFiAdvantageStrings();
            jsonCloseCombatWeapon.accessories = getItemAccessories(closeCombatWeapon);
            jsonCloseCombatWeapon.page = getPageString(closeCombatWeapon.getItem().getPage(), closeCombatWeapon.getItem().getProductNameShort());
            jsonCloseCombatWeapon.description = getDescription(closeCombatWeapon.getItem());
            jsonCloseCombatWeapon.primary = closeCombatWeapon.isPrimary();
            jsonCloseCombatWeaponList.add(jsonCloseCombatWeapon);
        }
        jsonCharacter.closeCombatWeapons = jsonCloseCombatWeaponList;
    }

    private void setLongRangeWeapons(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLongRangeWeapon> jsonLongRangeWeapons = new ArrayList<>();
        for (CarriedItem longRangeWeapon : getLongRangeWeapons(character)) {
            JSONLongRangeWeapon jsonLongRangeWeapon = new JSONLongRangeWeapon();
            jsonLongRangeWeapon.name = longRangeWeapon.getName();
            jsonLongRangeWeapon.type = longRangeWeapon.getUsedAsType().name();
            jsonLongRangeWeapon.subtype = longRangeWeapon.getUsedAsSubType().name();
            jsonLongRangeWeapon.skill = longRangeWeapon.getItem().getWeaponData().getSkill().getId();
            jsonLongRangeWeapon.pool = ShadowrunTools.getWeaponPool(character, longRangeWeapon);
            jsonLongRangeWeapon.damage = ShadowrunTools.getWeaponDamage(character, longRangeWeapon).toString();
            jsonLongRangeWeapon.attackRating = ShadowrunTools.getAttackRatingString((int[]) longRangeWeapon.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue());
            jsonLongRangeWeapon.mode = (String) longRangeWeapon.getAsObject(ItemAttribute.MODE).getValue();
            jsonLongRangeWeapon.ammunition = ShadowrunTools.getItemAttributeString(character, longRangeWeapon, ItemAttribute.AMMUNITION);
            jsonLongRangeWeapon.wifi = longRangeWeapon.getWiFiAdvantageStrings();
            jsonLongRangeWeapon.accessories = getItemAccessories(longRangeWeapon);
            jsonLongRangeWeapon.page = getPageString(longRangeWeapon.getItem().getPage(), longRangeWeapon.getItem().getProductNameShort());
            jsonLongRangeWeapon.description = getDescription(longRangeWeapon.getItem());
            jsonLongRangeWeapon.primary = longRangeWeapon.isPrimary();
            jsonLongRangeWeapons.add(jsonLongRangeWeapon);
        }
        jsonCharacter.longRangeWeapons = jsonLongRangeWeapons;
    }

    private void setQualities(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONQuality> jsonQualities = new ArrayList<>();
        for (QualityValue qualityValue : character.getQualities()) {
            JSONQuality jsonQuality = new JSONQuality();
            Quality quality = qualityValue.getModifyable();
            jsonQuality.name = quality.getName();
            jsonQuality.choice = qualityValue.getNameSecondLine();
            jsonQuality.id = quality.getId();
            if (quality.getMax()>0) {
                jsonQuality.rating = qualityValue.getModifiedValue();
            }
            jsonQuality.positive = quality.getType().equals(Quality.QualityType.POSITIVE);
            jsonQuality.page = getPageString(quality.getPage(), quality.getProductNameShort());
            jsonQuality.description = getDescription(quality);
            jsonQualities.add(jsonQuality);
        }
        jsonCharacter.qualities = jsonQualities;
    }

    private void setConnections(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONContact> jsonContacts = new ArrayList<>();
        for (Connection connection : character.getConnections()) {
            JSONContact jsonContact = new JSONContact();
            jsonContact.name = connection.getName();
            jsonContact.type = connection.getType();
            jsonContact.loyalty = connection.getLoyalty();
            jsonContact.influence = connection.getInfluence();
            jsonContact.description = connection.getDescription();
            jsonContact.favors = connection.getFavors();
            jsonContacts.add(jsonContact);
        }
        jsonCharacter.contacts = jsonContacts;
    }

    private void setMatrixItems(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONMatrixItem> matrixItems = new ArrayList<>();
        for (CarriedItem matrixItem : ShadowrunTools.getMatrixItems(character)) {
            JSONMatrixItem item = new JSONMatrixItem();
            item.name = matrixItem.getName();
            item.subType = matrixItem.getSubType();
            if (matrixItem.getUsedAsSubType()!=null) 
            	item.type =  matrixItem.getUsedAsType().name();
            item.attack = matrixItem.getAsValue(ItemAttribute.ATTACK).getModifiedValue();
            item.dataProcessing = matrixItem.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue();
            item.sleaze = matrixItem.getAsValue(ItemAttribute.SLEAZE).getModifiedValue();
            item.firewall = matrixItem.getAsValue(ItemAttribute.FIREWALL).getModifiedValue();
            if (matrixItem.getItem()!=null) {
            	item.page = getPageString(matrixItem.getItem().getPage(), matrixItem.getItem().getProductNameShort());
                item.deviceRating =  matrixItem.getItem().getDeviceRating();
                item.description = getDescription(matrixItem.getItem());
                item.concurrentPrograms = matrixItem.getAsValue(ItemAttribute.CONCURRENT_PROGRAMS).getModifiedValue();
                item.programs = getJsonItemList(getProgramsOnDevice(matrixItem));
                item.accessories = getJsonItemAccessories(getAccessoriesWithoutPrograms(matrixItem));
            }
            matrixItems.add(item);
        }
        jsonCharacter.matrixItems = matrixItems;
    }

    private void setItems(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<CarriedItem> items = character.getItems(true, ItemType.gearTypes());
        items.addAll(character.getItems(true, ItemType.MAGICAL));
        items.removeAll(ShadowrunTools.getMatrixItems(character));
        List<CarriedItem> filteredItems = new ArrayList<>();
        for (CarriedItem item : items) {
            ItemType type = item.getUsedAsType();
            if (type != ItemType.ARMOR) {
                filteredItems.add(item);
            }
        }
        jsonCharacter.items = getJsonItemList(filteredItems);
    }

    private List<JSONItem> getJsonItemList(List<CarriedItem> filteredItems) {
        List<JSONItem> jsonItems = new ArrayList<>();
        for (CarriedItem item : filteredItems) {
            JSONItem jsonItem = getJsonItem(item);
            jsonItems.add(jsonItem);
        }
        return jsonItems;
    }

    private List<JSONItemAccessory> getItemAccessories(CarriedItem item) {
        Collection<CarriedItem> carriedItemAccessories = item.getUserAddedAccessories();
        return getJsonItemAccessories(carriedItemAccessories);
    }

    private List<JSONItemAccessory> getJsonItemAccessories(Collection<CarriedItem> carriedItemAccessories) {
        List<JSONItemAccessory> jsonItemAccessoryList = new ArrayList<>();
        for (CarriedItem userAddedAccessory : carriedItemAccessories) {
            JSONItemAccessory jsonItemAccessory = new JSONItemAccessory();
            jsonItemAccessory.name = userAddedAccessory.getName();
            jsonItemAccessory.rating = userAddedAccessory.getRating();
            jsonItemAccessory.description = getDescription(userAddedAccessory.getItem());
            if (userAddedAccessory.getUsedAsType() != null) {
                jsonItemAccessory.type = userAddedAccessory.getUsedAsType().name();
            }
            if (userAddedAccessory.getSubType() != null) {
                jsonItemAccessory.subType = userAddedAccessory.getSubType().name();
            }
            jsonItemAccessoryList.add(jsonItemAccessory);
        }
        return jsonItemAccessoryList;
    }

    private void setSpells(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSpell> jsonSpells = new ArrayList<>();
        for (SpellValue spellValue : character.getSpells()) {
            JSONSpell jsonSpell = new JSONSpell();
            Spell spell = spellValue.getModifyable();
            jsonSpell.name = spell.getName();
            jsonSpell.id = spell.getId();
            jsonSpell.category = spell.getCategory().getName();
            jsonSpell.type = spell.getType().getName();
            jsonSpell.duration = spell.getDuration().getName();
            jsonSpell.range = spell.getRange().getName();
            jsonSpell.drain = spell.getDrain();
            jsonSpell.isAlchemistic = spellValue.isAlchemistic();
            jsonSpell.page = getPageString(spell.getPage(), spell.getProductNameShort());
            jsonSpell.features = spell.getFeatures().stream().map(f -> f.getFeature().getId()).collect(Collectors.toList());
            jsonSpell.influencedBy = spellValue.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
            jsonSpell.description = getDescription(spell);
            jsonSpells.add(jsonSpell);
        }
        jsonCharacter.spells = jsonSpells;
    }

    private String getPageString(int page, String productNameShort) {
        String pageBook;
        if (page == 0) {
            pageBook = " ";
        } else {
            pageBook = String.format("%s %s", productNameShort, page);
        }
        return pageBook;
    }

    private void setSkills(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSkill> jsonSkills = new ArrayList<>();
        for (SkillValue skillValue : character.getSkillValues()) {
            jsonSkills.add(getJSONSkill(skillValue, character));
        }
        jsonCharacter.skills = jsonSkills;
    }

    private JSONSkill getJSONSkill(SkillValue skillValue, ShadowrunCharacter character) {
        JSONSkill jsonSkill = new JSONSkill();
        jsonSkill.name = skillValue.getName();
        jsonSkill.id = skillValue.getModifyable().getId();
        Attribute attr = skillValue.getModifyable().getAttribute1();
        jsonSkill.attribute = attr.getName();
        jsonSkill.rating = skillValue.getPoints();
        jsonSkill.pool = ShadowrunTools.getSkillPool(character, skillValue.getModifyable());
        jsonSkill.description = getDescription(skillValue.getModifyable());
        List<SkillSpecializationValue> specializations = skillValue.getSkillSpecializations();
        List<JSONSkillSpecialization> jsonSkillSpecializationList = new ArrayList<>();
        for (SkillSpecializationValue specialization : specializations) {
            JSONSkillSpecialization jsonSpec = new JSONSkillSpecialization();
            Attribute specAttr = attr;
            if (specialization.getSpecial().getAttribute() != null) {
                specAttr = specialization.getSpecial().getAttribute();
            }
            jsonSpec.attribute = specAttr.getName();
            jsonSpec.name = specialization.getName();
            jsonSpec.id = specialization.getSpecial().getId();
            jsonSpec.expertise = specialization.isExpertise();
            jsonSpec.description = getDescription(specialization.getSpecial());
            int mod = specialization.isExpertise() ? 3 : 2;
            jsonSpec.pool = mod + ShadowrunTools.getSkillPool(character, skillValue.getModifyable(), specAttr);
            jsonSkillSpecializationList.add(jsonSpec);
        }
        jsonSkill.specializations = jsonSkillSpecializationList;
        jsonSkill.influencedBy = skillValue.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
        return jsonSkill;
    }

    private void setAttributes(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAttribute> jsonAttributes = new ArrayList<>();
        Collection<AttributeValue> attributes = character.getAttributes(Attribute.attributesToSave());
        attributes.addAll(character.getAttributes(Attribute.derivedValues()));
        attributes = attributes.stream().filter(a -> !a.getAttribute().equals(Attribute.INITIATIVE_ASTRAL) && !a.getAttribute().equals(Attribute.INITIATIVE_MATRIX) && !a.getAttribute().equals(Attribute.INITIATIVE_PHYSICAL)).collect(Collectors.toList());
        for (AttributeValue attribute : attributes) {
            jsonAttributes.add(getJSONAttribute(attribute));
        }
        jsonCharacter.attributes = jsonAttributes;
    }

    private JSONAttribute getJSONAttribute(AttributeValue attribute) {
        JSONAttribute jsonAttribute = new JSONAttribute();
        jsonAttribute.name = attribute.getModifyable().getName();
        jsonAttribute.id = attribute.getAttribute().name();
        jsonAttribute.points = attribute.getPoints();
        jsonAttribute.modifiedValue = attribute.getModifiedValue();
        return jsonAttribute;
    }

    private void setGeneralInfo(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        jsonCharacter.name = character.getRealName();
        jsonCharacter.streetName = character.getName();
        jsonCharacter.metaType = character.getMetatype().getName();
        jsonCharacter.size = character.getSize();
        jsonCharacter.weight = character.getWeight();
        jsonCharacter.age = character.getAge();
        jsonCharacter.gender = character.getGender().toString();
        jsonCharacter.karma = character.getKarmaFree() + character.getKarmaInvested();
        jsonCharacter.freeKarma = character.getKarmaFree();
        jsonCharacter.notes = character.getNotes();
        jsonCharacter.nuyen = character.getNuyen();
        jsonCharacter.heat = character.getAttribute(Attribute.HEAT).getModifiedValue();
        jsonCharacter.reputation = character.getAttribute(Attribute.REPUTATION).getModifiedValue();
        MagicOrResonanceType magicOrResonanceType = character.getMagicOrResonanceType();
        if (magicOrResonanceType != null && magicOrResonanceType.usesMagic()) {
            jsonCharacter.initiation = character.getInitiateSubmersionLevel();
            jsonCharacter.metamagic = getMetaMagicOrEcho(character);
        } else if (magicOrResonanceType != null && magicOrResonanceType.usesResonance()) {
            jsonCharacter.submersion = character.getInitiateSubmersionLevel();
            jsonCharacter.echoes = getMetaMagicOrEcho(character);
        }
    }

    private List<JSONMetaMagicOrEcho> getMetaMagicOrEcho(ShadowrunCharacter character) {
        List<JSONMetaMagicOrEcho> jsonMetaMagicOrEchoList = new ArrayList<>();
        for (MetamagicOrEchoValue metamagicOrEcho : character.getMetamagicOrEchoes()) {
            JSONMetaMagicOrEcho jsonMeta = new JSONMetaMagicOrEcho();
            jsonMeta.name = metamagicOrEcho.getName();
            jsonMeta.page = getPageString(metamagicOrEcho.getModifyable().getPage(), metamagicOrEcho.getModifyable().getProductNameShort());
            jsonMeta.description = getDescription(metamagicOrEcho.getModifyable());
            jsonMetaMagicOrEchoList.add(jsonMeta);
        }
        return jsonMetaMagicOrEchoList;
    }

    private static List<CarriedItem> getLongRangeWeapons(ShadowrunCharacter character) {
        List<CarriedItem> result = new ArrayList<>();
        List<CarriedItem> items = character.getItemsRecursive(true, ItemType.weaponTypes());
        for (CarriedItem weapon : items) {
            if (!weapon.isType(ItemType.WEAPON_CLOSE_COMBAT)) {
                result.add(weapon);
            }
        }
        return result;
    }

    private static List<CarriedItem> getCloseCombatWeapons(ShadowrunCharacter character) {
        List<CarriedItem> result = new ArrayList<>();
        List<CarriedItem> items = character.getItemsRecursive(true, ItemType.weaponTypes());
        for (CarriedItem weapon : items) {
            if (weapon.isType(ItemType.WEAPON_CLOSE_COMBAT)) {
                result.add(weapon);
            }
        }
        return result;
    }

    private static List<CarriedItem> getProgramsOnDevice(CarriedItem item) {
        Collection<CarriedItem> userAddedAccessories = item.getUserAddedAccessories();
        return userAddedAccessories.stream()
                .filter(carriedItem -> (carriedItem.isSubType(ItemSubType.BASIC_PROGRAM) || carriedItem.isSubType(ItemSubType.HACKING_PROGRAM))).collect(Collectors.toList());
    }

    private static List<CarriedItem> getAccessoriesWithoutPrograms(CarriedItem item) {
        Collection<CarriedItem> userAddedAccessories = item.getUserAddedAccessories();
        return userAddedAccessories.stream()
                .filter(carriedItem -> (!carriedItem.isSubType(ItemSubType.BASIC_PROGRAM) && !carriedItem.isSubType(ItemSubType.HACKING_PROGRAM))).collect(Collectors.toList());
    }
    
    private String getDescription(BasePluginData data) {
        if (data.hasCustomHelpText()) {
            return data.getCustomHelpText();
        } else {
            return "";
        }
    }
}
