package de.rpgframework.shadowrun6.print.json.model;

public class JSONInitiative {
    public String name;
    public String id;
    public int value;
    public String dice;
}
