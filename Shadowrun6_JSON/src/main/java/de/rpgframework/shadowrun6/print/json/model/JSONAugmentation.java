package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONAugmentation {
    public String name;
    public String level;
    public float essence;
    public String page;
    public String quality;
    public String description;
    public List<JSONItemAccessory> accessories;
}
