package de.rpgframework.shadowrun6.print.json.model;

public class JSONAdeptPower {
    public String name;
    public String activation;
    public int level;
    public float cost;
    public String page;
    public String description;
}

