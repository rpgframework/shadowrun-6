package de.rpgframework.shadowrun6.print.json.model;

import org.prelle.shadowrun6.items.ItemSubType;

import java.util.List;

public class JSONMatrixItem {
    public String name;
    public int dataProcessing;
    public int firewall;
    public int attack;
    public int sleaze;
    public int deviceRating;
    public String type;
    public ItemSubType subType;
    public String page;
    public int concurrentPrograms;
    public List<JSONItemAccessory> accessories;
    public List<JSONItem> programs;
    public String description;
}
