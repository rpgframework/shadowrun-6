package de.rpgframework.shadowrun6.print.json.model;

import java.util.Collection;
import java.util.List;

public class JSONCloseCombatWeapon {
    public String name;
    public String type;
    public String subtype;
    public String skill;
    public int pool;
    public String damage;
    public String attackRating;
    public Collection<String> wifi;
    public List<JSONItemAccessory> accessories;
    public String page;
    public String description;
    public boolean primary;
}
