/**
 * @author Stefan Prelle
 *
 */
module shadowrun6.json {
	exports de.rpgframework.shadowrun6.print.json;

	opens de.rpgframework.shadowrun6.print.json.model;

	provides de.rpgframework.character.RulePlugin with de.rpgframework.shadowrun6.print.json.JSONSR6ExportPlugin;

	requires java.prefs;
	requires org.apache.logging.log4j;
	requires de.rpgframework.core;
	requires transitive shadowrun6.core;
	requires de.rpgframework.chars;
	requires com.google.gson;
	requires de.rpgframework.print;
	requires shadowrun6.data;
}